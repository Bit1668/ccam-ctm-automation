import datetime as dt
import subprocess
import os

'''
This is the main routine to run the ensemble of the cases for the CTM 
'''

date = dt.datetime.now()
startdate = dt.datetime(2017,12,1)

RunDir=os.path.dirname(os.path.abspath(__file__))
my_env = os.environ.copy()
init = "source /usr/share/modules/init/bash |  module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp |"

#run the possibilities

# Changed basecase to run first as it's the quickest 
for day in range(31):
    date = startdate + dt.timedelta(days = day)
    #CTM 
    run = "python Python-Scripts/Ccam-CTM-forecast.py --dateAEDT {date} ".format(date=date.strftime("%Y%m%d%H%M"))
    print subprocess.Popen([init + run ], shell=True, env=my_env, cwd=RunDir).wait()

#CTM Wildfire + HRB
#run = "python Python-Scripts/Ccam-CTM-forecast.py --dateAEDT {date} --HRB --Wildfire".format(date=date.strftime("%Y%m%d%H%M"))
#print subprocess.Popen([init + run ], shell=True, env=my_env, cwd=RunDir).wait()

#CTM Wildfire 
#run = "python Python-Scripts/Ccam-CTM-forecast.py --dateAEDT {date} --Wildfire".format(date=date.strftime("%Y%m%d%H%M"))
#print subprocess.Popen([init + run ], shell=True, env=my_env, cwd=RunDir).wait()

#CTM HRB
#run = "python Python-Scripts/Ccam-CTM-forecast.py --dateAEDT {date} --HRB ".format(date=date.strftime("%Y%m%d%H%M"))
#print subprocess.Popen([init + run ], shell=True, env=my_env, cwd=RunDir).wait()


