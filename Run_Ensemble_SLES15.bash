#!/usr/bin/env bash
#pp=/mnt/appsource/python/python-2.7.13/bin/python
pp=/usr/bin/python
dir=/mnt/climate/cas/scratch/AQ-Forecast/ccam-ctm-automation-Airflow
source ~/modulefiles/init.bash
module load Python2-User-Module
ulimit -s 350000
cd $dir
echo "changed to "$dir
$pp Ensemble.py
