"""
.. module:: Ccam-CTM-forecast
   :platform: Unix
   :synopsis: Everything needed to build and run the CSIRO Chemical Transport Model.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
"""
import sys
import os
import datetime as dt
import Sources.RunConfig as RC
import logging
import Sources.InitLogging as IL
import Sources.Domain_definition.ResolutionProperties as RP 
import Sources.PrepCTM as PCTM
import Sources.RunCTM as RCTM
import Sources.CommandLineParsing as CLP
import Sources.Configuration as Conf
import Sources.DateMagics as DM
##########################################################################################################################
#
#                     ||    Main Below  || 
#                     ||                ||
#                     \/                \/
#
##########################################################################################################################

def Main(logger, Startdate, DAY_N, 
          EmissionBuildPath,
          SimulationBasePath,
          ConfigurationOptions, TimestampUTC):
##########################################################################################3
# scenarios and options
###########################################################################################
#   Chemical transport Options
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
#############
#  Number of days to forecast AEDT
    EndDate=Startdate + dt.timedelta(days=DAY_N,hours=23)
    
######################
    ResolutionProperties = ConfigurationOptions.ResolutionProperties
    ResolutionNames = ConfigurationOptions.ResolutionNames
    
    # Where the template dir is
    #templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatepath='Python-Scripts/Templates'

######################
#  Binaries location
    PrepBin = '/mnt/climate/cas/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2.x64'
    PrepBin = '/mnt/climate/cas/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2_opt_SLES15.x64'
    #PrepBin = '/mnt/climate/cas/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2_debug_SLES15.x64'
    #PrepBin = '/mnt/climate/cas/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2_opt_SLES15.x64.try'

    CTMBinPath = '/mnt/climate/cas/project/CTM/software/August-16/BIN'
 
    #SLES11
    CTMBin = 'ccam-ctm_cb05_aer2_filename.x64'
    
    #Test
    #CTMBin = 'ccam-ctm_cb05_aer2_filename_dust_opt.x64'
    #CTMBinPath = '/mnt/climate/cas/project/CTM/software/August-16/ctm/ccam'
    
    #SLES15
    #CTMBin = 'ccam-ctm_cb05_aer2_filename_dust_debug_SLES15.x64'
    CTMBin = 'ccam-ctm_cb05_aer2_filename_dust_opt_SLES15.x64'
    #CTMBin = 'ctm_prep_init_cb05_aer2_opt_SLES15.x64.try'
######################
#  Job scheduling options
    RunTimeHours=10
    PartitionName='Full'
    RuntimeCPU=40 
    PartitionName='CAS'
    RuntimeCPU=72
    
##########################################################################################################################################

    SimulationConfig = RC.SimulationConfigClass(logger, ResolutionNames, ResolutionProperties, SCENARIO, 
            Startdate, EndDate, TimestampUTC, 
            TRANSPORT, DIFF, SIOA, SOA, CHEM, PrepBin, 
            templatepath, EmissionBuildPath,
            SimulationBasePath, ConfigurationOptions, 
            CTMBinPath, CTMBin, RuntimeCPU, RunTimeHours, PartitionName,
            )


# Restart?
    SimulationConfig.WarmStart = False
#    SimulationConfig.WarmStart = True

    #Update Bushfire options
    SimulationConfig.Update_Fire_options(ConfigurationOptions)

    #Update Dust options
    SimulationConfig.Update_Dust_options(ConfigurationOptions)

    #Update Met options
    SimulationConfig.Update_Met_options(ConfigurationOptions)
    
    #update EDMS options
    SimulationConfig.Update_EDMS_options(ConfigurationOptions)
    
##########################################################
#   Directories
#########################################################
    SimulationConfig.ComputeDirectories()
    logger.info('Computing directory names'.ljust(SimulationConfig.justif-2,'.')+'OK')
    SimulationConfig.MakeDir()
    
##########################################################
#   Prep
#########################################################
    PCTM.CTM_prep(SimulationConfig)


#########################################################
#  Run
########################################################    
    SBatchConfig = RCTM.RunCTM(SimulationConfig, ConfigurationOptions ) 
       
#########################################################
#  Submit (Push the Button!)
########################################################    
    SBatchConfig.RunScriptSubmit()
    
    return
##########################################################################################################################
##########################################################################################################################
##########################################################################################################################
#
#             Main Below  || 
#                         ||
#                         \/
#
##########################################################################################################################
##########################################################################################################################
##########################################################################################################################
##########################################################################################################################

if __name__ == '__main__':

    loggername='CCAM-CTM'
    logger=IL.Initialise_logging(loggername)
    
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime.now(),"AEDT")

###########################################################################################################################
#    Parsing command line
###########################################################################################################################
    
    Startdate, timezone, Run_HRB, Run_Bushfire, Run_Dust, DustModel = CLP.CommandLineParsing(logger, )
    Manual_Run = False
    
    if not Startdate:
###########################################################
#  NO argument on the command line --> Manual Run 
###########################################################
        Manual_Run = True
    #  Day of the forecast:
    #       a particular date AEDT:

        timezone = "AEDT"

        
        ##################
        YEAR_S = 2020
        MONTH_S = 4
        DAY_S = 2
        Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,2)
        ##################
        # Or Today     
        #Startdate=dt.datetime.today().replace(hour= 2, minute = 0, second = 0)
        ###################
        logger.warning("Date has NOT been detected on the argline, CTM will be manually configured for {date}".format(date=Startdate.strftime("%Y%m%d_%H%M")))
        
#    define the timestamp to redo a run: must match date AND timestamp 
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,2,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,3,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,4,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,5,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,6,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,7,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,8,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,9,0,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2018,4,11,0,0,0),"AEDT")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,1,15,16,16,33),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,5,27,6,28,18),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,5,31,3,13,4),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,5,31,3,1,27),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,5,31,5,2,32),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,11,15,0,0,4),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,1,15,11,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,1,15,10,0,0),"UTC")
        TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,1,31,0,0,0),"UTC")
        TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,1,31,1,0,0),"UTC")
        TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,1,31,2,0,0),"UTC")
        TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,1,31,3,0,0),"UTC")
        #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2020,03,13,2,43,13),"UTC")

#############
#  Number of days to forecast AEDT
    DAY_N = 4
    #DAY_N = 2

#############
# Domain names to run
    ResolutionNames=['nsw','gmr','gsyd']
    #ResolutionNames=['nsw','gmr','psyd']
    #ResolutionNames=['gmr','gsyd']

##########################################################################################################################
#
#                 DATA path
#
##########################################################################################################################

    # where are/to build the static emission files
    EmissionBuildPath = '/mnt/climate/cas/scratch/CCAM_CTM-runs/Emissions'
    EmissionBuildPath = '/mnt/climate/cas/scratch/CCAM_CTM-runs/Emissions_SLES15'
    #EmissionBuildPath = '/mnt/scratch/CCAM_CTM-runs/Emissions_SLES15'
    #EmissionBuildPath = os.path.abspath('./Emissions')
    
    # root dir for of the simulation
    SimulationBasePath = '/mnt/climate/cas/scratch/CCAM_CTM-runs/Forecast'
    #SimulationBasePath = '/mnt/climate/cas/scratch/CCAM_CTM-runs/Test_cases/Forecast_SLES15'
    #SimulationBasePath = '/home/barthelemyx/Projects/AQ-Forecast/Forecast_SLES15'
    #SimulationBasePath = '/home/barthelemyx/Projects/AQ-Forecast/Forecast_EDMS'
    #SimulationBasePath = '/mnt/climate/cas/scratch/CCAM_CTM-runs/Test_cases/Forecast/EmissInvUpdate'
 
##########################################################################################################################
#    Configure options
##########################################################################################################################
    ConfigurationOptions = Conf.VarSetup(logger, Startdate, timezone, TimestampUTC, SimulationBasePath, 
                              ResolutionNames)

##########################################################################################################################
#    Emission database options
##########################################################################################################################

    #  What is the date for the emissions?
    # YEAR_E=EndDate.year
    YEAR_E = 2013
    MONTH_E = Startdate.month
    DAY_E = Startdate.day
    Emissiondate = dt.datetime(YEAR_E,MONTH_E,DAY_E)

    # where the EDMS sources are
    #EmissionModellingPath='/mnt/climate/cas/project/ccam-ctm/commonfiles' 
    #Emissions_edms_DATA = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/emission2008NH3'
    EmissionModellingPath='/mnt/climate/cas/project/EmissionSources-Forecast/commonfiles' 
    Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2008NH3'
    Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2013NH3_PM10'
    Emissions_edms_DATA_basedir = '/mnt/climate/cas/project/EmissionSources-Forecast'

    PMspeciation = 'PM10'
    #PMspeciation = 'PM25'

    ConfigurationOptions.Configure_EDMS(Emissiondate,
                                        Emissions_edms_DATA_basedir,EmissionModellingPath,
                                        PMspeciation,
                                        )
    # Which emission groups to include
    # if not False, then True! ;)
    #ConfigurationOptions.includeemission_vpx = False
    #ConfigurationOptions.includeemission_vdx = False 
    #ConfigurationOptions.includeemission_vlx = False
    #ConfigurationOptions.includeemission_vpv = False 
    #ConfigurationOptions.includeemission_gse = False
    #ConfigurationOptions.includeemission_whe = False

    # Do you need to force build the emissions?
    # if not False, then True! ;)
    ConfigurationOptions.buildemission_vpx = False
    ConfigurationOptions.buildemission_vdx = False
    ConfigurationOptions.buildemission_vlx = False
    ConfigurationOptions.buildemission_vpv = False
    ConfigurationOptions.buildemission_gse = False
    ConfigurationOptions.buildemission_whe = False

##########################################################################################################################
#    Met options
##########################################################################################################################

    MetSource = "Manual"
    #MetSource = "Last"
    #MetSource = "Airflow"

    # Where is the original CCAM met file dir
##### Remember original ccam file should be in netcdfpath #########
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2017'
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2018'
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2019'
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2020'
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2020APS3'

    #CCAMFlavour : determine how the ccam files are formatted. 
    #              2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'
    CCAMFlavour = 'OneFile/Day/Domain'
    CCAMFlavour = 'OneFile/Forecast/Domain'

    #CCAM_input_flow: input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA".
    CCAM_input_flow = "ERA"
    CCAM_input_flow = "Forecast"
    #CCAM_input_flow = "Forecast_Linux"
    
    if CCAM_input_flow == "Forecast_Linux" and MetSource == "Manual":
        Manual_CCAM_StatusJSONfile = "/mnt/climate/cas/scratch/CCAM_Runs/ConfigRuns/State_20190804UTC/CCAM_2019080412UTC---tstamp---20191014_010000UTC.json"
    else:  
        Manual_CCAM_StatusJSONfile = None
    ConfigurationOptions.Configure_Met(MetSource, 
                                    netcdfpath, CCAMFlavour, CCAM_input_flow,
                                    CCAM_StatusJSONfile = Manual_CCAM_StatusJSONfile)
##########################################################################################################################
#    Bushfire options
##########################################################################################################################

#   Which bushfire emission model

#  Including/building Bushfire module OEH flavour? 
    bushfireOEH=True
    bushfireOEH=False
#  Data path for the bushfire module OEH flavour
    emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireOEH'


#  Including/building Bushfire module CSIRO flavour? 
    bushfireCSIRO=True
    bushfireCSIRO=False
#  Data path for the bushfire module CSIRO flavour
    emissions_rfs_DATA_CSIRO='/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
#  Burn start hour for CSIRO in UTC
    burn_start_hour_csiro = 0


########################################################
#  Fire data stream
    BushfireSource = "Airflow"

    #BushfireSource = "Manual"

#######################################################
    if BushfireSource == "Manual":

#    Run HRB option ?
        Run_HRB = True
        Run_HRB = False
        
        Manual_HRB_Dir = None 
        Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-04-10/CSEM_HRB_emission_model--2018-04-10T23-19-23'
        Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-05-07/CSEM_HRB_emission_model--2018-05-07T06-27-53'
        Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-05-24/CSEM_HRB_emission_model--2018-05-24T02-30-48'
        Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2019-05-20/CSEM_HRB_emission_model--2019-05-20T04-11-07'
        #Manual_HRB_Dir = '/mnt/climate/cas/scratch/trieut/csem-emissions'
        Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2019-08-23/CSEM_HRB_emission_model--2019-08-23T02-41-35'
        Manual_HRB_Dir = None 

#    Run Bushfire option ?
        Run_Bushfire = True
        #Run_Bushfire = False
        Manual_Bushfire_Dir = None 
        Manual_Bushfire_Dir = '/home/barthelemyx/Projects/AQ-Forecast/testfires'
        #Manual_Bushfire_Dir = '/mnt/climate/cas/scratch/workflows/stage/emissions/2019-11-11/CSEM_bushfire_emission_model--2019-11-11T05-13-55'
        Manual_Bushfire_Dir = '/home/changl/CSEMfire'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/bushfire-emissions-20191119'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191119-Only3'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_100m/CSEM_bushfire_emission_model--2019-11-19T23-03-00-Only3'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191106'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191208T22-02'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191209T22-02'
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/CSEM_bushfire_emission_model--2020-01-05T01-01-46'
        #Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-1-PSE-only'
        #20200104 forecast
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/workflows/stage/emissions/2020-01-03/CSEM_bushfire_emission_model--2020-01-03T22-01-27'
        #20200105 forecast
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/workflows/stage/emissions/2020-01-04/CSEM_bushfire_emission_model--2020-01-04T01-01-34'
        #20191209 forecast
        #Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/CSEM_bushfire_emission_model--2019-12-08T22-02-47'
        #20200204 forecast
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/workflows/stage/emissions/2020-02-03/CSEM_bushfire_emission_model--2020-02-03T22-01-26'
        #20200107 forecast
        Manual_Bushfire_Dir  = '/mnt/climate/cas/scratch/workflows/stage/emissions/2020-01-07/CSEM_bushfire_emission_model--2020-01-07T02-32-37'
##########################################################################################################################
##########################################################################################################################

    if BushfireSource == "Airflow":
        if Manual_Run:
            Run_HRB = True
            Run_HRB = False

            Run_Bushfire = True
            #Run_Bushfire = False
        
        Manual_HRB_Dir = None 
        Manual_Bushfire_Dir = None 

    ConfigurationOptions.Configure_Bushfire(  
                           BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,     
                           emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, 
                           Manual_HRB_Dir, Manual_Bushfire_Dir
                           )

##########################################################################################################################
#    Dust options
##########################################################################################################################
########################################################
#  Dust data stream
    DustSource = "Airflow"
    DustSource = "Manual"

#######################################################
    Run_Dust = True
    #Run_Dust = False
#######################################################
    #DustModel  is grabbed from the command line parsing
    if DustSource == "Manual":
        DustModel = None
        DustModel = 'inline'
        #DustModel = 'Chappell'
        Manual_MEMS_dust_source_dir = '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
        Manual_MEMS_dust_source_dir = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/TestFiles'
        Manual_MEMS_dust_source_dir = '/mnt/climate/cas/project/hynesr/events/2018-11-20/scratch/dust-model/runtime/AustraliaDustForecast--e316b3fa408111e99381001e67926c70/emissions'

        Manual_MEMS_dust_source_file = 'ACCESS-R-dust-forecast-ctm_bin_range_area_emissions-2018-11-20-23.nc'
        Manual_MEMS_dust_source_file = 'TestDust2.nc4'
        Manual_MEMS_dust_source_file = 'ctm_bin_range_area_emissions-2018-11-all--bin-pm10.nc'
        
    ConfigurationOptions.Configure_Dust(  
                        DustSource, DustModel, Run_Dust,  
                        Manual_MEMS_dust_source_dir, Manual_MEMS_dust_source_file  
                        )

##########################################################################################################################
#  Run Main
    

    Main(logger, Startdate, DAY_N, 
          EmissionBuildPath,
          SimulationBasePath,
          ConfigurationOptions, TimestampUTC
          ) 
         
