"""
.. module:: Configuration
   :platform: Unix
   :synopsis: Position configuration variables, from manual or automation sources.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import sys
import Ensemble.FireNameConvention as FNC
import Ensemble.DustNameConvention as DNC
import Ensemble.MetNameConvention as MNC
import Ensemble.EDMSNameConvention as ENC
#import ResolutionProperties as RP 
import Domain_definition.ResolutionProperties as RP 

class VarSetup(object):
    def __init__(self, logger, Startdate, timezone, TimestampUTC, SimulationBasePath,
                      ResolutionNames):
        '''
        Function to configure position vars.
    
        It sets up the fire options house keeping, including the fire_ID to compute the ensemble directory name
    
        Parameters
        ----------
        logger : logging.logger
            instance of a logger to output messages.
        Startdate : datetime.datetime
            timezone of the origin date for the modelled emission windows.
        timezone : str
            origin date for the modelled emission windows.
        TimestampUTC : datetime.datetime
            timestamp to identify the run.
        SimulationBasePath : str
            base path where to create the rundir.
        ResolutionNames : str
            name of the domain to run      
        '''
        self.logger = logger
        self.Startdate = Startdate
        self.timezone = timezone
        self.TimestampUTC = TimestampUTC
        self.SimulationBasePath = SimulationBasePath
        self.ResolutionNames = ResolutionNames
        
        #dictionnary of domain properties
        self.ResolutionProperties = RP.ResolutionProperties_new()

        #fire var
        self.BushfireSource = None
        self.bushfireCSIRO = None
        self.bushfireOEH = None
        self.Run_HRB = None
        self.Run_Bushfire = None
        self.emissions_rfs_DATA_CSIRO = None
        self.emissions_rfs_DATA_OEH = None
        self.burn_start_hour_csiro = None
        self.Manual_HRB_Dir = None
        self.Manual_Bushfire_Dir = None
        self.FIRE_ID = None 
        
        #mems var
        self.includeemission_MEMS = False
        
        #dust var
        self.DustSource = None
        self.DustModel = None
        self.Run_Dust = False
        
        self.includeemission_MEMS_dust = False
        self.Manual_MEMS_dust_source_dir = None
        self.MEMS_dust_source_dir = None
        self.DUST_ID = None 
        
        #met var
        self.MetSource = None
        self.netcdfpath = None
        self.CCAMFlavour = None
        self.CCAM_input_flow = None
        self.MET_ID = None 
        self.CCAM_StatusJSONfile = None
        return

################################################################################
    def Configure_Bushfire(self,  
                           BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,     
                           emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, 
                           Manual_HRB_Dir, Manual_Bushfire_Dir
                           ):
        '''
        Function to configure bushfire vars.
        
        It sets up the fire options house keeping, including the fire_ID to compute the ensemble directory name
    
        Parameters
        ----------
        BushfireSource : str
            Can be "Manual" or "Airflow", define the origin or the fire data.
        bushfireCSIRO : bool
            (not) using bushfireCSIRO option.
        bushfireOEH : bool
            (not) using bushfireOEH option.
        Run_HRB : bool
            (not) including HRB option.
        Run_Bushfire : bool
            (not) including wildfire option.
        emissions_rfs_DATA_CSIRO : str
            base path where to prepare the CSIRO fire emission model data
        burn_start_hour_csiro : str
            UTC hour when the burns start in the CSIRO fire emission model
        Manual_HRB_Dir : str
            source fire data path when manually computing HRBs in the CSIRO fire emission model
        Manual_Bushfire_Dir  : str   
            source fire data path when manually computing wildfires in the CSIRO fire emission model
    
        Returns
        -------
        FIRE_ID : int
            Code name to identify fire options in the directory naming convention
        bushfireCSIRO : bool
            updated trigger to run the CSIRO fire emission model
        bushfireOEH : bool
            updated trigger to run the OEH fire emission model
        '''
        self.BushfireSource = BushfireSource
        self.bushfireCSIRO = bushfireCSIRO
        self.bushfireOEH = bushfireOEH
        self.Run_HRB = Run_HRB
        self.Run_Bushfire = Run_Bushfire
        self.emissions_rfs_DATA_CSIRO = emissions_rfs_DATA_CSIRO
        self.burn_start_hour_csiro = burn_start_hour_csiro
        self.Manual_HRB_Dir = Manual_HRB_Dir
        self.Manual_Bushfire_Dir = Manual_Bushfire_Dir

        if  self.BushfireSource == "Airflow":
            if self.bushfireCSIRO:
                import Airflow.ConfigFromAirflow as CFA
                self.logger.info("Airflow CSIRO Bushfire module stream detected")
                Grab_CSIRO_Bushfire_Config = CFA.ConfigFromAirflow(self.logger, self.Run_HRB, self.Run_Bushfire, self.Startdate, self.timezone, 
                                                                              self.SimulationBasePath, self.emissions_rfs_DATA_CSIRO, 
                                                                              self.burn_start_hour_csiro, self.TimestampUTC)

                self.bushfireCSIRO = Grab_CSIRO_Bushfire_Config.trigger_CSIRO_Bushfire
                self.Run_HRB = Grab_CSIRO_Bushfire_Config.CSIRO_HRB 
                self.Run_Bushfire = Grab_CSIRO_Bushfire_Config.CSIRO_bushfire
            if self.bushfireOEH:
                self.logger.error("Airflow OEH Bushfire option not yet implemented!")
                sys.exit("sorry :(")
    
        if self.BushfireSource == "Manual":
            if self.bushfireCSIRO:
                import Airflow.Airflow_CSIRO_bushfire_config as ACBC
                self.logger.info("Manual CSIRO Bushfire module stream detected")

                Grab_CSIRO_Bushfire_Config = ACBC.Airflow_CSIRO_bushfire_config(self.logger, self.Startdate, self.timezone, self.SimulationBasePath,
                                             self.emissions_rfs_DATA_CSIRO, self.TimestampUTC, self.Run_HRB, self.Run_Bushfire)
                Grab_CSIRO_Bushfire_Config.Run_manual(self.Run_Bushfire, self.Run_HRB, self.Manual_HRB_Dir, self.Manual_Bushfire_Dir)  
                self.bushfireCSIRO = Grab_CSIRO_Bushfire_Config.trigger_CSIRO_Bushfire
        
            if self.bushfireOEH:
                self.logger.info("Manual OEH Bushfire module stream detected")
                self.logger.error("OEH Bushfire option not yet implemented!")
                sys.exit("sorry :(")
        # fire naming convention for main Run directory
        self.FIRE_ID = FNC.FireNameConvention(self.logger, self.bushfireCSIRO, self.bushfireOEH, self.Run_HRB, self.Run_Bushfire)
        #return  FIRE_ID, bushfireCSIRO, bushfireOEH
        return                   

################################################################################
    def Configure_Dust(self,  
                        DustSource, DustModel, Run_Dust,  
                        Manual_MEMS_dust_source_dir, Manual_MEMS_dust_source_file = None):
        '''
        Function to configure dust vars.
        
        It sets up the dust options house keeping, including the DUST_ID to compute the ensemble directory name
        Parameters
        ----------
        DustSource : str
            Can be "Manual" or "Airflow", define the origin or the fire data.
        DustModel : str
            name of the dust model to run (ex: none, 'inline', 'Chappell')
        Run_Dust : bool
            (not) using Dust option.
            
        '''
        self.DustSource = DustSource
        self.DustModel = DustModel
        self.Run_Dust = Run_Dust
        self.Manual_MEMS_dust_source_dir = Manual_MEMS_dust_source_dir
        self.Manual_MEMS_dust_source_file = Manual_MEMS_dust_source_file
        if  self.DustSource == "Airflow":
                self.logger.error("Airflow Dust option not yet implemented!")
                sys.exit("sorry :(")
        if self.DustSource == "Manual":
            self.MEMS_dust_source_dir = self.Manual_MEMS_dust_source_dir
            if self.Manual_MEMS_dust_source_file:
                self.MEMS_dust_source_file = self.Manual_MEMS_dust_source_file
            pass
        
        #build binary string for name convention inline
        list_inline_dust_domain = ['0'] * len(self.ResolutionNames)
        for index, name in enumerate(self.ResolutionNames):
            if self.ResolutionProperties[name]['include_dust_when_inline_dust']:
                list_inline_dust_domain[index] = '1'
        self.string_inline_dust_domain = ''.join(list_inline_dust_domain)    
        
        self.DUST_ID = DNC.DustNameConvention(self.logger, self.DustModel, self.Run_Dust, self.string_inline_dust_domain)
        return
################################################################################
    def Configure_Met(self,  MetSource,
                       netcdfpath, CCAMFlavour, CCAM_input_flow,
                       CCAM_StatusJSONfile = None):
        '''
        Function to configure met vars.
        
        It sets up the met options house keeping, including the MET_ID to compute the ensemble directory name
        Parameters
        ----------
        MetSource : str
            Can be "Manual", "Last" or "Airflow", define the origin or the Met data for the "Forecast_Linux" option.
        netcdfpath : str
            Can be "Manual" or "Airflow", define the origin or the met data.
        CCAMFlavour : str
            determine how the ccam files are formatted. 2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'.
        CCAM_input_flow : str
            input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA".
        CCAM_StatusJSONfile : str
            fullpath filename where to find the CCAM json file when CCAM_input_flow is "Forecast_Linux".
            
        '''
        self.MetSource = MetSource
        self.netcdfpath = netcdfpath
        self.CCAMFlavour = CCAMFlavour
        self.CCAM_input_flow = CCAM_input_flow
        
        if self.CCAM_input_flow == "Forecast_Linux":
            if self.MetSource == "Manual":
                self.CCAM_StatusJSONfile = CCAM_StatusJSONfile
            if self.MetSource == "Last":
                self.CCAM_StatusJSONfile = '/mnt/climate/cas/scratch/CCAM_Runs/ConfigRuns/last.json'
                self.logger.error("Not implemented yet")
            if self.MetSource == "Airflow":
                self.logger.error("Not implemented yet")
            
        
        self.MET_ID = MNC.MetNameConvention(self.logger, self.CCAMFlavour, self.CCAM_input_flow)
        return

################################################################################
    def Configure_EDMS(self, Emissiondate,
                       Emissions_edms_DATA_basedir, EmissionModellingPath,
                       PMspeciation,
                       ):  
        '''
        Function to configure EDMS emission database vars.
        
        It sets up the met options house keeping, including the MET_ID to compute the ensemble directory name
        Parameters
        ----------
        MetSource : str
            Can be "Manual", "Last" or "Airflow", define the origin or the Met data for the "Forecast_Linux" option.
        netcdfpath : str
            Can be "Manual" or "Airflow", define the origin or the fire data.
        CCAMFlavour : str
            determine how the ccam files are formatted. 2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'.
        CCAM_input_flow : str
            input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA".
        CCAM_StatusJSONfile : str
            fullpath filename where to find the CCAM json file when CCAM_input_flow is "Forecast_Linux".
            
        '''
        self.Emissiondate = Emissiondate
        
        self.EmissionModellingPath = EmissionModellingPath 
        #self.Emissions_edms_DATA = Emissions_edms_DATA
        #self.Emissions_edms_DATA = Emissions_edms_DATA
        self.Emissions_edms_DATA_basedir = Emissions_edms_DATA_basedir
   
        self.PMspeciation = PMspeciation
        if self.PMspeciation == 'PM10':
            self.PM10speciation = True
        else:
            self.PM10speciation = False

        # Which emission groups to include
        # if not False, then True! ;)
        self.includeemission_vpx = True
        self.includeemission_vdx = True 
        self.includeemission_vlx = True
        self.includeemission_vpv = True 
        self.includeemission_gse = True
        self.includeemission_whe = True

        # Do you need to force build the emissions?
        # if not False, then True! ;)
        self.buildemission_vpx = False
        self.buildemission_vdx = False
        self.buildemission_vlx = False
        self.buildemission_vpv = False
        self.buildemission_gse = False
        self.buildemission_whe = False
        self.buildemission_pse = True

        self.EDMS_ID = ENC.EDMSNameConvention(self.logger, self.Emissiondate, self.PM10speciation)
                            
################################################################################
        
def VarSetup_old(logger, BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,
             Startdate, timezone, TimestampUTC, 
             SimulationBasePath, emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, Manual_HRB_Dir, Manual_Bushfire_Dir
             ):
    '''
    Function to configure position vars.
    
    It sets up the fire options house keeping, including the fire_ID to compute the ensemble directory name
    
    Parameters
    ----------
    logger : logging.logger
        instance of a logger to output messages.
    BushfireSource : str
        Can be "Manual" or "Airflow", define the origin or the fire data.
    bushfireCSIRO : bool
        (not) using bushfireCSIRO option.
    bushfireOEH : bool
        (not) using bushfireOEH option.
    Run_HRB : bool
        (not) including HRB option.
    Run_Bushfire : bool
        (not) including wildfire option.
    Startdate : datetime.datetime
        timezone of the origin date for the modelled emission windows.
    timezone : str
        origin date for the modelled emission windows.
    TimestampUTC : datetime.datetime
        timestamp to identify the run.
    SimulationBasePath : str
        base path where to create the rundir.  
    emissions_rfs_DATA_CSIRO : str
        base path where to prepare the CSIRO fire emission model data
    burn_start_hour_csiro : str
        UTC hour when the burns start in the CSIRO fire emission model
    Manual_HRB_Dir : str
        source fire data path when manually computing HRBs in the CSIRO fire emission model
    Manual_Bushfire_Dir  : str   
        source fire data path when manually computing wildfires in the CSIRO fire emission model
    
    Returns
    -------
    FIRE_ID : int
        Code name to identify fire options in the directory naming convention
    bushfireCSIRO : bool
        updated trigger to run the CSIRO fire emission model
    bushfireOEH : bool
        updated trigger to run the OEH fire emission model
    '''
    
    if  BushfireSource == "Airflow":
        if bushfireCSIRO:
            import Airflow.ConfigFromAirflow as CFA
            logger.info("Airflow CSIRO Bushfire module stream detected")
            Grab_CSIRO_Bushfire_Config = CFA.ConfigFromAirflow(logger,Run_HRB, Run_Bushfire, Startdate, timezone, 
                                                                              SimulationBasePath, emissions_rfs_DATA_CSIRO, 
                                                                              burn_start_hour_csiro, TimestampUTC)

            bushfireCSIRO = Grab_CSIRO_Bushfire_Config.trigger_CSIRO_Bushfire
            Run_HRB = Grab_CSIRO_Bushfire_Config.CSIRO_HRB 
            Run_Bushfire = Grab_CSIRO_Bushfire_Config.CSIRO_bushfire
       
        else:
            logger.error("Airflow Bushfire option not yet implemented!")
            sys.exit("sorry :(")
    
    if BushfireSource == "Manual":
        if bushfireCSIRO:
            import Airflow.Airflow_CSIRO_bushfire_config as ACBC
            logger.info("Manual CSIRO Bushfire module stream detected")

            Grab_CSIRO_Bushfire_Config = ACBC.Airflow_CSIRO_bushfire_config(logger, Startdate, timezone, SimulationBasePath,
                                             emissions_rfs_DATA_CSIRO, TimestampUTC, Run_HRB, Run_Bushfire)
            Grab_CSIRO_Bushfire_Config.Run_manual(Run_Bushfire, Run_HRB, Manual_HRB_Dir, Manual_Bushfire_Dir)  
            bushfireCSIRO = Grab_CSIRO_Bushfire_Config.trigger_CSIRO_Bushfire
        
        if bushfireOEH:
            logger.info("Manual OEH Bushfire module stream detected")
    # fire naming convention for main Run directory
    FIRE_ID = FNC.FireNameConvention(logger, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire)
    return  FIRE_ID, bushfireCSIRO, bushfireOEH

##########################################################################################################################
if __name__ == '__main__':
    import datetime as dt    
    try:
        from .. import InitLogging as IL
    except:
        sys.path.append('Python-Scripts/Sources')
        import InitLogging as IL

    loggername = 'Configuration'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    
    timezone = 'UTC'
    Startdate = dt.datetime(2009,9,14)
    enddate = Startdate + dt.timedelta(days=3, hours=00)
    emissiondate = dt.datetime(2008,1,1)
    TimestampUTC = dt.datetime(2018,2,1)
    SimulationBasePath = 'Emissions-Test'
    
    ResolutionNames = ['nsw', 'gmr', 'gsyd']
    
    ConfigurationOptions = VarSetup(logger, Startdate, timezone, TimestampUTC, SimulationBasePath,
                      ResolutionNames)

    #fire options
    bushfireOEH=True
    bushfireOEH=False
    emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireOEH'
    bushfireCSIRO=True
    bushfireCSIRO=False
    emissions_rfs_DATA_CSIRO='/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
    burn_start_hour_csiro = 0

    BushfireSource = "Airflow"
    BushfireSource = "Manual"
    Run_HRB = True
    Run_HRB = False
        
    Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-05-24/CSEM_HRB_emission_model--2018-05-24T02-30-48'

    Run_Bushfire = True
    Run_Bushfire = False
    Manual_Bushfire_Dir = None 

    if BushfireSource == "Airflow":
        Manual_HRB_Dir = None 
        Manual_Bushfire_Dir = None 

    ConfigurationOptions.Configure_Bushfire(  
                           BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,     
                           emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, 
                           Manual_HRB_Dir, Manual_Bushfire_Dir
                           )
    #Dust options
    DustSource = "Airflow"
    DustSource = "Manual"
    Run_Dust = True
    Run_Dust = False
    if DustSource == "Manual":
        DustModel = None
        DustModel = 'inline'
        #DustModel = 'Chappell'
        Manual_MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
        
    ConfigurationOptions.Configure_Dust(  
                        DustSource, DustModel, Run_Dust,  
                        Manual_MEMS_dust_source_dir  
                        )
    pass    
