import os
import os.path
import random
import datetime as dt
import glob
import shutil as shu
import ResolutionProperties as RP 
import ConfigPrepCTM as CPC
import jinja2
import JinjaUtils as JJU
import string

#####################################################################################################################################
def EmissionFilesList(CHEM,MONTH):
    CommonFileDir='/mnt/project/ccam-ctm/commonfiles'
    BaseFiles=['lineSegments.csv','locations.csv','gen_meteo_config.txt','nswbiogenic_def.txt', \
    'aus_soiltype80k4Syd.txt','nsw_soiltype27k4Syd.txt','gmr_soiltype09k4Syd.txt','syd_soiltype03k4Syd.txt', \
    'soil_param','soil_psd','ccam_60000m_lai_'+MONTH ,  'ccam_8000m_lai_'+MONTH ,'ccam_1000m_lai_'+MONTH,'ccam_60000m_laiG_'+MONTH ,'ccam_8000m_laiG_'+MONTH , \
    'ccam_1000m_laiG_'+MONTH,'ccam_60000m_laiW_'+MONTH,   'ccam_8000m_laiW_'+MONTH ,'ccam_1000m_laiW_'+MONTH , \
    'NO2_05dec2006.txt','NO3NO_05dec2006.txt','NO3_NO2_05dec2006.txt','O3O1D_05dec2006.txt','O3_O3P_05dec2006.txt','Jbase_profiles.dat']
    Files=[CommonFileDir+os.sep+BF for BF in BaseFiles]
    
    FilesC=glob.glob(CommonFileDir+os.sep+'C*.txt')
    FilesH=glob.glob(CommonFileDir+os.sep+'H*.txt')
    FilesN=glob.glob(CommonFileDir+os.sep+'N*.txt')
    FilesI=glob.glob(CommonFileDir+os.sep+'I*.txt')
    FilesG=glob.glob(CommonFileDir+os.sep+'G*.txt')
    FilesM=glob.glob(CommonFileDir+os.sep+'M*.txt')
    FilesD=glob.glob(CommonFileDir+os.sep+'D*.txt')
    FilesP=glob.glob(CommonFileDir+os.sep+'P*.txt')
    FilesA=glob.glob(CommonFileDir+os.sep+'A*.txt')
    AllFiles=list(set(Files + FilesC + FilesH + FilesN + FilesI + FilesG + FilesM + FilesD + FilesP + FilesA))
    # trick to avoid doublons: convert to set then list again: list(set(list))
    AllFilesUnique=list(set(AllFiles))
    #print len(AllFiles), len(AllFilesUnique)
    return CommonFileDir,AllFilesUnique


#####################################################################################################################################
def SymlinkEmissionFiles(CHEM,MONTH):
    CommonFileDir,Allfiles=EmissionFilesList(CHEM,MONTH)
    print Allfiles
    for ff in Allfiles:
        if os.path.isfile(CommonFileDir+os.sep+os.path.basename(ff)):
            print 'Linking ',os.path.basename(ff)
            os.symlink(CommonFileDir+os.sep+os.path.basename(ff),os.path.basename(ff))   
        else:
            print 'File ',ff,' Not Found'
    #symlinking the boundary conditions
    CommonFileDir='/mnt/project/ccam-ctm/commonfiles'
    ff= CommonFileDir+os.sep+'tcbc_'+CHEM+'_forecast.in'
    fflink='tcbc_'+CHEM+'.in'
    print 'Linking ',os.path.basename(ff)
    os.symlink(CommonFileDir+os.sep+os.path.basename(ff),fflink)
    return   
#####################################################################################################################################
def UnlinkEmissionFiles(CHEM,MONTH):
    CommonFileDir,Allfiles=EmissionFilesList(CHEM,MONTH)
    for ff in Allfiles:
        try:
            print 'Unlinking ',os.path.basename(ff)
            os.unlink(os.path.basename(ff))   
        except:
            pass
    #unsymlinking the boundary conditions
    try:
        fflink='tcbc_'+CHEM+'.in'
        print 'Unlinking ',fflink
        os.unlink(fflink)
    except:
        pass    
    return
#####################################################################################################################################
def Generate_Ctm_prep_init(filename,scenario,DAY_N,YEAR_S,MONTH_S,DAY_S,MONTH,ResolutionNames):
    import DumpCtm_prep_init as DCTM
    File= open(filename,'w')
    File.write(DCTM.DumpCtm_prep_init(scenario,DAY_N,YEAR_S,MONTH_S,DAY_S,MONTH,ResolutionNames))
    File.close()
    return
#####################################################################################################################################

def Generate_Ctm_config_grid(ResolutionProperties,ResolutionNames,filename,index,SCENARIO,CHEM,Startdate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,MONTH):
    import DumpConfigGrid as DCG
    File= open(filename,'w')
    FF=DCG.DumpConfigGrid(ResolutionProperties,ResolutionNames,index,SCENARIO,CHEM,Startdate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,MONTH)
    File.write(FF)
    File.close()
    return
#####################################################################################################################################
#def Generate_DumpTcbcFile(filename):
#    import DumpTcbcFile as DTF
#    File= open(filename,'w')
#    FF=DTF.DumpTcbcFile()
#    File.write(FF)
#    File.close()
#    return
#####################################################################################################################################
def ConfigTcbc(filename,templatepath,templatefile,TcbcConfig):
    File= open(filename,'w')
    template=JJU.LoadTemplate(templatepath,templatefile)
    tcbcfile=template.render({'TcbcConfig':TcbcConfig })
    File.write(tcbcfile)
    File.close()
    return 
#####################################################################################################################################
#def CTM_prep(ResolutionNames,SCENARIO,DAY_N,YEAR_S,MONTH_S,DAY_S,MONTH,Startdate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,PrepBin):
def CTM_prep(SimulationConfig):
    import subprocess
    my_env = os.environ.copy()
#    ResolutionProperties=RP.ResolutionProperties()
    UnlinkEmissionFiles(SimulationConfig.CHEM,string.lower(SimulationConfig.Emissiondate.strftime('%b')))
    SymlinkEmissionFiles(SimulationConfig.CHEM,string.lower(SimulationConfig.Emissiondate.strftime('%b')))
    Ctm_prep_init_filename='ctm_prep_init.inp'
    Generate_Ctm_prep_init(Ctm_prep_init_filename,SCENARIO,DAY_N,YEAR_S,MONTH_S,DAY_S,MONTH,ResolutionNames)
#    TcbcFilename='tcbc_cb05_aer2.in'
#    Generate_DumpTcbcFile(TcbcFilename)
    
    subprocess.Popen(['echo $LIBRARY_PATH'],shell=True, env=my_env).wait()
    print subprocess.Popen(["source ~/.bashrc | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp |"+ PrepBin +" "+Ctm_prep_init_filename],shell=True, env=my_env).wait()
#    print subprocess.call([PrepBin,Ctm_prep_init_filename],shell=True, env=my_env)
    
    for index,res in enumerate(ResolutionNames):
        Ctm_config_grid_filename='ctm_config_grid'+str(index+1)+'.txt'
        print ResolutionProperties[res]
        print 'ccam_' + str(ResolutionProperties[res][2]) + 'm_laiW_' + str(MONTH) + '  ccam_' + str(ResolutionProperties[res][2]) + 'm_laiG_' + str(MONTH) 
        Generate_Ctm_config_grid(ResolutionProperties,ResolutionNames,Ctm_config_grid_filename,index,SCENARIO,CHEM,Startdate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,MONTH)

    #UnlinkEmissionFiles(CHEM,MONTH)

    return
    
    
#####################################################################################################################################
if __name__ == '__main__':

    import RunConfig as RC
    import logging
    import InitLogging as IL
    loggername='PrepCTM'
    logger=IL.Initialise_logging(loggername)
   
    
##########################################################
# Switches for processing blocks
##########################################################
    buildemissions=true
    merge_ECMWF=False         # ECMWF+nsw-pse
    merge_RFS=False        # +RFS fires
    merge_DELWP=False         # +DELWP fires
    merge_TFS=False         # +TFS fires
    prep=true
    gen_meteo=true
    ctm_config=true
    Restart=False
    
   
##############################################
# scenarios and options
##############################################
    MONTH='nov'
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
    BIO=MONTH
    YEAR=2016

    PrepBin='/mnt/project/ccam-ctm/exec/ctm_prep_init_cb05_aer2'

   #cd ~/ctm/
##############################################
#start date; end date
# used by CTM
##############################################
    YEAR_S=2017
    MONTH_S=4
    DAY_S=4
    DAY_S_CTM=4
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)

    YEAR_EMISSIONS=YEAR_S
    MONTH_EMISSIONS=MONTH

##############################################
# simulation hours
##############################################
    DAY_N=3
    HOUR_N = DAY_N * 24
 ##############################################
#  end date
# used by CTM
##############################################
    EndDate=Startdate+dt.timedelta(days=DAY_N,hours=00)

#    YEAR_E=2016
#    MONTH_E=11
#    DAY_E=25
    YEAR_E=EndDate.year
    MONTH_E=EndDate.month
    DAY_E=EndDate.day
    Emissiondate=dt.datetime(YEAR_E,MONTH_E,DAY_E)
    
    ResolutionProperties=RP.ResolutionProperties()
    ResolutionNames=['nsw','gmr','gsyd']
    
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'

    SimulationConfig=RC.SimulationConfigClass(ResolutionNames,ResolutionProperties,SCENARIO,Startdate,EndDate,Emissiondate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,BIO,PrepBin,templatepath)
    
    for month in range(1,13):
        testdate=dt.datetime(YEAR_S,month,DAY_S)
        templatefile_tcbc="tcbc_"+CHEM+'_'+string.lower(testdate.strftime('%b'))+'.template'
        Tcbc_filename="tcbc_"+CHEM+'_'+string.lower(testdate.strftime('%b'))+".in"
        testfile_tcbc='./TestFiles/'+"tcbc_"+CHEM+'_'+string.lower(testdate.strftime('%b'))+".in"
        logger.debug('test BC file= %s testfile= %s template= %s',Tcbc_filename,testfile_tcbc,templatefile_tcbc)

        TcbcConfig=CPC.TcbcConfigClass()
        ConfigTcbc(Tcbc_filename,templatepath,templatefile_tcbc,TcbcConfig)
        logger.debug('-------------------------------')
        logger.debug('Diff Tcbc')
        IL.PrintDiff(Tcbc_filename, testfile_tcbc,logger)

#    UnlinkEmissionFiles(CHEM,MONTH)
    
#    CTM_prep(ResolutionNames,SCENARIO,DAY_N,YEAR_S,MONTH_S,DAY_S,MONTH,Startdate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,PrepBin)
#    CTM_prep(SimulationConfig)


#    ResolutionProperties=RP.ResolutionProperties()
 


#    SymlinkEmissionFiles(CHEM,MONTH)
#    UnlinkEmissionFiles(CHEM,MONTH)
#    Ctm_prep_init_filename='ctm_prep_init.inp'
#    Generate_Ctm_prep_init(Ctm_prep_init_filename,SCENARIO,DAY_N,YEAR_S,MONTH_S,DAY_S,MONTH,ResolutionNames)
#    TcbcFilename='tcbc_cb05_aer2.in'
#    Generate_DumpTcbcFile(TcbcFilename)
    

    
#    for index,res in enumerate(ResolutionNames):
#        Ctm_config_grid_filename='ctm_config_grid'+str(index+1)+'.txt'
#        print ResolutionProperties[res]
#        print 'ccam_' + str(ResolutionProperties[res][2]) + 'm_laiW_' + str(MONTH) + '  ccam_' + str(ResolutionProperties[res][2]) + 'm_laiG_' + str(MONTH) 
#        Generate_Ctm_config_grid(ResolutionProperties,ResolutionNames,Ctm_config_grid_filename,index,SCENARIO,CHEM,Startdate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,MONTH)

