"""
.. module:: DumpConfigGrid
    :platform: Unix
    :synopsis: Configure and write the template defining the nesting grid configuration and emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
   
"""
import datetime as dt
#import ResolutionProperties as RP 
import Sources.Domain_definition.ResolutionProperties as RP 
import JinjaUtils as JJU
import os
import string
import sys

class ConfigGridRestartConfigClass(object):
    """ 
    This class defines a ConfigGridRestartConfig class object.
    It configures ConfigGridRestartConfig Class which setup properties for the 
    domain-selected grid in order to feed the CTM prep.
    There is defined grid properties and nesting, and emissions to include with their factors
    
    The class is self contained and can be initialised from original data, 
    it doesn't depend on higher level classes.
    it contains all the methods to configure and generate ConfigGridRestartConfig.

    Attributes
    -----------
    logger : logging.logger
        instance of a logger to output messages.
    justif : int
        max message width to justify logger output.   
    ResolutionNames : str
        string identifying the computational domain name.
    SCENARIO : str
        run identification name, can be "forecast" or "XXX".
    startdate : datetime.datetime
        start date of the emissions to build as a UTC datetime.
    enddate : datetime.datetime
        end date of the emissions to build as a UTC datetime.
    emissiondate : datetime.datetime
        date of the emission. 
    TRANSPORT : str
        numerical scheme name for the transport.
    DIFF : str
        enable or not the NWP diffusivity.
    SIOA : str
        numerical scheme name for secondary inorganic atmospheric chemistry.
    SOA : str
        numerical scheme name for secondary atmospheric chemistry.
    CHEM : str
        numerical scheme name for carbon atmospheric chemistry.
    BIO : str
        month name of the emissions
    templatepath : str
        path to the template directory.
    templatefile : str
        name of the templatefile 
    ResolutionProperties : dic
        dictionary containing each computational domain properties.
    SimulationHours : int
        lenght of the simulation in hours 
    netcdfpath : str
        path to the base dir where the CCAM NWP is.
    SimulationPath : str
        path to the base dir where the Simulation will run.
    GridIndex : int
        grid index in the nesting table
    WarmStart : bool
        define if the code is restarting or not (init, yes/no)
    EmissionGlobalScaling : object
        container object that define all the emission scalings 
    includeemission_MEMS : bool
        Trigger to include the MEMS emissions 
    Run_Dust : bool
        Trigger to include dust emissions
    includeemission_MEMS_dust : bool
        Trigger to include MEMs dust emmissions if the correct model is selected
    DustModel : str
        Name of the dust model to include
    """
    def __init__(self, logger, justif, ResolutionNames, SCENARIO,
                 Startdate, Enddate, Emissiondate, TRANSPORT, DIFF, SIOA, SOA, CHEM, 
                 BIO, templatepath, templatefile, 
                 ResolutionProperties, SimulationHours, netcdfpath, SimulationPath, GridIndex, WarmStart,
                 EmissionGlobalScaling, 
                 includeemission_MEMS,
                 Run_Dust, includeemission_MEMS_dust, DustModel):
        self.logger = logger
        self.justif = justif
        self.ResolutionNames = ResolutionNames
        self.SCENARIO = SCENARIO
        self.Startdate = Startdate
        self.Enddate = Enddate
        self.Emissiondate = Emissiondate
        self.TRANSPORT = TRANSPORT
        self.DIFF = DIFF
        self.SIOA = SIOA
        self.SOA = SOA
        self.CHEM = CHEM
        self.BIO = BIO
        self.templatepath = templatepath
        self.ResolutionProperties = ResolutionProperties
        self.SimulationHours = SimulationHours
        self.netcdfpath = netcdfpath + os.sep
        self.SimulationPath = SimulationPath + os.sep
        self.GridIndex = GridIndex
        self.WarmStart = WarmStart
        
        self.resolution = self.ResolutionNames[self.GridIndex]
        #self.IndexNested = self.GridIndex + self.ResolutionProperties[self.resolution][9] 
        self.IndexNested = self.GridIndex + self.ResolutionProperties[self.resolution]['n_nested_master_grid'] 
        
        self.resolutionNested = self.ResolutionNames[self.IndexNested]
        #self.Ctm_config_grid_filename='ctm_config_grid'+str(self.GridIndex)+'.txt'
        #self.Ctm_config_grid_filename = 'ctm_config_'+str(self.GridIndex)+'.txt'
        self.Ctm_config_grid_filename = 'ctm_config_{gridindex}.txt'.format(gridindex = self.GridIndex)
        self.StartMonth = string.lower(self.Startdate.strftime('%b'))
        #self.templatefile='ConfigGrid_Restart.template'
        self.templatefile = templatefile
        self.EmissionGlobalScaling = EmissionGlobalScaling 
        
        #MEMS
        self.includeemission_MEMS = includeemission_MEMS
        
        #dust
        self.Run_Dust = Run_Dust
        self.includeemission_MEMS_dust = includeemission_MEMS_dust
        self.DustModel_from_config = DustModel
        
        self.LAIfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_lai_{month}'.format(
             swLonCorner = self.ResolutionProperties[self.resolution]['SW_corner_lon'], 
             swLatCorner = self.ResolutionProperties[self.resolution]['SW_corner_lat'],
             nLonpoints = self.ResolutionProperties[self.resolution]['nlon'], 
             nLatpoints = self.ResolutionProperties[self.resolution]['nlat'],
             gridres = self.ResolutionProperties[self.resolution]['dx_deg'],
             month = self.StartMonth)
        self.LAIWfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_laiW_{month}'.format(
             swLonCorner = self.ResolutionProperties[self.resolution]['SW_corner_lon'], 
             swLatCorner = self.ResolutionProperties[self.resolution]['SW_corner_lat'],
             nLonpoints = self.ResolutionProperties[self.resolution]['nlon'], 
             nLatpoints = self.ResolutionProperties[self.resolution]['nlat'],
             gridres = self.ResolutionProperties[self.resolution]['dx_deg'],
             month = self.StartMonth) 
        self.LAIGfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_laiG_{month}'.format(
             swLonCorner = self.ResolutionProperties[self.resolution]['SW_corner_lon'], 
             swLatCorner = self.ResolutionProperties[self.resolution]['SW_corner_lat'],
             nLonpoints = self.ResolutionProperties[self.resolution]['nlon'], 
             nLatpoints = self.ResolutionProperties[self.resolution]['nlat'],
             gridres = self.ResolutionProperties[self.resolution]['dx_deg'],
             month = self.StartMonth)
        self.SOILfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_soil'.format(
             swLonCorner = self.ResolutionProperties[self.resolution]['SW_corner_lon'], 
             swLatCorner = self.ResolutionProperties[self.resolution]['SW_corner_lat'],
             nLonpoints = self.ResolutionProperties[self.resolution]['nlon'], 
             nLatpoints = self.ResolutionProperties[self.resolution]['nlat'],
             gridres = self.ResolutionProperties[self.resolution]['dx_deg'],
             )
        self.VEGfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_veg'.format(
             swLonCorner = self.ResolutionProperties[self.resolution]['SW_corner_lon'], 
             swLatCorner = self.ResolutionProperties[self.resolution]['SW_corner_lat'],
             nLonpoints = self.ResolutionProperties[self.resolution]['nlon'], 
             nLatpoints = self.ResolutionProperties[self.resolution]['nlat'],
             gridres = self.ResolutionProperties[self.resolution]['dx_deg'],
             )
        self.DUSTfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_dust'.format(
             swLonCorner = self.ResolutionProperties[self.resolution]['SW_corner_lon'], 
             swLatCorner = self.ResolutionProperties[self.resolution]['SW_corner_lat'],
             nLonpoints = self.ResolutionProperties[self.resolution]['nlon'], 
             nLatpoints = self.ResolutionProperties[self.resolution]['nlat'],
             gridres = self.ResolutionProperties[self.resolution]['dx_deg'],
             )

#        self.LAIfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_lai_{month}'.format(
#             swLonCorner = self.ResolutionProperties[self.resolution][4], 
#             swLatCorner = self.ResolutionProperties[self.resolution][3],
#             nLonpoints = self.ResolutionProperties[self.resolution][5], 
#             nLatpoints = self.ResolutionProperties[self.resolution][6],
#             gridres = self.ResolutionProperties[self.resolution][7],
#             month = self.StartMonth)
#        self.LAIWfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_laiW_{month}'.format(
#             swLonCorner = self.ResolutionProperties[self.resolution][4], 
#             swLatCorner = self.ResolutionProperties[self.resolution][3],
#             nLonpoints = self.ResolutionProperties[self.resolution][5], 
#             nLatpoints = self.ResolutionProperties[self.resolution][6],
#             gridres = self.ResolutionProperties[self.resolution][7],
#             month = self.StartMonth) 
#        self.LAIGfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_laiG_{month}'.format(
#             swLonCorner = self.ResolutionProperties[self.resolution][4], 
#             swLatCorner = self.ResolutionProperties[self.resolution][3],
#             nLonpoints = self.ResolutionProperties[self.resolution][5], 
#             nLatpoints = self.ResolutionProperties[self.resolution][6],
#             gridres = self.ResolutionProperties[self.resolution][7],
#             month = self.StartMonth)
#        self.SOILfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_soil'.format(
#             swLonCorner = self.ResolutionProperties[self.resolution][4], 
#             swLatCorner = self.ResolutionProperties[self.resolution][3],
#             nLonpoints = self.ResolutionProperties[self.resolution][5], 
#             nLatpoints = self.ResolutionProperties[self.resolution][6],
#             gridres = self.ResolutionProperties[self.resolution][7]
#             )
#        self.VEGfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_veg'.format(
#             swLonCorner = self.ResolutionProperties[self.resolution][4], 
#             swLatCorner = self.ResolutionProperties[self.resolution][3],
#             nLonpoints = self.ResolutionProperties[self.resolution][5], 
#             nLatpoints = self.ResolutionProperties[self.resolution][6],
#             gridres = self.ResolutionProperties[self.resolution][7]
#             )
#        self.DUSTfile = 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_dust'.format(
#             swLonCorner = self.ResolutionProperties[self.resolution][4], 
#             swLatCorner = self.ResolutionProperties[self.resolution][3],
#             nLonpoints = self.ResolutionProperties[self.resolution][5], 
#             nLatpoints = self.ResolutionProperties[self.resolution][6],
#             gridres = self.ResolutionProperties[self.resolution][7]
#             )

        #Dust
        if self.DustModel_from_config not in [None, 'inline', 'Chappell']:
            self.logger.error('Problem with Dust config!'.ljust(self.justif-5,'.') + 'ABORT')
            sys.exit('Problem with Dust config!! :(')    
        
        self.DustModel = 'noDUST'
        if self.Run_Dust:
            if self.DustModel_from_config == 'inline' and self.ResolutionProperties[self.resolution]['include_dust_when_inline_dust']:
                self.DustModel = 'DUST'
            elif self.DustModel_from_config == 'Chappell' and  self.includeemission_MEMS_dust and self.includeemission_MEMS:
                self.DustModel = 'DUSTALBEDO'
            elif self.DustModel_from_config == None:
                self.DustModel = 'noDUST'
        
        return        

#####################################################################################################################################
    def Build_config_grid(self):
        """ This function write the config file to build the configuration file for one grid
        """
        filename=os.path.join(self.SimulationPath, self.Ctm_config_grid_filename)
        File= open(filename,'w')

        template=JJU.LoadTemplate(self.templatepath, self.templatefile)

        config=template.render({'ConfigGridRestart': self })
        File.write(config)
        File.close()
        
        self.logger.info('ConfigGrid configuration file'.ljust(self.justif-7,'.') + 'WRITTEN')
        return 

    
#####################################################################################################################################
if __name__ == '__main__':
    import InitLogging as IL
    import ResolutionProperties as RP 
    import BuildEmissions.EmissionScaling as ES
    import Configuration as Conf
    loggername='ConfigGrid'
    logger=IL.Initialise_logging(loggername)
    justif = 102
    
##############################################
# scenarios and options
##############################################
    MONTH='nov'
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
    BIO=MONTH
    YEAR=2016
    YEAR_S=2017
    MONTH_S=4
    DAY_S=4
    DAY_S_CTM=4
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)
    YEAR_EMISSIONS=YEAR_S
    MONTH_EMISSIONS=MONTH
    DAY_N=3
    HOUR_N = DAY_N * 24
    EndDate=Startdate+dt.timedelta(days=DAY_N,hours=00)
    YEAR_E=EndDate.year
    YEAR_E=2008
    MONTH_E=EndDate.month
    DAY_E=EndDate.day
    Emissiondate=dt.datetime(YEAR_E,MONTH_E,DAY_E)
    
    ResolutionProperties = RP.ResolutionProperties()
    ResolutionProperties = RP.ResolutionProperties_new()
    ResolutionNames=['nsw','gmr','gsyd']
    
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatefile='ConfigGrid_Restart.template'
    netcdfpath='../'
#    EmissionGroup=['vpx','vdx','vlx','vpv','gse','whe']
    EmissionModellingPath='/mnt/climate/cas/project/ccam-ctm/commonfiles'
    EmissionBuildPath='./'
    ccamfilepath='./'
    SimulationPath='./Forecast'
    SimulationPath='.'
    EmissionGlobalScaling = ES.EmissionGlobalScalingClass()
    EmissionGlobalScaling.SCALE_COMMERCIAL_DOMESTIC.VOC = 3.

    timezone = 'UTC'
    TimestampUTC = dt.datetime.now()
    
    ConfigurationOptions = Conf.VarSetup(logger, Startdate, timezone, TimestampUTC, SimulationPath)
    #ConfigurationOptions.Run_Dust = True
    ConfigurationOptions.includeemission_MEMS_dust = True
    ConfigurationOptions.DustModel = None
    ConfigurationOptions.DustModel = 'inline'
    ConfigurationOptions.DustModel = 'Chappell'
##############################################
    WarmStart=True
    WarmStart=False
    for GridIndex in range(3):
        ConfigGridRestartConfig = ConfigGridRestartConfigClass(logger, justif, ResolutionNames, SCENARIO,
                                     Startdate, EndDate, Emissiondate, TRANSPORT, DIFF, SIOA, SOA, CHEM, 
                                     BIO, templatepath, templatefile, 
                                     ResolutionProperties, HOUR_N, netcdfpath, SimulationPath, GridIndex, WarmStart,
                                     EmissionGlobalScaling, ConfigurationOptions)
        ConfigGridRestartConfig.Build_config_grid()            
   
