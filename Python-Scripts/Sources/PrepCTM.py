"""
.. module:: PrepCTM
   :platform: Unix
   :synopsis: Main script to configure the CTM prep.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import os
import os.path
import sys
import random
import datetime as dt
import glob
import shutil as shu
#import ResolutionProperties as RP 
import ConfigPrepCTM as CPC
import jinja2
import JinjaUtils as JJU
import string
import RunConfig as RC
import DumpTcbcFile as DTF
#import GenerateMeteoConfigFile as GMCF

#####################################################################################################################################
#def EmissionFilesList(CHEM,MONTH):
def EmissionFilesList(SimulationConfig):
    """
    This function prepare the list of the external emission source files to link for the prep script.
    
    EmissionFilesList(SimulationConfig):
    
    Parameters
    ---------- 
        SimulationConfig : SimulationConfig object
            Inputs the a SimulationConfig object that contains all the parameters of the simulation.
    Returns
    -------
        None : None
    """

    CommonFileDir=SimulationConfig.EmissionModellingPath
    LAIsubdir = SimulationConfig.LAIsubdir
    SOILsubdir = SimulationConfig.SOILsubdir
    VEGsubdir = SimulationConfig.VEGsubdir
    
    RP = SimulationConfig.ResolutionProperties
    
    SimulationFiles=['lineSegments.csv','locations.csv']
    SoilFiles=['soil_param','soil_psd','aus_biogenic_def.txt','nsw_biogenic_def.txt']
    
    # Old SOIL filenaming
    #LandFiles=['soiltype_ccam_'+str(SimulationConfig.ResolutionProperties[res][2])+'m.txt' for res in SimulationConfig.ResolutionProperties.keys()]
    
    monthname=string.lower(SimulationConfig.Startdate.strftime('%b'))
    
    # Old LAI filenaming
    #LaiFiles= ['ccam_'+str(SimulationConfig.ResolutionProperties[res][2])+'m_lai_'+monthname for res in SimulationConfig.ResolutionProperties.keys()]
    #LaiGFiles=['ccam_'+str(SimulationConfig.ResolutionProperties[res][2])+'m_laiG_'+monthname for res in SimulationConfig.ResolutionProperties.keys()]
    #LaiWFiles=['ccam_'+str(SimulationConfig.ResolutionProperties[res][2])+'m_laiW_'+monthname for res in SimulationConfig.ResolutionProperties.keys()]
    
    #New LAI files 
    LaiFiles = [os.path.join(
             LAIsubdir, domain, 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_lai_{month}'.format(
             swLonCorner = RP[domain]['SW_corner_lon'], 
             swLatCorner = RP[domain]['SW_corner_lat'],
             nLonpoints = RP[domain]['nlon'], 
             nLatpoints = RP[domain]['nlat'],
             gridres = RP[domain]['dx_deg'],
             month = monthname) 
             )
             for domain in SimulationConfig.ResolutionNames]
    LaiGFiles = [os.path.join(
             LAIsubdir, domain, 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_laiG_{month}'.format(
             swLonCorner = RP[domain]['SW_corner_lon'], 
             swLatCorner = RP[domain]['SW_corner_lat'],
             nLonpoints = RP[domain]['nlon'], 
             nLatpoints = RP[domain]['nlat'],
             gridres = RP[domain]['dx_deg'],
             month = monthname) 
             )
             for domain in SimulationConfig.ResolutionNames]
    LaiWFiles = [os.path.join(
             LAIsubdir, domain, 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_laiW_{month}'.format(
             swLonCorner = RP[domain]['SW_corner_lon'], 
             swLatCorner = RP[domain]['SW_corner_lat'],
             nLonpoints = RP[domain]['nlon'], 
             nLatpoints = RP[domain]['nlat'],
             gridres = RP[domain]['dx_deg'],
             month = monthname) 
             )
             for domain in SimulationConfig.ResolutionNames]

    SimulationConfig.logger.info('LAI file list'.ljust(SimulationConfig.justif-2,'.') + 'OK')

    #New SOIL files 
    LandFiles = [os.path.join(
             SOILsubdir, domain, 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_soil'.format(
             swLonCorner = RP[domain]['SW_corner_lon'], 
             swLatCorner = RP[domain]['SW_corner_lat'],
             nLonpoints = RP[domain]['nlon'], 
             nLatpoints = RP[domain]['nlat'],
             gridres = RP[domain]['dx_deg'],
             ) 
             )
             for domain in SimulationConfig.ResolutionNames]
    SimulationConfig.logger.info('SOIL file list'.ljust(SimulationConfig.justif-2,'.') + 'OK')

    #VegFiles= ['vegtype_ccam_'+str(SimulationConfig.ResolutionProperties[res][2])+'m.txt' for res in SimulationConfig.ResolutionProperties.keys()]
    VegFiles = [os.path.join(
             VEGsubdir, domain, 'ccam_E{swLonCorner}_N{swLatCorner}_{nLonpoints}nx_{nLatpoints}ny_{gridres}_veg'.format(
             swLonCorner = RP[domain]['SW_corner_lon'], 
             swLatCorner = RP[domain]['SW_corner_lat'],
             nLonpoints = RP[domain]['nlon'], 
             nLatpoints = RP[domain]['nlat'],
             gridres = RP[domain]['dx_deg'],
             )
             )
             for domain in SimulationConfig.ResolutionNames]


    # to use the LAI instead of the predefined veg type --> better isoprene emissions? 
    VegFiles= []
    SimulationConfig.logger.info('VEG file list'.ljust(SimulationConfig.justif-2,'.') + 'OK')


#    BaseFiles=['lineSegments.csv','locations.csv','nswbiogenic_def.txt', \
#    'aus_soiltype80k4Syd.txt','nsw_soiltype27k4Syd.txt','gmr_soiltype09k4Syd.txt','syd_soiltype03k4Syd.txt', \
#    'soil_param','soil_psd','ccam_60000m_lai_'+MONTH ,  'ccam_8000m_lai_'+MONTH ,'ccam_1000m_lai_'+MONTH,'ccam_60000m_laiG_'+MONTH ,'ccam_8000m_laiG_'+MONTH , \
#    'ccam_1000m_laiG_'+MONTH,'ccam_60000m_laiW_'+MONTH,   'ccam_8000m_laiW_'+MONTH ,'ccam_1000m_laiW_'+MONTH , \
#    'NO2_05dec2006.txt','NO3NO_05dec2006.txt','NO3_NO2_05dec2006.txt','O3O1D_05dec2006.txt','O3_O3P_05dec2006.txt','Jbase_profiles.dat']

    BaseFiles = SimulationFiles + SoilFiles+LandFiles + LaiFiles + LaiGFiles + LaiWFiles + VegFiles

    Files=[os.path.join(CommonFileDir,BF) for BF in BaseFiles]
    
#    FilesC=glob.glob(CommonFileDir+os.sep+'C*.txt')
#    FilesH=glob.glob(CommonFileDir+os.sep+'H*.txt')
#    FilesN=glob.glob(CommonFileDir+os.sep+'N*.txt')
#    FilesI=glob.glob(CommonFileDir+os.sep+'I*.txt')
#    FilesG=glob.glob(CommonFileDir+os.sep+'G*.txt')
#    FilesM=glob.glob(CommonFileDir+os.sep+'M*.txt')
#    FilesD=glob.glob(CommonFileDir+os.sep+'D*.txt')
#    FilesP=glob.glob(CommonFileDir+os.sep+'P*.txt')
#    FilesA=glob.glob(CommonFileDir+os.sep+'A*.txt')
#    AllFiles=list(set(Files + FilesC + FilesH + FilesN + FilesI + FilesG + FilesM + FilesD + FilesP + FilesA))
#    # trick to avoid doublons: convert to set then list again: list(set(list))
#    AllFilesUnique=list(set(AllFiles))
    AllFilesUnique=list(set(Files))

    #print len(AllFiles), len(AllFilesUnique)
#    return CommonFileDir,AllFilesUnique
    return AllFilesUnique


#####################################################################################################################################
def SymlinkEmissionFiles(SimulationConfig):
    """
    This function links the files in the list of the external emission source files.
    
    SymlinkEmissionFiles(SimulationConfig):
    
    Parameters
    ---------- 
        SimulationConfig : SimulationConfig object
            Inputs the a SimulationConfig object that contains all the parameters of the simulation.
    Returns
    -------
        None : None
    """
    Allfiles=EmissionFilesList(SimulationConfig)
    for ff in Allfiles:
        #print ff, os.path.isfile(os.path.join(SimulationConfig.EmissionModellingPath,os.path.basename(ff)))
        #print os.stat(os.path.join(SimulationConfig.EmissionModellingPath,os.path.basename(ff)))
        if os.path.isfile(ff):
            #os.symlink(os.path.join(SimulationConfig.EmissionModellingPath,os.path.basename(ff)),os.path.join(SimulationConfig.SimulationPath,os.path.basename(ff)))   
            os.symlink(ff , os.path.join(SimulationConfig.SimulationPath,os.path.basename(ff)))   
            SimulationConfig.logger.info('Linking {filename}'.format(filename = os.path.basename(ff)).ljust(SimulationConfig.justif-2,'.')+'OK')
        else:
            SimulationConfig.logger.error('File {filename} '.format(
                         filename = ff).ljust(SimulationConfig.justif-9,'.')+'NOT FOUND')
                         #filename = os.path.join(SimulationConfig.EmissionModellingPath,os.path.basename(ff))).ljust(SimulationConfig.justif-9,'.')+'NOT FOUND')
    return   
#####################################################################################################################################
def UnlinkEmissionFiles(SimulationConfig):
    """
    This function unlinks the files in the list of the external emission source files.
    
    UnlinkEmissionFiles(SimulationConfig):
    
    Parameters
    ---------- 
        SimulationConfig : SimulationConfig object
            Inputs the a SimulationConfig object that contains all the parameters of the simulation.
    Returns
    -------
        None : None
    """
    Allfiles=EmissionFilesList(SimulationConfig)
    for ff in Allfiles:
        try:
            os.unlink(os.path.join(SimulationConfig.SimulationPath,os.path.basename(ff)))   
            SimulationConfig.logger.info('UnLinking {filename}'.format(filename = os.path.basename(ff)).ljust(SimulationConfig.justif-2,'.')+'OK')
        except:
            SimulationConfig.logger.warn('UnLink {filename}'.format(filename = os.path.basename(ff)).ljust(SimulationConfig.justif-12,'.')+'NOT POSSIBLE')
            pass
    return

#####################################################################################################################################
def CTM_prep(SimulationConfig):
    """
    This function prepare the config files for the CTM prep and runs it.
    
    CTM_prep(SimulationConfig):
    
    Parameters
    ---------- 
        SimulationConfig : SimulationConfig object
            Inputs the a SimulationConfig object that contains all the parameters of the simulation.
    Returns
    -------
        None : None
    """
    import subprocess
    #import BuildEmission as BE
    my_env = os.environ.copy()
    
    logger = SimulationConfig.logger
    
    logger.info(''.ljust(SimulationConfig.justif,'-'))
    logger.info('CTM prep script'.center(SimulationConfig.justif,'|'))
    logger.info(''.ljust(SimulationConfig.justif,'-'))

    logger.info('Initialising Master Emissions definitions'.center(SimulationConfig.justif,'.'))
    EmissionConfig = SimulationConfig.init_Emission()
    logger.info('Master Emissions definitions'.ljust(SimulationConfig.justif-2,'.')+'OK')

    logger.info('Initialising Emissions subclass and Building (or not) Emissions'.center(SimulationConfig.justif,'.'))
    #BE.Build_all_emissions_final(EmissionConfig,logger)
    EmissionConfig.Build_Emissions()
    logger.info('Emissions subclasses'.ljust(SimulationConfig.justif-2,'.')+'OK')
    #sys.exit()

    logger.info('Building Boundary conditions'.center(SimulationConfig.justif,'.'))
#    templatefile_tcbc="tcbc_"+SimulationConfig.CHEM+'_'+string.lower(SimulationConfig.Startdate.strftime('%b'))+'.template'
    templatefile_tcbc = "tcbc_{chem}_{month}.template".format(chem = SimulationConfig.CHEM, month = SimulationConfig.Startdate.strftime('%b').lower())
#    Tcbc_filename="tcbc_"+SimulationConfig.CHEM+'_'+string.lower(SimulationConfig.Startdate.strftime('%b'))+".in"
#    Tcbc_filename="tcbc_"+SimulationConfig.CHEM+".in"
    Tcbc_filename = "tcbc_{chem}.in".format(chem = SimulationConfig.CHEM)
    TcbcConfig = SimulationConfig.init_TcbcConfig(templatefile_tcbc,Tcbc_filename)
    TcbcConfig.Build_Tcbc()
    logger.info('Boundary conditions'.ljust(SimulationConfig.justif-2,'.')+'OK')
         
    logger.info('Building CTM CtmPrepInitConfig -- First half'.center(SimulationConfig.justif,'.'))
    ConfigPrepCTM = SimulationConfig.init_CtmPrepInitConfig()

    logger.info('Building CTM CtmPrepInitConfig -- Configuring the second half and updating Simulation'.center(SimulationConfig.justif,'.'))
    EmissionConfig.Configure_CTM_prep_init_and_Update_Simulation(ConfigPrepCTM,SimulationConfig)
    logger.debug('Check if SimulationConfig got the updated emission groups = %s',SimulationConfig.EmissionGroup)

    logger.info('Building CTM CtmPrepInitConfig'.center(SimulationConfig.justif,'.'))
    ConfigPrepCTM.Build_Ctm_prep_init()
    logger.info('CTM Prep Configuration'.ljust(SimulationConfig.justif-2,'.')+'OK')
    
    logger.info('Linking Emission Files'.center(SimulationConfig.justif,'.'))
    UnlinkEmissionFiles(SimulationConfig)
    SymlinkEmissionFiles(SimulationConfig)
    logger.info('Emission Files'.ljust(SimulationConfig.justif-6,'.')+'LINKED')
   
    logger.info('Configuring the ingestion of the Met files'.center(SimulationConfig.justif,'.'))
    MC = SimulationConfig.init_MetConfig()
    MC.Run_all()
    SimulationConfig.netcdfpath = MC.OriginalCCAMdir
    SimulationConfig.meteo_config_filename_list = MC.meteo_config_filename_list
    logger.info('Met files catering'.ljust(SimulationConfig.justif-2,'.')+'OK')
   
    logger.info('Running the Prep'.center(SimulationConfig.justif,'.'))
    #print subprocess.Popen(['echo $LIBRARY_PATH'],shell=True, env=my_env).wait()
    #print subprocess.Popen(["source ~/.bashrc | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-Temp |" + \
    #        ConfigPrepCTM.PrepBin +" "+ConfigPrepCTM.Ctm_prep_init_filename],shell=True, env=my_env, cwd=ConfigPrepCTM.SimulationPath).wait()
    #initline = "source /usr/share/modules/init/bash | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-Temp |"
    initline = SimulationConfig.bininitline
    runline = "{initline} {prepbin} {filename}".format(initline = initline,
                                            prepbin=ConfigPrepCTM.PrepBin,
                                            filename=ConfigPrepCTM.Ctm_prep_init_filename)

    PIPE = subprocess.PIPE

    #print subprocess.Popen([initline + runline],shell=True, env=my_env, cwd=ConfigPrepCTM.SimulationPath).wait()
    p1 = subprocess.Popen([initline + runline],shell=True, env=my_env, cwd=ConfigPrepCTM.SimulationPath, stdout = PIPE, stdin = PIPE, stderr = PIPE)
    
    out, err = p1.communicate()
    output = out
    error = err
    
    logger.info(output)
    logger.info('errors:')
    logger.error(error)



    logger.info('CTM Prep'.ljust(SimulationConfig.justif-2,'.')+'OK')

    # MEMS
    if EmissionConfig.includeemission_MEMS:
        logger.info('MEMS Branch selection'.ljust(SimulationConfig.justif-2,'.')+'OK')
        MEMSConfig = EmissionConfig.init_MEMS()
        MEMSConfig.Run()
    else :     
        logger.info('MEMS emission '.ljust(SimulationConfig.justif-12,'.')+'NOT INCLUDED')
    return
    
    
#####################################################################################################################################
if __name__ == '__main__':

    import RunConfig as RC
    import logging
    import InitLogging as IL
    import DumpConfigGrid as DCG
    import ResolutionProperties as RP 
    import Configuration as Conf
    loggername='PrepCTM'
    logger=IL.Initialise_logging(loggername)
   
##############################################
# scenarios and options
##############################################
#    MONTH='nov'
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
#    BIO=MONTH
    YEAR=2016
    PrepBin='/mnt/climate/cas/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2'

    YEAR_S=2018
    MONTH_S=4
    DAY_S=3

    #YEAR_S=2018
    #MONTH_S=11
    #DAY_S = 21

    DAY_S_CTM=4
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)
    YEAR_EMISSIONS=YEAR_S
#    MONTH_EMISSIONS=MONTH
    DAY_N=3
    HOUR_N = DAY_N * 24
    Enddate=Startdate + dt.timedelta(days=DAY_N,hours=00)
    YEAR_E=Enddate.year
    YEAR_E=2008
    MONTH_E=Enddate.month
    DAY_E=Enddate.day
    Emissiondate=dt.datetime(YEAR_E,MONTH_E,DAY_E)
    timezone = 'UTC'
    TimestampUTC = dt.datetime.now()
    TimestampUTC = dt.datetime(2019,1,15,16,16,33)
    CCAMFlavour = 'NEWCCAM'
    
    ResolutionProperties = RP.ResolutionProperties_new()
    ResolutionNames=['nsw','gmr','gsyd']
    
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    netcdfpath='../'
#    EmissionGroup=['vpx','vdx','vlx','vpv','gse','whe']
    EmissionModellingPath='/mnt/climate/cas/project/EmissionSources-Forecast/commonfiles'
    EmissionBuildPath='Emissions/'
    #EmissionBuildPath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Emissions/'
    ccamfilepath='./'
    SimulationBasePath='/home/barthelemyx/Projects/AQ-Forecast/Forecast'
    Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2008NH3'
    
    CTMBin='toto'
    CTMBinPath='/mnt/project/CTM/software/August-16'
    
    RunTimeHours=1
    PartitionName='Full'
    RuntimeCPU=40 
    PartitionName='CAS'
    RuntimeCPU=72

    ConfigurationOptions = Conf.VarSetup(logger, Startdate, timezone, TimestampUTC, SimulationBasePath)
    #fire options
    bushfireOEH=True
    bushfireOEH=False
    emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireOEH'
    bushfireCSIRO=True
    #bushfireCSIRO=False
    emissions_rfs_DATA_CSIRO='/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
    burn_start_hour_csiro = 0

    BushfireSource = "Airflow"
    BushfireSource = "Manual"
    Run_HRB = True
    Run_HRB = False
        
    Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-05-24/CSEM_HRB_emission_model--2018-05-24T02-30-48'

    Run_Bushfire = True
    Run_Bushfire = False
    Manual_Bushfire_Dir = None 

    if BushfireSource == "Airflow":
        Manual_HRB_Dir = None 
        Manual_Bushfire_Dir = None 

    ConfigurationOptions.Configure_Bushfire(  
                           BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,     
                           emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, 
                           Manual_HRB_Dir, Manual_Bushfire_Dir
                           )
    #Dust options
    DustSource = "Airflow"
    DustSource = "Manual"
    Run_Dust = True
    #Run_Dust = False
    if DustSource == "Manual":
        DustModel = None
        DustModel = 'inline'
        DustModel = 'Chappell'
        Manual_MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
        Manual_MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/fuchsd/tmp'
        Manual_MEMS_dust_source_dir =  '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/TestFiles'
        Manual_MEMS_dust_source_file = 'ACCESS-R-dust-forecast-ctm_bin_range_area_emissions-2018-11-20-23.nc'
        Manual_MEMS_dust_source_file = 'TestDust.nc4'
        Manual_MEMS_dust_source_file = 'TestDust2.nc4'
        #Manual_MEMS_dust_source_file = 'tmp.nc'
    ConfigurationOptions.Configure_Dust(  
                        DustSource, DustModel, Run_Dust,  
                        Manual_MEMS_dust_source_dir, Manual_MEMS_dust_source_file  
                        )
##############################################

#    SimulationConfig=RC.SimulationConfigClass(logger,ResolutionNames,ResolutionProperties,SCENARIO, \
#            Startdate,EndDate,Emissiondate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,BIO,PrepBin, \
#            templatepath,netcdfpath,EmissionGroup,emissionmodellingpath,emissionbuildpath,\
#            ccamfilepath)
    SimulationConfig = RC.SimulationConfigClass(
                  logger, ResolutionNames, ResolutionProperties, SCENARIO, 
                  Startdate, Enddate, Emissiondate, TimestampUTC, TRANSPORT, 
                  DIFF, SIOA, SOA, CHEM, PrepBin, 
                  templatepath, netcdfpath, EmissionModellingPath, EmissionBuildPath, Emissions_edms_DATA, 
                  SimulationBasePath, ConfigurationOptions, 
                  CTMBinPath, CTMBin, RuntimeCPU, RunTimeHours, PartitionName, CCAMFlavour)
    #Update Bushfire options
    SimulationConfig.Update_Fire_options(ConfigurationOptions)
    #Update Dust options
    SimulationConfig.Update_Dust_options(ConfigurationOptions)

    SimulationConfig.ComputeDirectories()
    SimulationConfig.MakeDir()
    SimulationConfig.includeemission_vpx=False
    SimulationConfig.includeemission_vdx=False 
    SimulationConfig.includeemission_vlx=False
    SimulationConfig.includeemission_vpv=False 
    SimulationConfig.includeemission_gse=False
    SimulationConfig.includeemission_whe=False

    SimulationConfig.buildemission_vpx=False
    SimulationConfig.buildemission_vdx=False
    SimulationConfig.buildemission_vlx=False
    SimulationConfig.buildemission_vpv=False
    SimulationConfig.buildemission_gse=False
    SimulationConfig.buildemission_whe=False
    SimulationConfig.buildemission_pse=False
    
#    for month in range(1,13):
    for month in range(1,1):
        testdate=dt.datetime(YEAR_S,month,DAY_S)
        templatefile_tcbc="tcbc_"+CHEM+'_'+string.lower(testdate.strftime('%b'))+'.template'
        Tcbc_filename="tcbc_"+CHEM+'_'+string.lower(testdate.strftime('%b'))+".in"
        testfile_tcbc="./TestFiles/tcbc_"+CHEM+'_'+string.lower(testdate.strftime('%b'))+".in"
        logger.debug('test BC file= %s testfile= %s template= %s',os.path.join(SimulationPath,Tcbc_filename),testfile_tcbc,templatefile_tcbc)

        TcbcConfig=SimulationConfig.init_TcbcConfig(templatefile_tcbc,Tcbc_filename)
        TcbcConfig.Build_Tcbc()
        logger.debug('-------------------------------')
        logger.debug('Diff Tcbc')
        IL.PrintDiff(os.path.join(SimulationPath,Tcbc_filename), testfile_tcbc,logger)
        logger.debug('-'*80)

    for index in range(3):
        warmstart=False
#        warmstart=True
        RunConfig = RC.RunConfigClass(SimulationConfig, index, warmstart)
        ConfigGridRestart = RunConfig.init_ConfigGridRestartConfig(ConfigurationOptions)

        templatefile_config_grid=ConfigGridRestart.templatefile
        config_grid_filename=ConfigGridRestart.Ctm_config_grid_filename
        #testfile_config_grid="./TestFiles/"+config_grid_filename
        testfile_config_grid="Python-Scripts/TestFiles/{filename}".format(filename = config_grid_filename)
        logger.debug('test Config Grid file= %s testfile= %s template= %s',os.path.join(ConfigGridRestart.SimulationPath,config_grid_filename),testfile_config_grid,templatefile_config_grid)
        ConfigGridRestart.Build_config_grid()
        logger.debug('-------------------------------')
        logger.debug('Diff Config_grid')
        IL.PrintDiff(os.path.join(ConfigGridRestart.SimulationPath,config_grid_filename), testfile_config_grid,logger)
        logger.debug('-'*80)

    
    logger.debug('First half of configuring the prep of the CTM CtmPrepInitConfig')
    ConfigPrepCTM=SimulationConfig.init_CtmPrepInitConfig()
#    logger.debug('Try building the config file of the prep without configuring emissions')
#    try: 
#        DCPI.Build_Ctm_prep_init(ConfigPrepCTM.templatepath,templatefiledef_ConfigPrepCTM,ConfigPrepCTM)
#    except:
#        pass

    logger.debug('Second half of configuring the prep of the CTM CtmPrepInitConfig')
    logger.debug('Fist init the head emission config object')
    EmissionConfig=SimulationConfig.init_Emission()
#    EmissionConfig.includeemission_vdx=False 
#    EmissionConfig.includeemission_vpv=False 

    logger.debug('then the child objects')
    EmissionPetExhConfig = EmissionConfig.init_EmissionPetExhConfigClass()
    EmissionDieExhConfig = EmissionConfig.init_EmissionDieExhConfigClass()
    EmissionLpgExhConfig = EmissionConfig.init_EmissionLpgExhConfigClass()
    EmissionPetEvpConfig = EmissionConfig.init_EmissionPetEvpConfigClass()
    EmissionComdomesticConfig = EmissionConfig.init_EmissionComdomesticConfigClass()
    EmissionWoodheatersConfig = EmissionConfig.init_EmissionWoodHeatersConfigClass()
    EmissionPSEConfig = EmissionConfig.init_EmissionPSEConfigClass()

    logger.debug('update the master emission object')
    EmissionPetExhConfig.EmissionConfig_update(EmissionConfig)
    EmissionDieExhConfig.EmissionConfig_update(EmissionConfig)
    EmissionLpgExhConfig.EmissionConfig_update(EmissionConfig)
    EmissionPetEvpConfig.EmissionConfig_update(EmissionConfig)
    EmissionComdomesticConfig.EmissionConfig_update(EmissionConfig)
    EmissionWoodheatersConfig.EmissionConfig_update(EmissionConfig)
    EmissionPSEConfig.EmissionConfig_update(EmissionConfig)

    logger.debug('Configuring the second half of CTM prep CtmPrepInitConfig and updateing Simulation object')
    EmissionConfig.Configure_CTM_prep_init_and_Update_Simulation(ConfigPrepCTM,SimulationConfig)
    logger.debug('Check if SimulationConfig got the updated emission groups = %s',SimulationConfig.EmissionGroup)
    
    
    ConfigPrepCTM_filename=ConfigPrepCTM.Ctm_prep_init_filename
    templatefile_ConfigPrepCTM=ConfigPrepCTM.templatefile
    #testfile_ConfigPrepCTM="./TestFiles/"+ConfigPrepCTM_filename
    testfile_ConfigPrepCTM = "Python-Scripts/TestFiles/{filename}".format(filename = ConfigPrepCTM_filename)
    logger.debug('test ConfigPrepCTM file= %s testfile= %s template= %s',os.path.join(ConfigPrepCTM.SimulationPath,ConfigPrepCTM_filename),testfile_ConfigPrepCTM,templatefile_ConfigPrepCTM)
    ConfigPrepCTM.Build_Ctm_prep_init()
    logger.debug('-------------------------------')
    logger.debug('Diff ConfigPrepCTM')
    IL.PrintDiff(os.path.join(ConfigPrepCTM.SimulationPath,ConfigPrepCTM_filename), testfile_ConfigPrepCTM,logger)
    logger.debug('-'*80)

    for index in range(3):
        warmstart=False
#        warmstart=True
        RunConfig=RC.RunConfigClass(SimulationConfig,index,warmstart)
        MetConfig=RunConfig.init_MetConfig()
        meteo_config_filename=MetConfig.meteo_config_filename
        templatefile_met_config=MetConfig.templatefile
        #testfile_met_config="./TestFiles/"+meteo_config_filename
        testfile_met_config = "Python-Scripts/TestFiles/{filename}".format(filename = meteo_config_filename)
        logger.debug('test GenMet file= %s testfile= %s template= %s',os.path.join(MetConfig.SimulationPath,meteo_config_filename),testfile_met_config,templatefile_met_config)
        MetConfig.Build_Meteo_config_File()
        logger.debug('-------------------------------')
        logger.debug('Diff GenMet')
        IL.PrintDiff(os.path.join(MetConfig.SimulationPath,meteo_config_filename), testfile_met_config,logger)
        logger.debug('-'*80)

    logger.debug('-'*80)
    logger.debug('Testing Symlinking')
   
    logger.debug('list all files to symlink = %s',EmissionFilesList(SimulationConfig))
    logger.debug('Unlinking')
    UnlinkEmissionFiles(SimulationConfig)
    logger.debug('symlinking to')
    SymlinkEmissionFiles(SimulationConfig)          
    logger.debug('-'*80)
    logger.debug('Unlinking')
    UnlinkEmissionFiles(SimulationConfig)


    logger.debug('*'*80)
    logger.debug('*'*80)

    CTM_prep(SimulationConfig)

