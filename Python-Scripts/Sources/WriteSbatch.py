import os
import os.path
import random
import datetime as dt
import jinja2
import JinjaUtils as JJU
import subprocess

#####################################################################################################################################
class SBatchConfigClass(object):
    """ Configure SBatchConfig Class which defines the properties of one SLURM job submission for one grid run .
    """
    def __init__(self, logger, justif, 
                 BinName, BinPath, bininitline,
                 TimeHours, CpuPerTask, SimulationPath, gen_meteo_config_filename, 
                 templatepath, templatefile_Sbatch, templatefile_Script_Submit, 
                 SubmitScriptName, configfilenumber, ResolutionNames, **kwargs):
        RunName = kwargs.get('RunName', None)
        TimeMinutes = kwargs.get('TimeMinutes', 0)
        Outputname = kwargs.get('Outputname', 'Output-'+str(configfilenumber+1)+'.%j')
        Outputerror = kwargs.get('Outputerror', 'Error-'+str(configfilenumber+1)+'.%j')
        NumberofNodes = kwargs.get('Nodes', 1)
        Taskspernode = kwargs.get('Taskspernode', 1)
        PartitionName = kwargs.get('PartitionName', 'CAS')
        if not RunName:
            listrun=['Funkadelic','Kool','The_Gang','Earth','Wind','Fire','James','Brown','FamilySton','P-Funk','Parliament']
            listrun=['*-O-*','__O-*','*\O-*','*-O  ','  O-*','*\O  ','__O/*','*\O/*','*\O-*','*\O__','__O__']
            RunName=random.choice(listrun)
        
        self.logger=logger
        self.justif = justif
        self.RunName=RunName
        self.RunTime=dt.time(TimeHours,TimeMinutes,0)
        self.MemPerCpu=16384
        self.CpuPerTask=CpuPerTask
        self.OutputName=Outputname
        self.OutputError=Outputerror
        self.NumberofNodes=NumberofNodes
        self.Taskspernode=Taskspernode
        self.BinPath=BinPath
        self.BinName=BinName
        self.bininitline = bininitline
        self.gen_meteo_config_filename=gen_meteo_config_filename
        self.configfilenumber=configfilenumber
        self.templatepath=templatepath
        self.SimulationPath=SimulationPath
        self.ResolutionNames=ResolutionNames
        self.PartitionName=PartitionName

        self.templatefile_Sbatch=templatefile_Sbatch
        self.templatefile_Script_Submit=templatefile_Script_Submit
        self.SbatchFileName='CTM-submit-'+self.ResolutionNames[self.configfilenumber]+'.sub'
        self.SubmitScriptName=SubmitScriptName
        return
        
    def Build_SBatch_Threaded(self,):
        """ This function write the SLURM submit file for one run.
        """
        logger=self.logger
        logger.info("Generating the SLURM submit file for the grid number %i",self.configfilenumber)
        filename=os.path.join(self.SimulationPath,self.SbatchFileName)
        File= open(filename,'w')

        template=JJU.LoadTemplate(self.templatepath,self.templatefile_Sbatch)

        config=template.render({'SBatchConfig':self })
        File.write(config)
        File.close()
        SbatchFilename=filename
        logger.debug("SLURM submit file for grid number %i generated",self.configfilenumber)
        return SbatchFilename

    def WriteScriptSubmit(self,ListofSbatchFilename):
        """ This function write the SLURM submit exec file to run a whole simulation .
        """
        logger=self.logger
        logger.info("Generating the SLURM submit script")
        if ListofSbatchFilename != [] :
            filename = os.path.join(self.SimulationPath,self.SubmitScriptName)
            File= open(filename,'w')

            template=JJU.LoadTemplate(self.templatepath,self.templatefile_Script_Submit)

            config=template.render({'ScriptNames':ListofSbatchFilename })
            File.write(config)
            File.close()
            os.chmod(filename,0755)
            logger.debug("SLURM submit file for the whole run")
        else:
         logger.warn("Sorry, can't do it!! First generate submit file for invividual run (see Build_SBatch_Threaded method) ")
       
        return 
    def RunScriptSubmit(self):
        RunDir=self.SimulationPath
        my_env = os.environ.copy()
        #init = "source /usr/share/modules/init/bash |  module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp |"
        
        filename = os.path.join(self.SimulationPath,self.SubmitScriptName)
        run = "{filename}".format(filename = filename)
        print subprocess.Popen([self.bininitline + run ], shell=True, env=my_env, cwd=RunDir).wait()
        return
   
#####################################################################################################################################
if __name__ == '__main__':
    import logging
    import InitLogging as IL
    import ResolutionProperties as RP 
    loggername='SBatch'
    justif = 100
    logger=IL.Initialise_logging(loggername)
    filename='CTM-submit.sub'
    BinName='toto'
    BinPath='pathtoto'
    TimeHours=4
    CpuPerTask=15
    gen_meteo_config_filename='metfile'
    configfilenumber=1
    SimulationPath='.'
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatefile_Sbatch='SBatch-threaded.template'
    ResolutionNames=["nsw","gmr","gsyd"]
    SubmitScriptName='Submit'
    templatefile_Script_Submit='Script-Submit.template'
    bininitline = "source /home/barthelemyx/modulefiles/init.bash | ulimit -s 300000 |"
    
    ListofSbatchFilename=[]
    logger.debug("Configuring SBatch object")
    SBatchConfig = SBatchConfigClass(logger, justif, 
                                      BinName, BinPath, bininitline,
                                      TimeHours, CpuPerTask, SimulationPath, gen_meteo_config_filename, 
                                      templatepath, templatefile_Sbatch, templatefile_Script_Submit,
                                      SubmitScriptName, configfilenumber, ResolutionNames,
                                      TimeMinutes=15, Nodes=17, Taskspernode=12000)
    
    logger.debug("list of submit script %s",ListofSbatchFilename)
    logger.debug("Try to built the whole submit")   
    SBatchConfig.WriteScriptSubmit(ListofSbatchFilename)
      
    logger.debug("Built indidual submit for a grid")   
    SbatchFilename=SBatchConfig.Build_SBatch_Threaded()
    ListofSbatchFilename.append(SbatchFilename)
    logger.debug("list of submit script %s",ListofSbatchFilename)
    
    logger.debug("Configuring SBatch object, ... again")
    configfilenumber=2
    SBatchConfig = SBatchConfigClass(logger,BinName,BinPath,TimeHours,CpuPerTask,SimulationPath,gen_meteo_config_filename, \
                  templatepath,templatefile_Sbatch,templatefile_Script_Submit,SubmitScriptName,configfilenumber,ResolutionNames,TimeMinutes=15,Nodes=17,Taskspernode=12000)
    logger.debug("Built indidual submit for a grid")   
    SbatchFilename=SBatchConfig.Build_SBatch_Threaded()
    ListofSbatchFilename.append(SbatchFilename)
    logger.debug("list of submit script %s",ListofSbatchFilename)

    logger.debug("Try to built the whole submit, .... again")   
    SBatchConfig.WriteScriptSubmit(ListofSbatchFilename)
   

