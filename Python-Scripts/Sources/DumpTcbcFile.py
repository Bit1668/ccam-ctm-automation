import jinja2
import JinjaUtils as JJU
import os

###########################################################################################
class TcbcConfigClass(object):
    ''' Class that configure the CTM boundary conditions TcbcConfig 
    '''
    def __init__(self,logger,SimulationPath,templatepath,templatefile,filename):
        self.logger=logger
        self.SimulationPath=SimulationPath 
        self.templatepath=templatepath
        self.templatefile=templatefile
        self.filename=filename

    def __getitem__(self,attr):
        return self.__dict__[attr]

    def Build_Tcbc(self):
        logger=self.logger
        logger.info("Generating the Configuration file for the Boundary Conditions")
        File= open(os.path.join(self.SimulationPath,self.filename),'w')
        template=JJU.LoadTemplate(self.templatepath,self.templatefile)
        tcbcfile=template.render({'TcbcConfig':self })
        File.write(tcbcfile)
        File.close()
        logger.debug("Configuration file for the Boundary Conditions generated")
        return 
###########################################################################################
        
