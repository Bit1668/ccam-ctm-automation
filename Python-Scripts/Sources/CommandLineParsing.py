"""
.. module:: CommandLineParsing
   :platform: Unix
   :synopsis: Parser of the options passed to the main CTM script.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import argparse
import datetime as dt

def CommandLineParsing(logger, ):
    """
    This function parse the argument line send to the main CTM script.
    it helps to define the ensemble of runs.
    if no argument, then it's a manual run

    Parameters
    ---------- 
        logger : logging.logger
    Returns
    -------
        Startdate : datetime
            Date to run the forecast in AEDT. The date should be naive (no tz).
        timezone : str
            String reprenseting the timezone of the startdate. It will be used later to localise the startdate. 
        Run_HRB : bool
            Flag triggering the possibility to run the detection of HRB.
        Run_Bushfire : bool
            Flag triggering the possibility to run the detection of Wildfire.
    """
    justif = 102
    logger.info(''.ljust(justif,'-'))
    logger.info('CTM argument line parsing'.center(justif,'.'))
    logger.info(''.ljust(justif,'-'))
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--HRB", help="include HRB", action="store_true")
    parser.add_argument("--Wildfire", help="include Bushfire", action="store_true")
    parser.add_argument("--dateAEDT", help="date of the forecast to run")
    parser.add_argument("--Dust", help="include dust to run")
    
    args = parser.parse_args()
    if args.dateAEDT:
        print args.dateAEDT
        Startdate = dt.datetime.strptime(args.dateAEDT, "%Y%m%d%H%M")
        timezone = "AEDT"
        logger.info("Date has been detected on the argline, CTM will be configure for {date}".format(date=Startdate.strftime("%Y%m%d_%H%M")))
    else:
        Startdate = None
        timezone = None
        logger.info("")
    if args.HRB:
        Run_HRB = True
        logger.info("HRB option detected".ljust(justif,'-'))
    else:
        Run_HRB = False
        logger.info("No HRB".ljust(justif,'-'))
            
    if args.Wildfire:
        Run_Bushfire = True
        logger.info("Wildfire option detected".ljust(justif,'-'))
    else:
        Run_Bushfire = False
        logger.info("No Wildfire".ljust(justif,'-'))
    
    if  args.Dust:
        Run_Dust = True
        DustModel = args.Dust
        logger.info("Dust Flavour = {ddust}  detected".format(ddust = args.Dust).ljust(justif,'-'))
     
    else:
        Run_Dust = True
        DustModel = 'inline'
        logger.info("No Dust option detected, Running default inline Dust emissions".ljust(justif,'-'))
    
    return Startdate, timezone, Run_HRB, Run_Bushfire, Run_Dust, DustModel

