"""
.. module:: ResolutionProperties
    :platform: Unix
    :synopsis: dictionnary of the grid configurations.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
   
"""
def ResolutionProperties():
    '''
    Dictionary to define the main properties of the CTM domains.
    
    Returns
    -------
    ResolutionsCTM : dict
        Dictionary to define the main properties of the CTM domains.
    '''
    ResolutionsCTM = {
    #Key, Name, Resolution (km) Resolution (m); Master/inline grids -SW corner: ylat, xlong;  horiz. dimension: nx,ny   horiz. resol.: dx,dy (deg), CTM Nested Master grid, Dump files , flags for urban stability
    'nsw'               : ('nsw', '60km',60000, -57.86, 127.18, 80, 80, 0.60, 0.60, 1,False,'F F'),
    'gmr'               : ('gmr', '8km' ,8000 , -37.06, 147.98, 80, 80, 0.08, 0.08, 1,False,'F F'),
    'gsyd'              : ('gsyd','1km' ,1000 , -34.26, 150.78, 80, 80, 0.01, 0.01, 0,True,'T F'),
    'csyd'              : ('csyd','1km' ,1000 , -34.26, 150.78, 80, 80, 0.01, 0.01, 0,True,'T F'),
    }
    return ResolutionsCTM

def ResolutionProperties_new():
    '''
    Dictionary to define the main properties of the CTM domains.
    
    Returns
    -------
    ResolutionsCTM : dict
        Dictionary to define the main properties of the CTM domains.
    '''
    ResolutionsCTM = {
    #Key, Name, Resolution (km) Resolution (m); Master/inline grids -SW corner: ylat, xlong;  horiz. dimension: nx,ny   horiz. resol.: dx,dy (deg), CTM Nested Master grid, Dump files , flags for urban stability
    'nsw'  : {
          'name': 'nsw',
          'resolution_km': '60km',
          'resolution_m': 60000,
          'SW_corner_lat': -57.86,
          'SW_corner_lon':127.18,
          'nlon': 80,
          'nlat': 80,
          'dx_deg': 0.60,
          'dy_deg':0.60,
          'n_nested_master_grid': 1,
          'dump_extra_var': False,
          'urban_stab': 'F F',
          'include_dust_when_inline_dust': True
          },
    'gmr'  : {
          'name': 'gmr',
          'resolution_km': '8km',
          'resolution_m': 8000,
          'SW_corner_lat': -37.06,
          'SW_corner_lon': 147.98,
          'nlon': 80,
          'nlat': 80,
          'dx_deg': 0.08,
          'dy_deg': 0.08,
          'n_nested_master_grid': 1,
          'dump_extra_var': False,
          'urban_stab': 'F F',
          'include_dust_when_inline_dust': False
          },
    'gsyd' : {
          'name': 'gsyd',
          'resolution_km': '1km',
          'resolution_m': 1000,
          'SW_corner_lat': -34.26,
          'SW_corner_lon': 150.78,
          'nlon': 80,
          'nlat': 80,
          'dx_deg': 0.01,
          'dy_deg': 0.01,
          'n_nested_master_grid': 0,
          'dump_extra_var': True,
          'urban_stab': 'T F',
          'include_dust_when_inline_dust': False
          },
    'csyd' : {
          'name': 'csyd',
          'resolution_km': '1km',
          'resolution_m': 1000,
          'SW_corner_lat': -34.26,
          'SW_corner_lon': 150.78,
          'nlon': 80,
          'nlat': 80,
          'dx_deg': 0.01,
          'dy_deg': 0.01,
          'n_nested_master_grid': 0,
          'dump_extra_var': True,
          'urban_stab': 'T F',
          'include_dust_when_inline_dust': False
          },
    'psyd' : {
          'name': 'psyd',
          'resolution_km': '1km',
          'resolution_m': 1000,
          'SW_corner_lat': -34.836,
          'SW_corner_lon': 150.0791,
          'nlon': 180,
          'nlat': 180,
          'dx_deg': 0.01,
          'dy_deg': 0.01,
          'n_nested_master_grid': 0,
          'dump_extra_var': True,
          'urban_stab': 'T F',
          'include_dust_when_inline_dust': False
          },
    }
    return ResolutionsCTM

def ForecastDomain():
    OldForecastDomain = {
    'nsw'  : {'LL': {'lat': -57.86, 'lon': 127.18},  'UR': {'lat':-9.86,  'lon': 175.18}}, 
    'gmr'  : {'LL': {'lat': -37.06, 'lon': 147.98},  'UR': {'lat':-30.66, 'lon': 154.38}} ,
    'gsyd' : {'LL': {'lat': -34.26, 'lon': 150.78},  'UR': {'lat':-33.46, 'lon': 151.58}}, 
    'esyd' : {'LL': {'lat': -34.358289, 'lon':  150.361332},  'UR': {'lat': -33.388939, 'lon':  151.485216}},
    'psyd' : {'LL': {'lat': -34.836, 'lon':  150.0791},  'UR': {'lat': -33.006, 'lon':  151.9091}}
    }
    CentreOfTheWorld = {
    "Balmain" : {'lat':-33.86,  'lon':151.18 },
    "Building2":{'lat':-33.884, 'lon':151.042},
    "epaSyd":{'lat':-33.873614, 'lon':150.923274},
    }
        
    for domain in OldForecastDomain.keys():
        clat = 0.5 * ( OldForecastDomain[domain]['LL']['lat'] + OldForecastDomain[domain]['UR']['lat'])
        clon = 0.5 * ( OldForecastDomain[domain]['LL']['lon'] + OldForecastDomain[domain]['UR']['lon'])
        print domain, " centre = N", clat, " E",clon
    
    
    Newclat = CentreOfTheWorld['Building2']['lat']
    Newclon = CentreOfTheWorld['Building2']['lon']
    Newclat = CentreOfTheWorld['epaSyd']['lat']
    Newclon = CentreOfTheWorld['epaSyd']['lon']
    
    
    
    
    print 'New centre = N', Newclat, " E", Newclon
    difflat = Newclat - clat
    difflon = Newclon - clon
    
    print "domain shift N", difflat, " E",difflon
    
    NewForecastDomain = {
    'nsw'  : {'LL': {'lat': -57.86 + difflat, 'lon': 127.18 + difflon},  'UR': {'lat':-9.86 + difflat,  'lon': 175.18 + difflon}}, 
    'gmr'  : {'LL': {'lat': -37.06 + difflat, 'lon': 147.98 + difflon},  'UR': {'lat':-30.66 + difflat, 'lon': 154.38 + difflon}} ,
    'gsyd' : {'LL': {'lat': -34.26 + difflat, 'lon': 150.78 + difflon},  'UR': {'lat':-33.46 + difflat, 'lon': 151.58 + difflon}} 
    }
    
    domain = 'esyd'
    res = 0.01
    
    maxlon = OldForecastDomain[domain]['UR']['lon']
    minlon = OldForecastDomain[domain]['LL']['lon']  
    maxlat = OldForecastDomain[domain]['UR']['lat']
    minlat = OldForecastDomain[domain]['LL']['lat']   
    NY = ((maxlat-minlat) / res) +1
    NX = ((maxlon-minlon) / res) +1
     
    print "resolution", NX, NY
    
    return NewForecastDomain
if __name__ == '__main__':
    RP=ResolutionProperties()
    print RP.keys()
    Lat = -33.884
    lon = 151.042
    NewForecastDomain = ForecastDomain()
    print NewForecastDomain
150.0791 -34.836

