"""
.. module:: ConfigPrepCTM
    :platform: Unix
    :synopsis: Prepare one of the 2 templates necessary to run the prep of a CTM simulation.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
   
"""
import string
import datetime as dt
import os
import jinja2
import JinjaUtils as JJU
###########################################################################################
class CtmPrepInitConfigClass(object):
    ''' 
    Class that configure the prep of the CTM
    config is done in 2 times:
    
    * 1st part derives from the simulation class. Simulation class has a configure method
    * 2nd part is about emission. at initialiation this class is not totally configured, 
        and it needs the Configure_CTM_prep_init method of the Emission class to finish to set all the vars and paths
    '''
#    def __init__(self,logger,ResolutionNames,SCENARIO,Startdate,Enddate,Emissiondate,ResolutionProperties,SimulationHours, \
#                       netcdfpath,emissionmodellingpath,EmissionGroup,EmissionBuildPath,templatepath,templatefile):
    def __init__(self,logger,ResolutionNames,SCENARIO,Startdate,Enddate,Emissiondate,ResolutionProperties,SimulationHours, \
                       SimulationPath,netcdfpath,emissionmodellingpath,EmissionBuildPath_static,EmissionBuildPath_dynamic,
                       templatepath,templatefile,PrepBin,justif):
        self.logger = logger
        self.justif = justif
        self.ResolutionNames = ResolutionNames
        self.SCENARIO = SCENARIO
        self.Startdate = Startdate
        self.Enddate = Enddate
        self.Emissiondate = Emissiondate
        self.ResolutionProperties = ResolutionProperties
        self.SimulationHours = SimulationHours
        self.SimulationPath = os.path.abspath(SimulationPath)
        
        self.netcdfpath = netcdfpath
        self.emissionmodellingpath = emissionmodellingpath
#        self.EmissionGroup=EmissionGroup
        self.templatepath = templatepath
        self.templatefile = templatefile

        self.Ctm_prep_init_filename = 'ctm_prep_init.inp'
        self.StartMonth = string.lower(self.Startdate.strftime('%b'))
        self.SimulationDays = (self.Enddate-self.Startdate).days
        self.EmissionMonth = string.lower(self.Emissiondate.strftime('%b'))
        self.EmissionYear = self.Emissiondate.strftime('%Y')
        self.EmissionYr = self.Emissiondate.strftime('%y')
        
        self.emissionconfigured = False
        self.EmissionBuildPath_static = EmissionBuildPath_static
        self.EmissionBuildPath_dynamic = EmissionBuildPath_dynamic

#       Include or not emission groups.
        self.includeemission_vpx = 'not configured'
        self.includeemission_vdx = 'not configured'
        self.includeemission_vlx = 'not configured'
        self.includeemission_vpv = 'not configured'
        self.includeemission_gse = 'not configured'
        self.includeemission_whe = 'not configured'
        self.includeemission_pse = 'not configured'

#       Build or not emission groups.
        self.buildemission_vpx = 'not configured'
        self.buildemission_vdx = 'not configured'
        self.buildemission_vlx = 'not configured'
        self.buildemission_vpv = 'not configured'
        self.buildemission_gse = 'not configured'
        self.buildemission_whe = 'not configured'
        self.buildemission_pse = 'not configured'
        
        self.emissiongroupfilename = []
        self.EmissionGroup = []

        self.PrepBin = PrepBin
        return

###########################################################################################
    def Build_Ctm_prep_init(self):
        """ 
        This function write the config file for the CTM prep: ctm_prep_init.inp
        """
        
        self.logger.info("Generating the Configuration file for the CTM prep".ljust(self.justif,'.'))
        if self.emissionconfigured:
            self.Compute_SW_cell_centre_from_corner()
            filename=os.path.join(self.SimulationPath,self.Ctm_prep_init_filename)
            File= open(filename,'w')

            template=JJU.LoadTemplate(self.templatepath,self.templatefile)
            template.globals['int']=int
            template.globals['float']=float
        
            config=template.render({'CtmPrepInitConfig':self })
            File.write(config)
            File.close()
            self.logger.info('CTM prep configuration file '.ljust(self.justif-7,'.') + 'WRITTEN')
        
        else:
            self.logger.error("Emission have NOT been configured")
            raise Exception("Emission have NOT been configured",'')
            
        return 

###########################################################################################
    def Compute_SW_cell_centre_from_corner(self):
        '''
        Compute the SW cell centre from the SW cell corner, taken from the dictionnary 
        Resolutionproperties.
        It builds a dictionnary of its own
        '''
        self.cellcentre = {}
        for domain in self.ResolutionProperties.keys():
            SWLonCorner = self.ResolutionProperties[domain]['SW_corner_lon']
            SWLatCorner = self.ResolutionProperties[domain]['SW_corner_lat']
            gridresLon = self.ResolutionProperties[domain]['dx_deg']
            gridresLat = self.ResolutionProperties[domain]['dy_deg']
            
            SWLonCorner = SWLonCorner + 0.5 * gridresLon
            SWLatCorner = SWLatCorner + 0.5 * gridresLat
            
            self.cellcentre[domain] = (SWLonCorner, SWLatCorner)
            self.logger.info('SW cell centre computation'.ljust(self.justif-2,'.') + 'OK')
        return    
###########################################################################################

