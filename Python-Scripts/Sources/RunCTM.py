"""
.. module:: RunCTM
   :platform: Unix
   :synopsis: Main script to configure the CTM run.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import os
import os.path
import random
import datetime as dt
import glob
import shutil as shu
#import GenerateMeteoConfigFile as GMCF
import WriteSbatch as WSB
#import ResolutionProperties as RP
import RunConfig as RC
#####################################################################################################################################
	
def PostRunOneResolution(index,year,resolution,NumberOfDaysForecast,headertracefile,scenario):
    os.rename('CTM_*.lis1','CTM_'+scenario+'_grid_'+headertracefile+'lis')
    os.rename('CTM_timing.txt1','CTM_timing_grid_'+headertracefile+'.txt')
    os.rename('CTM_debug.txt1','TM_debug_grid_'+headertracefile+'.txt')
    CleaningSymLinkingOriginalCCAMFilesPreRun(year,resolution,NumberOfDaysForecast)
    return

#####################################################################################################################################

def ArchiveResultsAndCleaning(YEAR,resolutions,NumberOfDaysForecast):

    shu.make_archive('gridfiles_nsw.tar.bz2', 'bztar', './')
    
#cleaning CCAM symlinks
    for index,res in resolutions:
        CleaningSymLinkingOriginalCCAMFilesPreRun(YEAR,res,NumberOfDaysForecast)
    return
#####################################################################################################################################
    
def RunCTM(SimulationConfig, ConfigurationOptions):

    logger=SimulationConfig.logger

    logger.info(''.ljust(SimulationConfig.justif,'-'))
    logger.info('CTM Run script'.center(SimulationConfig.justif,'|'))
    logger.info(''.ljust(SimulationConfig.justif,'-'))

    logger.info('Initialising individual Grid Runs'.ljust(SimulationConfig.justif,'.'))
    ListofSbatchFilename=[]
    for index,res in enumerate(SimulationConfig.ResolutionNames):
    
#        warmstart=False
#        warmstart=True
        warmstart=SimulationConfig.WarmStart
        
        RunConfig = SimulationConfig.init_Run(index,warmstart)

        logger.info('Finding last run for an eventual Restart'.ljust(SimulationConfig.justif,'.'))
        RunConfig.FindLastRunDir()

    
        logger.info('Building individual domain grid configuration file'.ljust(SimulationConfig.justif,'.'))
        ConfigGridRestart = RunConfig.init_ConfigGridRestartConfig(ConfigurationOptions)
        ConfigGridRestart.Build_config_grid()
#       # setting the config grid filename
#        shu.copy2('ctm_config_grid'+str(index)+'.txt','ctm_config_'+str(index)+'.txt')

        #logger.info('Building individual domain grid Met file'.ljust(SimulationConfig.justif,'.'))
        #MetConfig=RunConfig.init_MetConfig()
        #MetConfig.Build_Meteo_config_File()

        logger.info("Building individual domain SLURM run scripts".ljust(SimulationConfig.justif,'.'))
        SBatchConfig = RunConfig.init_SBatchConfig(RunConfig.meteo_config_filename)
        SbatchFilename=SBatchConfig.Build_SBatch_Threaded()
        ListofSbatchFilename.append(SbatchFilename)
        
        #logger.info("Linking Met Files".ljust(SimulationConfig.justif,'.'))
        #CcamFilesHouseKeeping=RunConfig.init_CcamFilesHouseKeeping()
        #CcamFilesHouseKeeping.UnLinkingOriginalCCAMFiles()
        #CcamFilesHouseKeeping.CreateDestinationDir()
        #CcamFilesHouseKeeping.MovingOriginalCCAMFiles()
        #CcamFilesHouseKeeping.SymLinkingOriginalCCAMFiles()
        
        
    logger.info("Writing the whole SLURM submit file".ljust(SimulationConfig.justif,'.'))
    SBatchConfig.WriteScriptSubmit(ListofSbatchFilename)
    
    
    return  SBatchConfig  
#####################################################################################################################################
#####################################################################################################################################
if __name__ == '__main__':

    import RunConfig as RC
    import logging
    import InitLogging as IL
    import DumpConfigGrid as DCG
    import ResolutionProperties as RP 
    import Configuration as Conf
    loggername='PrepCTM'
    logger=IL.Initialise_logging(loggername)
   
##############################################
# scenarios and options
##############################################
#    MONTH='nov'
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
#    BIO=MONTH
    YEAR=2016
    PrepBin='/mnt/climate/cas/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2'

    YEAR_S=2018
    MONTH_S=4
    DAY_S=3

    #YEAR_S=2018
    #MONTH_S=11
    #DAY_S = 21

    DAY_S_CTM=4
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)
    YEAR_EMISSIONS=YEAR_S
#    MONTH_EMISSIONS=MONTH
    DAY_N=3
    HOUR_N = DAY_N * 24
    Enddate=Startdate + dt.timedelta(days=DAY_N,hours=00)
    YEAR_E=Enddate.year
    YEAR_E=2008
    MONTH_E=Enddate.month
    DAY_E=Enddate.day
    Emissiondate=dt.datetime(YEAR_E,MONTH_E,DAY_E)
    timezone = 'UTC'
    TimestampUTC = dt.datetime.now()
    TimestampUTC = dt.datetime(2019,1,15,16,16,33)
    CCAMFlavour = 'NEWCCAM'
    
    ResolutionProperties = RP.ResolutionProperties_new()
    ResolutionNames=['nsw','gmr','gsyd']
    
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'

    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2017'
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2018'
    #netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2019'
    
#    EmissionGroup=['vpx','vdx','vlx','vpv','gse','whe']
    EmissionModellingPath = '/mnt/climate/cas/project/EmissionSources-Forecast/commonfiles'
    EmissionBuildPath = 'Emissions/'
    #EmissionBuildPath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Emissions/'
    ccamfilepath='./'
    SimulationBasePath = '/home/barthelemyx/Projects/AQ-Forecast/Forecast'
    Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2008NH3'
    
    CTMBin = 'toto'
    CTMBin = 'ctm-ccam'
    CTMBinPath = '/mnt/project/CTM/software/August-16'
    CTMBinPath = '/mnt/climate/cas/project/CTM/software/August-16/ctm/ccam'
    
    RunTimeHours=1
    PartitionName='Full'
    RuntimeCPU=40 
    PartitionName='CAS'
    RuntimeCPU=72

    ConfigurationOptions = Conf.VarSetup(logger, Startdate, timezone, TimestampUTC, SimulationBasePath)
    #fire options
    bushfireOEH=True
    bushfireOEH=False
    emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireOEH'
    bushfireCSIRO=True
    #bushfireCSIRO=False
    emissions_rfs_DATA_CSIRO='/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
    burn_start_hour_csiro = 0

    BushfireSource = "Airflow"
    BushfireSource = "Manual"
    Run_HRB = True
    Run_HRB = False
        
    Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-05-24/CSEM_HRB_emission_model--2018-05-24T02-30-48'

    Run_Bushfire = True
    Run_Bushfire = False
    Manual_Bushfire_Dir = None 

    if BushfireSource == "Airflow":
        Manual_HRB_Dir = None 
        Manual_Bushfire_Dir = None 

    ConfigurationOptions.Configure_Bushfire(  
                           BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,     
                           emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, 
                           Manual_HRB_Dir, Manual_Bushfire_Dir
                           )
    #Dust options
    DustSource = "Airflow"
    DustSource = "Manual"
    Run_Dust = True
    #Run_Dust = False
    if DustSource == "Manual":
        DustModel = None
        DustModel = 'inline'
        DustModel = 'Chappell'
        Manual_MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
        Manual_MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/fuchsd/tmp'
        Manual_MEMS_dust_source_dir =  '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/TestFiles'
        Manual_MEMS_dust_source_file = 'ACCESS-R-dust-forecast-ctm_bin_range_area_emissions-2018-11-20-23.nc'
        Manual_MEMS_dust_source_file = 'TestDust.nc4'
        Manual_MEMS_dust_source_file = 'TestDust2.nc4'
        #Manual_MEMS_dust_source_file = 'tmp.nc'
    ConfigurationOptions.Configure_Dust(  
                        DustSource, DustModel, Run_Dust,  
                        Manual_MEMS_dust_source_dir, Manual_MEMS_dust_source_file  
                        )
##############################################

#    SimulationConfig=RC.SimulationConfigClass(logger,ResolutionNames,ResolutionProperties,SCENARIO, \
#            Startdate,EndDate,Emissiondate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,BIO,PrepBin, \
#            templatepath,netcdfpath,EmissionGroup,emissionmodellingpath,emissionbuildpath,\
#            ccamfilepath)
    SimulationConfig = RC.SimulationConfigClass(
                  logger, ResolutionNames, ResolutionProperties, SCENARIO, 
                  Startdate, Enddate, Emissiondate, TimestampUTC, TRANSPORT, 
                  DIFF, SIOA, SOA, CHEM, PrepBin, 
                  templatepath, netcdfpath, EmissionModellingPath, EmissionBuildPath, Emissions_edms_DATA, 
                  SimulationBasePath, ConfigurationOptions, 
                  CTMBinPath, CTMBin, RuntimeCPU, RunTimeHours, PartitionName, CCAMFlavour)
    #Update Bushfire options
    SimulationConfig.Update_Fire_options(ConfigurationOptions)
    #Update Dust options
    SimulationConfig.Update_Dust_options(ConfigurationOptions)

    SimulationConfig.ComputeDirectories()
    SimulationConfig.MakeDir()
    SimulationConfig.includeemission_vpx=False
    SimulationConfig.includeemission_vdx=False 
    SimulationConfig.includeemission_vlx=False
    SimulationConfig.includeemission_vpv=False 
    SimulationConfig.includeemission_gse=False
    SimulationConfig.includeemission_whe=False

    SimulationConfig.buildemission_vpx=False
    SimulationConfig.buildemission_vdx=False
    SimulationConfig.buildemission_vlx=False
    SimulationConfig.buildemission_vpv=False
    SimulationConfig.buildemission_gse=False
    SimulationConfig.buildemission_whe=False
    SimulationConfig.buildemission_pse=False
    

    RunCTM(SimulationConfig, ConfigurationOptions)



