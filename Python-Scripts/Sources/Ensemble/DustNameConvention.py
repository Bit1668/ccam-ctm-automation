"""
.. module:: DustNameConvention
   :platform: Unix
   :synopsis: Everything needed to build and run the CSIRO Chemical Transport Model.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
def DustNameConvention(logger, DustModel, Run_Dust, string_inline_dust):
    '''
    function to compute the DUST_ID name to implement in the ensemble directory name
    
    Parameters
    ----------
    logger : logging.logger
        instance of a logger to output messages.
    DustModel : str
        which dust model to run
    Run_Dust : bool
        (not) including dust emissions
    string_inline_dust : str
        binary string to represent which domain run inline dust when this option is actually selected  
         
    Returns
    -------
    Dust_ID : int
        Code name to identify dust options in the directory naming convention
    '''  
    #Dust Naming Convention
    Dust_Type_nameID = 0
    DUST_ID = 0
    if Run_Dust:
        if DustModel == 'inline':
            DUST_ID = 100
            # which domain run inline dust
            Dust_Type_nameID = int(string_inline_dust, 2)
    
        if DustModel == 'Chappell':
            DUST_ID = 200
        
            #no sub-option so far
            Dust_Type_nameID = 0
    
    DUST_ID = DUST_ID + Dust_Type_nameID
    
    return DUST_ID

##########################################################################################################################
if __name__ == '__main__':
    import sys
    sys.path.append('..')
    sys.path.append('Python-Scripts/Sources')
    import InitLogging as IL
    
    loggername='DustNameConvention'
    logger=IL.Initialise_logging(loggername)
    
    YesNo = [True, False]
    DustModels = [None, 'inline', 'Chappell']
    string_inline_dust_domain = ['001','010','011','100','101','110','111']
    for Run_Dust in YesNo:
        for DustModel in DustModels:
            for string in string_inline_dust_domain:
                did = DustNameConvention(logger, DustModel, Run_Dust, string)
                logger.info("Run dust = {Run_Dust} DustModel = {DustModel} dust string = {string} DUST_ID = {did}"
                           .format(Run_Dust = Run_Dust, DustModel = DustModel, string = string, did = did)
                            )

