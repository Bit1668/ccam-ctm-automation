"""
.. module:: EDMSNameConvention
   :platform: Unix
   :synopsis: Everything needed to build and run the CSIRO Chemical Transport Model.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
def EDMSNameConvention(logger, Emissiondate, PM10speciation):
    '''
    function to compute the EDMS_ID name to implement in the ensemble directory name
    
    Parameters
    ----------
    logger : logging.logger
        instance of a logger to output messages.
    CCAMFlavour : str
        determine how the ccam files are formatted. 2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'
    CCAM_input_flow : str
        input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA"
         
    Returns
    -------
    EDMS_ID : int
        Code name to identify Met options in the directory naming convention
    '''  
    #EDMS Naming Convention
    EDMS_ID = 0
    
    #based on the EDMS emission year
    if Emissiondate.year == 2008:
       EDMS_ID = 100
    if Emissiondate.year == 2013:
       EDMS_ID = 200
    
    # PM10 or PM25 speciation
    if PM10speciation:
       PMID = 10
    else:
       PMID = 20
        
    EDMS_ID += PMID
    
    return EDMS_ID

##########################################################################################################################
if __name__ == '__main__':
    import sys
    import datetime as dt
    sys.path.append('..')
    sys.path.append('Python-Scripts/Sources')
    
    import InitLogging as IL
    loggername='EDMSNameConvention'
    logger=IL.Initialise_logging(loggername)
    
    PM = [True, False]
    years = [2008, 2013, 2000]
    for PM10speciation in PM:
        for year in years:
            Emissiondate = dt.datetime(year, 1, 1)
            eid = EDMSNameConvention(logger, Emissiondate, PM10speciation)
            logger.info("PM10speciation = {PM10speciation} year = {year} Met_ID = {eid}"
                           .format(PM10speciation = PM10speciation, year = Emissiondate.year, eid = eid)
                            )

