"""
.. module:: FireNameConvention
   :platform: Unix
   :synopsis: Everything needed to build and run the CSIRO Chemical Transport Model.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
def FireNameConvention(logger, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire):
    '''
    function to compute the fire_ID name to implement in the ensemble directory name
    
    Parameters
    ----------
    logger : logging.logger
        instance of a logger to output messages.
    bushfireCSIRO : bool
        (not) using bushfireCSIRO option
    bushfireOEH : bool
        (not) using bushfireOEH option
    Run_HRB : bool
        (not) including HRB option
    Run_Bushfire : bool
        (not) including wildfire option
        
    Returns
    -------
    FIRE_ID : int
        Code name to identify fire options in the directory naming convention
    '''  
    #Fire Naming Convention
    FIRE_Type_nameID = 0
    if Run_HRB:
        FIRE_Type_nameID = 10
    if Run_Bushfire:
        FIRE_Type_nameID = 20
    if Run_Bushfire and Run_HRB:
        FIRE_Type_nameID = 30

    FIRE_ID = 0
    if bushfireCSIRO:
        FIRE_ID = 100 + FIRE_Type_nameID
     
    if bushfireOEH:
        FIRE_ID = 200 + FIRE_Type_nameID
    
    return FIRE_ID

##########################################################################################################################
if __name__ == '__main__':
    import sys
    sys.path.append('..')
    import InitLogging as IL
    
    loggername='FireNameConvention'
    logger=IL.Initialise_logging(loggername)
    
    YesNo = [True, False]
    
    for bushfireCSIRO in YesNo:
        for bushfireOEH in YesNo:
            if not (bushfireCSIRO and bushfireOEH):
                for Run_HRB in YesNo:
                    for Run_Bushfire in YesNo:
                        fid = FireNameConvention(logger, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire)
                        logger.info("CSIRO = {csiro} OEH = {oeh} HRB = {hrb} Wildfire = {bushfire} FIRE_ID = {fid}"
                           .format(csiro = bushfireCSIRO, oeh = bushfireOEH, hrb = Run_HRB, bushfire = Run_Bushfire, fid = fid)
                            )

