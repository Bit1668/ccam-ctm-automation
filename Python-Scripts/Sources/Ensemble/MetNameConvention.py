"""
.. module:: MetNameConvention
   :platform: Unix
   :synopsis: Everything needed to build and run the CSIRO Chemical Transport Model.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
def MetNameConvention(logger, CCAMFlavour, CCAM_input_flow):
    '''
    function to compute the MET_ID name to implement in the ensemble directory name
    
    Parameters
    ----------
    logger : logging.logger
        instance of a logger to output messages.
    CCAMFlavour : str
        determine how the ccam files are formatted. 2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'
    CCAM_input_flow : str
        input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA"
         
    Returns
    -------
    Met_ID : int
        Code name to identify Met options in the directory naming convention
    '''  
    #Dust Naming Convention
    Met_Type_nameID = 0
    Met_ID = 100
    if CCAM_input_flow == "Forecast":
        Met_Type_nameID = 0
    if CCAM_input_flow == "Forecast_Linux":
        Met_Type_nameID = 10
    if CCAM_input_flow == "ERA":
        Met_Type_nameID = 50
    
    Met_ID += Met_Type_nameID
    
    return Met_ID

##########################################################################################################################
if __name__ == '__main__':
    import sys
    sys.path.append('..')
    sys.path.append('Python-Scripts/Sources')
    import InitLogging as IL
    
    loggername='MetNameConvention'
    logger=IL.Initialise_logging(loggername)
    
    CCAMFlavour = ['OneFile/Forecast/Domain', 'OneFile/Day/Domain']
    CCAM_input_flow = ["Forecast", "Forecast_Linux", "ERA"]
    for CCF in CCAMFlavour:
        for CCIF in CCAM_input_flow:
            mid = MetNameConvention(logger, CCF, CCIF)
            logger.info("CCAM flavour = {CCF} Met_input_flow = {CCIF} Met_ID = {mid}"
                           .format(CCF = CCF, CCIF = CCIF, mid = mid)
                            )

