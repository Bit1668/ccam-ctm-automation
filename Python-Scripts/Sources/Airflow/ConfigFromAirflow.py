"""
.. module:: ConfigFromAirflow
    :platform: Unix
    :synopsis: Module used to configure the communication between the Airflow scheduler and the CTM.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
   
"""
import sys
import Airflow_CSIRO_bushfire_config as ACBC
def ConfigFromAirflow(logger, Run_HRB, Run_Bushfire, Startdate, timezone, SimulationBasePath, emissions_rfs_DATA_CSIRO, 
                      burn_start_hour_csiro, TimestampUTC):
        '''
        Module used to configure the communication between the Airflow scheduler and the CTM.
        So far, it only configures bushfires.

        Parameters
        ---------- 
        logger : logging.logger
            instance of a logger to output messages.
        Run_HRB : bool
            (not) including HRB option.
        Run_Bushfire : bool
            (not) including wildfire option.
        Startdate : datetime.datetime
            start date of the emissions to build as tz timezone.
        timezone : str
            timezone to localise the Date input.
        SimulationBasePath : str
            base path where to create the rundir.  
        emissions_rfs_DATA_CSIRO : str
            base path where to prepare the CSIRO fire emission model data
        burn_start_hour_csiro : str
            UTC hour when the burns start in the CSIRO fire emission model
        TimestampUTC : datetime.datetime
            timestamp to identify the run.
            
        Returns
        -------
        Grab_CSIRO_Bushfire_Config : object
            object to cater for config and data for the CSIRO fire emission model   
        '''
        
        Grab_CSIRO_Bushfire_Config = ACBC.Airflow_CSIRO_bushfire_config(logger, Startdate, timezone, SimulationBasePath,
                                             emissions_rfs_DATA_CSIRO, TimestampUTC, Run_HRB, Run_Bushfire)
        Grab_CSIRO_Bushfire_Config.burn_start_hour_csiro = burn_start_hour_csiro
        Grab_CSIRO_Bushfire_Config.Detect_everything()
        Grab_CSIRO_Bushfire_Config.Load_everything()
        
        #run the options if possible
        
        ##HRB
        if Run_HRB:
            if Grab_CSIRO_Bushfire_Config.CSIRO_HRB:
                logger.info("HRB option requested AND HRB fire(s) detected")
            else:    
                logger.error("HRB option not possible -- ABORTING")
                sys.exit("NO HRB Detected")
        else:
            Grab_CSIRO_Bushfire_Config.CSIRO_HRB = False
        
        ##Wildfire
        if Run_Bushfire:
            if Grab_CSIRO_Bushfire_Config.CSIRO_bushfire:
                logger.info("Wildfire option requested AND Bushfire(s) detected")
            else:    
                logger.error("Wildfire option not possible")
                sys.exit("NO Bushfire Detected")
        else:
            Grab_CSIRO_Bushfire_Config.CSIRO_bushfire = False
        
        # move the eventual files and set up the trigger option
        if Grab_CSIRO_Bushfire_Config.CSIRO_bushfire or Grab_CSIRO_Bushfire_Config.CSIRO_HRB:
            Grab_CSIRO_Bushfire_Config.Move_everything()
        
        return Grab_CSIRO_Bushfire_Config

