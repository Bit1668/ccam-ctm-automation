"""
.. module:: Airflow_CSIRO_bushfire_config
    :platform: Unix
    :synopsis: Everything needed to prepare data for the the CSIRO fire emission model from Airflow stream.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
   
"""
from __future__ import absolute_import
import os
import json
import shutil
import fnmatch
import datetime as dt
import sys
try:
    from .. import DateMagics as DM
except:
    sys.path.append('Python-Scripts/Sources')
    import DateMagics as DM

#import workflows path
sys.path.append('/mnt/climate/cas/project/workflows/server/secondary/code/workflows')

class Airflow_CSIRO_bushfire_config(object):
    '''
    Class to configure and grab CSIRO bushfire files from the Airflow stream.
    '''
    def __init__(self, logger, Date, timezone, SimulationBasePath, emissions_rfs_DATA_CSIRO, TimestampUTC, Run_HRB, Run_Bushfire):
        '''
        Class to configure and grab CSIRO bushfire files from the Airflow stream.
    
        Attributes
        -----------
        logger : logging.logger
            instance of a logger to output messages.
        Date : datetime.datetime
            start date of the emissions to build as tz timezone.
        timezone : str
            timezone to localise the Date input.
        SimulationBasePath : str
            base path where to create the rundir.  
        emissions_rfs_DATA_CSIRO : str
            base path where to prepare the CSIRO fire emission model data
        TimestampUTC : datetime.datetime
            timestamp to identify the run.
        Run_HRB : bool
            (not) including HRB option.
        Run_Bushfire : bool
            (not) including wildfire option.
        '''
 
        self.logger = logger
        #justification of the messages
        self.justif = 100

        self.Date = Date
        self.DateUTC, self.DateAEST, self.DateAEDT = DM.DateMagics(Date, timezone)
        
        self.TimestampUTC = TimestampUTC
        
        self.Run_HRB = Run_HRB
        self.Run_Bushfire = Run_Bushfire 
        
        self.SimulationBasePath = SimulationBasePath
        self.StateDir = os.path.join(SimulationBasePath,"State_{date}".format(date=self.DateAEST.strftime('%Y%m%dAEST')))
        self.state_filename = 'Airflow_CSIRO_bushfire---timestamp---{timestamp}.json'.format(timestamp=self.TimestampUTC.strftime('%Y%m%dT%H%M%S'))
        self.emissions_rfs_DATA_CSIRO = emissions_rfs_DATA_CSIRO

        self.Firedir = '{date}_{timestamp}'.format(date=self.DateAEDT.strftime('%Y%m%dAEDT'),
                                                   timestamp = self.TimestampUTC.strftime('%Y%m%dT%H%M%S')
                                                   )
        self.HRB_dir = "HRBs"
        self.Wildfire_dir = "Wildfires"
        self.CTM_dir = os.path.join(self.emissions_rfs_DATA_CSIRO, self.Firedir)
        self.CTM_dir_HRB = os.path.join(self.emissions_rfs_DATA_CSIRO, self.Firedir, self.HRB_dir)
        self.CTM_dir_Wildfire = os.path.join(self.emissions_rfs_DATA_CSIRO, self.Firedir, self.Wildfire_dir)
        self.trigger_CSIRO_Bushfire = False
        self.burn_start_hour_csiro = 0
        return
####################################################################################################################
    def Common_var(self):
        import common.emissions_path_spec as EPS
        #import common.airflow_variables as cv
        
        #self.EMISSIONS_LOAD_PATH = cv.getvar(cv.IRS_EMISSIONS_LOAD_PATH)
        #self.EMISSIONS_LOAD_PATH = '/mnt/climate/cas/scratch/airflow/emissions/stage/'
        self.EMISSIONS_LOAD_PATH = '/mnt/climate/cas/scratch/workflows/stage/emissions'
        self.Bushfire_CSIRO_module = EPS.EmissionsPaths(search_path=self.EMISSIONS_LOAD_PATH)
####################################################################################################################
    def Detect_bushfire(self):
        import common.dag_id as cdid
       # Wildfire

        self.spec_bushfire = self.Bushfire_CSIRO_module.get_path(cdid.CSEM_BUSHFIRE_EMISSION_MODEL)
        
        if self.spec_bushfire.files and len(self.spec_bushfire.files) > 0:
            path0 = os.path.join(self.spec_bushfire.path, self.spec_bushfire.files[0])

            print('path: ' + self.spec_bushfire.path)
            print('is cf: ' + str(self.spec_bushfire.is_cf))
            print('is valid: '+ str( self.spec_bushfire.is_valid))
            print('tstamp: ' + str(self.spec_bushfire.utc_timestamp))
        if self.spec_bushfire.is_valid:
            self.CSIRO_bushfire = True
            self.CSIRO_bushfire_path = self.spec_bushfire.path
            self.CSIRO_bushfire_timestamp = self.spec_bushfire.utc_timestamp.strftime('%Y%m%d_%H%M%S')
            if not self.Run_Bushfire:
                self.CSIRO_bushfire_path = None
                self.CSIRO_bushfire_timestamp = None
                
        else:    
            self.CSIRO_bushfire = False
            self.CSIRO_bushfire_path = None
            self.CSIRO_bushfire_timestamp = None
        print 'CSIRO bushfire = %s path=%s', self.CSIRO_bushfire, self.CSIRO_bushfire_path
        return

####################################################################################################################
    def Detect_HRB(self):
        import common.dag_id as cdid
        # Hazard Reduction Burns
        
        self.spec_HRB = self.Bushfire_CSIRO_module.get_path(cdid.CSEM_HRB_EMISSION_MODEL)
        if self.spec_HRB.files and len(self.spec_HRB.files) > 0:
            path0 = os.path.join(self.spec_HRB.path, self.spec_HRB.files[0])

            print('path: ' + self.spec_HRB.path)
            print('is cf: ' + str(self.spec_HRB.is_cf))
            print('is valid: ', self.spec_HRB.is_valid)
            print('tstamp: ' + str(self.spec_HRB.utc_timestamp))
            print 'dtype', type(self.spec_HRB.utc_timestamp)

        if self.spec_HRB.is_valid:
            self.CSIRO_HRB = True
            self.CSIRO_HRB_path = self.spec_HRB.path
            self.CSIRO_HRB_timestamp = self.spec_HRB.utc_timestamp.strftime('%Y%m%d_%H%M%S')
            if not self.Run_HRB:
                self.CSIRO_HRB_path = None
                self.CSIRO_HRB_timestamp = None
            
        else:    
            self.CSIRO_HRB = False
            self.CSIRO_HRB_path = None
            self.CSIRO_HRB_timestamp = None

        print 'CSIRO HRB = %s path=%s', self.CSIRO_HRB, self.CSIRO_HRB_path
        return

####################################################################################################################
    def Create_state_dir(self):
        """ This method build the state dir.
        """
        try :
            os.makedirs(self.StateDir)
            os.chmod(self.StateDir,0777)
            self.logger.info('Creation State Dir= {msg}'.format(msg=self.StateDir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('State Dir = {msg}'.format(msg=self.StateDir).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

####################################################################################################################
    def Create_CTM_dir(self):
        """ This method build the state dir.
        """
        try :
            os.makedirs(self.CTM_dir)
            os.chmod(self.CTM_dir, 0777)
            self.logger.info('Creation Working Dir= {msg}'.format(msg=self.CTM_dir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('Working Dir = {msg}'.format(msg=self.CTM_dir).ljust(self.justif-13,'.') + 'Already exist')
            pass
        try :
            os.makedirs(self.CTM_dir_HRB)
            os.chmod(self.CTM_dir_HRB, 0777)
            self.logger.info('Creation Working Dir= {msg}'.format(msg=self.CTM_dir_HRB).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('Working Dir = {msg}'.format(msg=self.CTM_dir_HRB).ljust(self.justif-13,'.') + 'Already exist')
            pass
        try :
            os.makedirs(self.CTM_dir_Wildfire)
            os.chmod(self.CTM_dir_Wildfire, 0777)
            self.logger.info('Creation Working Dir= {msg}'.format(msg=self.CTM_dir_Wildfire).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('Working Dir = {msg}'.format(msg=self.CTM_dir_Wildfire).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

####################################################################################################################
    def Move_files(self, src, dst):

        MatchingPattern = '*.csv'
        listdir = os.listdir(src)
        filteredlist = fnmatch.filter(listdir, MatchingPattern)

        listdir_dst = os.listdir(dst)
        filteredlist_dst = fnmatch.filter(listdir_dst, MatchingPattern)
        
        if filteredlist_dst:
            self.logger.info("Previous fire files detected in destination dir {ddir}".format(ddir = dst))
            for filename in filteredlist_dst:
                filedst = os.path.join(dst,filename)
                try:
                    os.remove(filedst)
                    self.logger.info("File {ff} removed".format(ff = filedst))
                except:
                    self.logger.debug("something went wrong when deleting {ff}".format(ff = filedst))
                    sys.exit("Sorry :( ")
                    pass    
       
        if filteredlist:
            for filename in filteredlist:
                self.logger.debug("Fire files in original fire dir %s",filename)
                try:
                    originalfile=os.path.join(src,filename)
                    destinationfile=os.path.join(dst,filename)
                    self.logger.info('Copying %s -> %s',originalfile,destinationfile)
                    shutil.copy(originalfile,destinationfile)
                except:
                    self.logger.debug("something went wrong when moving %s",originalfile)
                    sys.exit("Sorry :( ")
                    pass    
        return
    

####################################################################################################################
    def Detect_everything(self):
        self.Common_var()
        self.Detect_bushfire()
        self.Detect_HRB()
        self.Create_state_dir()
        self.Write_state_file()
        return

####################################################################################################################
    def Run_manual(self, Run_Bushfire, Run_HRB, Manual_HRB_Dir, Manual_Bushfire_Dir):
        
        self.CSIRO_HRB = Run_HRB
        self.CSIRO_HRB_timestamp = None
        self.CSIRO_HRB_path = Manual_HRB_Dir
        self.CSIRO_bushfire = Run_Bushfire
        self.CSIRO_bushfire_timestamp = None
        self.CSIRO_bushfire_path = Manual_Bushfire_Dir
    
        self.Create_state_dir()
        self.Write_state_file()
        self.Move_everything()
        return
####################################################################################################################
    def Write_state_file(self):
        with open(os.path.join(self.StateDir, self.state_filename), 'wb') as handle:
            self.struct = {
            'CSIRO_bushfire' : self.CSIRO_bushfire, 
            'CSIRO_bushfire_path' : self.CSIRO_bushfire_path, 
            'CSIRO_bushfire_timestamp' : self.CSIRO_bushfire_timestamp, 
            'CSIRO_HRB' : self.CSIRO_HRB,
            'CSIRO_HRB_path' : self.CSIRO_HRB_path,
            'CSIRO_HRB_timestamp' : self.CSIRO_HRB_timestamp,
            'CSIRO_burn_start_hour' : self.burn_start_hour_csiro,
            }
            #print 'timestamp',self.CSIRO_HRB_timestamp
            json.dump(self.struct,handle, indent=4, encoding='utf-8',)
        return

####################################################################################################################
    def Load_everything(self):
        with open(os.path.join(self.StateDir, self.state_filename), 'r') as handle:
            struct = json.load(handle)
            self.CSIRO_bushfire = struct['CSIRO_bushfire']
            self.CSIRO_bushfire_path = struct['CSIRO_bushfire_path'] 
            self.CSIRO_bushfire_timestamp = struct['CSIRO_bushfire_timestamp']
            self.CSIRO_HRB = struct['CSIRO_HRB']
            self.CSIRO_HRB_path = struct['CSIRO_HRB_path']
            self.CSIRO_HRB_timestamp = struct['CSIRO_HRB_timestamp']
            self.burn_start_hour_csiro = struct['CSIRO_burn_start_hour']
        return    
####################################################################################################################
    def Move_everything(self):
        self.Create_CTM_dir()
        if self.CSIRO_bushfire:
            self.Move_files(self.CSIRO_bushfire_path, self.CTM_dir_Wildfire)
            self.trigger_CSIRO_Bushfire = True
        if self.CSIRO_HRB:
            self.Move_files(self.CSIRO_HRB_path, self.CTM_dir_HRB)
            self.trigger_CSIRO_Bushfire = True
        self.StateDir =  self.CTM_dir 
        self.Write_state_file()  
        return    
    
    

