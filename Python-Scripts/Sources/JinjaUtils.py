"""
.. module:: JinjaUtils
   :platform: Unix
   :synopsis: Everything needed to initialise a jinja2 templating instance.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
                  

"""
import jinja2

def LoadTemplate(templatepath,templatefile):
    """
    This function defines and configures a jinja2 template instance.
     
    Parameters
    ---------- 
        templatepath : str
            path to the template.
        templatefile : str
            filename of the template.
    Returns
    -------
        template : jinja2 template instance
            jinja2 template instance ready to be rendered.        
    """
    templateLoader = jinja2.FileSystemLoader( searchpath=templatepath )
    templateEnv = jinja2.Environment( loader=templateLoader )

    template = templateEnv.get_template(templatefile)
    return template 

