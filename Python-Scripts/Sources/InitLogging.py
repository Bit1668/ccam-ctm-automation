"""
.. module:: InitLogging
   :platform: Unix
   :synopsis: Everything needed to initialise a logger.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
"""
import logging

############################################################################# 
def Initialise_logging(loggername):
    """
    This function defines and configures a logger instance.
     
    Parameters
    ---------- 
        loggername : str
            Name of the logger that will be also the log filename.
    Returns
    -------
        logger : logging.logger
            instance of the initialised logger.        
    """
# root logger
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.FATAL)

# create logger with 'plotting'
    logger = logging.getLogger(loggername)
    logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
    fileh = logging.FileHandler(loggername+'.log')
    fileh.setLevel(logging.DEBUG)
# create console handler with a higher log level
    consoleh = logging.StreamHandler()
    consoleh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
    formatterfile    = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatterconsole = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    fileh.setFormatter(formatterfile)
    consoleh.setFormatter(formatterconsole)
# add the handlers to the logger
    logger.addHandler(fileh)
    logger.addHandler(consoleh)
    
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LEVEL = logging.DEBUG

    logging.basicConfig(level=LOGGING_LEVEL,
        format=LOGGING_FORMAT)    
    logger.propagate = False    
#    logger.info('creating an instance of auxiliary_module.Auxiliary')
#    a = auxiliary_module.Auxiliary()
#    logger.info('created an instance of auxiliary_module.Auxiliary')
#    logger.info('calling auxiliary_module.Auxiliary.do_something')
#    a.do_something()
#    logger.info('finished auxiliary_module.Auxiliary.do_something')
#    logger.info('calling auxiliary_module.some_function()')
#    auxiliary_module.some_function()
#    logger.info('done with auxiliary_module.some_function()')
    return logger

############################################################################# 
def PrintDiff(f1,f2,logger):
    """
    This function compute and print the difference between 2 files.
    It try to reproduce the \*NIX "diff" command
     
    Parameters
    ---------- 
        f1 : str
            Name of the first file to compare.
        f2 : str
            Name of the second file to compare.
        logger : logging.logger
             instance of an initialised logger.        
    Returns
    -------
        Not much : str
            write in the logger the diff line by line 
    """
    import difflib
    diff = difflib.ndiff(open(f1,'r').readlines(),open(f2,'r').readlines())
#    diff = difflib.unified_diff(open(f1,'r').readlines(),open(f2,'r').readlines(), fromfile=f1, tofile=f2, n=("--lines 3"))
    changes = [l for l in diff if l.startswith('+ ') or l.startswith('- ')]
    logger.debug('-------------------------------')
    logger.debug('- %s',f1)
    logger.debug('+ %s',f2)
    logger.debug('-------------------------------')
    for c in changes:
       logger.debug('%s',c[:-1])
    return   

