"""
.. module:: RunConfig
    :platform: Unix
    :synopsis: Definition of the basic object classes to run a CTM simulation.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
   
"""
#from __future__ import absolute_import
import string
import pytz
import DateMagics as DM
import uuid
import os
import datetime as dt
import sys
import BuildEmissions.EmissionScaling as ES
###########################################################################################
class SimulationConfigClass(object):
    """ 
    This Global class defines a simulation object, that contains all the 
    parameters to run a whole simulation for every grid

    Attributes
    -----------
    logger : logging.logger
        instance of a logger to output messages.
    justif : int
        max message width to justify logger output.   
    ResolutionNames : str
        string identifying the computational domain name.
    ResolutionProperties : dic
        dictionary containing each compuational domain properties.
    SCENARIO : str
        run identification name, can be "forecast" or "XXX".
    StartdateAEDT : datetime.datetime
        naive start date of the run, in AEDT. 
    EnddateAEDT : datetime.datetime
        naive  end date of the run, in AEDT.  
    Emissiondate : datetime.datetime
        naive  date when to compute the emissions, in AEDT.  
    TimestampUTC : datetime.datetime
        timestamp ID to uniquely identify the run. It will be used to build the main directory.
    TRANSPORT : str
        numerical scheme name for the transport.
    DIFF : str
        enable or not the NWP diffusivity.
    SIOA : str
        numerical scheme name for secondary inorganic atmospheric chemistry.
    SOA : str
        numerical scheme name for secondary atmospheric chemistry.
    CHEM : str
        numerical scheme name for carbon atmospheric chemistry.
    PrepBin : str
        absolute full bin name of the prep.
    templatepath : str
        path to the template directory
    netcdfpath : str
        path to the base dir where the CCAM NWP is.
    EmissionModellingPath : str
        path to the common emission files.
    EmissionBuildPath : str
        path where to build the static EDMS emissions from source data.
    Emissions_edms_DATA : str
        path where the EDMS source date resides.
    SimulationBasePath : str
        base path where to create the rundir. 
    CTMBinPath : str
        path where to find the CTM bin.
    CTMBin : str
        name of the CTM bin.
    RuntimeCPU : int
        number of MP cpu to ask for in SLURM scheduler.
    RunTimeHours : int
        Runtime hours to ask for in SLURM scheduler.
    PartitionName : str
        Partition name to allocate in SLURM scheduler.
    CCAM_input_flow : str
        input flow of the CCAM, can be "Forecast" or "ERA"
    CCAMFlavour : str
        determine how the ccam files are formatted. 
        2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'
    """

    def __init__(self, logger, ResolutionNames, ResolutionProperties, SCENARIO, 
                  StartdateAEDT, EnddateAEDT, TimestampUTC,
                  TRANSPORT, DIFF, SIOA, SOA, CHEM, PrepBin, 
                  templatepath, EmissionBuildPath,
                  SimulationBasePath, ConfigurationOptions, 
                  CTMBinPath, CTMBin, RuntimeCPU, RunTimeHours, PartitionName, 
                  ):
        self.logger = logger
        self.ResolutionNames = ResolutionNames
        self.SCENARIO = SCENARIO
 
         #justification of the messages
        self.justif = 102

        self.logger.info(''.ljust(self.justif,'-'))
        self.logger.info('Configuring CTM'.center(self.justif,'|'))
        self.logger.info(''.ljust(self.justif,'-'))
       
        #input Startdate and Enddate are given in AEDT, but the CTM runs in UTC so we convert!! 
        #self.autz=pytz.timezone('Australia/Sydney')
        #self.StartdateADST = self.autz.localize(StartdateADST).replace(hour= 0)
        #self.EnddateADST = self.autz.localize(EnddateADST)#.replace(hour= 23)
        self.StartdateUTC, self.StartdateAEST, self.StartdateAEDT = DM.DateMagics(StartdateAEDT.replace(hour= 0), 'AEDT')
        self.EnddateUTC, self.EnddateAEST, self.EnddateAEDT = DM.DateMagics(EnddateAEDT.replace(hour= 0), 'AEDT')
        #conversion to UTC
        self.Startdate = self.StartdateUTC.replace(hour= 0)
        self.Enddate = self.EnddateUTC
        
        #timestamp
        self.TimestampUTC = TimestampUTC
        
       
        #Chemical model options
        self.TRANSPORT = TRANSPORT
        self.DIFF = DIFF
        self.SIOA = SIOA
        self.SOA = SOA
        self.CHEM = CHEM
        #self.BIO = Emissiondate.strftime('%b').lower()
        self.BIO = None

        #grid and length
        self.ResolutionProperties = ResolutionProperties
        self.SimulationHours = (self.EnddateUTC - self.StartdateUTC).total_seconds() // 3600
        
        # Where the template dir is
        self.templatepath = templatepath
        
        # where are/to build the emission files
        self.EmissionBuildPath_static = EmissionBuildPath
        self.EmissionBuildPath_dynamic = None #computed later
        
        # root dir of the simulation
        self.SimulationBasePath = SimulationBasePath
        
        # Simulation Dir
        self.SimulationPath = None
        self.LastSimulationPath='NONE'
        
        
        # June 2017: pattern for Last simulationDir  is ready for the next 12 years :) 
        self.PatternLastSimulationDir='20[0-2][0-9][0-1][0-9][0-3][0-9]*'
        # pattern for the Restart files
        self.PatternRestartFileName='WarmStart_[0-3]_01_025.bin'
        
        self.templatefile_CtmPrepInit='CTM_prep_init_Restart.template'
        self.templatefile_ConfigGrid='ConfigGrid_Restart.template'
        self.templatefile_Met='Meteo-Config-Files.template'
        self.templatefile_Sbatch='SBatch-threaded.template'
        self.templatefile_Script_Submit='Script-Submit.template'
        
        self.SubmitScriptName='Submit'
        self.WarmStart=False
        
        #Met files properties
        #self.CCAMFlavour = CCAMFlavour
        #self.CCAM_input_flow = CCAM_input_flow     
        self.CCAMFlavour = None
        self.CCAM_input_flow = None    
        # json of the linux ccam
        self.CCAM_StatusJSONfile = None
        self.CCAMLinkPrefixFilename = 'ccam_'
        self.meteo_config_filename_list = []
        
        # Where is the original CCAM met file dir
        self.netcdfpath = None
        # Where are the symlink renamed Destination CCAM dir --> Update, computed later in the method
        #self.CcamFilePath = ccamfilepath
        self.CcamFilePath = None
        #CCAM : number of forecast days in the Met files
        self.CCAMNdays = 4
        #CCAM : which forecast hour
        self.CCAMstarthour = 12

        
        #binaries
        self.PrepBin = PrepBin

        self.CTMBinPath = CTMBinPath
        self.CTMBin = CTMBin
        self.RuntimeCPU = RuntimeCPU
        self.RunTimeHours = RunTimeHours
        self.PartitionName = PartitionName
        # Binaries dir for building emission
        self.EmissionBasebinpath='/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran'
        #self.bininitline = "source /home/barthelemyx/modulefiles/init.bash | ulimit -s 350000 |"
        #self.EmissionBasebinpath='Emission-Building-Fortran/'
        self.bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | srun -p {partition} -N1 -x irscomp[01,12] "
        self.bininitline = self.bininitline_template.format(partition = self.PartitionName)
        self.bininitline = "source ~/modulefiles/init.bash | ulimit -s 350000 | "
        
        #Configuration options

        # EDMS emissions database options
        #input in emission are in AEST, and the timeshift to utc is done with an offset inside the module
        self.Emissiondate = None

        # where the EDMS sources are
        self.Emissions_edms_DATA_basedir = None
        self.EmissionModellingPath = None
        self.Emissions_edms_DATA = None
        self.LAIsubdir = None
        self.SOILsubdir = None
        self.VEGsubdir = None

#        self.EmissionGroup=EmissionGroup
        self.EmissionGroup = []

        # EDMS emission global scaling
        self.EmissionGlobalScaling = ES.EmissionGlobalScalingClass()
                     
        # PM10 or PM2.5 emission speciation
        self.PM10speciation = None
        self.PMspeciation = None

#       Include or not emission groups.
        self.includeemission_vpx = True
        self.includeemission_vdx = True
        self.includeemission_vlx = True
        self.includeemission_vpv = True
        self.includeemission_gse = True
        self.includeemission_whe = True
        self.includeemission_pse = True

#       Build or not emission groups.
        self.buildemission_vpx = True
        self.buildemission_vdx = True
        self.buildemission_vlx = True
        self.buildemission_vpv = True
        self.buildemission_gse = True
        self.buildemission_whe = True
        self.buildemission_pse = True

        self.EDMS_ID = None

        self.edms_updated = False        

#       MEMS
#       offline MEMS emission module inclusion?      
        self.includeemission_MEMS = False
       
        # Building/including Bushfire module OEH Flavour
        self.bushfireOEH = False
        # and where is the datadir
        self.emissions_rfs_DATA_OEH = None

        # Building/including Bushfire module CSIRO Flavour
        self.bushfireCSIRO = False
        # and where is the datadir
        self.emissions_rfs_DATA_CSIRO = None
        #Burns are starting at
        self.burn_start_hour_csiro = 0
        #fireID
        self.FIRE_ID = 0
        # keep track of the update
        self.bushfire_updated = False
        
        # Building/including Dust module flavour
        self.Run_Dust = False
        self.includeemission_MEMS_dust = False
        self.DustModel = None
        self.MEMS_dust_source_dir = None
        self.MEMS_dust_source_file = None
        # Dust ID
        self.DUST_ID = 0
        # keep track of the update
        self.dust_updated = False
        
        return
###########################################################################################
    def ComputeDirectories(self):
        ''' 
        This is where the directory names are computed
        '''

        if not self.bushfire_updated:
            self.logger.error("Bushfire options".ljust(self.justif-11,'.')+"NOT UPDATED")
            sys.exit("Sorry  :((")
            
        if not self.dust_updated :
            self.logger.error("Dust options".ljust(self.justif-11,'.')+"NOT UPDATED")
            sys.exit("Sorry  :((")

        if not self.met_updated :
            self.logger.error("Met options".ljust(self.justif-11,'.')+"NOT UPDATED")
            sys.exit("Sorry  :((")
       
        if not self.edms_updated :
            self.logger.error("EDMS options".ljust(self.justif-11,'.')+"NOT UPDATED")
            sys.exit("Sorry  :((")
       
       # Here is the computation of the main simulation dir

        self.SimulationDir = "{date}_M{MetID}_F{FireID}_D{DustID}_E{EDMS_ID}---timestamp---{timestamp}UTC".format(
                 date = self.StartdateAEDT.strftime('%Y%m%dAEDT') ,
                 MetID = str(self.MET_ID).zfill(3),
                 FireID = str(self.FIRE_ID).zfill(3),
                 DustID = str(self.DUST_ID).zfill(3),
                 EDMS_ID = str(self.EDMS_ID).zfill(3),
                 timestamp = self.TimestampUTC.strftime('%Y%m%d_%H%M%S')
                 )
        self.SimulationPath = os.path.join(self.SimulationBasePath, self.SimulationDir)

        # Here is the computation of the met dir
        RelativeCCAM = 'MetFiles'
        self.CcamFilePath = os.path.join(self.SimulationPath, RelativeCCAM)
        
        #Relative dynamic emissions
        EmissionBuildPath_dynamic = 'Emissions_dynamic'
        self.EmissionBuildPath_dynamic = os.path.join(self.SimulationPath, EmissionBuildPath_dynamic)
        return


###########################################################################################
    def MakeDir(self):
        """ This method build the Main Simulation dir.
        """

        try :
            os.makedirs(self.SimulationPath)
            os.chmod(self.SimulationPath, 0775)
            self.logger.info('Creation Main Simulation Dir= {msg}'.format(msg=self.SimulationPath).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('Main Simulation Dir = {msg}'.format(msg=self.SimulationPath).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

###########################################################################################
    def init_Run(self, GridIndex, WarmStart):
        ''' This method initialise the RunConfigClass object that define a run for a grid
        '''
        RunConfig = RunConfigClass(self, GridIndex, WarmStart)

#       Include or not emission groups. Yes by default, but can be modified thereafter
        RunConfig.includeemission_vpx = self.includeemission_vpx
        RunConfig.includeemission_vdx = self.includeemission_vdx
        RunConfig.includeemission_vlx = self.includeemission_vlx
        RunConfig.includeemission_vpv = self.includeemission_vpv
        RunConfig.includeemission_gse = self.includeemission_gse
        RunConfig.includeemission_whe = self.includeemission_whe
        RunConfig.includeemission_pse = self.includeemission_pse
        
        #met config files
        RunConfig.meteo_config_filename = self.meteo_config_filename_list[GridIndex]
        
        #MEMS
        RunConfig.includeemission_MEMS = self.includeemission_MEMS
        
        #Dust
        RunConfig.Run_Dust = self.Run_Dust
        RunConfig.includeemission_MEMS_dust = self.includeemission_MEMS_dust
        RunConfig.DustModel = self.DustModel
        
        return  RunConfig

###########################################################################################
    def init_Emission(self):
        ''' This method initialise the EmissionConfigClass object that sets all the emission data to build, or not
        '''
        #import ConfigBuildEmission as CBE
        try :
            import Sources.BuildEmissions.EmissionConfig as CBE
        except:
            import BuildEmissions.EmissionConfig as CBE
        
        logger = self.logger
        justif = self.justif
        
        startdate = self.StartdateAEST
        enddate = self.EnddateAEST
        emissiondate = self.Emissiondate
        TimestampUTC = self.TimestampUTC
        
        SimulationPath = self.SimulationPath
        buildemissionpath_static = self.EmissionBuildPath_static
        buildemissionpath_dynamic = self.EmissionBuildPath_dynamic
        
        Basebinpath = self.EmissionBasebinpath
        templatepath = self.templatepath
        Emissions_edms_DATA_basedir = self.Emissions_edms_DATA_basedir
        
        bininitline = self.bininitline
        
        PM10speciation = self.PM10speciation
        if self.PM10speciation:
            self.PMspeciation = 'PM10'
        else:
            self.PMspeciation = 'PM25'
        
        EmissionConfig = CBE.EmissionConfigClass(  logger, justif, 
                                                 startdate, enddate, emissiondate, TimestampUTC,
                                                 SimulationPath, buildemissionpath_static, buildemissionpath_dynamic, 
                                                 Basebinpath, templatepath, Emissions_edms_DATA_basedir, PM10speciation,
                                                 bininitline, binary = True)
        #coal power options
        EmissionConfig.timeresolvedcoalpower = True
        EmissionConfig.timeresolvedcoalpower = False
        
        #fire options
        EmissionConfig.Run_HRB = self.Run_HRB
        EmissionConfig.Run_Bushfire = self.Run_Bushfire
         
        EmissionConfig.bushfireOEH = self.bushfireOEH
        EmissionConfig.emissions_rfs_DATA_OEH = self.emissions_rfs_DATA_OEH

        EmissionConfig.bushfireCSIRO = self.bushfireCSIRO
        EmissionConfig.emissions_rfs_DATA_CSIRO = self.emissions_rfs_DATA_CSIRO
        EmissionConfig.burn_start_hour_csiro = self.burn_start_hour_csiro

#       Include or not emission groups. Yes by default, but can be modified thereafter
        EmissionConfig.includeemission_vpx = self.includeemission_vpx
        EmissionConfig.includeemission_vdx = self.includeemission_vdx
        EmissionConfig.includeemission_vlx = self.includeemission_vlx
        EmissionConfig.includeemission_vpv = self.includeemission_vpv
        EmissionConfig.includeemission_gse = self.includeemission_gse
        EmissionConfig.includeemission_whe = self.includeemission_whe
        EmissionConfig.includeemission_pse = self.includeemission_pse

#       Build or not emission groups.
        EmissionConfig.buildemission_vpx = self.buildemission_vpx
        EmissionConfig.buildemission_vdx = self.buildemission_vdx
        EmissionConfig.buildemission_vlx = self.buildemission_vlx
        EmissionConfig.buildemission_vpv = self.buildemission_vpv
        EmissionConfig.buildemission_gse = self.buildemission_gse
        EmissionConfig.buildemission_whe = self.buildemission_whe
        EmissionConfig.buildemission_pse = self.buildemission_pse

        #MEMS
        EmissionConfig.includeemission_MEMS = self.includeemission_MEMS
        EmissionConfig.ResolutionNames = self.ResolutionNames
        
        EmissionConfig.Run_Dust = self.Run_Dust
        EmissionConfig.includeemission_MEMS_dust = self.includeemission_MEMS_dust
        EmissionConfig.DustModel = self.DustModel
        EmissionConfig.MEMS_dust_source_dir = self.MEMS_dust_source_dir
        EmissionConfig.MEMS_dust_source_file = self.MEMS_dust_source_file
        EmissionConfig.dust_updated = self.dust_updated
        
        return EmissionConfig

###########################################################################################
    def init_CtmPrepInitConfig(self):
        """ 
        Class that configure the prep of the CTM
        config is done in 2 times:
    
        * 1st part derives from the simulation class. Simulation class has a configure method
        * 2nd part is about emission. at initialiation this class is not totally configured, 
            and it needs the Configure_CTM_prep_init method of the Emission class to finish to set all the vars and paths
        """
        import ConfigPrepCTM as CPC
        logger = self.logger
        ResolutionNames = self.ResolutionNames
        SCENARIO = self.SCENARIO
        Startdate = self.Startdate
        Enddate = self.Enddate
        Emissiondate = self.Emissiondate
        ResolutionProperties = self.ResolutionProperties
        SimulationHours = self.SimulationHours
        netcdfpath = self.netcdfpath
        EmissionModellingPath = self.EmissionModellingPath
#        EmissionGroup=self.EmissionGroup
        EmissionBuildPath_static = self.EmissionBuildPath_static
        EmissionBuildPath_dynamic = self.EmissionBuildPath_dynamic
        
        templatepath = self.templatepath
        templatefile_ConfigPrepCTM = self.templatefile_CtmPrepInit
        SimulationPath = self.SimulationPath
        PrepBin = self.PrepBin
        justif = self.justif 

        Ctm_prep_init_filename = 'ctm_prep_init.inp'

        StartMonth = string.lower(self.Startdate.strftime('%b'))
        SimulationDays = (self.Enddate-self.Startdate).days
        EmissionMonth = string.lower(self.Emissiondate.strftime('%b'))
        
        CtmPrepInitConfig=CPC.CtmPrepInitConfigClass(logger, ResolutionNames, SCENARIO, Startdate, Enddate, Emissiondate, ResolutionProperties, SimulationHours,
                                                       SimulationPath, netcdfpath, EmissionModellingPath, EmissionBuildPath_static, EmissionBuildPath_dynamic,
                                                       templatepath, templatefile_ConfigPrepCTM, PrepBin, justif)
       
        return CtmPrepInitConfig

###########################################################################################
    def init_MetConfig(self):
        ''' 
        This method initialise the Meteorlogy file sources and housekeeping class
        '''
        import Met.MetConfig as MC
        
        logger = self.logger
        justif = self.justif
        Startdate = self.StartdateUTC.replace(hour = self.CCAMstarthour, minute = 0, second=0)
        Enddate = Startdate + dt.timedelta(days = self.CCAMNdays)
        AlreadyRenamed = False, 
        SimulationPath = self.SimulationPath
        templatepath = self.templatepath
        templatefile = self.templatefile_Met
        OriginalCCAMdir = self.netcdfpath
        DestinationCCAMdir = self.CcamFilePath
        ResolutionNames = self.ResolutionNames
        ResolutionProperties = self.ResolutionProperties
        CCAM_input_flow = self.CCAM_input_flow
        CCAMLinkPrefixFilename = self.CCAMLinkPrefixFilename
        CCAMFlavour = self.CCAMFlavour 
        CCAM_StatusJSONfile = self.CCAM_StatusJSONfile
        
        MC = MC.MetConfigClass(logger, justif, 
                 Startdate, Enddate, AlreadyRenamed, 
                 SimulationPath, templatepath, templatefile,
                 OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, ResolutionProperties,
                 CCAM_input_flow,
                 CCAMLinkPrefixFilename, CCAMFlavour,
                 StatusJSONfile = CCAM_StatusJSONfile)
        return MC         

###########################################################################################
    def init_TcbcConfig(self, templatefile_tcbc, Tcbc_filename):
        ''' This method initialise the Emission boundary condition class TcbcConfigClass
        '''
        import DumpTcbcFile as DTF
        
        logger = self.logger
        SimulationPath = self.SimulationPath
        templatepath = self.templatepath
#        templatefile_tcbc=self.templatefile_tcbc
#        Tcbc_filename="tcbc_"+self.CHEM+'_'+string.lower(self.Startdate.strftime('%b'))+".in"
        
        TcbcConfig = DTF.TcbcConfigClass(logger, SimulationPath, templatepath, templatefile_tcbc, Tcbc_filename)
        return TcbcConfig

###########################################################################################
    def Update_Fire_options(self, ConfigurationOptions):
        ''' 
        This method update the position vars of the different bushfire options
        
        Parameters
        ----------
        ConfigurationOptions : obj
            object that contains the configuration option of the run 
        '''
        #kind?
        self.Run_HRB = ConfigurationOptions.Run_HRB
        self.Run_Bushfire = ConfigurationOptions.Run_Bushfire
        
        # Including/building Bushfire module OEH flavour? 
        self.bushfireOEH = ConfigurationOptions.bushfireOEH
        
        #  Data path for the bushfire module OEH flavour
        #SimulationConfig.emissions_rfs_DATA_OEH='/mnt/climate/cas/project/ccam-ctm/emission_modelling/bushfireDF'
        self.emissions_rfs_DATA_OEH = ConfigurationOptions.emissions_rfs_DATA_OEH

        # Including/building Bushfire module CSIRO flavour? 
        self.bushfireCSIRO = ConfigurationOptions.bushfireCSIRO
        #  Data path for the bushfire module CSIRO flavour
        #SimulationConfig.emissions_rfs_DATA_CSIRO = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/bushfireCSI'
        self.emissions_rfs_DATA_CSIRO = ConfigurationOptions.emissions_rfs_DATA_CSIRO
        # burn start hour    
        self.burn_start_hour_csiro = ConfigurationOptions.burn_start_hour_csiro
        
        self.FIRE_ID = ConfigurationOptions.FIRE_ID 
        
        self.bushfire_updated = True
        self.logger.info('Bushfire options update'.ljust(self.justif-2,'.')+"OK")
        return

###########################################################################################
    def Update_Dust_options(self, ConfigurationOptions):
        ''' 
        This method update the position vars of the different dust options
        
        Parameters
        ----------
        ConfigurationOptions : obj
            object that contains the configuration option of the run 
        '''
        # Including/building dust module ? 
        self.Run_Dust = ConfigurationOptions.Run_Dust

        # Including/building dust module flavour? 
        self.DustModel = ConfigurationOptions.DustModel
        
        #  Data path for MEMS Dust
        self.MEMS_dust_source_dir = ConfigurationOptions.MEMS_dust_source_dir
        
        #  Optional forcing of the Data file for MEMS Dust
        self.MEMS_dust_source_file = ConfigurationOptions.MEMS_dust_source_file
        
        # general switch
        if ConfigurationOptions.includeemission_MEMS:
            self.includeemission_MEMS = ConfigurationOptions.includeemission_MEMS

        if self.DustModel == 'Chappell' and self.Run_Dust:
            self.includeemission_MEMS = True
            self.includeemission_MEMS_dust = True
        
        self.DUST_ID = ConfigurationOptions.DUST_ID 
        
        self.dust_updated = True
        self.logger.info('Dust options update'.ljust(self.justif-2,'.')+"OK")
        return

###########################################################################################
    def Update_Met_options(self, ConfigurationOptions):
        ''' 
        This method update the position vars of the different Met options
        
        Parameters
        ----------
        ConfigurationOptions : obj
            object that contains the configuration option of the run 
        '''
        self.MetSource = ConfigurationOptions.MetSource
        self.CCAM_StatusJSONfile = ConfigurationOptions.CCAM_StatusJSONfile
        self.CCAMFlavour = ConfigurationOptions.CCAMFlavour
        self.CCAM_input_flow = ConfigurationOptions.CCAM_input_flow    
        # Where is the original CCAM met file dir
        self.netcdfpath = ConfigurationOptions.netcdfpath

        self.MET_ID = ConfigurationOptions.MET_ID 
        
        self.met_updated = True
        self.logger.info('Met options update'.ljust(self.justif-2,'.')+"OK")
        return

###########################################################################################
    def Update_EDMS_options(self, ConfigurationOptions):
        ''' 
        This method update the position vars of the different emissions database options
        
        Parameters
        ----------
        ConfigurationOptions : obj
            object that contains the configuration option of the run 
        '''
        self.Emissiondate = ConfigurationOptions.Emissiondate

        # where the EDMS sources are
        self.LAIsubdir = 'LAI'
        self.SOILsubdir = 'SOIL'
        self.VEGsubdir = 'VEG'
        
        
        self.EmissionModellingPath = ConfigurationOptions.EmissionModellingPath 
        #self.Emissions_edms_DATA = Emissions_edms_DATA
        #self.Emissions_edms_DATA = Emissions_edms_DATA
        self.Emissions_edms_DATA = None
        self.Emissions_edms_DATA_basedir = ConfigurationOptions.Emissions_edms_DATA_basedir
   
        self.PM10speciation = ConfigurationOptions.PM10speciation
        self.PMspeciation = ConfigurationOptions.PMspeciation

        # Which emission groups to include
        # if not False, then True! ;)
        self.includeemission_vpx = ConfigurationOptions.includeemission_vpx
        self.includeemission_vdx = ConfigurationOptions.includeemission_vdx
        self.includeemission_vlx = ConfigurationOptions.includeemission_vlx
        self.includeemission_vpv = ConfigurationOptions.includeemission_vpv 
        self.includeemission_gse = ConfigurationOptions.includeemission_gse
        self.includeemission_whe = ConfigurationOptions.includeemission_whe

        # Do you need to force build the emissions?
        # if not False, then True! ;)
        self.buildemission_vpx = ConfigurationOptions.buildemission_vpx
        self.buildemission_vdx = ConfigurationOptions.buildemission_vdx
        self.buildemission_vlx = ConfigurationOptions.buildemission_vlx
        self.buildemission_vpv = ConfigurationOptions.buildemission_vpv
        self.buildemission_gse = ConfigurationOptions.buildemission_gse
        self.buildemission_whe = ConfigurationOptions.buildemission_whe
        self.buildemission_pse = ConfigurationOptions.buildemission_pse
        
        self.EDMS_ID = ConfigurationOptions.EDMS_ID
        
        #define model  veg file
        self.BIO = self.Emissiondate.strftime('%b').lower()

        self.edms_updated = True
        self.logger.info('EDMS options update'.ljust(self.justif-2,'.')+"OK")
        return


###########################################################################################
class RunConfigClass(object):
    """ This class defines a run object, that contains all the parameters for a run for a selected grid.
    """
    def __init__(self, SimulationConfig, GridIndex, WarmStart):

        self.logger = SimulationConfig.logger
        self.justif = SimulationConfig.justif
        self.ResolutionNames = SimulationConfig.ResolutionNames
        self.SCENARIO = SimulationConfig.SCENARIO
        self.Startdate = SimulationConfig.Startdate
        self.Enddate = SimulationConfig.Enddate
        self.Emissiondate = SimulationConfig.Emissiondate
        self.TRANSPORT = SimulationConfig.TRANSPORT
        self.DIFF = SimulationConfig.DIFF
        self.SIOA = SimulationConfig.SIOA
        self.SOA = SimulationConfig.SOA
        self.CHEM = SimulationConfig.CHEM
        self.PrepBin = SimulationConfig.PrepBin
        self.BIO = SimulationConfig.BIO
        self.templatepath = SimulationConfig.templatepath
        self.ResolutionProperties = SimulationConfig.ResolutionProperties
        self.SimulationHours = SimulationConfig.SimulationHours
        self.netcdfpath = SimulationConfig.netcdfpath
        self.emissionmodellingpath = SimulationConfig.EmissionModellingPath
        self.EmissionGroup = SimulationConfig.EmissionGroup
        self.EmissionBuildPath_static = SimulationConfig.EmissionBuildPath_static
        self.EmissionBuildPath_dynamic = SimulationConfig.EmissionBuildPath_dynamic
        self.CcamFilePath = SimulationConfig.CcamFilePath
        self.SimulationBasePath = SimulationConfig.SimulationBasePath
        self.SimulationPath = SimulationConfig.SimulationPath
        self.LastSimulationPath = SimulationConfig.LastSimulationPath

        self.PatternLastSimulationDir = SimulationConfig.PatternLastSimulationDir
        self.PatternRestartFileName = SimulationConfig.PatternRestartFileName

        self.CTMBinPath = SimulationConfig.CTMBinPath
        self.CTMBin = SimulationConfig.CTMBin
        self.RuntimeCPU = SimulationConfig.RuntimeCPU
        self.RunTimeHours = SimulationConfig.RunTimeHours
        self.bininitline = SimulationConfig.bininitline

        self.GridIndex = GridIndex
        self.resolution = self.ResolutionNames[self.GridIndex]
        self.IndexNested = self.GridIndex + self.ResolutionProperties[self.resolution]['n_nested_master_grid'] 
        #self.IndexNested = self.GridIndex + self.ResolutionProperties[self.resolution][9] 
        self.resolutionNested = self.ResolutionNames[self.IndexNested]
        #self.Ctm_config_grid_filename = 'ctm_config_grid'+str(self.GridIndex+1)+'.txt'
        #self.Ctm_config_grid_filename = 'ctm_config_grid{gridindex}.txt'.format(gridindex = self.GridIndex+1)
        self.StartMonth = string.lower(self.Startdate.strftime('%b'))
        self.WarmStart = WarmStart
        self.CCAMFlavour = SimulationConfig.CCAMFlavour
        #met config files
        self.meteo_config_filename = ''
        
        self.EmissionGlobalScaling = SimulationConfig.EmissionGlobalScaling
        
        self.templatefile_ConfigGrid = SimulationConfig.templatefile_ConfigGrid
        self.templatefile_Met = SimulationConfig.templatefile_Met
        self.templatefile_Sbatch = SimulationConfig.templatefile_Sbatch
        self.templatefile_Script_Submit = SimulationConfig.templatefile_Script_Submit
        self.SubmitScriptName = SimulationConfig.SubmitScriptName
        self.PartitionName = SimulationConfig.PartitionName

#       Include or not emission groups. Yes by default, but can be modified thereafter
        self.includeemission_vpx = True
        self.includeemission_vdx = True
        self.includeemission_vlx = True
        self.includeemission_vpv = True
        self.includeemission_gse = True
        self.includeemission_whe = True
        self.includeemission_pse = True
        #MEMS
        self.includeemission_MEMS = False
        
        #Dust
        self.Run_Dust = False
        self.includeemission_MEMS_dust = False
        self.DustModel = None
        
        
        return
###########################################################################################
    def init_ConfigGridRestartConfig(self, ConfigurationOptions):
        """ This method initialise the ConfigGridRestartConfig Class which defines the properties of one selected grid for the simulation to feed the CTM prep.
        """
        import DumpConfigGrid as DCG

        logger = self.logger
        justif = self.justif
        ResolutionNames = self.ResolutionNames
        SCENARIO = self.SCENARIO
        Startdate = self.Startdate
        Enddate = self.Enddate
        Emissiondate = self.Emissiondate
        TRANSPORT = self.TRANSPORT
        DIFF = self.DIFF
        SIOA = self.SIOA
        SOA = self.SOA
        CHEM = self.CHEM
        BIO = self.BIO
        templatepath = self.templatepath
        ResolutionProperties = self.ResolutionProperties
        SimulationHours = self.SimulationHours
        netcdfpath = self.netcdfpath
        SimulationPath = self.SimulationPath 
        GridIndex = self.GridIndex
        WarmStart = self.WarmStart
        templatefile = self.templatefile_ConfigGrid
        EmissionGlobalScaling = self.EmissionGlobalScaling

        ConfigGridRestartConfig = DCG.ConfigGridRestartConfigClass(logger, justif, ResolutionNames, SCENARIO, 
                                       Startdate, Enddate, Emissiondate, TRANSPORT, DIFF, SIOA, SOA, CHEM,
                                       BIO, templatepath, templatefile,
                                       ResolutionProperties, SimulationHours, netcdfpath, SimulationPath, GridIndex, WarmStart,
                                       EmissionGlobalScaling, 
                                       self.includeemission_MEMS,
                                       self.Run_Dust, self.includeemission_MEMS_dust, self.DustModel)
        return  ConfigGridRestartConfig          

###########################################################################################
    def init_MetConfig(self):
        """ This method initialise the MetConfig Class which defines the properties of the Meteorological files to feed the CTM prep.
        """
        import GenerateMeteoConfigFile as GMCF
        import os
        logger = self.logger
        logger.warning("Adjusting Met start time to 1200 UTC")
        Startdate = self.Startdate.replace(hour= 12)
        logger.warning("Adjusting Met end time to 1200 UTC")
        #Enddate = self.Enddate.replace(hour= 12)
        Enddate = Startdate + dt.timedelta(days = 4)
        filepath3D = self.CcamFilePath + os.sep
        #fileprefix = 'ccam_' + self.resolution+'_'
        fileprefix = 'ccam_{res}_'.format(res = self.resolution)
        SimulationPath = self.SimulationPath
        templatepath = self.templatepath
        GridIndex = self.GridIndex
        templatefile_Met = self.templatefile_Met
        MetConfig = GMCF.MetConfigClass(logger,Startdate,Enddate,filepath3D,fileprefix,SimulationPath,templatepath,templatefile_Met,GridIndex)
        return MetConfig
    
###########################################################################################
    def init_SBatchConfig(self,meteo_config_filename,**kwargs):
        """ This method initialise the SBatchConfig Class which defines the properties of Slurm Run for each individual grid.
        """
        import WriteSbatch as WSB

        logger=self.logger
        justif = self.justif
        CTMBinPath=self.CTMBinPath
        CTMBin=self.CTMBin
        RuntimeCPU=self.RuntimeCPU
        RunTimeHours=self.RunTimeHours
        GridIndex=self.GridIndex
        templatepath=self.templatepath
        SimulationPath=self.SimulationPath
        ResolutionNames=self.ResolutionNames
        PartitionName=self.PartitionName

        templatefile_Sbatch=self.templatefile_Sbatch
        templatefile_Script_Submit=self.templatefile_Script_Submit
      
        SubmitScriptName=self.SubmitScriptName
        
        SBatchConfig=WSB.SBatchConfigClass(logger, justif,
                                           CTMBin, CTMBinPath, self.bininitline,
                                           RunTimeHours, RuntimeCPU, SimulationPath,meteo_config_filename, 
                                           templatepath, templatefile_Sbatch, templatefile_Script_Submit,
                                           SubmitScriptName, GridIndex, ResolutionNames, 
                                           PartitionName=PartitionName, **kwargs)
        return SBatchConfig 
        
###########################################################################################
    def init_CcamFilesHouseKeeping(self):
        """ This method initialise the CCAM file housekeeping which moves and symlink the CCAM files to feed the CTM.
        """
        import CcamFilesHouseKeeping as CFHK
        import pytz
        
        logger=self.logger
        justif = self.justif
        
        CCAMFlavour = self.CCAMFlavour

        Startdate = self.Startdate
        Enddate = self.Enddate
        AlreadyRenamed = False
        OriginalCCAMdir = self.netcdfpath
        DestinationCCAMdir = self.CcamFilePath
        
        
        # that was when startdate was in AEST
        #local = pytz.timezone('Australia/Sydney')
        #localStartdate=local.localize(Startdate.replace(hour= 00), is_dst=None)
        #UTCStartdate=localStartdate.astimezone(pytz.utc)
        #OriginalCCAMdatepattern=UTCStartdate.strftime("%Y%m%d")
        ##logger.debug('local time is %s, UTC time is %s',localStartdate,UTCStartdate)
        
        #now Startdate is in UTC
        OriginalCCAMdatepattern = Startdate.strftime("%Y%m%d")
        
        #OriginalFileMatchingPattern = 'ctm34S151E??km_'+OriginalCCAMdatepattern+'1200.nc'
        OriginalFileMatchingPattern = 'ctm34S151E??km_{datepattern}1200.nc'.format(datepattern = OriginalCCAMdatepattern)
        CCAMLinkPrefixFilename = 'ccam_'
        GridIndex = self.GridIndex
        ResolutionNames = self.ResolutionNames
        ResolutionProperties = self.ResolutionProperties
        CcamFilesHouseKeeping = CFHK.CcamFilesHouseKeepingClass(logger, justif, Startdate, Enddate, AlreadyRenamed,
                                  OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, 
                                  OriginalFileMatchingPattern, CCAMLinkPrefixFilename, ResolutionProperties,
                                  GridIndex, CCAMFlavour)
        return  CcamFilesHouseKeeping                         
 
###########################################################################################
    def FindLastRunDir(self):
        """ This method find the last dir before the actual date in the form YYYYMMDD.
        """
        import os
        import fnmatch
        import shutil as shu
        logger=self.logger
        FullList=os.listdir(self.SimulationBasePath)
        pattern=self.PatternLastSimulationDir
        datelist=fnmatch.filter(FullList,pattern)
        DirList=[f for f in datelist if os.path.isdir(os.path.join(self.SimulationBasePath,f))]
        if len(DirList) > 1 :
            self.LastSimulationPath=sorted(DirList)[-2]
            logger.info("Last simulation was run on the %s, Restart possible", self.LastSimulationPath)
            logger.info("Restart flag is %s", self.WarmStart)
            if self.WarmStart:
                logger.debug("Editing filename")
                FullList=os.listdir(os.path.join(self.SimulationBasePath,self.LastSimulationPath))
                pattern=self.PatternRestartFileName
                FilteredList=fnmatch.filter(FullList,pattern)
                if FilteredList:
                    logger.debug("filtered Rsstart files list %s",FilteredList)
                    for FF in FilteredList:
                        NewName=FF.rsplit('.')[0].rsplit('_',1)[0]
                        logger.info("copying %s -> %s",os.path.join(self.SimulationBasePath,self.LastSimulationPath,FF),os.path.join(self.SimulationPath,NewName)+".bin")
                        shu.copy(os.path.join(self.SimulationBasePath,self.LastSimulationPath,FF),os.path.join(self.SimulationPath,NewName)+".bin")
                else:
                     logger.warn("No Restart file found, Restart impossible")
                     self.WarmStart=False
        else:
            logger.warn("Last simulation not found, Restart impossible")
            self.WarmStart=False
        return                    


    
###########################################################################################
if __name__ == '__main__':
    import logging
    import InitLogging as IL
    import datetime as dt
    import Domain_definition.ResolutionProperties as RP 
    import Configuration as Conf
    loggername='Classdef'
    logger=IL.Initialise_logging(loggername)
   
    MONTH = 'nov'
    CHEM = 'cb05_aer2'
    SCENARIO = 'forecast'
    SIOA = 'ISORROPIA_IIa'
    SOA = 'VBS2'
    TRANSPORT = 'WALCEK'
    DIFF = 'noNWPDIFFUSIVITY'
    BIO = MONTH
    YEAR = 2016
    PrepBin = '/mnt/project/ccam-ctm/exec/ctm_prep_init_cb05_aer2'
    YEAR_S = 2017
    MONTH_S = 4
    DAY_S = 4
    DAY_S_CTM = 4
    Startdate = dt.datetime(YEAR_S,MONTH_S,DAY_S,00)
    YEAR_EMISSIONS = YEAR_S
    MONTH_EMISSIONS = MONTH
    DAY_N = 5
    HOUR_N = DAY_N * 24
    Enddate = Startdate + dt.timedelta(days=DAY_N,hours=00)
    YEAR_E = Enddate.year
    MONTH_E = Enddate.month
    DAY_E = Enddate.day
    Emissiondate = dt.datetime(YEAR_E,MONTH_E,DAY_E)
    TimestampUTC = dt.datetime.now()
    CCAMFlavour = 'NEWCCAM'
    #Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2008NH3'
    Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2013NH3_PM10'
    FIRE_ID = 'F000'
    timezone = 'UTC'
    #ResolutionProperties = RP.ResolutionProperties()
    ResolutionProperties = RP.ResolutionProperties_new()
    ResolutionNames = ['nsw','gmr','gsyd']
    
    #templatepath = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatepath = '/home/monkk/repositories/ccam-ctm-automation/Python-Scripts/Templates'                
    netcdfpath = '../'
#    EmissionGroup=['vpx','vdx','vlx','vpv','gse','whe']
    #EmissionModellingPath = '/mnt/project/ccam-ctm/emission_modelling'
    EmissionModellingPath='/mnt/climate/cas/project/EmissionSources-Forecast/commonfiles' 
    EmissionBuildPath = './emission'
    ccamfilepath = './ccamfilepath'

    PM10speciation = True

    SimulationBasePath = 'Forecast'

    CTMBin = 'toto'
    CTMBinPath = '/mnt/project/CTM/software/August-16'
    
    RunTimeHours = 1
    PartitionName = 'Full'
    RuntimeCPU = 40 
    PartitionName = 'CAS'
    RuntimeCPU = 72

    ConfigurationOptions = Conf.VarSetup(logger, Startdate, timezone, TimestampUTC, SimulationBasePath, ResolutionNames)

    #fire options
    bushfireOEH=True
    bushfireOEH=False
    emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireOEH'
    bushfireCSIRO=True
    bushfireCSIRO=False
    emissions_rfs_DATA_CSIRO='/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
    burn_start_hour_csiro = 0

    BushfireSource = "Airflow"
    BushfireSource = "Manual"
    Run_HRB = True
    Run_HRB = False
        
    Manual_HRB_Dir = '/mnt/climate/cas/scratch/airflow/emissions/stage/2018-05-24/CSEM_HRB_emission_model--2018-05-24T02-30-48'

    Run_Bushfire = True
    Run_Bushfire = False
    Manual_Bushfire_Dir = None 

    if BushfireSource == "Airflow":
        Manual_HRB_Dir = None 
        Manual_Bushfire_Dir = None 

    ConfigurationOptions.Configure_Bushfire(  
                           BushfireSource, bushfireCSIRO, bushfireOEH, Run_HRB, Run_Bushfire,     
                           emissions_rfs_DATA_CSIRO, burn_start_hour_csiro, 
                           Manual_HRB_Dir, Manual_Bushfire_Dir
                           )
    #Dust options
    DustSource = "Airflow"
    DustSource = "Manual"
    Run_Dust = True
    #Run_Dust = False
    if DustSource == "Manual":
        DustModel = None
        DustModel = 'inline'
        DustModel = 'Chappell'
        Manual_MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
        Manual_MEMS_dust_source_file = None
        
    ConfigurationOptions.Configure_Dust(  
                        DustSource, DustModel, Run_Dust,  
                        Manual_MEMS_dust_source_dir, Manual_MEMS_dust_source_file 
                        )


#
################################
#    SimulationConfig=RC.SimulationConfigClass(ResolutionNames,ResolutionProperties,SCENARIO, \
#            Startdate,EndDate,Emissiondate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,BIO,PrepBin, \
#            templatepath,netcdfpath,EmissionGroup,emissionmodellingpath,emissionbuildpath,\
#            ccamfilepath)
    SimulationConfig = SimulationConfigClass(
                  logger, ResolutionNames, ResolutionProperties, SCENARIO, 
                  Startdate, Enddate, Emissiondate, TimestampUTC, TRANSPORT, 
                  DIFF, SIOA, SOA, CHEM, PrepBin, 
                  templatepath, EmissionModellingPath, EmissionBuildPath, Emissions_edms_DATA, PM10speciation, 
                  SimulationBasePath, ConfigurationOptions, 
                  CTMBinPath, CTMBin, RuntimeCPU, RunTimeHours, PartitionName)

    #Update Bushfire options
    SimulationConfig.Update_Fire_options(ConfigurationOptions)
    #Update Dust options
    SimulationConfig.Update_Dust_options(ConfigurationOptions)
    
    SimulationConfig.ComputeDirectories()
    
    CtmPrepInitConfig = SimulationConfig.init_CtmPrepInitConfig()
    logger.info('Config start %s', CtmPrepInitConfig.Startdate)
    logger.info('CTM prep init config hours number %i', CtmPrepInitConfig.SimulationHours)

    logger.info('Simulation dir = %s',SimulationConfig.SimulationDir)
    
    
    
    logger.info('scenario name= %s', SimulationConfig.SCENARIO)
    index = 1
    warmstart = False
    RunConfig = RunConfigClass(SimulationConfig, index, warmstart)
    logger.info('scenario index= %i', RunConfig.GridIndex)
    logger.info('scenario name= %s', RunConfig.SCENARIO)
    
    MetConfig = RunConfig.init_MetConfig()
    logger.info('Met config start %s', MetConfig.startdate)
    logger.info('Met config hours number %i', MetConfig.hours)
    
    
    RunConfig2=SimulationConfig.init_Run(index,warmstart)
    logger.info('scenario2 index= %i', RunConfig2.GridIndex)
    logger.info('scenario2 name= %s', RunConfig2.SCENARIO)
    
    EmissionConfig=SimulationConfig.init_Emission()
    logger.info('Emission emissions_power_DATA= %s', EmissionConfig.emissions_power_DATA)
    logger.info('Emission startdate= %s', str(EmissionConfig.startdate))
    logger.info('Emission timeresolvedcoalpower= %s', EmissionConfig.timeresolvedcoalpower)
    
