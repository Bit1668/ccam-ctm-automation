import datetime as dt
import jinja2
import JinjaUtils as JJU
import os

class MetConfigClass(object):
    """ Configure MetConfig Class which defines the properties of the Meteorological files to feed the CTM prep.
    """
    def __init__(self,logger,Startdate,Enddate,filepath3D,fileprefix,SimulationPath,templatepath,templatefile,index):
        """ 
        This class defines a Meteorological object, that contains all the 
        parameters to configure the CTM run for every grid

        Attributes
        -----------
        logger : logging.logger
            instance of a logger to output messages.
        Startdate : datetime.datetime
            date to indicate when the forecast of metfile starts
        Enddate : datetime.datetime
            date to indicate when the forecast of metfile stops
        filepath3D : str
            path to where to find the met files. Should be with a trailing '/'. 
        fileprefix : str
            str representing the generic name of the metfile.
        SimulationPath : str
            str to represent the path of the main simulation dir.
        templatepath : str
            path to find the templates.
        templatefile : str
            filename of the template
        index : int
            grid number ID in the downscaling process.
        """ 
        self.logger = logger
#        self.days=(Enddate-Startdate).days
#        self.hours='25'
        
        #how many hours per file read 
        self.hours = int((Enddate-Startdate).total_seconds() // 3600)
        #days is actually the number of met file to read
        self.days = '1'
        
        self.individualhourlyfiles = 'F'
        self.timeinterval = '1'
        self.filepath2D = 'NONE'
        self.filepath3D = filepath3D
        self.fileprefix = fileprefix
        self.SimulationPath = SimulationPath
        self.templatepath = templatepath
#        self.templatefile='Meteo-Config-Files.template'
        self.templatefile = templatefile
        self.meteo_config_filename = 'gen_meteo_config_'+str(index)+'.txt'
        self.startdate = Startdate.strftime('%Y %m %d %H')
        return
        
    def Build_Meteo_config_File(self):
        """ This function write the config file for processing the Met files
        """
        
        self.logger.info("Generating the Configuration file for the Met files")
        
        filename = os.path.join(self.SimulationPath,self.meteo_config_filename)
        File = open(filename,'w')

        template = JJU.LoadTemplate(self.templatepath,self.templatefile)

        config = template.render({'MetConfig':self })
        File.write(config)
        File.close()
        self.logger.debug("Configuration file for the Met files generated")
        return 
        
#####################################################################################################################################
if __name__ == '__main__':

    import RunConfig as RC
    import logging
    import InitLogging as IL
    import DumpConfigGrid as DCG
    loggername='GenMet'
    logger=IL.Initialise_logging(loggername)
   
##############################################
# scenarios and options
##############################################
    YEAR=2016
    YEAR_S=2017
    MONTH_S=4
    DAY_S=4
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)
    DAY_N=3
    Enddate=Startdate+dt.timedelta(days=DAY_N,hours=00)
    
    ResolutionNames=['nsw','gmr','gsyd']
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    netcdfpath='../'
    ccamfilepath='./'
    SimulationPath='./Forecast'
    ccamfilepath='./'
    templatefile='Meteo-Config-Files.template'
##############################################

    for GridIndex in range(3):
        resolution=ResolutionNames[GridIndex]
        fileprefix='ccam_'+resolution
        MetConfig=MetConfigClass(logger,Startdate,Enddate,ccamfilepath,fileprefix,SimulationPath,templatepath,templatefile,GridIndex)
        MetConfig.Build_Meteo_config_File()

