"""
.. module:: CcamFilesHouseKeeping
   :platform: Unix
   :synopsis: Everything to cater files for CCAM and linking/renaming.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import os
import datetime as dt
import subprocess
import numpy as np
import fnmatch
import sys
import json

##########################################################################################################################
class CcamFilesHouseKeepingClass(object):
    ''' CCAM files manipulation and renaming.
    '''
    def __init__(self, logger, justif, 
                 Startdate, Enddate, AlreadyRenamed, 
                 OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, ResolutionProperties,
                 CCAM_input_flow, CCAM_link_name_template,
                 CCAMLinkPrefixFilename, CCAMFlavour,
                 StatusJSONfile=None):
        """ 
        This class defines a Meteorological object, that contains all the 
        parameters to configure the CTM run for every grid

        Attributes
        -----------
        logger : logging.logger
            instance of a logger to output messages.
        justif : int
            max message width to justify logger output.   
        Startdate : datetime.datetime
            date to indicate when the forecast starts
        Enddate : datetime.datetime
            date to indicate when the forecast stops
        AlreadyRenamed : bool
            Yes/no trigger to be able to cater CCAM file when they have already been renamed. Offered in promotion for legacy. 
        OriginalCCAMdir : str
            path to the source CCAM dir.
        DestinationCCAMdir : str
            path to where the symlink are to be.
        ResolutionNames : list of str
            list of the resolution names to be run. Must match the dictionary of resolutions ResolutionProperties.
        ResolutionProperties : dict
            Dictionnary of each resolution parametre.
        CCAM_input_flow : str
            input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA".
        CCAM_link_name_template : str
            template of the link name that will be created in the simulation Met dir.
        CCAMFlavour : str
            determine how the ccam files are formatted. 
            2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'.
        StatusJSONfile : str
            fullpath filename where to find the CCAM json file when CCAM_input_flow is "Forecast_Linux".

        """ 
        self.logger = logger
        self.justif = justif
        
        self.Startdate = Startdate
        self.Enddate = Enddate
        self.AlreadyRenamed = AlreadyRenamed
        self.OriginalCCAMdir = OriginalCCAMdir
        self.DestinationCCAMdir = DestinationCCAMdir
        self.ResolutionNames = ResolutionNames
        self.ResolutionProperties = ResolutionProperties
        self.CCAM_input_flow = CCAM_input_flow

        self.NumberOfDaysForecast = (self.Enddate-self.Startdate).days
        self.CCAMLinkPrefixFilename = CCAMLinkPrefixFilename
        self.CCAMFlavour = CCAMFlavour
        self.StatusJSONfile = StatusJSONfile

        #self.CCAM_link_name_template = '{prefix}{resolution}_{date}.nc'
        self.CCAM_link_name_template = CCAM_link_name_template
        self.CCAM_Forecast_file_pattern = "ctm{lat}S{lon}E{resolution}km_{date}.nc"
        self.CCAM_ERA_file_pattern = "ccam_{date}.nc"
        
        #######
        return

################################################################################
    def Building_original_CCAM_search_pattern(self, resolutionname, date):
        '''
        define the search pattern to find the original CCAM files
        differs if Forecast files or ERA_int
        '''
        if self.CCAM_input_flow == "Forecast":
            Filepattern = self.CCAM_Forecast_file_pattern
            
            #lat = np.rint(np.abs(self.ResolutionProperties[resolutionname]['SW_corner_lat']))
            #lon = np.rint(np.abs(self.ResolutionProperties[resolutionname]['SW_corner_lon']))
            #resolution = np.rint(self.ResolutionProperties[resolutionname]['resolution_m'] / 1000)
            
            cornerlat = self.ResolutionProperties[resolutionname]['SW_corner_lat']
            cornerlon = self.ResolutionProperties[resolutionname]['SW_corner_lon']
            
            centrelat = cornerlat + self.ResolutionProperties[resolutionname]['dy_deg'] * self.ResolutionProperties[resolutionname]['nlat'] * 0.5
            centrelon = cornerlon + self.ResolutionProperties[resolutionname]['dx_deg'] * self.ResolutionProperties[resolutionname]['nlon'] * 0.5

            lat = np.asarray(np.rint(np.abs(centrelat)),  dtype = np.int16)
            lon = np.asarray(np.rint(np.abs(centrelon)),  dtype = np.int16)
            
            resolution = np.asarray(np.rint(self.ResolutionProperties[resolutionname]['resolution_m'] / 1000),  dtype = np.int16)
            
            self.OriginalFileMatchingPattern = Filepattern.format(
                                               lat = lat,
                                               lon = lon,
                                               resolution = str(resolution).zfill(2),
                                               date = date.strftime('%Y%m%d%H%M')
                                               )
        #########################################                                       
        elif self.CCAM_input_flow == "ERA":
            Filepattern = self.CCAM_ERA_file_pattern
            self.OriginalFileMatchingPattern = Filepattern.format(
                                               date = date.strftime('%Y%m%d')
                                               )
        
        #########################################                                       
        elif self.CCAM_input_flow == "Forecast_Linux": 
            if os.path.isfile(self.StatusJSONfile):
                with open(self.StatusJSONfile, 'r') as handle:
                    struct = json.load(handle)
                    self.OriginalCCAMdir = struct[resolutionname]["Pcc2hist_ConversionOutputFilePath"]
                    self.OriginalFileMatchingPattern = struct[resolutionname]['Pcc2hist_ConversionOutputfilename']
            else:
                self.logger.error("ERROR")
                self.logger.error("NO LINUX FORECAST MET JSON FILE FOUND")
                self.logger.error("You need to find %s",self.StatusJSONfile )
                sys.exit("Sorry :(")      
             
        #########################################                                       
        else:
            self.logger.error("No configuration pattern for the CCAM input flow")
            sys.exit("Sorry :(")
        return          

################################################################################
    def Moving_one_file(self, date, resname, listdir, listdestinationdir):
        """
        Wrapper to check if the destination file exist and if not, move the file
        """
        self.Building_original_CCAM_search_pattern(resname, date)
        
        filtereddestinationlist = fnmatch.filter(listdestinationdir, self.OriginalFileMatchingPattern)
        if filtereddestinationlist:
            self.logger.warning("WARNING")
            self.logger.warning("TRYING TO MOVE MET FILE(S)")
            self.logger.warning("MET FILE(S) FOUND IN DESTINATION DIR")
            self.logger.warning("File(s) %s already exist(s)", self.OriginalFileMatchingPattern)
            self.logger.warning("In the following dir %s", os.path.join(self.OriginalCCAMdir, resname))
            self.logger.warning("CANNOT MOVE FILE" )
        else:
            filteredlist = fnmatch.filter(listdir, self.OriginalFileMatchingPattern)
                      
            if filteredlist:
                for filename in filteredlist:
                    try:
                        originalfile = os.path.join(self.OriginalCCAMdir, filename)
                        destinationfile = os.path.join(self.OriginalCCAMdir, resname, filename)
                        self.logger.info('Moving %s -> %s', originalfile, destinationfile)
                        os.rename(originalfile, destinationfile)
                    except:
                        self.logger.debug("something went wrong when moving %s", originalfile)
                                    
            else:
                self.logger.warning("WARNING")
                self.logger.warning("NOT ABLE TO MOVE MET FILE(S)")
                self.logger.warning("NO MET FILE(S) FOUND")
                self.logger.warning("You need to find %s", self.OriginalFileMatchingPattern)
                self.logger.warning("In the following dir %s", self.OriginalCCAMdir)
        return
                                                           
################################################################################
    def House_keeping_original_CCAM(self):
        """
        behaviour depends on the input flow:
        try to move the forecast files into subdir if CCAM_input_flow == "Forecast",
        in this case it's one file per forecast per domain. 
        
        or do nothing if CCAM_input_flow == "ERA" or "Forecast_Linux"
        """
        if self.CCAM_input_flow == "Forecast":
            
            listdir = os.listdir(self.OriginalCCAMdir)
            for resname in self.ResolutionNames:
                self.CreateOriginalDir(resname)
                listdestinationdir = os.listdir(os.path.join(self.OriginalCCAMdir, resname))
                
                #########
                if self.CCAMFlavour == 'OneFile/Forecast/Domain':
                    date = self.Startdate
                    self.Moving_one_file(date, resname, listdir, listdestinationdir)    
                ##########
                if self.CCAMFlavour == 'OneFile/Day/Domain':
                    for nday in np.arange(self.NumberOfDaysForecast + 1):
                        date = self.Startdate + dt.timedelta(days = nday)
                        self.Moving_one_file(date, resname, listdir, listdestinationdir)    

        elif self.CCAM_input_flow == "ERA":
            self.logger.info("ERA interim met files, no need to move them")

        elif self.CCAM_input_flow == "Forecast_Linux": 
            if os.path.isfile(self.StatusJSONfile):
                with open(self.StatusJSONfile, 'r') as handle:
                    struct = json.load(handle)
                    for resname in self.ResolutionNames:
                        self.OriginalCCAMdir = struct[resname]["Pcc2hist_ConversionOutputFilePath"]
            self.logger.info("CCAM Linux Forecast met files, no need to move them")

        else:
            self.logger.error("Met file input flow not defined or unknown")
            sys.exit("Sorry :(")      
             
           
        return

################################################################################
    def symlink_onefile(self, original, link):
        """
        Method that unlink then relink original into link
        """
        try:
            os.unlink(link)
            self.logger.info('Cleaning {msg1}'.format(msg1 = link).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Cleaning {msg1}'.format(msg1 = link).ljust(self.justif-12,'.') + 'Already done')

        try:
            os.symlink(original, link)
            self.logger.info('linking {msg} -> {msg1}'.format(msg = original, msg1 = link).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('file link = {msg}'.format(msg = link).ljust(self.justif-13,'.') + 'Already exist')
        return

################################################################################
    def wrappersymlink(self, origindir, listdir, date, resname):
        """
        Method that take care of linking one file
        """
        self.Building_original_CCAM_search_pattern(resname, date)
                 
        filteredlist = fnmatch.filter(listdir, self.OriginalFileMatchingPattern)
        if filteredlist:
            for filename in filteredlist:
                      
                filelinkname = self.CCAM_link_name_template.format(
                                    prefix = self.CCAMLinkPrefixFilename,
                                    resolution = resname, 
                                    date = date.strftime('%Y%m%d')
                                    )
                original = os.path.join(origindir, filename)
                link = os.path.join(self.DestinationCCAMdir, filelinkname)
                        
                self.symlink_onefile(original, link)
        else:
            self.logger.error("ERROR")
            self.logger.error("NO MET FILE(S) FOUND")
            self.logger.error("You need to find %s",self.OriginalFileMatchingPattern )
            self.logger.error("In the following dir %s", origindir )
            sys.exit("Sorry :(")      
        return      

################################################################################
    def wrapperUnlink(self, date):
        """
        Method that take care of linking one file
        """
        filelinkname = self.CCAM_link_name_template.format(
                                    prefix = self.CCAMLinkPrefixFilename,
                                    resolution = resname, 
                                    date = date.strftime('%Y%m%d')
                                    )
        link = os.path.join(self.DestinationCCAMdir, filelinkname)
                        
        try:
            os.unlink(link)
            self.logger.info('Cleaning {msg1}'.format(msg1 = link).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Cleaning {msg1}'.format(msg1 = link).ljust(self.justif-12,'.') + 'Already done')
        return      

################################################################################
    def SymLinkingOriginalCCAMFiles(self):
        ''' 
        Method to symlink the original CCAM file, coming fom ERA_int or Forecast to the Met dir inside the Run directory
        The linking also symplifies the naming convention, going from 
        ctm34S151E01km_201611231200.nc to ctm_20161123.nc
        '''

        self.CreateDestinationDir()
        for resname in self.ResolutionNames:
            if self.CCAM_input_flow == "Forecast":
                origindir = os.path.join(self.OriginalCCAMdir, resname)
            elif self.CCAM_input_flow == "ERA" or self.CCAM_input_flow == "Forecast_Linux":
                origindir = self.OriginalCCAMdir
            #origindir = os.path.join(self.OriginalCCAMdir, resname)
            else:
                self.logger.error("No valid CCAM input flow")
                origindir = None    
            
            listdir = os.listdir(origindir)
            #########
            if self.CCAMFlavour == 'OneFile/Forecast/Domain':
                date = self.Startdate
                self.wrappersymlink(origindir, listdir, date, resname) 
            ##########
            if self.CCAMFlavour == 'OneFile/Day/Domain':
                for nday in np.arange(self.NumberOfDaysForecast + 1):
                    date = self.Startdate + dt.timedelta(days = nday)
                    self.wrappersymlink(origindir, listdir, date, resname) 
        return
##########################################################################################################################
    def UnLinkingOriginalCCAMFiles(self):
        ''' 
        Method to unlink the CCAM file in the Met dir inside the Run directory
        '''

        for resname in self.ResolutionNames:
            #########
            if self.CCAMFlavour == 'OneFile/Forecast/Domain':
                date = self.Startdate
                self.wrapperUnlink(date) 
            ##########
            if self.CCAMFlavour == 'OneFile/Day/Domain':
                for nday in np.arange(self.NumberOfDaysForecast + 1):
                    date = self.Startdate + dt.timedelta(days = nday)
                    self.wrapperUnlink(date) 
        return
##############################################3
#        
#        my_env = os.environ.copy()
#        
#        listdir=os.listdir(os.path.join(self.OriginalCCAMdir,self.resolution))
#        filteredlist=fnmatch.filter(listdir,self.OriginalFileMatchingPattern)
#        if filteredlist:
#            for filename in filteredlist:
#                logger.info("file in original ccam dir %s",filename)
#        
##        for filename in os.listdir(os.path.join(self.OriginalCCAMdir,self.resolution)):
##            logger.debug("file in original ccam dir %s",filename)
##            if fnmatch.fnmatch(filename, self.OriginalFileMatchingPattern):
#                split = str.rsplit(filename,'_')
#                res = split[0][-4:]
#                stringdate = split[1][:-7]
#                date = dt.datetime.strptime(stringdate,'%Y%m%d')
#                
#                # symlink to the file, one per day, so making NumberOfDaysForecast symlink to the same file, with different link names
#                NumberOfDaysForecast = (self.Enddate-self.Startdate).days
#                
#                # when newccam, only one link to one met file!
#                if self.CCAMFlavour == 'NEWCCAM': NumberOfDaysForecast = 0
#                
#                #iteration on the number of forecast days
#                for ii in range(NumberOfDaysForecast+1):
#                    dd = date + dt.timedelta(days=ii)
#                    #filelinkname = self.CCAMLinkPrefixFilename+self.resolution+'_'+dd.strftime('%Y%m%d')+'.nc'
#                    filelinkname = self.CCAM_link_name_template.format(
#                                    prefix = self.CCAMLinkPrefixFilename,
#                                    resolution = self.resolution, 
#                                    date = dd.strftime('%Y%m%d')
#                                    )
#                    
#                    if ii == 0: filelinknameday1 = filelinkname
#                    
#                    try:
#                        os.unlink(os.path.join(self.DestinationCCAMdir,filelinkname))
#                    except:
#                        pass    
#                        
#                    # if NewCCAM, then split met file per day
#                    if self.CCAMFlavour == 'NEWCCAM-SplitFiles':
#                        logger.info('splitting %s/%s/%s ->  %s/%s',self.OriginalCCAMdir,self.resolution,filename,self.DestinationCCAMdir,filelinkname)
#                        logger.warning("USING NEW CCAM Flavour and splitting met files") 
#                        #giving a temporary name to the first file for the future merge to get 25 hours in the first file
#                        if ii == 0 : 
#                            filelinknameday1 = filelinkname
#                            filelinkname = filelinkname+'-temp'
#                        try:
#                            CDOargumentline = "cdo select,startdate={startdate},enddate={startdate}T23:59:59 {inputfile} {outputfile}".format(   \
#                                startdate=dd.strftime("%Y-%m-%d"),inputfile=os.path.join(self.OriginalCCAMdir,self.resolution,filename), \
#                                outputfile=os.path.join(self.DestinationCCAMdir,filelinkname))
#                            #print  CDOargumentline   
#                            print subprocess.Popen([CDOargumentline],shell=True, env=my_env, cwd=self.DestinationCCAMdir).wait()
#                            #duplicate second file into first, with time shift then merge to create 25 hours for the first day
#                            if ii == 1:
#                                #split to get the first 12 hours
#                                filesplitname=filelinkname+'-split'
#                                CDOargumentline = "cdo select,startdate={startdate},enddate={startdate}T12:59:59 {inputfile} {outputfile}".format(   \
#                                    startdate=dd.strftime("%Y-%m-%d"),inputfile=os.path.join(self.DestinationCCAMdir,filelinkname), \
#                                    outputfile=os.path.join(self.DestinationCCAMdir,filesplitname))
#                                print subprocess.Popen([CDOargumentline],shell=True, env=my_env, cwd=self.DestinationCCAMdir).wait()
#                                #shift 1 day backwards
#                                fileshifted=filelinkname+'-TempShifted.nc'
#                                CDOargumentline = "cdo shifttime,-1day {inputfile} {outputfile}".format(   \
#                                    inputfile=os.path.join(self.DestinationCCAMdir,filesplitname), \
#                                    outputfile=os.path.join(self.DestinationCCAMdir,fileshifted))
#                                print subprocess.Popen([CDOargumentline],shell=True, env=my_env, cwd=self.DestinationCCAMdir).wait()
#                                #merge with the first file
#                                CDOargumentline = "cdo mergetime {inputfile1} {inputfile2} {outputfile}".format(   \
#                                    inputfile1=os.path.join(self.DestinationCCAMdir,filesplitname), \
#                                    inputfile2=os.path.join(self.DestinationCCAMdir,filelinknameday1+'-temp'), \
#                                    outputfile=os.path.join(self.DestinationCCAMdir,filelinknameday1))
#                                print subprocess.Popen([CDOargumentline],shell=True, env=my_env, cwd=self.DestinationCCAMdir).wait()
#                                   
#                        except:
#                            logger.error("Met file splitting went wrong")
#                            logger.error("trying to split %s into %s for day %s", \
#                                    os.path.join(self.OriginalCCAMdir,self.resolution,filename),os.path.join(self.DestinationCCAMdir,filelinkname),dd.strftime('%Y%m%d'))
#                            sys.exit("sorry, don't forget to load CDO module!!! :(  ")        
#                    else:
#                        logger.info('Linking %s/%s/%s ->  %s/%s',self.OriginalCCAMdir,self.resolution,filename,self.DestinationCCAMdir,filelinkname)
#                        logger.warning("USING OLD CCAM Flavour and symlinking met files" )
#                        try:
#                            os.symlink(os.path.join(self.OriginalCCAMdir,self.resolution,filename),os.path.join(self.DestinationCCAMdir,filelinkname))
#                        except:
#                            logger.debug("file link already exist")
#        
#        else:
#            logger.error("ERROR")
#            logger.error("NO MET FILE(S) FOUND")
#            logger.error("You need to find %s",self.OriginalFileMatchingPattern )
#            logger.error("In the following dir %s", os.path.join(self.OriginalCCAMdir,self.resolution)  )
#            sys.exit("Sorry :(")      
#        
#        return

##########################################################################################################################
#    def UnLinkingOriginalCCAMFiles(self):
#        ''' Going from ctm34S151E01km_201611231200.nc to ctm_20161123.nc and expanding instead of copying-renaming
#        '''
#        import fnmatch
#        logger = self.logger
#
#        listdir=os.listdir(os.path.join(self.OriginalCCAMdir,self.resolution))
#        filteredlist=fnmatch.filter(listdir,self.OriginalFileMatchingPattern)
#        if filteredlist:
#            for filename in filteredlist:
#                logger.info("file in original ccam dir %s",filename)
#
##        for filename in os.listdir(os.path.join(self.OriginalCCAMdir,self.resolution)):
##            logger.debug("Trying to unlinking file in original ccam dir %s",filename)
##            if fnmatch.fnmatch(filename, self.OriginalFileMatchingPattern):
#
#                split = str.rsplit(filename,'_')
#                res = split[0][-4:]
#                stringdate = split[1][:-7]
#                date = dt.datetime.strptime(stringdate,'%Y%m%d')
#                # symlink to the file, one per day, so making NumberOfDaysForecast symlink to the same file, with different link names
#                NumberOfDaysForecast = (self.Enddate-self.Startdate).days
#
#                for ii in range(NumberOfDaysForecast+1):
#                    dd = date+dt.timedelta(days=ii)
#                    filelinkname = self.CCAM_link_name_template.format(
#                                    prefix = self.CCAMLinkPrefixFilename,
#                                    resolution = self.resolution, 
#                                    date = dd.strftime('%Y%m%d')
#                                    )
#                    #filelinkname = self.CCAMLinkPrefixFilename+self.resolution+'_'+dd.strftime('%Y%m%d')+'.nc'
#                    logger.info('Unlinking %s/%s/%s ->  %s/%s',self.OriginalCCAMdir,self.resolution,filename,self.DestinationCCAMdir,filelinkname)
#                    try:
#                        os.unlink(os.path.join(self.DestinationCCAMdir,filelinkname))
#                    except:
#                        logger.warning("file link does not exist")    
#        return			
#
################################################################################
    def CreateDestinationDir(self):
        ''' 
        This method build the ccam link path.
        '''
        try :
            os.makedirs(self.DestinationCCAMdir)
            self.logger.info('Creation Met Dir= {msg}'.format(msg=self.DestinationCCAMdir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Met Dir = {msg}'.format(msg=self.DestinationCCAMdir).ljust(self.justif-13,'.') + 'Already exist')
            pass
        
################################################################################
    def CreateOriginalDir(self, resname):
        ''' 
        This method build the original ccam path for housekeeping: OriginalCCAMdir/resolutionname.
        '''
        try :
            dirdir = os.path.join(self.OriginalCCAMdir, resname)
            os.makedirs(dirdir)
            self.logger.info('Creation Met Dir= {msg}'.format(msg=dirdir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Met Dir = {msg}'.format(msg=dirdir).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

##########################################################################################################################
    def LinkingFilesPreRunAlreadyRenamedPerResolution(self):
        ''' This method creates the ccam files links. Files have already been renamed and put in the working dir
        '''
    #files already renamed ccam_res.nc 
        logger = self.logger
        files=glob.glob(os.path.join(self.DestinationCCAMdir,'*.nc'))
        for ff in files:
            logger.info('linking %s ->  %s',ff,os.path.basename(ff))
            os.symlink(ff, os.path.basename(ff))
        return   

##########################################################################################################################
    def CleaningFilesPreRunAlreadyRenamedPerResolution(self):
        ''' This method removes the ccam files links. Files have already been renamed and put in the working dir
        '''
    #files already renamed ccam_res.nc 
        logger = self.logger
        files=glob.glob(os.path.join(self.DestinationCCAMdir,'*.nc'))
        for ff in files:
            try:
                logger.info('linking %s ->  %s',ff,os.path.basename(ff))
                os.unlink(ff, os.path.basename(ff))
            except:
                logger.info('link %s does not exist',os.path.basename(ff))
                pass
        return   
##########################################################################################################################
    def MovingOriginalCCAMFilesPreRun(basedirsymlink,year):
        ''' moving self.OriginalCCAMdir/ctm34S151E01km_201611231200.nc to symlink self.DestinationCCAMdir directory for each resolution
        '''
        logger = self.logger
        files=glob.glob(os.path.join(self.OriginalCCAMdir,'*.nc'))
        for ff in files:
            filename=os.path.basename(ff)
            #split the filename
            split=str.rsplit(filename,'_')
            resolution=(split[0][-4:]).lstrip('0')
            stringdate=split[1][:-7]
            date=dt.datetime.strptime(stringdate,'%Y%m%d')
		    #move the file
            logger.info('Renaming %s into %s',ff,os.path.join(self.DestinationCCAMdir,filename))
            os.rename(ff,os.path.join(self.DestinationCCAMdir,filename))
        return			

##########################################################################################################################
    def MovingOriginalCCAMFiles(self):
        ''' moving from self.OriginalCCAMdir/ctm34S151E01km_201611231200.nc to self.OriginalCCAMdir/res/ctm34S151E01km_201611231200.nc
        '''
        import fnmatch
        import sys
        logger = self.logger
        
        MatchingPattern=self.OriginalFileMatchingPattern[:10]+self.ResolutionProperties[self.resolution]['resolution_km'][:-2].zfill(2) +self.OriginalFileMatchingPattern[12:]
        listdir=os.listdir(self.OriginalCCAMdir)
        filteredlist=fnmatch.filter(listdir,MatchingPattern)

        listdestinationdir=os.listdir(os.path.join(self.OriginalCCAMdir,self.resolution))
        
        filtereddestinationlist=fnmatch.filter(listdestinationdir,MatchingPattern)
        if filtereddestinationlist:
            logger.warning("WARNING")
            logger.warning("TRYING TO MOVE MET FILE(S)")
            logger.warning("MET FILE(S) FOUND IN DESTINATION DIR")
            logger.warning("File(s) %s already exist(s)",MatchingPattern)
            logger.warning("In the following dir %s", os.path.join(self.OriginalCCAMdir,self.resolution)  )
            logger.warning("CANNOT MOVE FILE" )
            return      

        if filteredlist:
            for filename in filteredlist:
                logger.debug("file in original ccam dir %s",filename)
                try:
                    originalfile=os.path.join(self.OriginalCCAMdir,filename)
                    destinationfile=os.path.join(self.OriginalCCAMdir,self.resolution,filename)
                    logger.info('Moving %s -> %s',originalfile,destinationfile)
                    os.rename(originalfile,destinationfile)
                except:
                    logger.debug("something went wrong when moving %s",originalfile)
                    pass    
        
        else:
            logger.warning("WARNING")
            logger.warning("NOT ABLE TO MOVE MET FILE(S)")
            logger.warning("NO MET FILE(S) FOUND")
            logger.warning("You need to find %s",MatchingPattern )
            logger.warning("In the following dir %s", self.OriginalCCAMdir  )
        
        return
 
###########################################################################################
if __name__ == '__main__':
    import RunConfig as RC
    import logging
    import InitLogging as IL
    import datetime as dt
    import ResolutionProperties as RP 
    loggername='CCAMhousekeeping'
    logger=IL.Initialise_logging(loggername)
    justif = 102
    CCAMFlavour = 'NEWCCAM'
    
    MONTH='nov'
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
    BIO=MONTH
    YEAR=2016
    PrepBin='/mnt/project/ccam-ctm/exec/ctm_prep_init_cb05_aer2'
    YEAR_S=2016
    MONTH_S=11
    DAY_S=23
    DAY_S_CTM=4
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)
    YEAR_EMISSIONS=YEAR_S
    MONTH_EMISSIONS=MONTH
    DAY_N=3
    HOUR_N = DAY_N * 24
    EndDate=Startdate+dt.timedelta(days=DAY_N,hours=00)
    YEAR_E=EndDate.year
    MONTH_E=EndDate.month
    DAY_E=EndDate.day
    Emissiondate=dt.datetime(YEAR_E,MONTH_E,DAY_E)
    
    ResolutionProperties=RP.ResolutionProperties_new()
    ResolutionNames=['nsw','gmr','gsyd']
    
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    netcdfpath='./TestFiles/OriginalCCAM'
    netcdfpath='/mnt/climate/cas/project/ccam/ccam_output/forecast2017'
#    EmissionGroup=['vpx','vdx','vlx','vpv','gse','whe']
    emissionmodellingpath='mnt/project/ccam-ctm/emission_modelling'
    emissionbuildpath='./emission'
    ccamfilepath='./ccamfilepath'

    SimulationPath='Forecast'

    CTMBin='toto'
    CTMBinPath='/mnt/project/CTM/software/August-16'
    
    RunTimeHours=1
    RuntimeCPU=40
    PartitionName='Full'
    RuntimeCPU=40 
    PartitionName='CAS'
    RuntimeCPU=72
#
################################
    GridIndex=1
    AlreadyRenamed=False
    OriginalCCAMdir=netcdfpath
    DestinationCCAMdir=ccamfilepath
    OriginalCCAMdatepattern=Startdate.strftime("%Y%m%d")
    OriginalFileMatchingPattern='ctm34S151E??km_'+OriginalCCAMdatepattern+'*.nc'
#    OriginalFileMatchingPattern='ctm*'
    CCAMLinkPrefixFilename='ccam_'
    
    logger.debug('-'*80)
    logger.debug('Testing CCAM Housekeeping')
    for GridIndex in range(3):
        resolution=ResolutionNames[GridIndex]
        CcamFilesHouseKeeping=CcamFilesHouseKeepingClass(logger, justif, Startdate, EndDate,
                                   AlreadyRenamed, OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, 
                                   OriginalFileMatchingPattern, CCAMLinkPrefixFilename, ResolutionProperties, 
                                   GridIndex, CCAMFlavour)

        logger.debug('-'*80)
        logger.debug('Testing Unlinking')
        #CcamFilesHouseKeeping.UnLinkingOriginalCCAMFiles()
        logger.debug('-'*80)
        logger.debug('Creating Work dir')
        CcamFilesHouseKeeping.CreateDestinationDir()

        logger.debug('using touch to create CCAM test files')
        filename='ctm34S151E60km_201611231200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,resolution,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        filename='ctm34S151E60km_201611241200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,resolution,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        filename='ctm34S151E60km_201611251200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,resolution,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        logger.debug('-'*80)
        logger.debug('Test linking')
        #CcamFilesHouseKeeping.SymLinkingOriginalCCAMFiles()
    
        logger.debug('using touch to create CCAM test files')
        filename='ctm34S151E60km_201611231200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        logger.debug('-'*80)
        logger.debug('Test moving')
        CcamFilesHouseKeeping.MovingOriginalCCAMFiles()

#    SimulationConfig=RC.SimulationConfigClass(logger,ResolutionNames,ResolutionProperties,SCENARIO, \
#            Startdate,EndDate,Emissiondate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,BIO,PrepBin, \
#            templatepath,netcdfpath,emissionmodellingpath,emissionbuildpath,\
#            ccamfilepath,SimulationPath, \
#            CTMBinPath,CTMBin,RuntimeCPU,RunTimeHours,PartitionName)
#    index=1
#    warmstart=False
#    RunConfig=RC.RunConfigClass(SimulationConfig,index,warmstart)

