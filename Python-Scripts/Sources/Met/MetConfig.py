"""
.. module:: MetConfig
   :platform: Unix
   :synopsis: Class to configure, manipulate met file and write the config file for CTM.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>
"""
from __future__ import absolute_import
import os
import datetime as dt
import numpy as np
import fnmatch
import sys
from . import CcamFilesHouseKeeping as CFHK
try:
    from .. import JinjaUtils as JJU
except:
    sys.path.insert(0,'..')
    import JinjaUtils as JJU   
##########################################################################################################################
class MetConfigClass(object):
    ''' 
    Class to configure, manipulate met file and write the config file for CTM.
    '''
    def __init__(self, logger, justif, 
                 Startdate, Enddate, AlreadyRenamed, 
                 SimulationPath, templatepath, templatefile,
                 OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, ResolutionProperties,
                 CCAM_input_flow,
                 CCAMLinkPrefixFilename, CCAMFlavour,
                 StatusJSONfile=None):
        """ 
        This class defines a Meteorological object, that contains all the 
        parameters to configure the CTM run for every grid

        Attributes
        -----------
        logger : logging.logger
            instance of a logger to output messages.
        justif : int
            max message width to justify logger output.   
        Startdate : datetime.datetime
            date to indicate when the forecast starts
        Enddate : datetime.datetime
            date to indicate when the forecast stops
        AlreadyRenamed : bool
            Yes/no trigger to be able to cater CCAM file when they have already been renamed. Offered in promotion for legacy. 
        OriginalCCAMdir : str
            path to the source CCAM dir.
        DestinationCCAMdir : str
            path to where the symlink are to be.
        ResolutionNames : list of str
            list of the resolution names to be run. Must match the dictionary of resolutions ResolutionProperties.
        ResolutionProperties : dict
            Dictionnary of each resolution parametre.
        CCAM_input_flow : str
            input flow of the CCAM, can be "Forecast", "Forecast_Linux" or "ERA"
        CCAMFlavour : str
            determine how the ccam files are formatted. 
            2 options so far, 'OneFile/Forecast/Domain' or 'OneFile/Day/Domain'
        CCAMLinkPrefixFilename : str
            prefix of the symbolic link of the ccam file in the SimulationPath/DestinationCCAMdir met dir.
        SimulationPath : str
            str to represent the path of the main simulation dir.
        templatepath : str
            path to find the templates.
        templatefile : str
            filename of the template
        StatusJSONfile : str
            fullpath filename where to find the CCAM json file when CCAM_input_flow is "Forecast_Linux"
        """ 
        self.logger = logger
        self.justif = justif
        
        self.Startdate = Startdate
        self.Enddate = Enddate
        self.AlreadyRenamed = AlreadyRenamed
        self.OriginalCCAMdir = OriginalCCAMdir
        self.DestinationCCAMdir = DestinationCCAMdir
        self.ResolutionNames = ResolutionNames
        self.ResolutionProperties = ResolutionProperties
        self.CCAM_input_flow = CCAM_input_flow

        self.NumberOfDaysForecast = (self.Enddate-self.Startdate).days
        self.CCAMLinkPrefixFilename = CCAMLinkPrefixFilename
        self.CCAMFlavour = CCAMFlavour
        
        self.CCAM_link_name_template = '{prefix}{resolution}_{date}.nc'
        self.CCAM_link_name_no_date_template = '{prefix}{resolution}_'
        self.meteo_config_filename_list = []
        self.StatusJSONfile = StatusJSONfile
        ############
        self.individualhourlyfiles = 'F'
        self.timeinterval = '1'
        self.filepath2D = 'NONE'
        self.filepath3D = self.DestinationCCAMdir + os.sep
        #self.fileprefix = fileprefix
        self.SimulationPath = SimulationPath
        self.templatepath = templatepath
#        self.templatefile='Meteo-Config-Files.template'
        self.templatefile = templatefile
        self.meteo_config_filename_template = 'gen_meteo_config_{gridindex}.txt'
        self.startdate = Startdate.strftime('%Y %m %d %H')
        return
################################################################################
    def Write_config_File(self,):
        """ 
        This method write one CTM Met config file from template.
        """
        
        
        filename = os.path.join(self.SimulationPath, self.meteo_config_filename)
        File = open(filename,'w')

        template = JJU.LoadTemplate(self.templatepath, self.templatefile)

        config = template.render({'MetConfig':self })
        File.write(config)
        File.close()
        return 

################################################################################
    def Configure_CTM(self,):
        '''
        Configure and write the CTM met config files
        '''
        for gridindex,resname in enumerate(self.ResolutionNames):
    
            if self.CCAMFlavour == 'OneFile/Forecast/Domain':
                #how many hours per file read 
                self.hours = int((self.Enddate - self.Startdate).total_seconds() // 3600)
                #days is actually the number of met file to read
                self.days = '1'
            ##########
            if self.CCAMFlavour == 'OneFile/Day/Domain':
                self.days = (self.Enddate - self.Startdate).days
                self.hours = '25'
                print 
            self.fileprefix = self.CCAM_link_name_no_date_template.format(
                                    prefix = self.CCAMLinkPrefixFilename,
                                    resolution = resname, 
                                    )
            self.meteo_config_filename = self.meteo_config_filename_template.format(gridindex = gridindex)
            self.Write_config_File()
            self.meteo_config_filename_list.append(self.meteo_config_filename)
            self.logger.info('Met config file for domain = {domain}'.format(domain = resname).ljust(self.justif-2,'.') + 'OK')
        return
        
################################################################################
    def init_CcamFileHouseKeeping(self,):
        '''
        Configure the housekeeping object 
        '''
        CC = CFHK.CcamFilesHouseKeepingClass(
                 self.logger, self.justif, 
                 self.Startdate, self.Enddate, self.AlreadyRenamed, 
                 self.OriginalCCAMdir, self.DestinationCCAMdir, self.ResolutionNames, self.ResolutionProperties,
                 self.CCAM_input_flow, self.CCAM_link_name_template,
                 self.CCAMLinkPrefixFilename, self.CCAMFlavour,
                 self.StatusJSONfile)
        return CC         

################################################################################
    def Run_all(self,):
        CC = self.init_CcamFileHouseKeeping()
        self.logger.info("Linking Met Files".ljust(self.justif,'.'))
        CC.CreateDestinationDir()
        CC.House_keeping_original_CCAM()
        CC.SymLinkingOriginalCCAMFiles()  
        self.OriginalCCAMdir  = CC.OriginalCCAMdir
        self.logger.info('Building individual domain grid configuration file'.ljust(self.justif,'.'))
        self.Configure_CTM()
        return 
        
###########################################################################################
###########################################################################################
if __name__ == '__main__':

    #ys.path.insert(0,'..')
    import InitLogging as IL
    import ResolutionProperties as RP 

    loggername='Met'
    logger=IL.Initialise_logging(loggername)
    justif = 102

    Startdate = dt.datetime(2019, 4, 2, 12)
    Enddate = Startdate + dt.timedelta(days=3,hours=00)
    AlreadyRenamed = False
    
    SimulationPath = '/home/barthelemyx/Projects/AQ-Forecast/Forecast'
    templatepath = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatefile = 'Meteo-Config-Files.template'
    
    OriginalCCAMdir = os.path.join(SimulationPath, 'testOriginalCCAM') 
    DestinationCCAMdir = os.path.join(SimulationPath, 'testDestinationCCAM')
    
    ResolutionNames = ['nsw','gmr','gsyd']
    ResolutionProperties = RP.ResolutionProperties_new()
    CCAMLinkPrefixFilename = 'ccam_'
    
    CCAM_input_flow = "Forecast"
    CCAMFlavour = 'OneFile/Forecast/Domain'
    CCAMFlavour = 'OneFile/Day/Domain'
    
    MetConfig = MetConfigClass(logger, justif, 
                 Startdate, Enddate, AlreadyRenamed, 
                 SimulationPath, templatepath, templatefile,
                 OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, ResolutionProperties,
                 CCAM_input_flow,
                 CCAMLinkPrefixFilename,CCAMFlavour,
                 )
    
    logger.debug('-'*80)
    logger.debug('Testing CCAM Housekeeping')
    CC = MetConfig.init_CcamFileHouseKeeping()


    logger.debug('-'*80)
    logger.debug('Testing CCAM workdir')
    CC.CreateDestinationDir()

    Filepattern = CC.CCAM_Forecast_file_pattern
            
    for resname in ResolutionNames:
        CC.CreateOriginalDir(resname)
        lat = np.asarray(np.rint(np.abs(ResolutionProperties[resname]['SW_corner_lat'])),  dtype = np.int16)
        lon = np.asarray(np.rint(np.abs(ResolutionProperties[resname]['SW_corner_lon'])),  dtype = np.int16)
        resolution = np.asarray(np.rint(ResolutionProperties[resname]['resolution_m'] / 1000),  dtype = np.int16)
    
        if CCAMFlavour == 'OneFile/Day/Domain':
            for dday in range ((Enddate - Startdate).days + 1):
                ddate = Startdate + dt.timedelta(days = dday)
                OriginalFileMatchingPattern = Filepattern.format(
                                               lat = lat,
                                               lon = lon,
                                               resolution = str(resolution).zfill(2),
                                               date = ddate.strftime('%Y%m%d%H')
                                               )
                ffile = os.path.join(OriginalCCAMdir, OriginalFileMatchingPattern)
                try:
                    os.system('touch {ffile}'.format(ffile = ffile))
                    logger.info('Creating %s',ffile)
                except:
                    pass
        if CCAMFlavour == 'OneFile/Forecast/Domain':
            ddate = Startdate 
            OriginalFileMatchingPattern = Filepattern.format(
                                               lat = lat,
                                               lon = lon,
                                               resolution = str(resolution).zfill(2),
                                               date = ddate.strftime('%Y%m%d%H')
                                               )
            ffile = os.path.join(OriginalCCAMdir, OriginalFileMatchingPattern)
            try:
                os.system('touch {ffile}'.format(ffile = ffile))
                logger.info('Creating %s',ffile)
            except:
                pass
    CC.House_keeping_original_CCAM()
    CC.SymLinkingOriginalCCAMFiles()   
    ####################################################################################################################
    logger.debug('-'*80)

    CCAM_input_flow = "ERA"
    MetConfig = MetConfigClass(logger, justif, 
                 Startdate, Enddate, AlreadyRenamed, 
                 SimulationPath, templatepath, templatefile,
                 OriginalCCAMdir, DestinationCCAMdir, ResolutionNames, ResolutionProperties,
                 CCAM_input_flow,
                 CCAMLinkPrefixFilename,CCAMFlavour,
                 )
    
    logger.debug('-'*80)
    logger.debug('Testing CCAM Housekeeping')
    CC = MetConfig.init_CcamFileHouseKeeping()
    Filepattern = CC.CCAM_ERA_file_pattern

    for resname in ResolutionNames:
        CC.CreateOriginalDir(resname)
        lat = np.asarray(np.rint(np.abs(ResolutionProperties[resname]['SW_corner_lat'])),  dtype = np.int16)
        lon = np.asarray(np.rint(np.abs(ResolutionProperties[resname]['SW_corner_lon'])),  dtype = np.int16)
        resolution = np.asarray(np.rint(ResolutionProperties[resname]['resolution_m'] / 1000),  dtype = np.int16)
    
        if CCAMFlavour == 'OneFile/Day/Domain':
            MetConfig.CCAMFlavour = 'OneFile/Day/Domain'
            for dday in range ((Enddate - Startdate).days + 1):
                ddate = Startdate + dt.timedelta(days = dday)
                OriginalFileMatchingPattern = Filepattern.format(
                                               lat = lat,
                                               lon = lon,
                                               resolution = str(resolution).zfill(2),
                                               date = ddate.strftime('%Y%m%d')
                                               )
                ffile = os.path.join(OriginalCCAMdir, resname, OriginalFileMatchingPattern)
                try:
                    os.system('touch {ffile}'.format(ffile = ffile))
                    logger.info('Creating %s',ffile)
                except:
                    logger.error("no!")
                    pass
        if CCAMFlavour == 'OneFile/Forecast/Domain':
            ddate = Startdate 
            OriginalFileMatchingPattern = Filepattern.format(
                                               lat = lat,
                                               lon = lon,
                                               resolution = str(resolution).zfill(2),
                                               date = ddate.strftime('%Y%m%d')
                                               )
            ffile = os.path.join(OriginalCCAMdir, resname, OriginalFileMatchingPattern)
            try:
                os.system('touch {ffile}'.format(ffile = ffile))
                logger.info('Creating %s',ffile)
            except:
                logger.error("NO!")
                pass
    CC.House_keeping_original_CCAM()
    CC.SymLinkingOriginalCCAMFiles()   

    ####################################################################################################################
    MetConfig.Configure_CTM()
    ####################################################################################################################

    sys.exit()
    logger.debug('-'*80)
    logger.debug('Testing Unlinking')
    logger.debug('-'*80)
    logger.debug('Creating Work dir')
    
    if 1:
        logger.debug('using touch to create CCAM test files')
        filename='ctm34S151E60km_201611231200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,resolution,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        filename='ctm34S151E60km_201611241200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,resolution,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        filename='ctm34S151E60km_201611251200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,resolution,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        logger.debug('-'*80)
        logger.debug('Test linking')
        #CcamFilesHouseKeeping.SymLinkingOriginalCCAMFiles()
    
        logger.debug('using touch to create CCAM test files')
        filename='ctm34S151E60km_201611231200.nc'
        OriginalFile=os.path.join(OriginalCCAMdir,filename)
        try:
            logger.debug('Creating %s',OriginalFile)
            os.system('touch '+OriginalFile)
        except:
            pass
        logger.debug('-'*80)
        logger.debug('Test moving')
        CcamFilesHouseKeeping.MovingOriginalCCAMFiles()

#    SimulationConfig=RC.SimulationConfigClass(logger,ResolutionNames,ResolutionProperties,SCENARIO, \
#            Startdate,EndDate,Emissiondate,HOUR_N,TRANSPORT,DIFF,SIOA,SOA,CHEM,BIO,PrepBin, \
#            templatepath,netcdfpath,emissionmodellingpath,emissionbuildpath,\
#            ccamfilepath,SimulationPath, \
#            CTMBinPath,CTMBin,RuntimeCPU,RunTimeHours,PartitionName)
#    index=1
#    warmstart=False
#    RunConfig=RC.RunConfigClass(SimulationConfig,index,warmstart)

