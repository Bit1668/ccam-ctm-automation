"""
.. module:: MEMSDustConfig
   :platform: Unix
   :synopsis: Everything needed to include the offline dust emissions form the MEMS.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import pytz
import numpy as np
import sys
import os
import datetime as dt
import fnmatch
import shutil
try:
    from .. import DateMagics as DM
except:
    sys.path.append('Python-Scripts/Sources')
    import DateMagics as DM
#from .. import InitLogging as IL

###########################################################################################
class MEMSDustConfigClass(object):
    def __init__(self, logger, justif,
                 startdate, enddate, emissiondate, TimestampUTC, 
                 SimulationPath, EmissionBuildPath_static, EmissionBuildPath_dynamic, 
                 MEMS_main_dir, MEMS_dust_dir, MEMS_dust_source_dir, MEMS_dust_source_file, 
                 prefix_dust_file, ListOfCTMFiles,
                  ):
        """
        This class defines and configures MEMS emission objects.
        It contains all the parameters and the methods to build MEMS emissions.
        
        Parameters
        ---------- 
        logger : logging.logger
            instance of a logger to output message.
        justif : int
            max message width to justify logger output.    
        buildemissionpath : str
            root of the main emission directory to build in. 
        buildemissionsubdir : str
            path from the main emission directory to build in.
        startdate : datetime
            start date of the emissions to build as a datetime with defined timezone.
        enddate : datetime
            end date date of the burns as a datetime with defined timezone.
        emissiondate : datetime
            emission date for when MEMS emissions are involved.
        TimestampUTC : datetime
            timestamp in UTC to mark the run. 
        datapath : str
            path to find the MEMS data and parametres.
        offset : str
            time difference to offset the emissions.
        """
        self.justif = justif
        self.logger = logger          
        self.startdate = startdate
        self.enddate =  enddate
        self.emissiondate =  emissiondate
        self.TimestampUTC = TimestampUTC

        # directories
        # root dir of the simulation
        self.SimulationPath = SimulationPath

        self.EmissionBuildPath_static = EmissionBuildPath_static
        self.EmissionBuildPath_dynamic = EmissionBuildPath_dynamic

        # main MEMS emission sudbdir
        self.MEMS_main_dir = MEMS_main_dir
        
        # MEMS dust subdirs
        self.MEMS_dust_dir = MEMS_dust_dir

        # Dust source dir
        self.MEMS_dust_source_dir = MEMS_dust_source_dir    
        # Dust optional source file
        self.MEMS_dust_source_file = MEMS_dust_source_file

#       Include or not emission groups.
        self.includeemission_MEMS_dust = False
        
        # List of input CTM files
        self.ListOfCTMFiles = ListOfCTMFiles
        
        # List of MEMS emission files
        self.MEMS_dust_emission_file = None
        self.prefix_dust_file = prefix_dust_file
        self.DustFileMatchingPattern = self.startdate.strftime('vertical_area_emissions-%Y-%m-%d.nc')
        self.Dust_CTM_Outputfile_Template = '{prefix}_{ctmfile}'.format(prefix = self.prefix_dust_file, ctmfile = '{ctmfile}')
        
        self.Dust_CTM_Outputfile_list = []

############################################################################################################################
    def fullpath_Dust_MEMS_final_emission_file_before_merge(self):
        """
        Method to output the output full path filename
        """
        name = os.path.join(self.EmissionBuildPath_dynamic, self.MEMS_main_dir, self.MEMS_dust_dir, self.MEMS_final_emission_file_before_merge)
        return name

############################################################################################################################
    def relativepath_Dust_MEMS_final_emission_file_before_merge(self):
        """
        Method to output the output relative (to the build emission directory) path filename
        """
        name = os.path.join(self.MEMS_main_dir, self.MEMS_dust_dir, self.MEMS_final_emission_file_before_merge)
        return name

############################################################################################################################
    def fullpath_Dust_MEMS_final_emission_dir_before_merge(self):
        """
        Method to output the output full path filename
        """
        name = os.path.join(self.EmissionBuildPath_dynamic, self.MEMS_main_dir, self.MEMS_dust_dir)
        return name

############################################################################################################################
    def relativepath_Dust_MEMS_final_emission_dir_before_merge(self):
        """
        Method to output the output relative (to the build emission directory) path filename
        """
        name = os.path.join(self.MEMS_main_dir, self.MEMS_dust_dir)
        return name

############################################################################################################################
    def fullpath_Dust_MEMS_source_emission_file(self):
        """
        Method to output the output full path filename
        """
        name = os.path.join(self.MEMS_dust_source_dir, self.MEMS_dust_emission_file[0])
        return name


############################################################################################################################
    def MEMSDustSourceDataDirExist(self):
        """
        Method to detect the existence of the data directory 
        """
        if not os.path.isdir(self.MEMS_dust_source_dir):
            self.logger.error("ERROR")
            self.logger.error("NO MEMS DATA DIR FOUND")
            self.logger.error("You need to find %s",self.datafiredir)
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def DeleteDirAndItsContents(self, directory):
        """
        Method to delete previous dust directory dataset
        """
        if os.path.isdir(directory):
            self.logger.warn("Previous dataset found")
            self.logger.warn("Removing %s and all its contents",directory)
            shutil.rmtree(directory)
        else:
            self.logger.info("No previous dataset found ... Continuing")
            
        return          

############################################################################################################################
    def DeleteFile(self, filename):
        """
        Method to delete previous output emission file
        """
        if os.path.isfile(filename):
            self.logger.warn("Previous dataset found")
            self.logger.warn("Removing %s ",filename)
            shutil.rmtree(directory)
        else:
            self.logger.info("No previous dataset found ... Continuing")
            
        return          

############################################################################################################################
    def DustInDataDirExist(self):
        """
        Method to check the existence of dust data in the source directory
        """
        listdir = os.listdir(self.MEMS_dust_source_dir)
        filteredlist = fnmatch.filter(listdir, self.DustFileMatchingPattern)
        self.MEMS_dust_emission_file = filteredlist
        if not filteredlist:
            self.logger.error("ERROR")
            self.logger.error("NO MEMS DUST DATA DATA FOUND")
            self.logger.error("You need to find fire filename matching pattern %s", self.DustFileMatchingPattern)
            self.logger.error("In the following dir %s", self.MEMS_dust_source_dir)
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def Create_MEMS_Dust_dir(self):
        """ This method build the MEMS dust dirs.
        """
        dirdir = self.fullpath_Dust_MEMS_final_emission_dir_before_merge()
        try :
            os.makedirs(dirdir)
            self.logger.info('Creation MEMS Dust Dir= {msg}'.format(msg=dirdir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('MEMS Dust Dir = {msg}'.format(msg=dirdir).ljust(self.justif-13,'.') + 'Already exist')
            pass

############################################################################################################################
    def Adapt_mems_to_ctm(self):
        
        #sys.path.append('/home/barthelemyx/Projects/mems')
        sys.path.append('/home/monkk/mems')
        import core.pluggins.ctm.ctm_emissions_conversion_flow as cef
        import core.pluggins.ctm.conversion_specs as specs
        
        ipath = self.fullpath_Dust_MEMS_source_emission_file()
        odir = self.fullpath_Dust_MEMS_final_emission_dir_before_merge()
        scratch_path = odir
        date = self.startdate
        
        dust_spec = specs.DustEmissionWithBinRangeConversionSpec(ipath)
        
        for ctmfile in self.ListOfCTMFiles:
            sample_path = ctmfile
            outputfile = self.Dust_CTM_Outputfile_Template.format(ctmfile = os.path.basename(ctmfile))
            datestringFromCTMfile = os.path.basename(ctmfile).split("_")[1]
            dateUTC, dateAEST, dateAEDT = DM.DateMagicsConvert(datestringFromCTMfile, "UTC", '%Y%m%d')
            opath = os.path.join(odir, outputfile)

            self.logger.info('date %s',dateUTC.replace(hour= 0))
            self.logger.info('input %s',ipath)
            self.logger.info('sample %s',sample_path)
            self.logger.info('scratch %s',scratch_path)
            self.logger.info('ouput %s',opath)
            
            conv = cef.CtmEmissionsFromMemsAndSample(
                sample_path = sample_path,
                output_path = opath,
                scratch_path = scratch_path,
                area_with_range_input_specs = [dust_spec],
                points_input_specs = None,
                the_date = dateUTC.replace(hour= 0),
                dust_support = True)


            conv.init()

            conv.convert()
            self.Dust_CTM_Outputfile_list.append(opath)

############################################################################################################################
    def Merge_to_CTM(self, destination):
        #sys.path.append('/home/barthelemyx/Projects/mems')
        sys.path.append('/home/monkk/mems')
        import core.pluggins.ctm.ctm_merge as cm

        merger = cm.NFileMerge(destination, append = True,
                add_offline_dust_variable=True)
        
        merger.merge([sample_path, dust_path])

############################################################################################################################
    def Run_all(self, FirstRunDust):
        '''
        Run the whole Dust module
        '''
        # Forcing the source dust filename when provided instead of auto looking for the pattern
        if self.MEMS_dust_source_file :
            self.DustFileMatchingPattern = self.MEMS_dust_source_file
            
        self.MEMSDustSourceDataDirExist()
        self.DustInDataDirExist()
    
        directory = self.fullpath_Dust_MEMS_final_emission_dir_before_merge()
        self.FirstRunDust = FirstRunDust
        if self.FirstRunDust:
            self.DeleteDirAndItsContents(directory)
            self.FirstRunDust = False
    
        self.Create_MEMS_Dust_dir()
        self.Adapt_mems_to_ctm()
        self.logger.info("list of output ready to merge dust ctm emission files %s", self.Dust_CTM_Outputfile_list)
        return
############################################################################################################################
if __name__ == '__main__':
    try:
        from .. import InitLogging as IL
    except:
        sys.path.append('Python-Scripts/Sources')
        import InitLogging as IL

    loggername = 'MEMS-Dust'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    

    startdate = dt.datetime(2009,9,14)
    enddate = startdate + dt.timedelta(days=3, hours=00)
    emissiondate = dt.datetime(2008,1,1)
    emissiondate = dt.datetime(2018,11,21)
    TimestampUTC = dt.datetime(2018,2,1)

    SimulationPath = 'Emissions-Test'
    
    EmissionBuildPath_static = os.path.join(SimulationPath, 'static')
    EmissionBuildPath_dynamic = os.path.join(SimulationPath,'dynamic')
    MEMS_main_dir = 'MEMS'
    MEMS_dust_dir = 'DUST'
    prefix_dust_file = 'Dust'
    MEMS_dust_source_dir = '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
    MEMS_dust_source_dir =  '/mnt/climate/cas/scratch/fuchsd/tmp'
    MEMS_dust_source_file = None
    MEMS_dust_source_file = 'ACCESS-R-dust-forecast-ctm_bin_range_area_emissions-2018-11-20-23.nc'
    ctmdir = '/mnt/climate/cas/scratch/fuchsd/test/data/ctm-no-dust-200909/'
    ctmfiles = ['aus_20090914_cb05_aer2_0.800dg_sep09.nc','aus_20090915_cb05_aer2_0.800dg_sep09.nc']
    ListOfCTMFiles = [os.path.join(ctmdir, ffile) for ffile in ctmfiles]
    #ListOfCTMFiles = ctmfiles
    
    MEMSDustConfig = MEMSDustConfigClass(logger, justif,
                 startdate, enddate, emissiondate, TimestampUTC, 
                 SimulationPath, EmissionBuildPath_static, EmissionBuildPath_dynamic, 
                 MEMS_main_dir, MEMS_dust_dir, MEMS_dust_source_dir, MEMS_dust_source_file,
                 prefix_dust_file, ListOfCTMFiles, )
    MEMSDustConfig.Run_all()             
    MEMSDustConfig.MEMSDustSourceDataDirExist()
    MEMSDustConfig.DustInDataDirExist()
    
    #cwd = os.getcwd()
    directory = MEMSDustConfig.fullpath_Dust_MEMS_final_emission_dir_before_merge()
    #lastleafofdirtree = os.path.dirname(directory)
    #os.chdir(lastleafofdirtree)
    #directory = MEMSDustConfig.MEMS_dust_dir
    MEMSDustConfig.DeleteDirAndItsContents(directory)
    #os.chdir(cwd)
    
    MEMSDustConfig.Create_MEMS_Dust_dir()
    MEMSDustConfig.Adapt_mems_to_ctm()
    logger.info("list of output ready to merge dust ctm emission files %s", MEMSDustConfig.Dust_CTM_Outputfile_list)
