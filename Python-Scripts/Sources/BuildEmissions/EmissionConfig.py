"""
.. module:: EmissionConfig
   :platform: Unix
   :synopsis: Everything needed to build the Emission files.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import string
import datetime as dt
import pytz
import os
import sys
#sys.path.append('..')
import subprocess
try :
    import Sources.JinjaUtils as JJU
except :
    sys.path.append('/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts')
    import Sources.JinjaUtils as JJU
import Sources.InitLogging as IL
import Sources.BuildEmissions.PSEmergeMulti as PSEMM
import Sources.BuildEmissions.EmissionScaling as ES
import Sources.BuildEmissions.Tools
import Sources.BuildEmissions.Inventory_Ratios_2008 as IR2008
import Sources.BuildEmissions.Inventory_Ratios_PM10_2013 as IR2013PM10
import Sources.BuildEmissions.Inventory_Ratios_PM25_2013 as IR2013PM25
###########################################################################################
class EmissionConfigClass(object):
    """ 
    This meta class defines a Emission class object, that contains all the parameters 
    to configure each individual contribution in the emissions. 
    
    The class is self contained and can be initialised from original data, 
    it doesn't depend on higher level classes.
    it contains all the methods to configure and generate emissions.

    Attributes
    -----------
    logger : logging.logger
        instance of a logger to output messages.
    justif : int
        max message width to justify logger output.   
    startdate : datetime.datetime
        start date of the emissions to build as a AEDT datetime.
    enddate : datetime.datetime
        end date of the emissions to build as a AEDT datetime.
    emissiondate : datetime.datetime
        date of the emission. 
    TimestampUTC : datetime.datetime
        timestamp ID of the run.
    SimulationPath : str
        base path where the main simulation files are.
    buildemissionpath_static : str
        base path where to build the static emissions from EDMS.
    buildemissionpath_dynamic : str
        base path where to build the dynamical and/or MEMS emissions.
    bininitline : str
        init line to setup env when running a binary
    Basebinpath : str
        Base path to find the binary to build individual emissions.
    templatepath : str
        path to the template directory.
    emissions_edms_DATA : str
        path to the EDMS source directory.
    """
    def __init__(self, logger, justif, 
                   startdate, enddate, emissiondate, TimestampUTC,
                   SimulationPath, buildemissionpath_static, buildemissionpath_dynamic,  
                   Basebinpath, templatepath, emissions_edms_DATA_basedir, PM10speciation,
                   bininitline, 
                   **kwargs):
    
        self.kwargs = kwargs
        self.justif = justif
        self.logger = logger          
#        self.buildemissionpath='Emissions/'
#        self.Basebinpath='/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran/'
#        self.templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
        self.buildemissionpath_static = buildemissionpath_static
        self.buildemissionpath_dynamic = buildemissionpath_dynamic
        self.SimulationPath = SimulationPath
        self.Basebinpath = Basebinpath
        self.templatepath = templatepath
 
        self.bininitline = bininitline

        self.build_motorVehicle_emissions_glo_bindir = 'build_motorVehicle_emissions_glo'
        self.build_commercialDomestic_emissions_glo_bindir = 'build_commercialDomestic_emissions_glo'
        self.build_elevated_emissions_glo_bindir = 'build_elevated_emissions_glo'
        self.build_elevated_emissions_bushfire_bindir = 'build_elevated_emissions_bushfire'
        self.build_smoke_emissions_bindir = 'build_smoke_emissions'
        
        #Petrol exhaust
        self.templatefile_petExh = 'Build_emission_petExh.template'
        self.binpath_petExh = os.path.join(self.Basebinpath,self.build_motorVehicle_emissions_glo_bindir)
        self.bin_petExh = 'build_motorvehicle_emissions_glo'
        self.bin_petExh = 'build_motorvehicle_emissions_glo_sles15'
        self.buildemissionsubdir_petExh = 'vpx'

        #Diesel Exhaust
        self.templatefile_dieExh = 'Build_emission_dieExh.template'
        self.binpath_dieExh = os.path.join(self.Basebinpath,self.build_motorVehicle_emissions_glo_bindir)
        self.bin_dieExh = 'build_motorvehicle_emissions_glo'
        self.bin_dieExh = 'build_motorvehicle_emissions_glo_sles15'
        self.buildemissionsubdir_dieExh = 'vdx'
        
        # LPG
        self.templatefile_lpgExh = 'Build_emission_lpgExh.template'
        self.binpath_lpgExh = os.path.join(self.Basebinpath,self.build_motorVehicle_emissions_glo_bindir)
        self.bin_lpgExh = 'build_motorvehicle_emissions_glo'
        self.bin_lpgExh = 'build_motorvehicle_emissions_glo_sles15'
        self.buildemissionsubdir_lpgExh = 'vlx'
        
        # Petrol Evaporation
        self.templatefile_petEvp = 'Build_emission_petEvp.template'
        self.binpath_petEvp = os.path.join(self.Basebinpath,self.build_motorVehicle_emissions_glo_bindir)
        self.bin_petEvp = 'build_motorvehicle_emissions_glo'
        self.bin_petEvp = 'build_motorvehicle_emissions_glo_sles15'
        self.buildemissionsubdir_petEvp = 'vpv' 

        #Commercial Domestic
        self.templatefile_comdomestic = 'Build_emission_comdomestic.template'
        self.binpath_comdomestic = os.path.join(self.Basebinpath,self.build_commercialDomestic_emissions_glo_bindir)
        self.bin_comdomestic = 'build_commercialdomestic_emissions_glo'
        self.bin_comdomestic = 'build_commercialdomestic_emissions_glo_sles15'
        self.buildemissionsubdir_comdomestic = 'gse'
        
        #Wood Heaters
        self.templatefile_woodheaters = 'Build_emission_woodheaters.template'
        self.binpath_whe = os.path.join(self.Basebinpath,self.build_commercialDomestic_emissions_glo_bindir)
        self.bin_whe = 'build_commercialdomestic_emissions_glo'
        self.bin_whe = 'build_commercialdomestic_emissions_glo_sles15'
        self.buildemissionsubdir_woodheaters = 'whe'

        #PSE
        self.buildemissionsubdir_pse = 'pse'
        
        self.templatefile_coalonlywkdays = 'Build_emission_coalonly_wkdays.template'
        self.binpath_coalonlywkdays = os.path.join(self.Basebinpath,self.build_elevated_emissions_glo_bindir)
        self.bin_coalonlywkdays = 'build_elevated_emissions_glo'
        self.bin_coalonlywkdays = 'build_elevated_emissions_glo_sles15'
        self.buildemissionsubdir_coalonlywkdays = 'coalonlywkdays'

        self.templatefile_gasonlywkdays = 'Build_emission_gasonly_wkdays.template'
        self.binpath_gasonlywkdays = os.path.join(self.Basebinpath,self.build_elevated_emissions_glo_bindir)
        self.bin_gasonlywkdays = 'build_elevated_emissions_glo'
        self.bin_gasonlywkdays = 'build_elevated_emissions_glo_sles15'
        self.buildemissionsubdir_gasonlywkdays = 'gasonlywkdays'

        self.templatefile_restwkdays = 'Build_emission_rest_wkdays.template'
        self.binpath_restwkdays = os.path.join(self.Basebinpath,self.build_elevated_emissions_glo_bindir)
        self.bin_restwkdays = 'build_elevated_emissions_glo'
        self.bin_restwkdays = 'build_elevated_emissions_glo_sles15'
        self.buildemissionsubdir_restwkdays = 'restwkdays'

        #Coal Power station
        self.templatefile_power = 'Build_emission_power.template'
        self.binpath_power = os.path.join(self.Basebinpath,self.build_elevated_emissions_glo_bindir)
        self.bin_power = 'build_elevated_emissions_glo'
        self.bin_power = 'build_elevated_emissions_glo_sles15'
        self.buildemissionsubdir_power = 'power'
#       if timeresolvedcoalpower is True, build hourly data for coal powerplant. If False, it's based on EDMS data.
        self.timeresolvedcoalpower = False
        #coal powerplant Data dir if time resolved
        self.emissions_power_DATA = '/home/trieut/power/emission_non_edms'
        

        #fire options
        self.Run_HRB = None
        self.Run_Bushfire = None

        #Bushfire OEH flavour.
        self.templatefile_rfs_OEH = 'Build_pems_generic.template'
        self.binpath_rfs_OEH = os.path.join(self.Basebinpath,self.build_elevated_emissions_bushfire_bindir)
        self.bin_rfs_OEH = 'build_elevated_emissions_bushfire_noglo'
        self.bin_rfs_OEH = 'build_elevated_emissions_bushfire_noglo_sles15'
        self.buildemissionsubdir_rfs_OEH =  'bushfire_oeh'
        self.bushfireOEH = False
        self.emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/bushfireDF'

        #Bushfire CSIROflavour.
        self.templatefile_rfs_CSIRO = 'Build_bushfire_csiro.template'
        self.binpath_rfs_CSIRO = os.path.join(self.Basebinpath,self.build_smoke_emissions_bindir)
        self.bin_rfs_CSIRO = 'build_smoke_emissions'
        self.bin_rfs_CSIRO = 'build_smoke_emissions_sles15'
        self.bin_rfs_CSIRO = 'build_smoke_emissions_opt_sles15_heap349'
        self.bin_rfs_CSIRO = 'build_smoke_emissions_opt_sles15_heap349_mcmedium'
        self.bin_rfs_CSIRO = 'build_smoke_emissions_opt_sles15_heap_mcmedium'
        self.buildemissionsubdir_rfs_CSIRO = 'bushfire_csiro'
        self.bushfireCSIRO = False
        self.emissions_rfs_DATA_CSIRO = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/bushfireCSI'
        self.offset_bushfire_csiro = 0
        self.burn_start_hour_csiro = 9
       
        #merge
        self.templatefile_PSEmerge = 'PSEmerge.template'
        self.psemerge_bindir = 'psemerge'
        self.binpath_PSEmerge = os.path.join(self.Basebinpath,self.psemerge_bindir)
        self.bin_PSEmerge = 'psemerge'
        self.bin_PSEmerge = 'psemerge_sles15'
    
        self.templatefile_PSEMM = 'PSEmergeMulti.template'
        self.PSEMM_bindir = 'psemergem'
        self.binpath_PSEMM = os.path.join(self.Basebinpath,self.PSEMM_bindir)
        self.bin_PSEMM = 'psemergem'
        self.bin_PSEMM = 'psemergem_sles15'
        self.bin_PSEMM = 'psemergem_opt_sles15_heap349_mcmedium'
        self.bin_PSEMM = 'psemergem_opt_sles15_heap_mcmedium'
        #self.bin_PSEMM = 'psemergem_hourly_opt_sles15_heap349_mcmedium'
           
        #time windows
        self.startdate = startdate
        self.startdateUTC = self.startdate.astimezone(pytz.utc)
        self.enddate = enddate
        self.emissiondate = emissiondate
        self.TimestampUTC = TimestampUTC
        
        self.emissiondate_veh = dt.datetime(self.emissiondate.year,2,1)
        self.emissiondate_whe = dt.datetime(self.emissiondate.year,7,1)
        
#       Include or not emission groups.
        self.includeemission_vpx = True
        self.includeemission_vdx = True
        self.includeemission_vlx = True
        self.includeemission_vpv = True
        self.includeemission_gse = True
        self.includeemission_whe = True
        self.includeemission_pse = True
        
#       Build or not emission groups.
        self.buildemission_vpx = True
        self.buildemission_vdx = True
        self.buildemission_vlx = True
        self.buildemission_vpv = True
        self.buildemission_gse = True
        self.buildemission_whe = True
        self.buildemission_pse = True

#      Each of the emission group has updated the emissionconfig object?
        self.buildemission_vpx_updated = False
        self.buildemission_vdx_updated = False
        self.buildemission_vlx_updated = False
        self.buildemission_vpv_updated = False
        self.buildemission_gse_updated = False
        self.buildemission_whe_updated = False
        self.buildemission_pse_updated = False

#       EDMS emission global scaling
        self.EmissionGlobalScaling = ES.EmissionGlobalScalingClass()

#       EDMS emission PM ratios
        self.PM10speciation = PM10speciation
        
        self.Emissions_ratios = None
        self.PMspeciation = None
        self.EDMS_dirname_template = None

        self.Configure_EDMS_PM_speciation()

        #EDMS Data dir
        #self.emissions_edms_DATA = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/emission2008NH3'
        #self.emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Cooked/Emission2008NH3_0.1NO'
        #self.emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Cooked/Emission2008NH3_0.01NO'
        #self.emissions_edms_DATA = emissions_edms_DATA
        self.emissions_edms_DATA_basedir = emissions_edms_DATA_basedir
        self.emissions_edms_DATA = None
        self.Compute_EDMS_directory()
        """ This method compute the EDMS directory.
        """

#       MEMS
#       offline MEMS emission module inclusion?      
        self.includeemission_MEMS = False
        self.ResolutionNames = None
        
        self.Run_Dust = False
        self.includeemission_MEMS_dust = None
        self.DustModel = None
        self.MEMS_dust_source_dir = None
        self.MEMS_dust_source_file = None
        self.dust_updated = False
        
  
#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
# KM changed all offsets to 10 as EDMS is in AEST not AEDT.
        self.offset = -10
        self.offset_veh = -10
        self.offset_whe = -10

############33
    def init_EmissionPetExhConfigClass(self):
        ''' This method initialise the EmissionPetExhConfigClass
        '''
        import Sources.BuildEmissions.EmissionPetExhConfig as PEC
        
        buildemissionpath = self.buildemissionpath_static
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_petExh = self.templatefile_petExh
        binpath_petExh = self.binpath_petExh
        bin_petExh = self.bin_petExh
        buildemissionsubdir_petExh = self.buildemissionsubdir_petExh
        bininitline = self.bininitline
        
        startdate = self.startdate
        enddate = self.enddate
        emissiondate_veh = self.emissiondate_veh
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset_veh = self.offset_veh
        
        Emissions_ratios = self.Emissions_ratios
        PMspeciation = self.PMspeciation        
                
        EmissionPetExhConfig = PEC.EmissionPetExhConfigClass(self.logger, self.justif, templatepath, templatefile_petExh,
                                                             buildemissionpath, buildemissionsubdir_petExh,
                                                             binpath_petExh, bin_petExh, bininitline,
                                                             startdate, enddate, emissiondate_veh, 
                                                             emissions_edms_DATA, offset_veh,
                                                             Emissions_ratios, PMspeciation,
                                                             **self.kwargs)
        return EmissionPetExhConfig                     

############33
    def init_EmissionDieExhConfigClass(self):
        ''' This method initialise the EmissionDieExhConfigClass
        '''
        import Sources.BuildEmissions.EmissionDieExhConfig as DEC
        
        buildemissionpath = self.buildemissionpath_static
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_dieExh = self.templatefile_dieExh
        binpath_dieExh = self.binpath_dieExh
        bin_dieExh = self.bin_dieExh
        buildemissionsubdir_dieExh = self.buildemissionsubdir_dieExh
        bininitline = self.bininitline

        startdate = self.startdate
        enddate = self.enddate
        emissiondate_veh = self.emissiondate_veh
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset_veh = self.offset_veh
        
        Emissions_ratios = self.Emissions_ratios
        PMspeciation = self.PMspeciation                
        
        EmissionDieExhConfig = DEC.EmissionDieExhConfigClass(self.logger, self.justif, templatepath, templatefile_dieExh,
                                                             buildemissionpath, buildemissionsubdir_dieExh,
                                                             binpath_dieExh, bin_dieExh, bininitline,
                                                             startdate, enddate, emissiondate_veh,
                                                             emissions_edms_DATA, offset_veh,
                                                             Emissions_ratios, PMspeciation,
                                                             **self.kwargs)
        return EmissionDieExhConfig
                         
############33
    def init_EmissionLpgExhConfigClass(self):
        ''' This method initialise the EmissionLpgExhConfigClass
        '''
        import Sources.BuildEmissions.EmissionLpgExhConfig as LEC
        
        buildemissionpath = self.buildemissionpath_static
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_lpgExh = self.templatefile_lpgExh
        binpath_lpgExh = self.binpath_lpgExh
        bin_lpgExh = self.bin_lpgExh
        buildemissionsubdir_lpgExh = self.buildemissionsubdir_lpgExh
        bininitline = self.bininitline

        startdate = self.startdate
        enddate = self.enddate
        emissiondate_veh = self.emissiondate_veh
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset_veh = self.offset_veh
        
        Emissions_ratios = self.Emissions_ratios       
        PMspeciation = self.PMspeciation                 
        
        EmissionLpgExhConfig = LEC.EmissionLpgExhConfigClass(self.logger, self.justif, 
                                                             templatepath, templatefile_lpgExh, 
                                                             buildemissionpath, buildemissionsubdir_lpgExh,
                                                             binpath_lpgExh, bin_lpgExh, bininitline,
                                                             startdate, enddate, emissiondate_veh, 
                                                             emissions_edms_DATA, offset_veh,
                                                             Emissions_ratios, PMspeciation,                                                            
                                                             **self.kwargs)
        return EmissionLpgExhConfig     
                      
############33
    def init_EmissionPetEvpConfigClass(self):
        ''' This method initialise the EmissionPetEvpConfigClass
        '''
        import Sources.BuildEmissions.EmissionPetEvpConfig as PEVC
        
        buildemissionpath = self.buildemissionpath_static
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_petEvp = self.templatefile_petEvp
        binpath_petEvp = self.binpath_petEvp
        bin_petEvp = self.bin_petEvp
        buildemissionsubdir_petEvp = self.buildemissionsubdir_petEvp 
        bininitline = self.bininitline

        startdate = self.startdate
        enddate = self.enddate
        emissiondate_veh = self.emissiondate_veh
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset_veh = self.offset_veh

        Emissions_ratios = self.Emissions_ratios      
        PMspeciation = self.PMspeciation          
        
        EmissionPetEvpConfig = PEVC.EmissionPetEvpConfigClass(self.logger, self.justif, 
                                                              templatepath, templatefile_petEvp, 
                                                              buildemissionpath, buildemissionsubdir_petEvp,
                                                              binpath_petEvp,bin_petEvp, bininitline,
                                                              startdate, enddate, emissiondate_veh, 
                                                              emissions_edms_DATA, offset_veh,
                                                              Emissions_ratios, PMspeciation,
                                                              **self.kwargs)
        return  EmissionPetEvpConfig   
                           
############33
    def init_EmissionComdomesticConfigClass(self):
        ''' This method initialise the EmissionComdomesticConfigClass
        '''
        import Sources.BuildEmissions.EmissionComdomesticConfig as CDC
        
        buildemissionpath = self.buildemissionpath_static
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_comdomestic = self.templatefile_comdomestic
        binpath_comdomestic = self.binpath_comdomestic
        bin_comdomestic = self.bin_comdomestic
        buildemissionsubdir_comdomestic = self.buildemissionsubdir_comdomestic
        bininitline = self.bininitline

        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference between local time and UTC, (remember daylight savings)
        offset = self.offset

        Emissions_ratios = self.Emissions_ratios 
        PMspeciation = self.PMspeciation               
        
        EmissionComdomesticConfig = CDC.EmissionComdomesticConfigClass(self.logger, self.justif, 
                                                                       templatepath, templatefile_comdomestic, 
                                                                       buildemissionpath, buildemissionsubdir_comdomestic, 
                                                                       binpath_comdomestic, bin_comdomestic, bininitline,
                                                                       startdate, enddate, emissiondate, 
                                                                       emissions_edms_DATA, offset,
                                                                       Emissions_ratios, PMspeciation,                                                        
                                                                       **self.kwargs)
        return EmissionComdomesticConfig 
                            
############33
    def init_EmissionWoodHeatersConfigClass(self):
        ''' This method initialise the EmissionWoodheatersConfigClass
        '''
        
        import Sources.BuildEmissions.EmissionWoodHeatersConfig as WC
        
        buildemissionpath = self.buildemissionpath_static
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_woodheaters = self.templatefile_woodheaters
        binpath_whe = self.binpath_whe
        bin_whe = self.bin_whe
        buildemissionsubdir_woodheaters = self.buildemissionsubdir_woodheaters
        bininitline = self.bininitline

        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        emissiondate_whe = self.emissiondate_whe
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset_whe = self.offset_whe

        Emissions_ratios = self.Emissions_ratios
        PMspeciation = self.PMspeciation                
        
        EmissionWoodheatersConfig = WC.EmissionWoodheatersConfigClass(self.logger, self.justif, 
                                                                      templatepath, templatefile_woodheaters, 
                                                                      buildemissionpath, buildemissionsubdir_woodheaters, 
                                                                      binpath_whe, bin_whe, bininitline,
                                                                      startdate, enddate, emissiondate_whe, 
                                                                      emissions_edms_DATA, offset_whe,
                                                                      Emissions_ratios, PMspeciation,                                                             
                                                                      **self.kwargs)
        return EmissionWoodheatersConfig  
                           
############33
    def init_EmissionPSEConfigClass(self):
        ''' This method initialise the EmissionPSEConfigClass
        '''
        import Sources.BuildEmissions.EmissionPSEConfig as PSEC
        
        justif = self.justif
        logger = self.logger
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath
        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        TimestampUTC = self.TimestampUTC
        
        offset = self.offset
        templatepath = self.templatepath
        emissions_edms_DATA = self.emissions_edms_DATA
        emissions_power_DATA = self.emissions_power_DATA
        buildemissionpath_static = self.buildemissionpath_static
        buildemissionpath_dynamic = self.buildemissionpath_dynamic
        buildemissionsubdir_pse = self.buildemissionsubdir_pse

        templatefile_PSEmerge = self.templatefile_PSEmerge
        binpath_PSEmerge = self.binpath_PSEmerge
        bin_PSEmerge = self.bin_PSEmerge
        
        templatefile_PSEMM = self.templatefile_PSEMM
        binpath_PSEMM = self.binpath_PSEMM
        bin_PSEMM = self.bin_PSEMM
        
        templatefile_coalonlywkdays = self.templatefile_coalonlywkdays
        binpath_coalonlywkdays = self.binpath_coalonlywkdays
        bin_coalonlywkdays = self.bin_coalonlywkdays
        buildemissionsubdir_coalonlywkdays = self.buildemissionsubdir_coalonlywkdays

        templatefile_gasonlywkdays = self.templatefile_gasonlywkdays
        binpath_gasonlywkdays = self.binpath_gasonlywkdays
        bin_gasonlywkdays = self.bin_gasonlywkdays
        buildemissionsubdir_gasonlywkdays = self.buildemissionsubdir_gasonlywkdays

        templatefile_restwkdays = self.templatefile_restwkdays
        binpath_restwkdays = self.binpath_restwkdays
        bin_restwkdays = self.bin_restwkdays
        buildemissionsubdir_restwkdays = self.buildemissionsubdir_restwkdays
        

        
#########
#    Coal powerstation
        templatefile_power = self.templatefile_power
        binpath_power = self.binpath_power
        bin_power = self.bin_power
        buildemissionsubdir_power = self.buildemissionsubdir_power
        timeresolvedcoalpower = self.timeresolvedcoalpower

#######
#    Fires
        Run_HRB = self.Run_HRB 
        Run_Bushfire = self.Run_Bushfire

#######        
#    Bushfire OEH
        emissions_rfs_DATA_OEH = self.emissions_rfs_DATA_OEH
        templatefile_rfs_OEH = self.templatefile_rfs_OEH
        binpath_rfs_OEH = self.binpath_rfs_OEH
        bin_rfs_OEH = self.bin_rfs_OEH
        buildemissionsubdir_rfs_OEH = self.buildemissionsubdir_rfs_OEH
#       include or not Bushfire OEH flavour.
        bushfireOEH = self.bushfireOEH 

#######        
#    Bushfire CSIRO
        emissions_rfs_DATA_CSIRO = self.emissions_rfs_DATA_CSIRO
        templatefile_rfs_CSIRO = self.templatefile_rfs_CSIRO
        binpath_rfs_CSIRO = self.binpath_rfs_CSIRO
        bin_rfs_CSIRO = self.bin_rfs_CSIRO
        buildemissionsubdir_rfs_CSIRO = self.buildemissionsubdir_rfs_CSIRO
#       include or not Bushfire CSIRO flavour.
        bushfireCSIRO = self.bushfireCSIRO 
        offset_bushfire_csiro = self.offset_bushfire_csiro
        burn_start_hour_csiro = self.burn_start_hour_csiro
#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset = self.offset
        
        Emissions_ratios = self.Emissions_ratios
        PMspeciation = self.PMspeciation        
        
        EmissionPSEConfig = PSEC.EmissionPSEConfigClass(logger, justif, 
                 templatepath, Basebinpath, self.bininitline,
                 templatefile_coalonlywkdays, templatefile_gasonlywkdays, templatefile_restwkdays, templatefile_power, 
                 buildemissionpath_static, buildemissionpath_dynamic, buildemissionsubdir_pse, templatefile_PSEmerge, binpath_PSEmerge, bin_PSEmerge,
                 buildemissionsubdir_coalonlywkdays, buildemissionsubdir_gasonlywkdays, buildemissionsubdir_restwkdays, buildemissionsubdir_power,
                 Emissions_ratios, PMspeciation,
                 binpath_coalonlywkdays, binpath_gasonlywkdays, binpath_restwkdays, binpath_power,
                 bin_coalonlywkdays, bin_gasonlywkdays, bin_restwkdays, bin_power,
                 startdate, enddate, emissiondate, TimestampUTC,
                 emissions_edms_DATA, emissions_power_DATA, offset, timeresolvedcoalpower,
                 Run_HRB, Run_Bushfire,
                 emissions_rfs_DATA_OEH, templatefile_rfs_OEH, binpath_rfs_OEH, bin_rfs_OEH, buildemissionsubdir_rfs_OEH, bushfireOEH,
                 emissions_rfs_DATA_CSIRO, templatefile_rfs_CSIRO, binpath_rfs_CSIRO, bin_rfs_CSIRO, buildemissionsubdir_rfs_CSIRO, bushfireCSIRO, offset_bushfire_csiro, burn_start_hour_csiro, 
                 templatefile_PSEMM, binpath_PSEMM, bin_PSEMM,
                 **self.kwargs)
                 
        return EmissionPSEConfig                       

############33
    def Configure_CTM_prep_init_and_Update_Simulation(self, CtmPrepInitConfig, SimulationConfig):
        ''' 
        Grab the latest Emission configuration to finish the emission characteristics and paths in the config file of the prep of CTM
        the output is a modified CtmPrepInitConfig config object
        Also, update the main simulation object with the global emission scaling for the ConfigGrid template
        '''
#       maybe each true/false state should be a method for each sub category of the emission config object?
#         to be sure we grab the lastest update after pimping each emission group?
        CtmPrepInitConfig.includeemission_vpx = self.includeemission_vpx
        CtmPrepInitConfig.includeemission_vdx = self.includeemission_vdx
        CtmPrepInitConfig.includeemission_vlx = self.includeemission_vlx
        CtmPrepInitConfig.includeemission_vpv = self.includeemission_vpv
        CtmPrepInitConfig.includeemission_gse = self.includeemission_gse
        CtmPrepInitConfig.includeemission_whe=  self.includeemission_whe
        CtmPrepInitConfig.includeemission_pse=  self.includeemission_pse
        
        #emissiongroupfilename=[]
        # a dictionary instead ?
        emissiongroupfilename = {}
        EmissionGroup = []
        if CtmPrepInitConfig.includeemission_vpx:
            if self.buildemission_vpx_updated:
           #emissiongroupfilename.append(EmissionPetExhConfig.fullnameoutputfile()) 
                emissiongroupfilename["vpx"] = self.EmissionPetExhConfig_fullnameoutputfile
                EmissionGroup.append("vpx")
            else:
                self.logger.warning("vpx emission not updated")    
        if CtmPrepInitConfig.includeemission_vdx:
            if self.buildemission_vdx_updated:
           #emissiongroupfilename.append(EmissionDieExhConfig.fullnameoutputfile()) 
                emissiongroupfilename["vdx"] = self.EmissionDieExhConfig_fullnameoutputfile
                EmissionGroup.append("vdx")
            else:
                self.logger.warning("vdx emission not updated")    
        if CtmPrepInitConfig.includeemission_vlx:
            if self.buildemission_vlx_updated:
           #emissiongroupfilename.append(EmissionLpgExhConfig.fullnameoutputfile()) 
                emissiongroupfilename["vlx"] = self.EmissionLpgExhConfig_fullnameoutputfile
                EmissionGroup.append("vlx")
            else:
                self.logger.warning("vlx emission not updated")  
        if CtmPrepInitConfig.includeemission_vpv:
            if self.buildemission_vpv_updated:
           #emissiongroupfilename.append(EmissionPetEvpConfig.fullnameoutputfile()) 
                emissiongroupfilename["vpv"] = self.EmissionPetEvpConfig_fullnameoutputfile
                EmissionGroup.append("vpv")
            else:
                self.logger.warning("vpv emission not updated")   
        if CtmPrepInitConfig.includeemission_gse:
            if self.buildemission_gse_updated:
           #emissiongroupfilename.append(EmissionComdomesticConfig.fullnameoutputfile()) 
                emissiongroupfilename["gse"] = self.EmissionComdomesticConfig_fullnameoutputfile
                EmissionGroup.append("gse")
            else:
                self.logger.warning("gse emission not updated")    
        if CtmPrepInitConfig.includeemission_whe:
            if self.buildemission_whe_updated:
           #emissiongroupfilename.append(EmissionWoodheatersConfig.fullnameoutputfile()) 
                emissiongroupfilename["whe"] = self.EmissionWoodheatersConfig_fullnameoutputfile
                EmissionGroup.append("whe")
            else:
                self.logger.warning("whe emission not updated")   
        if CtmPrepInitConfig.includeemission_pse:
            if self.buildemission_pse_updated:
           #emissiongroupfilename.append(XXXXX) 
                emissiongroupfilename["pse"] = self.EmissionPSEConfig_fullnameoutputfile
            else:
                self.logger.warning("pse emission not updated")    
        
        CtmPrepInitConfig.emissiongroupfilename = emissiongroupfilename
        CtmPrepInitConfig.EmissionGroup = EmissionGroup
        SimulationConfig.emissiongroupfilename = emissiongroupfilename
        SimulationConfig.EmissionGroup = EmissionGroup
        SimulationConfig.EmissionGlobalScaling = self.EmissionGlobalScaling
        SimulationConfig.emissions_edms_DATA = self.emissions_edms_DATA
        
        CtmPrepInitConfig.emissionconfigured = True
        return  

################################
    def Create_Emission_dirs(self):
        """ This method build the emission dirs.
        """
        try :
            os.makedirs(self.buildemissionpath_static)
            self.logger.info('Creation static emission Dir= {msg}'.format(msg=self.buildemissionpath_static).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('static emission Dir = {msg}'.format(msg=self.buildemissionpath_static).ljust(self.justif-13,'.') + 'Already exist')
            pass

        try :
            os.makedirs(self.buildemissionpath_dynamic)
            self.logger.info('Creation dynamic emission Dir= {msg}'.format(msg=self.buildemissionpath_dynamic).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('dynamic emission Dir = {msg}'.format(msg=self.buildemissionpath_dynamic).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

################################
    def Configure_EDMS_PM_speciation(self):
        """ This method sets up the specifics of the PM speciations of the inventory.
        """
        if self.PM10speciation:
            self.PMspeciation = 'PM10'
        else:
            self.PMspeciation = 'PM25'
        
        if self.emissiondate.year == 2008:
            if self.PM10speciation:
                self.Emissions_ratios = IR2008.Inventory_Ratios_2008(self.logger, self.justif)
                self.EDMS_dirname_template = "emission{year}NH3"
                self.logger.info("EDMS 2008 inventory, PM10 speciation configuration".ljust(self.justif-2,'.') + 'OK')
            else :
                self.logger.error("EDMS 2008 inventory, PM2.5 speciation DO NOT EXIST")
                os.sys.exit("Sorry :(((")

        if self.emissiondate.year == 2013:
            if self.PM10speciation:
                self.Emissions_ratios = IR2013PM10.Inventory_Ratios_PM10_2013(self.logger, self.justif)
                self.EDMS_dirname_template = "emission{year}NH3_{PM}"
                self.logger.info("EDMS 2013 inventory, PM10 speciation configuration".ljust(self.justif-2,'.') + 'OK')
            else :
                self.Emissions_ratios = IR2013PM25.Inventory_Ratios_PM25_2013(self.logger, self.justif)
                self.EDMS_dirname_template = "emission{year}NH3_{PM}"
                self.logger.info("EDMS 2013 inventory, PM2.5 speciation configuration".ljust(self.justif-2,'.') + 'OK')
        return

################################
    def Compute_EDMS_directory(self):
        """ This method compute the EDMS directory.
        """
        edmsdir = self.EDMS_dirname_template.format(year = self.emissiondate.year, PM = self.PMspeciation)
        self.emissions_edms_DATA = os.path.join(self.emissions_edms_DATA_basedir, edmsdir)
        return
        
################################
    def init_MEMS(self,):
        """ This method configure the offline MEMS emissions module.
        """
        from . import MEMSConfig as MEMS
        MEMSConfig = MEMS.MEMSConfigClass(
                 self.logger, self.justif, self.bininitline,
                 self.SimulationPath, self.buildemissionpath_static, self.buildemissionpath_dynamic,
                 self.startdateUTC, self.enddate, self.emissiondate, self.TimestampUTC, 
                 self.ResolutionNames,
                 )
        MEMSConfig.includeemission_MEMS = self.includeemission_MEMS
        
        MEMSConfig.Run_Dust = self.Run_Dust
        MEMSConfig.includeemission_MEMS_dust = self.includeemission_MEMS_dust
        MEMSConfig.MEMS_dust_source_dir = self.MEMS_dust_source_dir
        MEMSConfig.MEMS_dust_source_file = self.MEMS_dust_source_file
        MEMSConfig.DustModel = self.DustModel
        MEMSConfig.dust_updated = self.dust_updated
                 
        return MEMSConfig

################################
    def Build_Emissions(self):
        """ This method build the whole emissions.
        """
        self.logger.info(''.ljust(self.justif,'-'))
        self.logger.debug('Building Emissions with the lot'.center(self.justif,'|'))
        self.logger.info(''.ljust(self.justif,'-'))

        self.Create_Emission_dirs()
        self.logger.info('EmissionConfig dir creation'.ljust(self.justif-2,'.')+"OK")

#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #PetExh
        self.logger.info('Building petExh'.ljust(self.justif,'.'))
        EmissionPetExhConfig = self.init_EmissionPetExhConfigClass()
        self.logger.info('EmissionPetExhConfig object init'.ljust(self.justif-2,'.')+"OK")
        if (self.includeemission_vpx and 
                not self.buildemission_vpx and
                not EmissionPetExhConfig.Detect_outputfile()):
           self.logger.warning("vpx emission outpufile".ljust(self.justif-9,'.')+"NOT FOUND")
           self.logger.warning("vpx emission include ON but vpx emission build OFF")
           self.logger.warning("auto building triggered")
           self.buildemission_vpx = True
        
        if self.buildemission_vpx:  
            self.logger.info('Building vpx'.ljust(self.justif,'.'))
            EmissionPetExhConfig.Build_template_emission_petExh()
            self.logger.info('EmissionPetExhConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionPetExhConfig.Build_petExh()
            self.logger.info('EmissionPetExhConfig emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building vpx'.ljust(self.justif,'.'))
        EmissionPetExhConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")

#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #DieExh
        self.logger.info('Building dieExh'.ljust(self.justif,'.'))
        EmissionDieExhConfig = self.init_EmissionDieExhConfigClass()
        self.logger.info('EmissionDieExhConfig object init'.ljust(self.justif-2,'.')+"OK")
        if (self.includeemission_vdx and 
                not self.buildemission_vdx and
                not EmissionDieExhConfig.Detect_outputfile()):
           self.logger.warning("vdx emission outpufile".ljust(self.justif-9,'.')+"NOT FOUND")
           self.logger.warning("vdx emission include ON but vdx emission build OFF")
           self.logger.warning("auto building triggered")
           self.buildemission_vdx = True

        if self.buildemission_vdx:
            self.logger.info('Building vdx'.ljust(self.justif,'.'))
            EmissionDieExhConfig.Build_template_dieExh()
            self.logger.info('EmissionDieExhConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionDieExhConfig.Build_dieExh()
            self.logger.info('EmissionDieExhConfig emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building vdx'.ljust(self.justif,'.'))
        EmissionDieExhConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")

#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #LpgExh
        self.logger.info('Building lpgExh'.ljust(self.justif,'.'))
        EmissionLpgExhConfig = self.init_EmissionLpgExhConfigClass()
        self.logger.info('EmissionLpgExhConfig object init'.ljust(self.justif-2,'.')+"OK")
        if (self.includeemission_vlx and 
                not self.buildemission_vlx and
                not EmissionLpgExhConfig.Detect_outputfile()):
           self.logger.warning("vlx emission outpufile".ljust(self.justif-9,'.')+"NOT FOUND")
           self.logger.warning("vlx emission include ON but vlx emission build OFF")
           self.logger.warning("auto building triggered")
           self.buildemission_vlx = True

        if self.buildemission_vlx:
            self.logger.info('Building vlx'.ljust(self.justif,'.'))
            EmissionLpgExhConfig.Build_template_lpgExh()
            self.logger.info('EmissionLpgExhConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionLpgExhConfig.Build_lpgExh()
            self.logger.info('EmissionLpgExhConfig emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building vlx'.ljust(self.justif,'.'))
        EmissionLpgExhConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")

#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #PetEvp
        self.logger.info('Building petEvp'.ljust(self.justif,'.'))
        EmissionPetEvpConfig = self.init_EmissionPetEvpConfigClass()
        self.logger.info('EmissionPetEvpConfig object init'.ljust(self.justif-2,'.')+"OK")
        if (self.includeemission_vpv and 
                not self.buildemission_vpv and
                not EmissionPetEvpConfig.Detect_outputfile()):
           self.logger.warning("vpv emission outpufile".ljust(self.justif-9,'.')+"NOT FOUND")
           self.logger.warning("vpv emission include ON but vpv emission build OFF")
           self.logger.warning("auto building triggered")
           self.buildemission_vpv = True

        if self.buildemission_vpv:
            self.logger.info('Building vpv'.ljust(self.justif,'.'))
            EmissionPetEvpConfig.Build_template_petEvp()
            self.logger.info('EmissionPetEvpConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionPetEvpConfig.Build_petEvp()
            self.logger.info('EmissionPetEvpConfig emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building vpv'.ljust(self.justif,'.'))
        EmissionPetEvpConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")

#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #ComDom
        self.logger.info('Building commercial domestic'.ljust(self.justif,'.'))
        EmissionComdomesticConfig = self.init_EmissionComdomesticConfigClass()
        self.logger.info('EmissionComdomesticConfig object init'.ljust(self.justif-2,'.')+"OK")
        if (self.includeemission_gse and 
                not self.buildemission_gse and
                not EmissionComdomesticConfig.Detect_outputfile()):
           self.logger.warning("gse emission outpufile".ljust(self.justif-9,'.')+"NOT FOUND")
           self.logger.warning("gse emission include ON but gse emission build OFF")
           self.logger.warning("auto building triggered")
           self.buildemission_gse = True

        if self.buildemission_gse:
            self.logger.info('Building gse'.ljust(self.justif,'.'))
            EmissionComdomesticConfig.Build_template_comdomestic()
            self.logger.info('EmissionComdomesticConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionComdomesticConfig.Build_comdomestic()
            self.logger.info('EmissionComdomesticConfig emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building gse'.ljust(self.justif,'.'))
        EmissionComdomesticConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")

#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #Woodheaters
        self.logger.debug('Building woodheaters'.ljust(self.justif,'.'))
        EmissionWoodheatersConfig = self.init_EmissionWoodHeatersConfigClass()
        self.logger.info('EmissionWoodheatersConfig object init'.ljust(self.justif-2,'.')+"OK")
        if (self.includeemission_whe and 
                not self.buildemission_whe and
                not EmissionWoodheatersConfig.Detect_outputfile()):
           self.logger.warning("whe emission outpufile".ljust(self.justif-9,'.')+"NOT FOUND")
           self.logger.warning("whe emission include ON but whe emission build OFF")
           self.logger.warning("auto building triggered")
           self.buildemission_whe = True

        if self.buildemission_whe:    
            self.logger.info('Building whe'.ljust(self.justif,'.'))
            EmissionWoodheatersConfig.Build_template_woodheaters()
            self.logger.info('EmissionWoodheatersConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionWoodheatersConfig.Build_wood_heaters()
            self.logger.info('EmissionWoodheatersConfig emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building whe'.ljust(self.justif,'.'))
        EmissionWoodheatersConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")
#--------------------------------------------------------------------------------------------------------------------------------------------------------
        #PSE
        self.logger.info('Building  PSE'.ljust(self.justif,'.'))
        EmissionPSEConfig = self.init_EmissionPSEConfigClass()
        self.logger.info('EmissionPSEConfig object init'.ljust(self.justif-2,'.')+"OK")
        EmissionPSEConfig.Create_Emission_dirs()
        self.logger.info('EmissionPSEConfig dir creation'.ljust(self.justif-2,'.')+"OK")
        if self.buildemission_pse:    
            EmissionPSEConfig.Build_Emissions()
            self.logger.info('PSE Emissions file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.info('NOT Building pse'.ljust(self.justif,'.'))
        EmissionPSEConfig.EmissionConfig_update(self)
        self.logger.info('Master emission object'.ljust(self.justif-7,'.')+"UPDATED")

        return
#################################################################################################################################################################

if __name__ == '__main__':
    #from .. import InitLogging as IL
    import Sources.DateMagics as DM
    
    loggername = 'build_emission'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    
    buildemissionpath = 'Emissions-Test' 
    buildemissionpath = '/mnt/climate/cas/scratch/CCAM_CTM-runs/Emissions_SLES15'
    SimulationPath = 'Emissions-Test'
    Basebinpath = '/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran'
    templatepath = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    #templatepath = '/home/monkk/repositories/ccam-ctm-automation/Python-Scripts/Templates'                
    buildemissionpath_static = os.path.join(buildemissionpath, 'static')
    buildemissionpath_dynamic = os.path.join(buildemissionpath,'dynamic')

    startdate = dt.datetime(2017,4,4)
    #startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2019,11,19),"UTC")
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2017,4,4),"UTC")
    enddate = startdate + dt.timedelta(days=3, hours=00)
    emissiondate = dt.datetime(2013,1,1)
    #emissiondate = dt.datetime(2008,1,1)
    TimestampUTC = dt.datetime(2018,2,1)

    PM10speciation = True
    PM10speciation = False

    PartitionName = 'CAS'
    PartitionName = 'SLES15'
    bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | srun -p {partition} -n1 "
    #bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | valgrind --leak-check=yes "
    #bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | "
    bininitline = bininitline_template.format(partition = PartitionName)

    Emissions_edms_DATA = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/emission2008NH3'
    #Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2008NH3'
    Emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2013NH3_PM10'
    Emissions_edms_DATA_base = '/mnt/climate/cas/project/EmissionSources-Forecast'

    logger.info(''.ljust(justif,'-'))
    logger.debug('Building emission tests'.center(justif,'|'))
    logger.info(''.ljust(justif,'-'))

    testfile_petExh = '../../TestFiles_13/petExh.run'
    buildemissionsubdir_petExh = 'vpx'

    testfile_dieExh = '../../TestFiles_13/dieExh.run'
    buildemissionsubdir_dieExh = 'vdx'

    testfile_lpgExh = '../../TestFiles_13/lpgExh.run'
    buildemissionsubdir_lpgExh = 'vlx'

    testfile_petEvp = '../../TestFiles_13/petEvp.run'
    buildemissionsubdir_petEvp='vpv' 

    testfile_comdomestic = '../../TestFiles_13/comdomestic.run'
    buildemissionsubdir_comdomestic = 'gse'

    testfile_woodheaters = '../../TestFiles_13/woodheaters.run'
    buildemissionsubdir_woodheaters = 'whe'
    
    
#####################################################################################
    EmissionConfig = EmissionConfigClass(logger, justif, 
                   startdate, enddate, emissiondate, TimestampUTC,
                   SimulationPath, buildemissionpath_static, buildemissionpath_dynamic,  
                   Basebinpath, templatepath, Emissions_edms_DATA_base, PM10speciation,
                   bininitline, binary = True)
    logger.info('EmissionConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionConfig.Create_Emission_dirs()
    logger.info('EmissionPSEConfig dir creation'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #PetExh
    EmissionPetExhConfig = EmissionConfig.init_EmissionPetExhConfigClass()
    logger.info('EmissionPetExhConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionPetExhConfig.Build_template_emission_petExh()
    logger.debug('-------------------------------')
    logger.debug('Diff petExh')
    #IL.PrintDiff(os.path.join(buildemissionpath_static, buildemissionsubdir_petExh, 'petExh.run'), testfile_petExh, logger)
    logger.info('EmissionPetExhConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionPetExhConfig.Build_petExh()
    logger.info('petExh full output name= %s', EmissionPetExhConfig.fullnameoutputfile())
    logger.info('EmissionPetExhConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #DieExh
    EmissionDieExhConfig = EmissionConfig.init_EmissionDieExhConfigClass()
    logger.info('EmissionDieExhConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionDieExhConfig.Build_template_dieExh()
    logger.debug('-------------------------------')
    logger.debug('Diff dieExh')
    #IL.PrintDiff(os.path.join(buildemissionpath_static, buildemissionsubdir_dieExh, 'dieExh.run'), testfile_dieExh, logger)
    logger.info('EmissionDieExhConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionDieExhConfig.Build_dieExh()
    logger.info('dieExh full output name= %s', EmissionDieExhConfig.fullnameoutputfile())
    logger.info('EmissionDieExhConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #LpgExh
    EmissionLpgExhConfig = EmissionConfig.init_EmissionLpgExhConfigClass()
    logger.info('EmissionLpgExhConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionLpgExhConfig.Build_template_lpgExh()
    logger.debug('-------------------------------')
    logger.debug('Diff lpgExh')
    #IL.PrintDiff(os.path.join(buildemissionpath_static, buildemissionsubdir_lpgExh, 'lpgExh.run'), testfile_lpgExh, logger)
    logger.info('EmissionLpgExhConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionLpgExhConfig.Build_lpgExh()
    logger.info('LpgExh full output name= %s', EmissionLpgExhConfig.fullnameoutputfile())
    logger.info('EmissionLpgExhConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #PetEvp
    EmissionPetEvpConfig = EmissionConfig.init_EmissionPetEvpConfigClass()
    logger.info('EmissionPetEvpConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionPetEvpConfig.Build_template_petEvp()
    logger.debug('-------------------------------')
    logger.debug('Diff petEvp')
    #IL.PrintDiff(os.path.join(buildemissionpath_static, buildemissionsubdir_petEvp, 'petEvp.run'), testfile_petEvp, logger)
    logger.info('EmissionPetEvpConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionPetEvpConfig.Build_petEvp()
    logger.info('PetEvp full output name= %s', EmissionPetEvpConfig.fullnameoutputfile())
    logger.info('EmissionPetEvpConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #ComDom
    EmissionComdomesticConfig = EmissionConfig.init_EmissionComdomesticConfigClass()
    logger.info('EmissionComdomesticConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionComdomesticConfig.Build_template_comdomestic()
    logger.debug('-------------------------------')
    logger.debug('Diff comdomestic')
    #IL.PrintDiff(os.path.join(buildemissionpath_static, buildemissionsubdir_comdomestic, 'comdomestic.run'), testfile_comdomestic, logger)
    logger.info('EmissionComdomesticConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionComdomesticConfig.Build_comdomestic()
    logger.info('Comdomestic full output name= %s',EmissionComdomesticConfig.fullnameoutputfile())
    logger.info('EmissionComdomesticConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    #Woodheaters
    EmissionWoodheatersConfig = EmissionConfig.init_EmissionWoodHeatersConfigClass()
    logger.info('EmissionWoodheatersConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionWoodheatersConfig.Build_template_woodheaters()
    logger.debug('-------------------------------')
    logger.debug('Diff woohheaters')
    #IL.PrintDiff(os.path.join(buildemissionpath_static, buildemissionsubdir_woodheaters, 'woodheaters.run'), testfile_woodheaters, logger)
    logger.info('EmissionWoodheatersConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionWoodheatersConfig.Build_wood_heaters()
    logger.debug('Woodheaters full output name= %s',EmissionWoodheatersConfig.fullnameoutputfile())
    logger.info('EmissionWoodheatersConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    #PSE
    EmissionPSEConfig = EmissionConfig.init_EmissionPSEConfigClass()
    logger.info('EmissionPSEConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionPSEConfig.Create_Emission_dirs()
    logger.info('EmissionPSEConfig dir creation'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    #EmissionPSEConfig.Build_Emissions()
    logger.debug('PSE output name= %s',EmissionPSEConfig.fullnameoutputfile())
    logger.info('PSE Emissions file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    #Build all
    EmissionConfig.Build_Emissions()
    logger.info('Whole Emission files with the lot build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
   
    
