"""
.. module:: EmissionScaling
   :platform: Unix
   :synopsis: Everything needed to scale the emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
###########################################################################################
class EmissionGlobalScalingClass(object):
    '''
    This class is the global container of the scalings of the emissions
    '''
    def __init__(self):
        '''
        This class is a generic container of the emission scalings.
        This scalings appears in the ConfigGrid template, written in DumpConfigGrid.py
        Default init is a scaling of 1. 
        '''

        self.SCALE_MOTOR_VEHICLES =  EmissionGroupScalingClass()
        self.SCALE_COMMERCIAL_DOMESTIC =  EmissionGroupScalingClass()
        self.SCALE_INDUSTRIAL =  EmissionGroupScalingClass() 
        self.SCALE_FIRE =  EmissionGroupScalingClass() 
        self.SCALE_DUST =  EmissionGroupScalingClass() 
        self.SCALE_NATURAL_BIOGENIC =  EmissionGroupScalingClass()
        self.APA = APAGroupScalingClass()
        self.AOA = AOABOAGroupScalingClass()
        return
        
###########################################################################################
class EmissionGroupScalingClass(object):
    '''
    This class is the global multiplier to apply to emission groups
    '''
    def __init__(self):
        '''
        This class is generic, and define the scales for the 4 emissition var.
        This scaling appears in the ConfigGrid template, written in DumpConfigGrid.py
        Default init is a scaling of 1. 
        '''
        self.NOX = 1.
        self.VOC = 1.
        self.PM10 = 1.
        self.CO = 1.
        return
###########################################################################################
class APAGroupScalingClass(object):
    '''
    This class is the scalings to apply to APA chemical groups
    '''
    def __init__(self):
        '''
        This class is generic, and define the scales for the APA chemicals.
        This scaling appears in the ConfigGrid template, written in DumpConfigGrid.py
        Default init is a scaling of 1. 
        '''
        self.APA1 = 1.
        self.APA2 = 1.
        self.APA3 = 1.
        self.APA4 = 1.
        self.APA5 = 1.
        self.APA6 = 1.
        self.APA7 = 1.
        self.APA8 = 1.
        self.APA9 = 1.
        self.OC10 = 1.
        return
###########################################################################################
class AOABOAGroupScalingClass(object):
    '''
    This class is the scalings to apply to AOA and BOA chemical groups
    '''
    def __init__(self):
        '''
        This class is generic, and define the scales for the APA and BOA chemicals.
        This scaling appears in the ConfigGrid template, written in DumpConfigGrid.py
        Default init is a scaling of 1. 
        '''
        self.AOA1 = 1.
        self.AOA2 = 1.
        self.AOA3 = 1.
        self.AOA4 = 1.
        self.AOA5 = 1.
        self.AOA6 = 1.
        self.AOA7 = 1.
        self.AOA8 = 1.
        self.BOA1 = 1.
        self.BOA2 = 1.
        self.BOA3 = 1.
        self.BOA4 = 1.
        self.BOA5 = 1.
        self.BOA6 = 1.
        self.OOA = 1.
        return
###########################################################################################
if __name__ == '__main__':
    try:
        from .. import InitLogging as IL
    except:
        import sys
        sys.path.append('/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Sources')
        import InitLogging as IL
    
    loggername = 'Emission_scaling'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    EGS = EmissionGlobalScalingClass()
    print EGS.SCALE_MOTOR_VEHICLES.NOX
