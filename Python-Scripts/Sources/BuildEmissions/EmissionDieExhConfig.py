"""
.. module:: EmissionDieExhConfig
   :platform: Unix
   :synopsis: Everything needed to build the EmissionDieExh emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import string
import datetime as dt
import os
import sys
import subprocess
import fnmatch
#sys.path.append('..')
try:
    from . import Tools
    from .. import JinjaUtils as JJU
except:
    import Tools
    sys.path.append('..')
    import JinjaUtils as JJU
###########################################################################################
class EmissionDieExhConfigClass(object):
    """ This class defines a emission object, that contains all the parameters to build diesel exhaust emissions 
    This class defines and configures a Diesel Exhaust emission object.
    It contains all the parameters and the methods to build DieExh emissions.
    
    The class is self contained and can be initialised from original data, it doesn't depend on higher level classes.
    it contains all the methods to configure and generate emissions.
    
    Attributes
    -----------
        logger : logging.logger
            instance of a logger to output message
        justif : int
            max message width to justify logger output     
        startdate : datetime
            start date of the emissions to build as a UTC datetime.
        startyear : str
            year of the starting date of the emission windows.
        startmonth : str
            month of the starting date of the emission windows.
        startday : str
            day of the starting date of the emission windows.
        enddate : datetime
            end date of the emissions to build as a UTC datetime.
        endyear : str
            year of the ending date of the emission windows.
        endmonth : str
            month of the ending date of the emission windows.
        endday : str
            day of the ending date of the emission windows.
        datapath : str
            path to find the RFS fire data and parametres.
        emissiondate : datetime.datetime
            date of the emission 
        emissionmonth : str
            reduced month name from the emission date.
        emissionyear : str
            year of the emissions
        self.offset_veh : int
            hour offset of the emission to take in account timezones 
        binpath : str
            path to find the binary to build individual scar emissions.
        bin : str
            binary filename to build individual scar emissions.
        bininitline : str
            init line to setup env when running a binary
        userfile : str
            name of an user file = 'cogen_24h_emissions.csv'
        templatepath : str
            path to the template directory.
        templatefile : str
            name of the templatefile to build an emission file.
        buildemissionpath : str
            root of the main emission directory to build in. 
        buildemissionsubdir : str
            path from the main emission directory to build in.
        output_file_detected : bool
            logical switch to flag the existence or not of the output file to 
            autobuild.     
    
    """
    def __init__(self, logger, justif, 
                 templatepath, templatefile, 
                 buildemissionpath, buildemissionsubdir,
                 binpath, bin, bininitline,
                 startdate, enddate, emissiondate,
                 datapath, offset,
                 Inventory_ratios, PMspeciation,
                 **kwargs):
        self.binary = kwargs.get('binary', True)

        self.logger = logger
        self.justif = justif
        self.startdate = startdate
        self.enddate = enddate
        self.startyear = startdate.strftime('%Y')
        self.startmonth = startdate.strftime('%m')
        self.startday = startdate.strftime('%d')
        self.endyear = enddate.strftime('%Y')
        self.endmonth = enddate.strftime('%m')
        self.endday = enddate.strftime('%d')
        self.datapath = datapath
        self.emissiondate = emissiondate
        self.emissionmonth = string.lower(startdate.strftime('%b'))
        self.emissionyear = emissiondate.strftime('%y')
        self.offset_veh = str(offset)
        self.emission_name_year = self.emissionmonth+self.emissionyear
        self.binpath = binpath
        self.bin = bin
        self.Inventory_ratios = Inventory_ratios
        self.PMspeciation = PMspeciation
                
        self.ndays = (self.enddate - self.startdate).days
        self.fileoutput_suffix = 'emsns_gmr.vdx'
        if self.binary:
            self.fileoutput_suffix = '{prefix}.bin'.format(prefix = self.fileoutput_suffix)
        #self.fileoutput = '{date}_{suffix}'.format(date = self.startdate.strftime('%Y_%b'), suffix = self.fileoutput_suffix).lower()
        self.fileoutput = '{emissionyear}_{PMspec}_{date}_{ndays}days_{suffix}'.format(emissionyear = self.emissiondate.strftime('%Y'),
                                                                             PMspec = self.PMspeciation,
                                                                             date = self.startdate.strftime('%b_%a'), 
                                                                             ndays = self.ndays,
                                                                             suffix = self.fileoutput_suffix).lower()
        
        
        self.templatepath = templatepath
        self.templatefile = templatefile
        self.buildemissionpath = buildemissionpath
        self.buildemissionsubdir = buildemissionsubdir
        self.bininitline = bininitline
        self.output_file_detected = False
        self.filename = self.fileoutput
        return
###################################

    def fullnameoutputfile(self):
        #self.filename=self.startyear+'_'+string.lower(self.startdate.strftime('%b'))+'_'+self.fileoutput
        #self.filename = '{date}_{suffix}'.format(date = self.startdate.strftime('%Y_%b'), suffix = self.fileoutput_suffix).lower()
        self.filename = '{emissionyear}_{PMspec}_{date}_{ndays}days_{suffix}'.format(emissionyear = self.emissiondate.strftime('%Y'),
                                                                             PMspec = self.PMspeciation,
                                                                             date = self.startdate.strftime('%b_%a'), 
                                                                             ndays = self.ndays,
                                                                             suffix = self.fileoutput_suffix).lower()
        name = os.path.join(self.buildemissionpath,self.buildemissionsubdir, self.filename)
        self.fileoutput = self.filename
        return name
###################################
    def EmissionConfig_update(self,EmissionConfig):
        EmissionConfig.EmissionDieExhConfig_fullnameoutputfile = self.fullnameoutputfile()
        EmissionConfig.buildemission_vdx_updated=True        

###################################
    def Build_dirs(self):
        path = os.path.join(self.buildemissionpath, self.buildemissionsubdir)
        try :
            os.makedirs(path)
            self.logger.info('Creation Working Dir= {msg}'.format(msg=path).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('Working Dir = {msg}'.format(msg=path).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

############################################################################################################################
    def Build_template_dieExh(self):
        """ This function write the config file to build emission for the diesel exh emission
        """
        dirextension = self.buildemissionsubdir
        self.Build_dirs()

        filename = os.path.join(self.buildemissionpath,dirextension,'dieExh.run')
        File= open(filename,'w')

        template = JJU.LoadTemplate(self.templatepath,self.templatefile)

        emission = template.render({'EmissionDieExhConfig':self })
        File.write(emission)
        File.close()

        self.logger.info('Template creation'.ljust(self.justif-2,'.') + 'OK')
        return 

############################################################################################################################
    def Build_dieExh(self):
        """ This function write the emission file for the petrol exh emission
        """
        dirextension = self.buildemissionsubdir
        RunDir = os.path.join(self.buildemissionpath, dirextension)
        
        my_env = os.environ.copy()
        #Runline = "source /etc/profile.d/modules.sh | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp | " + \
        #               os.path.join(self.binpath, self.bin) +" dieExh.run"
        
        run = "{bin} dieExh.run".format(bin=os.path.join(self.binpath, self.bin))
        Runline = '{init} {run}'.format(init = self.bininitline, 
                                        run = run)

        
        PIPE = subprocess.PIPE
            
        #print(subprocess.Popen([ Runline ], shell=True, env=my_env, cwd=RunDir).wait())
        p1 = subprocess.Popen([Runline],shell=True, env=my_env, cwd=RunDir, bufsize=350000, stdout = PIPE, stdin = PIPE, stderr = PIPE)
    
        out, err = p1.communicate()
        output = out
        error = err
    
        self.logger.info(output)
        self.logger.error('Errors:')
        self.logger.error(error)

    
        self.logger.info('Emission file'.ljust(self.justif-2,'.') + 'OK')

        midinsert = 'vdx_'
        endinsert = '_cb05'
        Tools.Rename_emission_file(os.path.join(self.buildemissionpath, dirextension), midinsert, endinsert, self.logger)
        return 

################################################################################
    def Detect_outputfile(self):
        """
        This method detects the existence of the outputfile and return a boolean
        in addition to update a local var
        """
        MatchingPattern = self.fileoutput
        RunDir = os.path.join(self.buildemissionpath, self.buildemissionsubdir)
        self.output_file_detected = False
        if not os.path.isdir(RunDir):
            self.logger.warning("Emission directory".ljust(self.justif-9,'.') + 'NOT FOUND')
            return self.output_file_detected
        listdir = os.listdir(RunDir)
        filteredlist = fnmatch.filter(listdir, MatchingPattern)
        if filteredlist:
            self.logger.info("Emission output file".ljust(self.justif-5,'.') + 'FOUND')
            self.output_file_detected = True
        else:
            self.logger.warning("Emission output file".ljust(self.justif-9,'.') + 'NOT FOUND')
            
        return self.output_file_detected  
    
###################################

if __name__ == '__main__':
    import sys
    #sys.path.append('..')
    import InitLogging as IL
    loggername = 'build_emission'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    
    buildemissionpath = 'Emissions-Test'
    Basebinpath = '/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran'
    #templatepath = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatepath = '/home/monkk/repositories/ccam-ctm-automation/Python-Scripts/Templates'
    bininitline = "source /home/monkk/modulefiles/init.bash | ulimit -s 300000 |"

    templatefile_dieExh = 'Build_emission_dieExh.template'
    testfile_dieExh = '../../TestFiles_13/dieExh.run'
    binpath_dieExh = os.path.join(Basebinpath, 'build_motorVehicle_emissions_glo')
    bin_dieExh = 'build_motorvehicle_emissions_glo'
    buildemissionsubdir_dieExh = 'vdx'

    
    startdate = dt.datetime(2017,4,4)
    enddate = startdate + dt.timedelta(days=3, hours=00)
    emissiondate = dt.datetime(2013,1,1)
    emissiondate_veh = dt.datetime(2013,2,1)
    emissiondate_whe = dt.datetime(2013,7,1)
    #emissions_edms_DATA = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/emission2008NH3'
    emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2013NH3_PM10'

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
    offset = -10
    offset_veh = -10
    offset_whe = -10
    
    import Inventory_Ratios_PM10_2013 as IR2013PM10
    Inventory_ratios =  IR2013PM10.Inventory_Ratios_PM10_2013(logger, justif)
    PMspeciation = 'PM10'

    logger.debug('Building emission tests')

    CurrentDir = os.getcwd()
#--------------------------------------------------------------------------------------------------------------------------------------------------------
    EmissionDieExhConfig = EmissionDieExhConfigClass(logger, justif, 
                                                     templatepath, templatefile_dieExh, 
                                                     buildemissionpath, buildemissionsubdir_dieExh,
                                                     binpath_dieExh, bin_dieExh, bininitline,
                                                     startdate, enddate, emissiondate_veh, 
                                                     emissions_edms_DATA, offset_veh, 
                                                     Inventory_ratios, PMspeciation)
    logger.info('EmissionDieExhConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionDieExhConfig.Build_template_dieExh()
    logger.debug('-------------------------------')
    logger.debug('Diff dieExh')
    IL.PrintDiff(os.path.join(buildemissionpath, buildemissionsubdir_dieExh, 'dieExh.run'), testfile_dieExh, logger)
    logger.info('EmissionDieExhConfig template'.ljust(justif-2,'.')+"OK")

    EmissionDieExhConfig.Build_dieExh()
    logger.debug('dieExh full output name= %s', EmissionDieExhConfig.fullnameoutputfile())
    logger.info('EmissionDieExhConfig emission file build'.ljust(justif-2,'.')+"OK")




