"""
.. module:: generic_inventory_ratios
   :platform: Unix
   :synopsis: To process the met extract files

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import numpy as np
import datetime as dt

################################################################################        
class GenericEmissionRatios(object):
    def __init__(self, logger, justif, ):
        '''
        Class to read  and process Met data from 4 sources: BOM, CCAM, CTM, and OBS from the extracted data files
        
        Attributes
        -----------
        logger : logging.logger
            instance of a logger to output messages.
        justif : int
            max message width to justify logger output.   
         WSpath : str
             path to the data
         WSfile : str
             input filename containing the raw data extracted with the ForecastDataExtraction.py script
         WSOutputMean24h : str
             output filename conatining the 24h means of the data
         WSOutputStats24h : str
             output filename of the stats of the 24h means of the data
         WSOutputStatshourly : str
             output filename of the stats generated per forecast hours
        '''
        self.logger = logger
        self.justif = justif
        
        # MVEMS 
        self.pm25pm10ratio_PetExh       = None
        self.pm25pm10ratio_DieExh       = None  
        self.pm25pm10ratio_PetEvp       = None  
        self.pm25pm10ratio_LpgExh       = None  
                
        # Commercial domestic
        self.pm25pm10ratio_aircraft     = None
        self.pm25pm10ratio_comveh       = None
        self.pm25pm10ratio_indveh       = None
        self.pm25pm10ratio_loco         = None        
        self.pm25pm10ratio_nonexhaustPM = None        
        self.pm25pm10ratio_shipping     = None        
        self.pm25pm10ratio_other        = None 
        self.pm25pm10ratio_fugitives    = None  
        
        # WHE
        self.pm25pm10ratio_WHE          = None 
        
        # PSE emissions
        self.pm25pm10ratio_GasGen       = None  
        self.pm25pm10ratio_CoalGen      = None   
        self.pm25pm10ratio_PSEother     = None 
        return  
        
###########################################################################################
if __name__ == '__main__':
    try:
        from .. import InitLogging as IL
    except:
        import sys
        sys.path.append('/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Sources')
        import InitLogging as IL
    
    loggername = 'Emission_scaling'
    logger = IL.Initialise_logging(loggername)
 
