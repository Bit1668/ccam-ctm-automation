"""
.. module:: PSEmergeMulti
   :platform: Unix
   :synopsis: Everything needed to merge N PSE file ionto one.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import os
import sys
import fnmatch
import subprocess
#sys.path.append('..')
try:
    from .. import JinjaUtils as JJU
except:
    import JinjaUtils as JJU

###########################################################################################
class PSEmergeMultiConfigClass(object):
    """ 
    This class defines and configures a multiple PSE file merger into one.
    It contains all the parameters and the methods to build and run the merge.
    
    The class is self contained and can be initialised from original data, ti doesn't depend on higher level classes.
    it contains all the methods to configure and generate emissions from the bushfires knowing the shape.
    
    Attributes
    -----------
    """
    def __init__(self, logger, justif, templatepath, templatefile, buildemissionpath, listfilename,
                 fileoutput, numberofdays, binpath, bin, bininitline, FileMatchingPattern,
                 **kwargs):
        """
    This class defines and configures a multiple PSE file merger into one.
    It contains all the parameters and the methods to build and run the merge.
        
        Parameters
        ---------- 
            logger : logging.logger
                instance of a logger to output message.
            justif : int
                max message width to justify logger output.    
            templatepath : str
                path to the template directory.
            templatefile : str
                name of the templatefile to merge N PSE files.
            buildemissionpath : str
                working dir where the program is executed. 
            listfilename : list of str
                list of the PSE files to merge.
            fileoutput : str
                filename of the output merged file.
            numberofdays : int
                number of days of the output file.
            binpath : str
                path to find the binary to merge.
            bin : str
                binary filename to merge.
            bininitline : str
                init line to setup env when running a binary
            FileMatchingPattern : str
                pattern to check if the files to be merged exist.
                
        """
        self.logger = logger
        self.justif = justif
        self.templatepath = templatepath
        self.templatefile = templatefile
        self.buildemissionpath = buildemissionpath
        self.listfilename = listfilename
        self.fileoutput = fileoutput
        self.numberofdays = numberofdays
        self.binpath = binpath
        self.bin = bin
        self.bininitline = bininitline
        self.FileMatchingPattern = FileMatchingPattern
        return
############################################################################################################################
    def Build_dirs(self):
        try :
            os.makedirs(self.buildemissionpath)
            self.logger.info('Creation Working Dir= {msg}'.format(msg=self.buildemissionpath).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.debug('Working Dir = {msg}'.format(msg=self.buildemissionpath).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

############################################################################################################################
    def Build_template(self):
        filename=os.path.join(self.buildemissionpath,'psemergem.run')
        File= open(filename,'w')

        template=JJU.LoadTemplate(self.templatepath,self.templatefile)

        emission=template.render({'PSEmergeMultiConfig':self})
        File.write(emission)
        File.close()
        return

############################################################################################################################
    def Run_Merge(self):
        my_env = os.environ.copy()
        self.logger.info("current pwd = %s",os.getcwd())
        self.logger.info("psemerge working dir= %s",self.buildemissionpath)
        self.logger.info("psemerge bin dir= %s  bin = %s",self.binpath, self.bin)
        self.logger.info("merging list %s -> %s",self.listfilename,self.fileoutput)
        
        #commandline=os.path.join(self.binpath, self.bin)+ " psemergem.run"
        run = "{bin} psemergem.run".format(bin=os.path.join(self.binpath, self.bin))
        
        Runline = '{init} {run}'.format(init = self.bininitline, 
                                        run = run)

        
        PIPE = subprocess.PIPE
        ##print subprocess.Popen([Runline],shell=True, env=my_env, cwd=self.buildemissionpath, bufsize=300000).wait()
            
        p1 = subprocess.Popen([Runline],shell=True, env=my_env, cwd=self.buildemissionpath, bufsize=350000, stdout = PIPE, stdin = PIPE, stderr = PIPE)
    
        out, err = p1.communicate()
        output = out
        error = err
    
        self.logger.info(output)
        self.logger.error('Errors:')
        self.logger.error(error)
        
        return
############################################################################################################################
    def FileInListExist(self):
        listdir=os.listdir(self.RunDir)
        filteredlist=fnmatch.filter(listdir,self.FileMatchingPattern)
        if not filteredlist:
            self.logger.error("ERROR")
            self.logger.error("NO FILE DATA FOUND")
            self.logger.error("You need to find filename matching pattern %s",self.FileMatchingPattern)
            self.logger.error("In the following dir %s", self.RunDir )
            sys.exit("Sorry :(")
        return          
############################################################################################################################
if __name__ == '__main__':
    
    print 'toto'
