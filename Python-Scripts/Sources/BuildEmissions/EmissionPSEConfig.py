"""
.. module:: EmissionPSEConfig
   :platform: Unix
   :synopsis: Everything needed to build the PSE Emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import string
import datetime as dt
import os
import sys
import subprocess
#sys.path.append('..')
try :
    import Sources.JinjaUtils as JJU
except :
    sys.path.append('/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts')
    import Sources.JinjaUtils as JJU
import Sources.BuildEmissions.Tools
###########################################################################################
class EmissionPSEConfigClass(object):
    """ 
    This meta class defines a PSE (point source emission) class object, that contains all the parameters to configure each individual contribution 
     in the pse emissions 
    
    The class is self contained and can be initialised from original data, it doesn't depend on higher level classes.
    it contains all the methods to configure and generate PSE emissions.
    
    Attributes
    -----------
    logger : logging.logger
        instance of a logger to output messages.
    justif : int
        max message width to justify logger output  .   
    templatepath : str
        path to the template directory.
    Basebinpath : str
        path to find the binary to build individual emissions.
    bininitline : str
        init line to setup env when running a binary
    templatefile_coalonlywkdays : str
        name of the templatefile to build an coalonlywkdays emission file.
    templatefile_gasonlywkdays : str
        name of the templatefile to build an gasonlywkdays emission file.
    templatefile_restwkdays : str
        name of the templatefile to build an restwkdays emission file.
    templatefile_power : str
        name of the templatefile to build an coalonlywkday semission file.
    buildemissionpath_static : str
        basepath where to build the PSE emissions based on the EDMS database.
    buildemissionpath_dynamic : str
        basepath where to build the PSE emissions based of dynamic and/or modeled emissions.
    buildemissionsubdir_pse : str
        subdirectory where to build the PSE emissions. It will be build as a a subdir of buildemissionpath_dynamic and buildemissionpath_static.
    templatefile_PSEmerge : str
        name of the templatefile to use PSEmerge (deprecated because of bug in psem).
    binpath_PSEmerge : str
        path where to find binary file to exec PSEmerge (deprecated because of bug in psem).
    bin_PSEmerge : str
        binary filename to exec PSEmerge (deprecated because of bug in psem).
    buildemissionsubdir_coalonlywkdays : str
        subdir of buildemissionpath_static where to build the EDMS coalonlywkdays emissions.
    buildemissionsubdir_gasonlywkdays : str
        subdir of buildemissionpath_static where to build the EDMS gasonlywkdays emissions.
    buildemissionsubdir_restwkdays : str
        subdir of buildemissionpath_static where to build the EDMS restwkdays emissions.
    buildemissionsubdir_power : str
        subdir of buildemissionpath_dynamic where to build the coal powerstations emissions.
    binpath_coalonlywkdays : str
        subdir of Basebinpath where lives the binary to build to build EDMS coalonlywkdays emissions.
    binpath_gasonlywkdays : str
        subdir of Basebinpath where lives the binary to build to build EDMS gasonlywkdays emissions.
    binpath_restwkdays : str
        subdir of Basebinpath where lives the binary to build to build EDMS restwkdays emissions.
    binpath_power : str
        subdir of Basebinpath where lives the binary to build to build coal dynamic powerstation emissions.
    bin_coalonlywkdays : str
        binary name use to build to build coalonlywkdays emissions.
    bin_gasonlywkdays : str
        binary name use to build to build gasonlywkdays emissions.
    bin_restwkdays : str
        binary name use to build to build restonlywkdays emissions.
    bin_power : str
        binary name use to build to build coal dynamic powerstation emissions.
    startdate : datetime.datetime
        start date of the emissions to build as a UTC datetime.
    enddate : datetime.datetime
        end date of the emissions to build as a UTC datetime.
    emissiondate : datetime.datetime
        date of the emission 
    TimestampUTC : datetime.datetime
        timestamp ID of the run
    emissions_edms_DATA : str
        path to the EDMS source directory.
    emissions_power_DATA : str
        path to the dynamic coal powerstation data directory.
    offset: int
        hour offset of the emission to take in account timezones 
    emissions_rfs_DATA_OEH : str
        path to the dynamic OEH fire data directory.
    templatefile_rfs_OEH : str
        name of the templatefile to build OEH fire emission config file.
    binpath_rfs_OEH : str
        subdir of Basebinpath where lives the binary to build to build OEH fire emissions.
    bin_rfs_OEH : str
        binary name use to build to build OEH fire emissions.
    buildemissionsubdir_rfs_OEH : str
        subdir of buildemissionpath_dynamic where to build OEH fire emissions.
    bushfireOEH : boolean
        trigger to activate OEH busfire flavour.
    emissions_rfs_DATA_CSIRO : str
        path to the dynamic CSIRO fire data directory.
    templatefile_rfs_CSIRO : str
        name of the templatefile to build CSIRO fire emission config file.
    binpath_rfs_CSIRO : str
        subdir of Basebinpath where lives the binary to build to build CSIRO fire emissions.
    bin_rfs_CSIRO : str
        binary name use to build to build CSIRO fire emissions.
    buildemissionsubdir_rfs_CSIRO : str
        subdir of buildemissionpath_dynamic where to build CSIRO fire emissions.
    bushfireCSIRO : boolean
        trigger to activate CSIRO busfire flavour.
    offset_bushfire_csiro : str
        hour offset of the emission to take in account timezones.
    burn_start_hour_csiro : str
        Starting hour of the emission profile for the CSIRO bushfire module. 
    templatefile_PSEMM : str
        name of the templatefile to use the PSE multiple files merge.
    binpath_PSEMM : str
        subdir of Basebinpath where lives the binary to Multi merge pse files.
    bin_PSEMM : str
        binary name to Multi merge pse files.
    """
    def __init__(self, logger, justif, 
                 templatepath, Basebinpath, bininitline,
                 templatefile_coalonlywkdays, templatefile_gasonlywkdays, templatefile_restwkdays, templatefile_power, 
                 buildemissionpath_static, buildemissionpath_dynamic, buildemissionsubdir_pse, templatefile_PSEmerge, binpath_PSEmerge, bin_PSEmerge,
                 buildemissionsubdir_coalonlywkdays, buildemissionsubdir_gasonlywkdays, buildemissionsubdir_restwkdays, buildemissionsubdir_power, 
                 Emissions_ratios, PMspeciation,
                 binpath_coalonlywkdays, binpath_gasonlywkdays, binpath_restwkdays, binpath_power,
                 bin_coalonlywkdays, bin_gasonlywkdays, bin_restwkdays, bin_power,
                 startdate, enddate, emissiondate, TimestampUTC,
                 emissions_edms_DATA, emissions_power_DATA, offset, timeresolvedcoalpower,
                 Run_HRB, Run_Bushfire,
                 emissions_rfs_DATA_OEH, templatefile_rfs_OEH, binpath_rfs_OEH, bin_rfs_OEH, buildemissionsubdir_rfs_OEH, bushfireOEH,
                 emissions_rfs_DATA_CSIRO, templatefile_rfs_CSIRO, binpath_rfs_CSIRO, bin_rfs_CSIRO, buildemissionsubdir_rfs_CSIRO, bushfireCSIRO, offset_bushfire_csiro, burn_start_hour_csiro,
                 templatefile_PSEMM, binpath_PSEMM, bin_PSEMM,
                 **kwargs):
        
        self.kwargs = kwargs
        self.binary = kwargs.get('binary', True)

        self.logger = logger
        self.justif = justif
        self.startdate = startdate
        self.enddate = enddate
        self.emissiondate = emissiondate
        self.TimestampUTC = TimestampUTC
        self.ndays = (self.enddate - self.startdate).days
        
        self.offset = str(offset)
        self.Basebinpath = Basebinpath
        self.bininitline = bininitline
        
        self.emissionmonth = (startdate.strftime('%b')).lower()
        self.emissionyear = emissiondate.strftime('%y')
        self.emission_name_year = self.emissionmonth + self.emissionyear

        self.templatepath = templatepath
        self.emissions_edms_DATA = emissions_edms_DATA
        
        self.buildemissionpath_static = buildemissionpath_static
        self.buildemissionpath_dynamic = buildemissionpath_dynamic
        
        self.templatefile_PSEmerge = templatefile_PSEmerge
        self.binpath_PSEmerge = binpath_PSEmerge
        self.bin_PSEmerge = bin_PSEmerge
        self.buildemissionsubdir_pse = buildemissionsubdir_pse

        self.templatefile_coalonlywkdays = templatefile_coalonlywkdays
        self.binpath_coalonlywkdays = binpath_coalonlywkdays
        self.bin_coalonlywkdays = bin_coalonlywkdays
        self.buildemissionsubdir_coalonlywkdays = buildemissionsubdir_coalonlywkdays

        self.templatefile_gasonlywkdays = templatefile_gasonlywkdays
        self.binpath_gasonlywkdays = binpath_gasonlywkdays
        self.bin_gasonlywkdays = bin_gasonlywkdays
        self.buildemissionsubdir_gasonlywkdays = buildemissionsubdir_gasonlywkdays


        self.templatefile_restwkdays = templatefile_restwkdays
        self.binpath_restwkdays = binpath_restwkdays
        self.bin_restwkdays = bin_restwkdays
        self.buildemissionsubdir_restwkdays = buildemissionsubdir_restwkdays
        
        self.Emissions_ratios = Emissions_ratios
        self.PMspeciation = PMspeciation
    
#########
#    Coal powerstation
        self.templatefile_power = templatefile_power
        self.binpath_power = binpath_power
        self.bin_power = bin_power
        self.buildemissionsubdir_power = buildemissionsubdir_power
        self.emissions_power_DATA = emissions_power_DATA
        self.timeresolvedcoalpower = timeresolvedcoalpower
#######
#    Fires
        self.Run_HRB = Run_HRB 
        self.Run_Bushfire = Run_Bushfire
#######        
#    Bushfire OEH
        self.emissions_rfs_DATA_OEH = emissions_rfs_DATA_OEH
        self.templatefile_rfs_OEH = templatefile_rfs_OEH
        self.binpath_rfs_OEH = binpath_rfs_OEH
        self.bin_rfs_OEH = bin_rfs_OEH
        self.buildemissionsubdir_rfs_OEH = buildemissionsubdir_rfs_OEH
        #logic flag include or not
        self.bushfireOEH = bushfireOEH
        
#######        
#    Bushfire CSIRO
        self.emissions_rfs_DATA_CSIRO = emissions_rfs_DATA_CSIRO
        self.templatefile_rfs_CSIRO = templatefile_rfs_CSIRO
        self.binpath_rfs_CSIRO = binpath_rfs_CSIRO
        self.bin_rfs_CSIRO = bin_rfs_CSIRO
        self.buildemissionsubdir_rfs_CSIRO = buildemissionsubdir_rfs_CSIRO
#       include or not Bushfire CSIRO flavour.
        self.bushfireCSIRO = bushfireCSIRO 
        self.offset_bushfire_csiro = offset_bushfire_csiro
        self.burn_start_hour_csiro = burn_start_hour_csiro

#######        
#    PSEMM
        self.templatepath_PSEMM = templatepath
        self.templatefile_PSEMM = templatefile_PSEMM
        self.binpath_PSEMM = binpath_PSEMM
        self.bin_PSEMM =  bin_PSEMM      


        self.fileoutput_suffix ='wkday_anthropogenic.pse'
        if self.binary:
            self.fileoutput_suffix = '{prefix}.bin'.format(prefix = self.fileoutput_suffix)
        
        #self.fileoutput = 'emsn_gmr_'+self.emission_name_year+'wkday_anthropogenic.pse.bin'
        #self.fileoutput='emsn_gmr_'+self.emission_name_year+'wkday_anthropogenic.pse'
        #self.fileoutput = 'emsn_gmr_{date}_{suffix}'.format(date = self.emission_name_year, 
        #                                                   suffix = self.fileoutput_suffix)
        self.fileoutput = 'emsn_gmr_{PMspec}_{date}_{ndays}days_{suffix}'.format(PMspec = self. PMspeciation, date = self.startdate.strftime('%Y%m%d'), 
                                                           ndays = self.ndays,
                                                           suffix = self.fileoutput_suffix)
        self.fullpathfileoutput = os.path.join(self.buildemissionpath_dynamic,self.buildemissionsubdir_pse, self.fileoutput)
        return
###################################
    def fullnameoutputfile(self):
        name=os.path.join(self.buildemissionpath_dynamic, self.buildemissionsubdir_pse, self.fileoutput)
        return name

###################################
    def relativenameoutputfile(self):
        name=os.path.join(self.buildemissionsubdir_pse, self.fileoutput)
        return name


###################################
    def init_EmissionCoalonlywkdaysConfigClass(self):
        ''' This method initialise the EmissionCoalonlywkdaysConfigClass
        '''
        import Sources.BuildEmissions.EmissionCoalonlywkdaysConfig as COW
                
        buildemissionpath  = os.path.join(self.buildemissionpath_static, self.buildemissionsubdir_pse)
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_coalonlywkdays = self.templatefile_coalonlywkdays
        binpath_coalonlywkdays = self.binpath_coalonlywkdays
        bin_coalonlywkdays = self.bin_coalonlywkdays
        buildemissionsubdir_coalonlywkdays = self.buildemissionsubdir_coalonlywkdays

        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset = self.offset
        
        Emissions_ratios = self.Emissions_ratios 
        PMspeciation = self.PMspeciation       
        
        EmissionCoalonlywkdaysConfig = COW.EmissionCoalonlywkdaysConfigClass(self.logger, self.justif, 
                                                                             templatepath, templatefile_coalonlywkdays, 
                                                                             buildemissionpath, buildemissionsubdir_coalonlywkdays, 
                                                                             binpath_coalonlywkdays, bin_coalonlywkdays, self.bininitline,
                                                                             startdate, enddate, emissiondate, 
                                                                             emissions_edms_DATA, offset,
                                                                             Emissions_ratios, PMspeciation,
                                                                             **self.kwargs)
        return   EmissionCoalonlywkdaysConfig 
                            
###################################
    def init_EmissionGasonlywkdaysConfigClass(self):
        ''' This method initialise the EmissionGasonlywkdaysConfigClass
        '''
        import Sources.BuildEmissions.EmissionGasonlywkdaysConfig as GOW

        buildemissionpath = os.path.join(self.buildemissionpath_static, self.buildemissionsubdir_pse)
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_gasonlywkdays = self.templatefile_gasonlywkdays
        binpath_gasonlywkdays = self.binpath_gasonlywkdays
        bin_gasonlywkdays = self.bin_gasonlywkdays
        buildemissionsubdir_gasonlywkdays = self.buildemissionsubdir_gasonlywkdays

        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset = self.offset

        Emissions_ratios = self.Emissions_ratios  
        PMspeciation = self.PMspeciation      
        
        EmissionGasonlywkdaysConfig = GOW.EmissionGasonlywkdaysConfigClass(self.logger, self.justif, 
                                                                           templatepath, templatefile_gasonlywkdays, 
                                                                           buildemissionpath, buildemissionsubdir_gasonlywkdays,
                                                                           binpath_gasonlywkdays, bin_gasonlywkdays, self.bininitline,
                                                                           startdate, enddate, emissiondate, emissions_edms_DATA, offset,
                                                                           Emissions_ratios, PMspeciation,
                                                                             **self.kwargs)
        return EmissionGasonlywkdaysConfig 
                           
###################################
    def init_EmissionRestwkdaysConfigClass(self):
        ''' This method initialise the EmissionRestwkdaysConfigClass
        '''
        import Sources.BuildEmissions.EmissionRestwkdaysConfig as ROW
        buildemissionpath = os.path.join(self.buildemissionpath_static, self.buildemissionsubdir_pse)
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_restwkdays = self.templatefile_restwkdays
        binpath_restwkdays = self.binpath_restwkdays
        bin_restwkdays = self.bin_restwkdays
        buildemissionsubdir_restwkdays = self.buildemissionsubdir_restwkdays

        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        emissions_edms_DATA = self.emissions_edms_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset = self.offset
        
        Emissions_ratios = self.Emissions_ratios 
        PMspeciation = self.PMspeciation               
        
        EmissionRestwkdaysConfig = ROW.EmissionRestwkdaysConfigClass(self.logger, self.justif, 
                                                                      templatepath, templatefile_restwkdays, 
                                                                      buildemissionpath, buildemissionsubdir_restwkdays,
                                                                      binpath_restwkdays, bin_restwkdays, self.bininitline,  
                                                                      startdate, enddate, emissiondate, 
                                                                      emissions_edms_DATA, offset,
                                                                      Emissions_ratios, PMspeciation,        
                                                                      **self.kwargs)
        return EmissionRestwkdaysConfig                       


############33
# Coal Powerstation
    def init_EmissionPowerConfigClass(self):
        ''' This method initialise the EmissionPowerConfigClass
        '''
        import Sources.BuildEmissions.EmissionPowerConfig as EPC
        self.logger.error("ERROR in dynamic Coal powerstation emissions")
        self.logger.error("NO PROPER TESTING UNDERTAKEN")
        self.logger.error("IT CAN'T BE USED YET" )
        sys.exit("Sorry :(")
        
        buildemissionpath = os.path.join(self.buildemissionpath_dynamic, self.buildemissionsubdir_pse)
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_power = self.templatefile_power
        binpath_power = self.binpath_power
        bin_power = self.bin_power
        buildemissionsubdir_power = self.buildemissionsubdir_power

        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        emissions_power_DATA = self.emissions_power_DATA

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset = self.offset
        
        EmissionPowerConfig = EPC.EmissionPowerConfigClass(self.logger, self.justif, 
                                                           templatepath, templatefile_power, 
                                                           buildemissionpath, buildemissionsubdir_power, 
                                                           binpath_power, bin_power, self.bininitline,
                                                           startdate, enddate, emissiondate, 
                                                           emissions_power_DATA, offset,
                                                           **self.kwargs)
        return  EmissionPowerConfig                      

############33
#    Bushfire
    def init_BushfireOEH(self):
        ''' This method initialise the EmissionPowerConfigClass
        '''
        import Sources.BuildEmissions.BushfireOEH as BFOEH

        self.logger.error("ERROR in BushfireOEH")
        self.logger.error("NO PROPER TESTING UNDERTAKEN")
        self.logger.error("IT CAN'T BE USED YET" )
        sys.exit("Sorry :(")

        buildemissionpath=os.path.join(self.buildemissionpath_dynamic, self.buildemissionsubdir_pse)
        Basebinpath=self.Basebinpath
        templatepath=self.templatepath

        templatefile_rfs_OEH=self.templatefile_rfs_OEH
        binpath_rfs_OEH=self.binpath_rfs_OEH
        bin_rfs_OEH=self.bin_rfs_OEH
        buildemissionsubdir_rfs_OEH=self.buildemissionsubdir_rfs_OEH
    
        startdate=self.startdate
        enddate=self.enddate
        emissiondate=self.emissiondate
        emissions_rfs_DATA_OEH=self.emissions_rfs_DATA_OEH

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset=self.offset

        BushfireOEHConfig=BFOEH.BushfireOEHConfigClass(self.logger, self.justif,
                                                      templatepath, templatefile_rfs_OEH,
                                                      buildemissionpath, buildemissionsubdir_rfs_OEH,
                                                      binpath_rfs_OEH, bin_rfs_OEH, self.bininitline,
                                                      startdate, enddate, emissiondate,
                                                      emissions_rfs_DATA_OEH, offset,
                                                      **self.kwargs)

        return BushfireOEHConfig

############33
    def init_BushfireCSIRO(self):
        import Sources.BuildEmissions.BushfireCSIRO as BFCSIRO
        logger = self.logger
        justif = self.justif
        buildemissionpath = os.path.join(self.buildemissionpath_dynamic, self.buildemissionsubdir_pse)
        Basebinpath = self.Basebinpath
        templatepath = self.templatepath

        templatefile_rfs_CSIRO = self.templatefile_rfs_CSIRO
        binpath_rfs_CSIRO = self.binpath_rfs_CSIRO
        bin_rfs_CSIRO = self.bin_rfs_CSIRO
        buildemissionsubdir_rfs_CSIRO = self.buildemissionsubdir_rfs_CSIRO
    
        startdate = self.startdate
        enddate = self.enddate
        emissiondate = self.emissiondate
        TimestampUTC = self.TimestampUTC
        
        emissions_rfs_DATA_CSIRO=self.emissions_rfs_DATA_CSIRO
        
        binpath_PSEMM = self.binpath_PSEMM
        bin_PSEMM = self.bin_PSEMM
        templatefile_PSEMM = self.templatefile_PSEMM
        
        burn_start_hour_csiro = self.burn_start_hour_csiro
#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
        offset = self.offset_bushfire_csiro

        # kind
        Run_HRB = self.Run_HRB 
        Run_Bushfire = self.Run_Bushfire
        
        BushfireCSIROConfig = BFCSIRO.BushfireCSIROConfigClass(logger, justif, 
                                                               templatepath, templatefile_rfs_CSIRO, 
                                                               buildemissionpath, buildemissionsubdir_rfs_CSIRO, 
                                                               binpath_rfs_CSIRO, bin_rfs_CSIRO, self.bininitline,
                                                               startdate, enddate, emissiondate, TimestampUTC, burn_start_hour_csiro,
                                                               emissions_rfs_DATA_CSIRO, offset, 
                                                               Run_HRB, Run_Bushfire,
                                                               binpath_PSEMM, bin_PSEMM, templatefile_PSEMM,
                                                               **self.kwargs)
        return BushfireCSIROConfig

############33
    def EmissionConfig_update(self,EmissionConfig):
        EmissionConfig.EmissionPSEConfig_fullnameoutputfile = self.fullnameoutputfile()
        EmissionConfig.buildemission_pse_updated=True        

##################
    def Create_Emission_dirs(self):
        """ This method build the PSE emission dirs.
        """
        dirdir = os.path.join(self.buildemissionpath_dynamic,self.buildemissionsubdir_pse)
        try :
            os.makedirs(dirdir)
            self.logger.info('Creation PSE Dir= {msg}'.format(msg=dirdir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Working PSE Dir = {msg}'.format(msg=dirdir).ljust(self.justif-13,'.') + 'Already exist')
            pass
      
        dirdir = os.path.join(self.buildemissionpath_static,self.buildemissionsubdir_pse)
        try :
            os.makedirs(dirdir)
            self.logger.info('Creation PSE Dir= {msg}'.format(msg=dirdir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Working PSE Dir = {msg}'.format(msg=dirdir).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return
##################
    def init_PSEMM(self, PSEfileToMerge):
        """ 
        This method initialise the multi merge PSEMM.
        Attributes
        -----------
            PSEfileToMerge : list of str
                list of filename to merge using pse multi merge.
       """
        import Sources.BuildEmissions.PSEmergeMulti as PSEMM
 
        templatefile_PSEM = self.templatefile_PSEMM
        templatepath_PSEM = self.templatepath_PSEMM
        buildemissionpath_PSEM = os.path.join(self.buildemissionpath_dynamic, self.buildemissionsubdir_pse)
        binpath_PSEM = self.binpath_PSEMM
        bin_PSEM = self.bin_PSEMM
        fileoutput_PSEM = self.fileoutput
        numberofdays_PSEM = (self.enddate - self.startdate).days
        FileMatchingPattern = '*'
        
        PSEMergeMultiConfig = PSEMM.PSEmergeMultiConfigClass(self.logger, self.justif, templatepath_PSEM, templatefile_PSEM,
                                                               buildemissionpath_PSEM, PSEfileToMerge, fileoutput_PSEM, numberofdays_PSEM,
                                                               binpath_PSEM, bin_PSEM, self.bininitline, FileMatchingPattern,
                                                                             **self.kwargs)
       
        self.logger.info('PSEMergeMultiConfig object init'.ljust(self.justif-2,'.')+"OK")
        return PSEMergeMultiConfig
        
##################
    def Build_Emissions(self):
        """ This method build the PSE emissions.
        """
        self.logger.info(''.ljust(self.justif,'-'))
        self.logger.debug('Building PSE emissions'.center(self.justif,'|'))
        self.logger.info(''.ljust(self.justif,'-'))

        self.Create_Emission_dirs()
        self.logger.info('EmissionPSEConfig dir creation'.ljust(self.justif-2,'.')+"OK")
    
    
        #######
        # Building individual components
        ######    
        # coal wkdays or dynamic coal powerstations
        #self.logger.warning('tiermresolved = {tn}'.format(tn=self.timeresolvedcoalpower))
        if self.timeresolvedcoalpower:
            self.logger.warning('NOT Building EDMS coal powerplant only wkdaystime'.ljust(self.justif,'.'))
            self.logger.info('Building  PSE -- Building time resolved coal power'.ljust(self.justif,'.'))
            #Coal powerstations
            EmissionPowerConfig = self.init_EmissionPowerConfigClass()
            logger.info('EmissionPowerConfig object init'.ljust(self.justif-2,'.')+"OK")
            EmissionPowerConfig.Build_template_power()
            self.logger.info('EmissionPowerConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionPowerConfig.Build_power()
            self.logger.info('EmissionPowerConfig emission file build'.ljust(self.justif-2,'.')+"OK")

        else:
            self.logger.warning('NOT Building time resolved coal power'.ljust(self.justif,'.'))
            self.logger.info('Building  PSE -- Building EDMS coal powerplant only wkdays'.ljust(self.justif,'.'))
            #Coal EDMS
            EmissionCoalonlywkdaysConfig = self.init_EmissionCoalonlywkdaysConfigClass()
            self.logger.info('EmissionCoalonlywkdaysConfig object init'.ljust(self.justif-2,'.')+"OK")
            EmissionCoalonlywkdaysConfig.Build_template_coalonlywkdays()
            self.logger.info('EmissionCoalonlywkdaysConfig template'.ljust(self.justif-2,'.')+"OK")
            EmissionCoalonlywkdaysConfig.Build_coalonlywkdays()
            self.logger.info('EmissionCoalonlywkdaysConfig emission file build'.ljust(self.justif-2,'.')+"OK")


        #gas wkdays
        self.logger.info('Building  PSE -- Building EDMS gas only wkdays'.ljust(self.justif,'.'))
        EmissionGasonlywkdaysConfig = self.init_EmissionGasonlywkdaysConfigClass()
        self.logger.info('EmissionGasonlywkdaysConfig object init'.ljust(self.justif-2,'.')+"OK")
        EmissionGasonlywkdaysConfig.Build_template_gasonlywkdays()
        self.logger.info('EmissionGasonlywkdaysConfig template'.ljust(self.justif-2,'.')+"OK")
        EmissionGasonlywkdaysConfig.Build_gasonlywkdays()
        self.logger.info('EmissionGasonlywkdaysConfig emission file build'.ljust(self.justif-2,'.')+"OK")
    
    
        #others
        self.logger.info('Building  PSE -- Building  rest wkdays'.ljust(self.justif,'.'))
        EmissionRestwkdaysConfig = self.init_EmissionRestwkdaysConfigClass()
        self.logger.info('EmissionRestwkdaysConfig object init'.ljust(self.justif-2,'.')+"OK")
        EmissionRestwkdaysConfig.Build_template_restwkdays()
        self.logger.info('EmissionRestwkdaysConfig template'.ljust(self.justif-2,'.')+"OK")
        EmissionRestwkdaysConfig.Build_restwkdays()
        self.logger.info('EmissionRestwkdaysConfig emission file build'.ljust(self.justif-2,'.')+"OK")

        #bushfire CSIRO
        # if bushfireCSIRO build hourly data for bushfire, CSIRO flavour.
        if self.bushfireCSIRO:
            self.logger.info('Building  PSE -- Building time resolved Bushfire CSIRO flavour'.ljust(self.justif,'.'))
            BushfireCSIROConfig = self.init_BushfireCSIRO()
            self.logger.info('BushfireCSIROConfig object init'.ljust(self.justif-2,'.')+"OK")
            BushfireCSIROConfig.Build_bushfire()
            self.logger.info('Bushfire CSIRO emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.warning('NOT Building time resolved Bushfire CSIRO flavour'.ljust(self.justif,'.'))

        #bushfire OEH
        # if bushfireOEH build hourly data for bushfire, OEH flavour.
        if self.bushfireOEH:
            self.logger.info('Building  PSE -- Building time resolved Bushfire OEH flavour'.ljust(self.justif,'.'))
            BushfireOEHConfig = self.init_BushfireOEH()
            self.logger.info('BushfireOEHConfig object init'.ljust(self.justif-2,'.')+"OK")
            BushfireOEHConfig.Build_template(self.logger)
            self.logger.info('BushfireOEHConfig template'.ljust(self.justif-2,'.')+"OK")
            BushfireOEHConfig.Build_bushfire(self.logger)
            self.logger.info('Bushfire OEH emission file build'.ljust(self.justif-2,'.')+"OK")
        else:
            self.logger.warning('NOT Building time resolved Bushfire OEH flavour'.ljust(self.justif,'.'))
         
        #######
        # Merging components
        ######    
        self.logger.info('Building  PSE -- Merging emission files'.ljust(self.justif,'.'))
        PSEfileToMerge = []
        if self.timeresolvedcoalpower:
            PSEfileToMerge.append(os.path.abspath(EmissionPowerConfig.fullnameoutputfile()))
            self.logger.info('Adding time-resolved Coal powerstation to merging queue'.ljust(self.justif,'.'))
        else:
            PSEfileToMerge.append(os.path.abspath(EmissionCoalonlywkdaysConfig.fullnameoutputfile()))
            self.logger.info('Adding EDMS weekdays coal powerplant to merging queue'.ljust(self.justif,'.'))

        PSEfileToMerge.append(os.path.abspath(EmissionGasonlywkdaysConfig.fullnameoutputfile()))
        self.logger.info('Adding EDMS weekdays gas powerplant to merging queue'.ljust(self.justif,'.'))
        
        # bushfire OEH?
        if self.bushfireOEH:
            PSEfileToMerge.append(os.path.abspath(BushfireOEHConfig.fullnameoutputfile()))
            self.logger.info('Adding Bushfire OEH emissions to merging queue'.ljust(self.justif,'.'))

        # bushfire CSIRO?
        if self.bushfireCSIRO:
            llist = [os.path.abspath(fullname) for fullname in BushfireCSIROConfig.fullnameoutputfile_list()]
            PSEfileToMerge.extend(llist)
            self.logger.info('Adding Bushfire CSIRO emissions to merging queue'.ljust(self.justif,'.'))
        
        PSEfileToMerge.append(os.path.abspath(EmissionRestwkdaysConfig.fullnameoutputfile()))
        self.logger.info('Adding EDMS weekdays others to merging queue'.ljust(self.justif,'.'))
        
        #print 'PSE=', PSEfileToMerge
        #sys.exit()
        
        PSEMergeMultiConfig = self.init_PSEMM( PSEfileToMerge)

        PSEMergeMultiConfig.Build_template()
        self.logger.info('PSEMergeMultiConfig template'.ljust(self.justif-2,'.')+"OK")

        PSEMergeMultiConfig.Run_Merge()
        self.logger.info('PSE Multi Merge'.ljust(self.justif-8,'.')+"COMPLETE")

#        if EmissionConfig.timeresolvedcoalpower:
#            filename1=EmissionPower.fullnameoutputfile()
#        else:
#            filename1=EmissionCoalonlywkdaysConfig.fullnameoutputfile()
#
#        logger.info('merging coal and gas'.ljust(justif,'.'))
#        filename2=EmissionGasonlywkdaysConfig.fullnameoutputfile()
#        fileoutput=os.path.join(EmissionConfig.buildemissionpath_dynamic, EmissionConfig.buildemissionsubdir_pse, 'coalgas.pse.bin')
#        fileoutput=os.path.join(EmissionConfig.buildemissionpath_dynamic, EmissionConfig.buildemissionsubdir_pse, 'coalgas.pse')
#        logger.info("merging %s with %s -> %s",filename1,filename2,fileoutput)
#        #numberofdays=1
#        numberofdays = (EmissionPSEConfig.enddate - EmissionPSEConfig.startdate).days
#        binpath=EmissionPSEConfig.binpath_PSEmerge
#        bin=EmissionPSEConfig.bin_PSEmerge
#        templatepath=EmissionPSEConfig.templatepath
#        templatefile_PSEmerge=EmissionPSEConfig.templatefile_PSEmerge
#        
#        PSEmergeConfig=CBE.PSEmergeConfigClass(templatepath,templatefile_PSEmerge,EmissionConfig.buildemissionpath_dynamic,filename1,filename2,fileoutput,numberofdays,binpath,bin)
#        PSEmerge(PSEmergeConfig,logger)
#
#        # bushfire OEH?
#        if EmissionPSEConfig.bushfireOEH:
#             filename1=fileoutput
#             filename2=BushfireOEHConfig.fullnameoutputfile()
#             fileoutput=os.path.join(EmissionConfig.buildemissionpath_dynamic,EmissionConfig.buildemissionsubdir_pse, 'coalgasbushfireOEH.pse.bin')
#             logger.info("merging %s with %s -> %s",filename1,filename2,fileoutput)
#             #numberofdays=1
#             numberofdays = (EmissionPSEConfig.enddate - EmissionPSEConfig.startdate).days
#             binpath=EmissionConfig.binpath_PSEmerge
#             bin=EmissionConfig.bin_PSEmerge
#             templatepath=EmissionPSEConfig.templatepath
#             templatefile_PSEmerge=EmissionPSEConfig.templatefile_PSEmerge
#             PSEmergeConfig=CBE.PSEmergeConfigClass(templatepath,templatefile_PSEmerge,EmissionConfig.buildemissionpath_dynamic,filename1,filename2,fileoutput,numberofdays,binpath,bin)
#             PSEmerge(PSEmergeConfig,logger)
#           
#        # bushfire CSIRO?
#        if EmissionPSEConfig.bushfireCSIRO:
#             filename1 = fileoutput
#             filename2 = BushfireCSIROConfig.fullnameoutputfile()
#             fileoutput = os.path.join(EmissionConfig.buildemissionpath_dynamic,EmissionConfig.buildemissionsubdir_pse, 'coalgasbushfireCSIRO.pse.bin')
#             fileoutput = os.path.join(EmissionConfig.buildemissionpath_dynamic,EmissionConfig.buildemissionsubdir_pse, 'coalgasbushfireCSIRO.pse')
#             logger.info("merging %s with %s -> %s",filename1,filename2,fileoutput)
#             #numberofdays=1
#             numberofdays = (EmissionPSEConfig.enddate - EmissionPSEConfig.startdate).days
#             binpath = EmissionConfig.binpath_PSEmerge
#             bin = EmissionConfig.bin_PSEmerge
#             templatepath = EmissionPSEConfig.templatepath
#             templatefile_PSEmerge = EmissionPSEConfig.templatefile_PSEmerge
#             PSEmergeConfig = CBE.PSEmergeConfigClass(templatepath,templatefile_PSEmerge,EmissionConfig.buildemissionpath_dynamic,filename1,filename2,fileoutput,numberofdays,binpath,bin)
#             PSEmerge(PSEmergeConfig,logger)
#        
#    
#        logger.info('merging coalgas and other to give anthropogenic'.ljust(justif,'.'))
#        filename1 = fileoutput
#        filename2 = EmissionRestwkdaysConfig.fullnameoutputfile()
#        fileoutput = EmissionPSEConfig.fullnameoutputfile()
#        logger.info("merging %s with %s -> %s",filename1,filename2,fileoutput)
#        #numberofdays=1
#        numberofdays = (EmissionPSEConfig.enddate - EmissionPSEConfig.startdate).days
#        binpath = EmissionConfig.binpath_PSEmerge
#        bin = EmissionConfig.bin_PSEmerge
#        templatepath = EmissionPSEConfig.templatepath
#        templatefile_PSEmerge = EmissionPSEConfig.templatefile_PSEmerge
#        PSEmergeConfig = CBE.PSEmergeConfigClass(templatepath,templatefile_PSEmerge,EmissionConfig.buildemissionpath_dynamic,filename1,filename2,fileoutput,numberofdays,binpath,bin)
#        PSEmerge(PSEmergeConfig,logger)

        #logger.info('updating the master emission object'.ljust(justif,'.'))
        #self.EmissionConfig_update(EmissionConfig)
        return
   

#################################################################################################################################################################

if __name__ == '__main__':
    import Sources.DateMagics as DM
    import Sources.InitLogging as IL
    
    loggername = 'build_emission_PSE'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    
    buildemissionpath = 'Emissions'
    #bininitline = "source /home/barthelemyx/modulefiles/init.bash | ulimit -s 300000 |"
    PartitionName = 'CAS'
    PartitionName = 'SLES15'
    bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | srun -p {partition} -N1 -x irscomp12 "
    #bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | valgrind --leak-check=yes "
    bininitline = bininitline_template.format(partition = PartitionName)
    bininitline = "source ~/modulefiles/init.bash | ulimit -s 350000 | "
    
    Basebinpath = '/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran'
    #templatepath = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatepath = '/home/monkk/repositories/ccam-ctm-automation/Python-Scripts/Templates'                
    buildemissionpath_static = os.path.join(buildemissionpath, 'static')
    buildemissionpath_dynamic = os.path.join(buildemissionpath,'dynamic')
    buildemissionsubdir_pse = 'pse'
    
    templatefile_coalonlywkdays = 'Build_emission_coalonly_wkdays.template'
    testfile_coalonlywkdays = '../../TestFiles_13/run_coalonly_wkday.run'
    binpath_coalonlywkdays = os.path.join(Basebinpath, 'build_elevated_emissions_glo')
    bin_coalonlywkdays = 'build_elevated_emissions_glo_sles15'
    buildemissionsubdir_coalonlywkdays = 'coalonlywkdays'

    templatefile_gasonlywkdays = 'Build_emission_gasonly_wkdays.template'
    testfile_gasonlywkdays = '../../TestFiles_13/run_gasonly_wkday.run'
    binpath_gasonlywkdays = os.path.join(Basebinpath, 'build_elevated_emissions_glo')
    bin_gasonlywkdays = 'build_elevated_emissions_glo'
    buildemissionsubdir_gasonlywkdays = 'gasonlywkdays'

    templatefile_restwkdays = 'Build_emission_rest_wkdays.template'
    testfile_restwkdays = '../../TestFiles_13/run_rest_wkday.run'
    binpath_restwkdays = os.path.join(Basebinpath, 'build_elevated_emissions_glo')
    bin_restwkdays = 'build_elevated_emissions_glo'
    buildemissionsubdir_restwkdays = 'restwkdays'

    startdate = dt.datetime(2019,11,19)
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2019,11,19),"UTC")
    enddate = startdate + dt.timedelta(days=5, hours=00)
    emissiondate = dt.datetime(2013,1,1)
    #TimestampUTC = dt.datetime(2018,2,1)
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2018,5,28,4,17,15),"UTC")
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,8,22,2,00),"UTC")

    #emissions_edms_DATA = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/emission2008NH3'
    emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2013NH3_PM10'

    templatefile_power = 'Build_emission_power.template'
    testfile_power = '../../TestFiles/run_power.run'
    binpath_power = os.path.join(Basebinpath, 'build_elevated_emissions_glo')
    bin_power = 'build_elevated_emissions_glo'
    buildemissionsubdir_power = 'power'
    emissions_power_DATA = '/home/trieut/power/emission_non_edms'
    timeresolvedcoalpower = False
    
    templatefile_rfs_OEH = 'Build_pems_generic.template'
    testfile_rfs_OEH = '../../TestFiles/run_rfs.run'
    binpath_rfs_OEH = os.path.join(Basebinpath,'build_elevated_emissions_bushfire')
    bin_rfs_OEH = 'build_elevated_emissions_bushfire_noglo'
    buildemissionsubdir_rfs_OEH = 'bushfireOEH'
    emissions_rfs_DATA_OEH = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireOEH'
    bushfireOEH = False

    templatefile_rfs_CSIRO = 'Build_bushfire_csiro.template'
    testfile_rfs_CSIRO = '../../TestFiles/build_smoke_emissions.run'
    binpath_rfs_CSIRO = os.path.join(Basebinpath,'build_smoke_emissions')
    #bin_rfs_CSIRO = 'build_smoke_emissions'
    #bin_rfs_CSIRO = 'build_smoke_emissions'
    #bin_rfs_CSIRO          = 'build_smoke_emissions_opt_sles15_heap349_mcmedium'
    #bin_rfs_CSIRO          = 'build_smoke_emissions_debug_sles15_heap349_mcmedium'
    bin_rfs_CSIRO          = 'build_smoke_emissions_opt_sles15_heap_mcmedium'
    buildemissionsubdir_rfs_CSIRO = 'bushfireCSIRO'
    buildemissionsubdir_rfs_CSIRO = os.path.join('/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-1-PSE-only','bushfireCSIRO_corrected_D1.0')
    #buildemissionsubdir_rfs_CSIRO = os.path.join('/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-1-PSE-only','bushfireCSIRO_corrected_D0.2')
    bushfireCSIRO = True
    emissions_rfs_DATA_CSIRO = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
    burn_start_hour_csiro = 5
    burn_start_hour_csiro = 0
    offset_bushfire_csiro = 0

    Run_HRB = True
    Run_HRB = False
    Run_Bushfire = True
    
    datafiredir_Wildfire = '/mnt/climate/cas/scratch/bushfire-emissions-20191119'
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-1-PSE-only'
    datafiredir_HRB  = '/mnt/climate/cas/scratch/bushfire-emissions-20191119'

#   offset is the difference between local time and UTC, (remember daylight savings)
    offset = 0

    templatefile_PSEmerge = 'PSEmerge.template'
    binpath_PSEmerge = os.path.join(Basebinpath, 'psemerge')
    bin_PSEmerge = 'psemerge'

    binpath_PSEMM = os.path.join(Basebinpath,'psemergem')
    #bin_PSEMM = 'psemergem'
    #bin_PSEMM = 'psemergem_opt_sles15_heap349_mcmedium'
    bin_PSEMM = 'psemergem_opt_sles15_heap_mcmedium'
    templatefile_PSEMM = 'PSEmergeMulti.template'

    import Inventory_Ratios_PM10_2013 as IR2013PM10
    Inventory_ratios =  IR2013PM10.Inventory_Ratios_PM10_2013(logger, justif)
    PMspeciation = 'PM10'

    logger.info(''.ljust(justif,'-'))
    logger.debug('Building emission tests'.center(justif,'|'))
    logger.info(''.ljust(justif,'-'))
    EmissionPSEConfig = EmissionPSEConfigClass(logger, justif, 
                 templatepath, Basebinpath, bininitline,
                 templatefile_coalonlywkdays, templatefile_gasonlywkdays, templatefile_restwkdays, templatefile_power, 
                 buildemissionpath_static, buildemissionpath_dynamic, buildemissionsubdir_pse, templatefile_PSEmerge, binpath_PSEmerge, bin_PSEmerge,
                 buildemissionsubdir_coalonlywkdays, buildemissionsubdir_gasonlywkdays, buildemissionsubdir_restwkdays, buildemissionsubdir_power,
                 Inventory_ratios, PMspeciation, 
                 binpath_coalonlywkdays, binpath_gasonlywkdays, binpath_restwkdays, binpath_power,
                 bin_coalonlywkdays, bin_gasonlywkdays, bin_restwkdays, bin_power,
                 startdate, enddate, emissiondate, TimestampUTC,
                 emissions_edms_DATA, emissions_power_DATA, offset, timeresolvedcoalpower,
                 Run_HRB, Run_Bushfire,
                 emissions_rfs_DATA_OEH, templatefile_rfs_OEH, binpath_rfs_OEH, bin_rfs_OEH, buildemissionsubdir_rfs_OEH, bushfireOEH,
                 emissions_rfs_DATA_CSIRO, templatefile_rfs_CSIRO, binpath_rfs_CSIRO, bin_rfs_CSIRO, buildemissionsubdir_rfs_CSIRO, bushfireCSIRO, offset_bushfire_csiro, burn_start_hour_csiro,
                 templatefile_PSEMM, binpath_PSEMM, bin_PSEMM,
                 datafiredir_Wildfire = datafiredir_Wildfire,
                 datafiredir_HRB = datafiredir_HRB,
                 binary = False,
                 )

    logger.info('EmissionPSEConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionPSEConfig.Create_Emission_dirs()
    logger.info('EmissionPSEConfig dir creation'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    
    # coal wkdays
    EmissionCoalonlywkdaysConfig = EmissionPSEConfig.init_EmissionCoalonlywkdaysConfigClass()
    logger.info('EmissionCoalonlywkdaysConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionCoalonlywkdaysConfig.Build_template_coalonlywkdays()
    logger.debug('-------------------------------')
    logger.debug('Diff coalonlywkdays')
    Builtfile = os.path.join(buildemissionpath_static, buildemissionsubdir_pse, buildemissionsubdir_coalonlywkdays, 'run_coalonly_wkday.run')
    #IL.PrintDiff(Builtfile, testfile_coalonlywkdays, logger)
    logger.info('EmissionCoalonlywkdaysConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionCoalonlywkdaysConfig.Build_coalonlywkdays()
    logger.info('Coalonlywkdays full output name= %s',EmissionCoalonlywkdaysConfig.fullnameoutputfile())
    logger.info('EmissionCoalonlywkdaysConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))


    #gas wkdays
    EmissionGasonlywkdaysConfig = EmissionPSEConfig.init_EmissionGasonlywkdaysConfigClass()
    logger.info('EmissionGasonlywkdaysConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionGasonlywkdaysConfig.Build_template_gasonlywkdays()
    logger.debug('-------------------------------')
    logger.debug('Diff gasonlywkdays')
    Builtfile = os.path.join(buildemissionpath_static, buildemissionsubdir_pse, buildemissionsubdir_gasonlywkdays, 'run_gasonly_wkday.run')
    #IL.PrintDiff(Builtfile, testfile_gasonlywkdays, logger)
    logger.info('EmissionGasonlywkdaysConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionGasonlywkdaysConfig.Build_gasonlywkdays()
    logger.info('Gasonlywkdays full output name= %s',EmissionGasonlywkdaysConfig.fullnameoutputfile())
    logger.info('EmissionGasonlywkdaysConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    
    #others
    EmissionRestwkdaysConfig = EmissionPSEConfig.init_EmissionRestwkdaysConfigClass()
    logger.info('EmissionRestwkdaysConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionRestwkdaysConfig.Build_template_restwkdays()
    logger.debug('-------------------------------')
    logger.debug('Diff restwkdays')
    Builtfile =  os.path.join(buildemissionpath_static, buildemissionsubdir_pse, buildemissionsubdir_restwkdays, 'run_rest_wkday.run')   
    #IL.PrintDiff(Builtfile, testfile_restwkdays, logger)
    logger.info('EmissionRestwkdaysConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionRestwkdaysConfig.Build_restwkdays()
    logger.info('Restwkdays full output name= %s',EmissionRestwkdaysConfig.fullnameoutputfile())
    logger.info('EmissionRestwkdaysConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #bushfire CSIRO
    #startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2018,5,26),"UTC")
    #enddate = startdate + dt.timedelta(days=1,hours=00)
    #TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2018,5,28,4,17,15),"UTC")
    #EmissionPSEConfig.startdate = startdate
    #EmissionPSEConfig.enddate = enddate
    #EmissionPSEConfig.TimestampUTC = TimestampUTC
    
    BushfireCSIROConfig = EmissionPSEConfig.init_BushfireCSIRO()
    logger.info('BushfireCSIROConfig object init'.ljust(justif-2,'.')+"OK")
    BushfireCSIROConfig.Build_bushfire()
    logger.info('Bushfire CSIRO emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))

    #bushfire OEH
    #BushfireOEHConfig = EmissionPSEConfig.init_BushfireOEH()
    logger.info('BushfireOEHConfig object init'.ljust(justif-2,'.')+"OK")
    #BushfireOEHConfig.Build_template(logger)
    logger.debug('-------------------------------')
    logger.debug('Diff Rfs_OEH')
    Built_file=os.path.join(buildemissionpath_dynamic, buildemissionsubdir_pse, buildemissionsubdir_rfs_OEH,'run_rfs.run')
    #IL.PrintDiff(Built_file, testfile_rfs_OEH,logger)
    logger.info('BushfireOEHConfig template'.ljust(justif-2,'.')+"OK")
    #BushfireOEHConfig.Build_bushfire(logger)
    #logger.debug('BushFireOEH full output name= %s',BushfireOEHConfig.fullnameoutputfile())
    logger.info('Bushfire OEH emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    #Coal powerstations
    #EmissionPowerConfig = EmissionPSEConfig.init_EmissionPowerConfigClass()
    logger.info('EmissionPowerConfig object init'.ljust(justif-2,'.')+"OK")
    #EmissionPowerConfig.Build_template_power()
    logger.debug('-------------------------------')
    logger.debug('Diff run_power.run')
    Builtfile =  os.path.join(buildemissionpath_dynamic, buildemissionsubdir_pse, buildemissionsubdir_power, 'run_power.run')   
    #IL.PrintDiff(Builtfile, testfile_power, logger)
    logger.info('EmissionPowerConfig template'.ljust(justif-2,'.')+"OK")
    #EmissionPowerConfig.Build_power()
    #logger.debug('Power full output name= %s',EmissionPowerConfig.fullnameoutputfile())
    logger.info('EmissionPowerConfig emission file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    
    #Build all
    EmissionPSEConfig.Build_Emissions()
    logger.debug('PSE output name= %s',EmissionPSEConfig.fullnameoutputfile())
    logger.info('PSE Emissions file build'.ljust(justif-2,'.')+"OK")
    logger.info(''.ljust(justif,'-'))
    
    

