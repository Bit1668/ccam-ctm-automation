"""
.. module:: MEMSConfig
   :platform: Unix
   :synopsis: Everything needed to include the offline emissions form the MEMS.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *
import pytz
import numpy as np
import sys
import os
import datetime as dt
import fnmatch
import shutil
###########################################################################################
class MEMSConfigClass(object):
    def __init__(self, logger, justif, bininitline,
                 SimulationPath, EmissionBuildPath_static, EmissionBuildPath_dynamic,
                 startdate, enddate, emissiondate, TimestampUTC, 
                 ResolutionNames,
                  ):
        """
        This class defines and configures MEMS emission objects.
        It contains all the parameters and the methods to build MEMS emissions.
        
        Parameters
        ---------- 
        logger : logging.logger
            instance of a logger to output message.
        justif : int
            max message width to justify logger output.    
        bininitline : str
            init line to setup env when running a binary
        buildemissionpath : str
            root of the main emission directory to build in. 
        buildemissionsubdir : str
            path from the main emission directory to build in.
        startdate : datetime
            start date of the emissions to build as a datetime with defined timezone.
        enddate : datetime
            end date date of the burns as a datetime with defined timezone.
        emissiondate : datetime
            emission date for when MEMS emissions are involved.
        TimestampUTC : datetime
            timestamp in UTC to mark the run. 
        datapath : str
            path to find the MEMS data and parametres.
        offset : str
            time difference to offset the emissions.
        ResolutionNames : str
            string identifying the computational domain name.
        """
        self.justif = justif
        self.logger = logger          
        self.bininitline = bininitline
        #self.startdate = startdate.astimezone(pytz.utc)
        #aedt = pytz.timezone('Australia/Sydney')
        #self.startdateAEDT = startdate.astimezone(aedt)
        self.startdate = startdate
        self.enddate =  enddate
        self.TimestampUTC = TimestampUTC
        self.emissiondate = emissiondate
        
        # directories
        # root dir of the simulation
        self.SimulationPath = SimulationPath

        self.EmissionBuildPath_static = EmissionBuildPath_static
        self.EmissionBuildPath_dynamic = EmissionBuildPath_dynamic
        
        #which domain to include
        self.ResolutionNames = ResolutionNames

       
        self.DoMerge = False
       
       # main MEMS emission sudbdir
        self.MEMS_main_dir = 'MEMS'
        
        # List of MEMS emission files
        self.MEMS_emission_file_list = []
        self.MEMS_CF_final_emission_file_before_merge = '{domain}_MEMS_CF_final_emission.nc'
        self.MEMS_CTM_final_emission_file_before_merge = '{domain}_MEMS_CTM_final_emission.nc'

        # List of input CTM files
        self.ListOfCTMFiles = None
        self.CTMFileMatchingPattern = '{domain}*_cb05*'
        
        # MEMS module subdirs
#       Include or not emission groups.
        self.includeemission_MEMS = False
        
        # Dust
        self.FirstRunDust = True
        self.includeemission_MEMS_dust = False
        self.prefix_dust_file = 'Dust'
        self.MEMS_dust_dir = 'Dust'
        self.MEMS_dust_source_dir = None
        self.MEMS_dust_source_file = None
        self.DustModel = None
        self.Run_Dust = False
        self.dust_updated = False
        self.Dust_CTM_Outputfile_list = []
        
        return        
        
############################################################################################################################
    def fullpath_MEMS_final_emission_file_before_merge(self):
        """
        Method to output the output full path filename
        """
        name = os.path.join(self.EmissionBuildPath_dynamic, self.MEMS_main_dir, self.MEMS_final_emission_file_before_merge)
        return name

############################################################################################################################
    def relativepath_MEMS_final_emission_file_before_merge(self):
        """
        Method to output the output relative (to the build emission directory) path filename
        """
        name = os.path.join(self.MEMS_main_dir, self.MEMS_final_emission_file_before_merge)
        return name

############################################################################################################################
    def fullpath_MEMS_final_emission_dir_before_merge(self):
        """
        Method to output the output full path filename
        """
        name = os.path.join(self.EmissionBuildPath_dynamic, self.MEMS_main_dir)
        return name

############################################################################################################################
    def relativepath_MEMS_final_emission_dir_before_merge(self):
        """
        Method to output the output relative (to the build emission directory) path filename
        """
        name = os.path.join(self.MEMS_main_dir)
        return name

############################################################################################################################
    def MEMSDataDirExist(self):
        """
        Method to detect the existence of the data directory 
        """
        if not os.path.isdir(self.fullpath_MEMS_final_emission_file_before_merge()):
            self.logger.error("ERROR")
            self.logger.error("NO MEMS DATA DIR FOUND")
            self.logger.error("You need to find %s",self.datafiredir)
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def DeleteDirAndItsContents(self, directory):
        """
        Method to delete previous fire directory dataset
        """
        if os.path.isdir(directory):
            self.logger.warn("Previous dataset found")
            self.logger.warn("Removing %s and all its contents",directory)
            shutil.rmtree(directory)
        else:
            self.logger.info("No previous dataset found ... Continuing")
            
        return          

############################################################################################################################
    def DeleteFile(self, filename):
        """
        Method to delete previous output emission file
        """
        if os.path.isfile(filename):
            self.logger.warn("Previous dataset found")
            self.logger.warn("Removing %s ",filename)
            shutil.rmtree(directory)
        else:
            self.logger.info("No previous dataset found ... Continuing")
            
        return          

############################################################################################################################
    def CTMFileListPerDomain(self, domain):
        """
        Method to check the existence of dust data in the source directory
        """
        listdir = os.listdir(self.SimulationPath)
        filteredlist = fnmatch.filter(listdir,  self.CTMFileMatchingPattern.format(domain = domain))
        self.ListOfCTMFiles = [os.path.join(self.SimulationPath, ffile) for ffile in filteredlist]
        print(listdir, filteredlist)
        if not filteredlist:
            self.logger.error("ERROR")
            self.logger.error("NO CTM Prep file FOUND")
            self.logger.error("You need to find fire filename matching pattern %s", self.CTMFileMatchingPattern.format(domain = domain))
            self.logger.error("In the following dir %s", self.SimulationPath)
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def init_MEMSDustConfig(self):
        """
        Method to initialise the MEMS Dust module
        """
        from . import MEMSDustConfig as Dust
        MEMSDustConfig = Dust.MEMSDustConfigClass(
                 self.logger, self.justif,
                 self.startdate, self.enddate, self.emissiondate, self.TimestampUTC, 
                 self.SimulationPath, self.EmissionBuildPath_static, self.EmissionBuildPath_dynamic, 
                 self.MEMS_main_dir, self.MEMS_dust_dir, self.MEMS_dust_source_dir, self.MEMS_dust_source_file, 
                 self.prefix_dust_file, self.ListOfCTMFiles,
                  )
        return  MEMSDustConfig         

############################################################################################################################
    def FinalMergeIntoCTM(self, final_merged_file, ListOfFilesToMerge, ):
        """
        Method to check the existence of fire data in the source directory
        """
        #sys.path.append('/home/barthelemyx/Projects/mems')
        sys.path.append('/home/monkk/mems')
        import core.pluggins.ctm.ctm_merge as cm
        
        #destination = self.ListOfCTMFiles
        append = True
        #append = False
        merger = cm.NFileMerge(final_merged_file, append = append,
                add_offline_dust_variable=True)
        print('inside', ListOfFilesToMerge)
        merger.merge(ListOfFilesToMerge)
        return          

############################################################################################################################
    def Create_MEMS_dir(self):
        """ 
        This method build the MEMS dust dirs.
        """
        dirdir = self.fullpath_MEMS_final_emission_dir_before_merge()
        try :
            os.makedirs(dirdir)
            self.logger.info('Creation MEMS Dir= {msg}'.format(msg=dirdir).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('MEMS Dir = {msg}'.format(msg=dirdir).ljust(self.justif-13,'.') + 'Already exist')
            pass

############################################################################################################################
    def Run(self):
        """
        Method to Run and prepare the MEMS data source to the local CTM files
        """
        
        self.logger.info(''.ljust(self.justif,'-') )
        self.logger.info('MEMS Emission Module Hook'.center(self.justif,' ') )
        self.logger.info(''.ljust(self.justif,'-') )
       
        self.Create_MEMS_dir()
        
        self.FirstRunDust = True
        
        #iterate for each domain
        for domain in self.ResolutionNames:
            #list the CTM files
            self.CTMFileListPerDomain(domain)
            print(self.ListOfCTMFiles)
            
            #########
            # Preping the files per component
            #Dust
            
            if self.includeemission_MEMS_dust:
                MEMSDustConfig = self.init_MEMSDustConfig()
                MEMSDustConfig.Run_all(self.FirstRunDust)
                self.FirstRunDust = False
                self.Dust_CTM_Outputfile_list = MEMSDustConfig.Dust_CTM_Outputfile_list
                self.MEMS_emission_file_list += self.Dust_CTM_Outputfile_list
                self.DoMerge = True
                self.logger.info('MEMS Dust Emission Module file'.ljust(self.justif-2,'.')+'OK' )
            else:
                self.logger.info('MEMS Dust Emission Module file'.ljust(self.justif-12,'.')+'NOT INCLUDED' )
                
            ##########
            # Merging
            if self.DoMerge :
                for findex, ctmfile in enumerate(self.ListOfCTMFiles):
                    if self.includeemission_MEMS_dust:
                        MatchingPattern = '*{prefix}_{ctmfile}'.format(prefix = self.prefix_dust_file, ctmfile = os.path.basename(ctmfile))
                        print('matching merge pattern', MatchingPattern)
                        #print('Mems file list', self.MEMS_emission_file_list)
                        ListOfFilesToMerge = fnmatch.filter(self.MEMS_emission_file_list,  MatchingPattern)
                        print('file to merge', ListOfFilesToMerge)
                        final_merged_file = ctmfile
                        print('final file to merge in',final_merged_file)
                        if ListOfFilesToMerge:
                            self.FinalMergeIntoCTM(final_merged_file, ListOfFilesToMerge, )
                            self.logger.info('MEMS Dust Emission file merge'.ljust(self.justif-2,'.')+'OK' )
                        else:
                            self.logger.info('MEMS Dust Emission file merge'.ljust(self.justif-7,'.')+'ABORTED' )
                            
                
############################################################################################################################
############################################################################################################################
if __name__ == '__main__':
    import sys
    
    try:
        from .. import InitLogging as IL
    except:
        sys.path.append('Python-Scripts/Sources')
        import InitLogging as IL

    loggername = 'Mems'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    bininitline = "source /home/barthelemyx/modulefiles/init.bash | ulimit -s 300000 |"
    
    startdate = dt.datetime(2009,9,14)
    enddate = startdate + dt.timedelta(days=3, hours=00)
    emissiondate = dt.datetime(2008,1,1)
    TimestampUTC = dt.datetime(2018,2,1)

    SimulationPath = 'Emissions-Test'
    
    EmissionBuildPath_static = os.path.join(SimulationPath, 'static')
    EmissionBuildPath_dynamic = os.path.join(SimulationPath,'dynamic')
    MEMS_main_dir = 'MEMS'
    MEMS_dust_dir = 'DUST'
    MEMS_dust_source_dir = '/mnt/climate/cas/scratch/hynesr/EMS/tests/dust/vertical_dust_2009/'
    ctmdir = '/mnt/climate/cas/scratch/fuchsd/test/data/ctm-no-dust-200909/'
    ctmfiles = ['aus_20090914_cb05_aer2_0.800dg_sep09.nc','aus_20090915_cb05_aer2_0.800dg_sep09.nc']
    ListOfCTMFiles = [os.path.join(ctmdir, ffile) for ffile in ctmfiles]
    #ListOfCTMFiles = ctmfiles
    
    ResolutionNames = ['aus']
    
    SimulationPath = ctmdir
    
    MEMSConfig = MEMSConfigClass(
                 logger, justif, bininitline,
                 SimulationPath, EmissionBuildPath_static, EmissionBuildPath_dynamic,
                 startdate, enddate, emissiondate, TimestampUTC, 
                 ResolutionNames,
                 )
    MEMSConfig.includeemission_MEMS_dust = True
    MEMSConfig.MEMS_dust_source_dir = MEMS_dust_source_dir
    MEMSConfig.Run()
