"""
.. module:: Tools
   :platform: Unix
   :synopsis: Everything needed to build the emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import os
############################################################################################################################
def Rename_emission_file(emissionpath, midinsert, endinsert,logger):
    """ This function rename the output file from the build emission scripts
    """
    name1=["CITemissions_"]
    name2=["wkday_","wkend_"]
    name3=['NO_','NO2_','CO_','SO2_','ALD2_','PART_','ETH_','FORM_','ISOP_','OLE_','PAR_','TOL_','XYL_','ETOH_','MEOH_','UNR_','NH3_']
    name4=['kgPerDay']
    ext='.grd'
    
    for n1 in name1:
        for n2 in name2:
            for n3 in name3:
                for n4 in name4:
                    filenamebefore=n1+n2+n3+n4+ext
                    filenameafter=n1+n2+midinsert+n3+n4+endinsert+ext
                    logger.debug('%s -> %s',filenamebefore, filenameafter)
                    try:
                        os.rename(os.path.join(emissionpath,filenamebefore),os.path.join(emissionpath,filenameafter))
                    except:
                        logger.info('file %s do not exist', os.path.join(emissionpath,filenamebefore))
                        pass
    try:
        oldname=os.path.join(emissionpath,'wkday_emissions_kgPerHour.csv')
        newname=os.path.join(emissionpath,'wkday_emissions_'+midinsert+'geo_kgPerHour'+ endinsert+'.csv')
        os.rename(oldname,newname)             
    except:
        logger.info('file %s do not exist',oldname)
        pass
    try:
        oldname=os.path.join(emissionpath,'wkend_emissions_kgPerHour.csv')
        newname=os.path.join(emissionpath,'wkend_emissions_'+midinsert+'geo_kgPerHour'+ endinsert+'.csv')
        os.rename(oldname,newname)             
    except:
        logger.info('file %s do not exist',oldname)
        pass
    return 

############################################################################################################################

