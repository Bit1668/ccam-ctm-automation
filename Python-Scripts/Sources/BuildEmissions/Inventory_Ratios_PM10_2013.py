"""
.. module:: 2008_inventory
   :platform: Unix
   :synopsis: To process the temperature extract files

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
import InventoryRatios as IR
################################################################################        
class Inventory_Ratios_PM10_2013(IR.GenericEmissionRatios):
    def __init__(self, logger, justif, ):    
        '''
        Class to read  and process Met Temperature data from 4 sources: BOM, CCAM, CTM, and OBS from the extracted data files
        
        Attributes
        -----------
        logger : logging.logger
            instance of a logger to output messages.
        justif : int
            max message width to justify logger output.   
         WSpath : str
             path to the data
         WSfile : str
             input filename containing the raw data extracted with the ForecastDataExtraction.py script
         WSOutputMean24h : str
             output filename conatining the 24h means of the data
         WSOutputStats24h : str
             output filename of the stats of the 24h means of the data
         WSOutputStatshourly : str
             output filename of the stats generated per forecast hours
        '''

        IR.GenericEmissionRatios.__init__(self, logger, justif, )
        
        # MVEMS 
        self.pm25pm10ratio_PetExh       = 0.953
        self.pm25pm10ratio_DieExh       = 0.970  
        self.pm25pm10ratio_PetEvp       = 0.953  
        self.pm25pm10ratio_LpgExh       = 0.953  
                
        # Commercial domestic
        self.pm25pm10ratio_aircraft     = 0.983
        self.pm25pm10ratio_comveh       = 0.970
        self.pm25pm10ratio_indveh       = 0.970
        self.pm25pm10ratio_loco         = 0.970        
        self.pm25pm10ratio_nonexhaustPM = 0.532        
        self.pm25pm10ratio_shipping     = 0.920        
        self.pm25pm10ratio_other        = 0.336 
        self.pm25pm10ratio_fugitives    = 0.150  
        
        # WHE
        self.pm25pm10ratio_WHE          = 0.963  
        
        # PSE emissions
        self.pm25pm10ratio_GasGen       = 0.998   
        self.pm25pm10ratio_CoalGen      = 0.408   
        self.pm25pm10ratio_PSEother     = 0.680    
        return

