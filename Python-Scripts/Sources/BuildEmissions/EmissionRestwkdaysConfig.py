"""
.. module:: EmissionRestwkdaysConfig
   :platform: Unix
   :synopsis: Everything needed to build the Restwkdays emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import string
import datetime as dt
import os
import sys
import subprocess
try:
    from . import Tools
    from .. import JinjaUtils as JJU
except:
    import Tools
    sys.path.append('..')
    import JinjaUtils as JJU
###########################################################################################
class EmissionRestwkdaysConfigClass(object):
    """ This class defines a emission object, that contains all the parameters to build Restwkdays emissions 
    This class defines and configures a Restwkdays emission object.
    It contains all the parameters and the methods to build Restwkdays emissions.
    
    The class is self contained and can be initialised from original data, it doesn't depend on higher level classes.
    it contains all the methods to configure and generate emissions.
    
    Attributes
    -----------
        logger : logging.logger
            instance of a logger to output message
        justif : int
            max message width to justify logger output     
        startdate : datetime
            start date of the emissions to build as a UTC datetime.
        startyear : str
            year of the starting date of the emission windows.
        startmonth : str
            month of the starting date of the emission windows.
        startday : str
            day of the starting date of the emission windows.
        enddate : datetime
            end date of the emissions to build as a UTC datetime.
        endyear : str
            year of the ending date of the emission windows.
        endmonth : str
            month of the ending date of the emission windows.
        endday : str
            day of the ending date of the emission windows.
        datapath : str
            path to find the RFS fire data and parametres.
        emissiondate : datetime.datetime
            date of the emission 
        emissionmonth : str
            reduced month name from the emission date.
        emissionyear : str
            year of the emissions
        self.offset_veh : int
            hour offset of the emission to take in account timezones 
        binpath : str
            path to find the binary to build individual scar emissions.
        bin : str
            binary filename to build individual scar emissions.
        bininitline : str
            init line to setup env when running a binary
        userfile : str
            name of an user file = 'cogen_24h_emissions.csv'
        templatepath : str
            path to the template directory.
        templatefile : str
            name of the templatefile to build an emission file.
        buildemissionpath : str
            root of the main emission directory to build in. 
        buildemissionsubdir : str
            path from the main emission directory to build in.
    
    """
    def __init__(self, logger, justif, 
                 templatepath, templatefile, 
                 buildemissionpath, buildemissionsubdir,
                 binpath, bin, bininitline, 
                 startdate, enddate, emissiondate, 
                 datapath, offset,
                 Inventory_ratios, PMspeciation,
                 **kwargs):
        self.binary = kwargs.get('binary', True)

        self.logger = logger
        self.justif = justif
        self.startdate = startdate
        self.enddate = enddate
        self.startyear = startdate.strftime('%Y')
        self.startmonth = startdate.strftime('%m')
        self.startday = startdate.strftime('%d')
        self.endyear = enddate.strftime('%Y')
        self.endmonth = enddate.strftime('%m')
        self.endday = enddate.strftime('%d')
        self.datapath = datapath
        self.emissiondate = emissiondate
        self.emissionmonth = string.lower(startdate.strftime('%b'))
        self.emissionyear = emissiondate.strftime('%y')
        self.offset = str(offset)
        self.emission_name_year = self.emissionmonth+self.emissionyear
        self.binpath = binpath
        self.bin = bin
        self.bininitline = bininitline
        self.userfile = 'cogen_24h_emissions.csv'
        self.templatepath = templatepath
        self.templatefile = templatefile
        self.buildemissionpath = buildemissionpath
        self.buildemissionsubdir = buildemissionsubdir
        self.Inventory_ratios = Inventory_ratios
        self.PMspeciation = PMspeciation

        #self.fileoutput = 'emsn_gmr_' + self.emission_name_year + 'wkday_other.pse.bin'
        #self.fileoutput='emsn_gmr_'+self.emission_name_year+'wkday_other.pse'

        self.ndays = (self.enddate - self.startdate).days
        self.fileoutput_suffix ='wkday_other.pse'
        if self.binary:
            self.fileoutput_suffix = '{prefix}.bin'.format(prefix = self.fileoutput_suffix)
        
        #self.fileoutput = 'emsn_gmr_'+self.emission_name_year+'wkday_anthropogenic.pse.bin'
        #self.fileoutput='emsn_gmr_'+self.emission_name_year+'wkday_anthropogenic.pse'
        #self.fileoutput = 'emsn_gmr_{date}_{suffix}'.format(date = self.emission_name_year, 
        #                                                   suffix = self.fileoutput_suffix)
        self.fileoutput = 'emsn_gmr_{emissionyear}_{PMspec}_{date}_{ndays}days_{suffix}'.format(emissionyear = self.emissiondate.strftime('%Y'),
                                                                             PMspec = self.PMspeciation,
                                                                             date = self.startdate.strftime('%b_%a'), 
                                                                             ndays = self.ndays,
                                                                             suffix = self.fileoutput_suffix)
        self.fullpathfileoutput = os.path.join(self.buildemissionpath, self.buildemissionsubdir, self.fileoutput)
        return
        
###################################
    def fullnameoutputfile(self):
        name = os.path.join(self.buildemissionpath, self.buildemissionsubdir, self.fileoutput)
        return name

###################################
    def relativenameoutputfile(self):
        name = os.path.join(self.buildemissionsubdir, self.fileoutput)
        return name
###################################
    def Build_dirs(self):
        path = os.path.join(self.buildemissionpath, self.buildemissionsubdir)
        try :
            os.makedirs(path)
            self.logger.info('Creation Working Dir= {msg}'.format(msg=path).ljust(self.justif-2,'.') + 'OK')
        except:
            self.logger.info('Working Dir = {msg}'.format(msg=path).ljust(self.justif-13,'.') + 'Already exist')
            pass
        return

############################################################################################################################
    def Build_template_restwkdays(self):
        """ This function write the config file to build emission for every other sources for weeks days
        """
        dirextension = self.buildemissionsubdir

        self.Build_dirs()

        filename = os.path.join(self.buildemissionpath, dirextension, 'run_rest_wkday.run')
        File = open(filename,'w')

        template = JJU.LoadTemplate(self.templatepath, self.templatefile)

        emission = template.render({'EmissionRestwkdaysConfig':self })
        File.write(emission)
        File.close()
        
        self.logger.info('Template creation'.ljust(self.justif-2,'.') + 'OK')
        return 
    
############################################################################################################################
    def Build_restwkdays(self):
        """ This function write the emission file for every other sources for weeks days emission
        """
        dirextension = self.buildemissionsubdir
        buildemissionpath = self.buildemissionpath

        RunDir=os.path.join(self.buildemissionpath, dirextension)
        my_env = os.environ.copy()
        
        #Runline = "source /etc/profile.d/modules.sh | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp | " + \
        #               os.path.join(self.binpath, self.bin) +" run_rest_wkday.run"
        
        run = "{bin} run_rest_wkday.run".format(bin=os.path.join(self.binpath, self.bin))
        Runline = '{init} {run}'.format(init = self.bininitline, 
                                        run = run)

        PIPE = subprocess.PIPE
            
        #print(subprocess.Popen([ Runline ], shell=True, env=my_env, cwd=RunDir).wait())
        p1 = subprocess.Popen([Runline],shell=True, env=my_env, cwd=RunDir, bufsize=350000, stdout = PIPE, stdin = PIPE, stderr = PIPE)
    
        out, err = p1.communicate()
        output = out
        error = err
    
        self.logger.info(output)
        self.logger.error('Errors:')
        self.logger.error(error)
    
        self.logger.info('Emission file'.ljust(self.justif-2,'.') + 'OK')
        return 
###################################

if __name__ == '__main__':
    import sys
    #sys.path.append('..')
    import InitLogging as IL
    loggername = 'build_emission'
    logger = IL.Initialise_logging(loggername)
    
    justif = 100
    
    buildemissionpath = 'Emissions-Test'
    Basebinpath = '/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran'
    #templatepath = '/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates'
    templatepath = '/home/monkk/repositories/ccam-ctm-automation/Python-Scripts/Templates'        
    bininitline = "source /home/monkk/modulefiles/init.bash | ulimit -s 300000 |"

    templatefile_restwkdays = 'Build_emission_rest_wkdays.template'
    testfile_restwkdays = '../../TestFiles_13/run_rest_wkday.run'
    binpath_restwkdays = os.path.join(Basebinpath, 'build_elevated_emissions_glo')
    bin_restwkdays = 'build_elevated_emissions_glo'
    buildemissionsubdir_restwkdays = os.path.join('pse', 'restwkdays')

    startdate = dt.datetime(2017,4,4)
    enddate = startdate + dt.timedelta(days=3, hours=00)
    emissiondate = dt.datetime(2013,1,1)
    emissiondate_veh = dt.datetime(2013,2,1)
    emissiondate_whe = dt.datetime(2013,7,1)
    #emissions_edms_DATA = '/mnt/climate/cas/project/ccam-ctm/emission_modelling/emission2008NH3'
    emissions_edms_DATA = '/mnt/climate/cas/project/EmissionSources-Forecast/emission2013NH3_PM10'

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
    offset = -10
    offset_veh = -10
    offset_whe = -10
    
    import Inventory_Ratios_PM10_2013 as IR2013PM10
    Inventory_ratios =  IR2013PM10.Inventory_Ratios_PM10_2013(logger, justif)
    PMspeciation = 'PM10'

    logger.debug('Building emission tests')

    CurrentDir = os.getcwd()
#--------------------------------------------------------------------------------------------------------------------------------------------------------
    EmissionRestwkdaysConfig = EmissionRestwkdaysConfigClass(logger, justif, 
                                                             templatepath, templatefile_restwkdays, 
                                                             buildemissionpath, buildemissionsubdir_restwkdays, 
                                                             binpath_restwkdays, bin_restwkdays, bininitline,
                                                             startdate, enddate, emissiondate, 
                                                             emissions_edms_DATA, offset, 
                                                             Inventory_ratios, PMspeciation)
    logger.info('EmissionRestwkdaysConfig object init'.ljust(justif-2,'.')+"OK")
    EmissionRestwkdaysConfig.Build_template_restwkdays()
    logger.debug('-------------------------------')
    logger.debug('Diff restwkdays')
    Builtfile =  os.path.join(buildemissionpath, buildemissionsubdir_restwkdays, 'run_rest_wkday.run')   
    IL.PrintDiff(Builtfile, testfile_restwkdays, logger)
    logger.info('EmissionRestwkdaysConfig template'.ljust(justif-2,'.')+"OK")
    
    EmissionRestwkdaysConfig.Build_restwkdays()
    logger.info('Restwkdays full output name= %s',EmissionRestwkdaysConfig.fullnameoutputfile())
    logger.info('EmissionRestwkdaysConfig emission file build'.ljust(justif-2,'.')+"OK")



