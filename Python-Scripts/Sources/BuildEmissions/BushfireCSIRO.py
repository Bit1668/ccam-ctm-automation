"""
.. module:: BushfireCSIRO
   :platform: Unix
   :synopsis: Everything needed to build the CSIRO bushfire emissions.

.. moduleauthor:: Xavier Barthelemy <xavier.barthelemy@environment.nsw.gov.au>


"""
from __future__ import absolute_import
import os
import sys
#sys.path.append('..')
import string
import pytz
import shutil
import fnmatch
import subprocess
import numpy as np
try :
    import Sources.JinjaUtils as JJU
except :
    sys.path.append('/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts')
    import Sources.JinjaUtils as JJU
    
import Sources.InitLogging as IL
import Sources.BuildEmissions.PSEmergeMulti as PSEMM

###########################################################################################
class BushfireCSIROConfigClass(object):
    """
    This class defines and configures a CSIRO bushfire emission object.
    It contains all the parameters and the methods to build CSIRO bushfire emissions.
    
    The class is self contained and can be initialised from original data, ti doesn't depend on higher level classes.
    it contains all the methods to configure and generate emissions from the bushfires knowing the shape.
    
    Attributes
    -----------
    logger : logging.logger
        instance of a logger to output messages.
    justif : int
        max message width to justify logger output.     
    startdate : datetime
        start date of the emissions to build as a UTC datetime.
    aedt : tz
        Sydney AEDT timezone.
    startdateAEDT : datetime
        start date of the emissions to build as a AEDT datetime.
    TimestampUTC : datetime
        timestamp in UTC to mark the run.
    startyear : str
        year of the starting date of the emission windows.
    startmonth : str
        month of the starting date of the emission windows.
    startday : str
        day of the starting date of the emission windows.
    enddate : datetime
        end date of the emissions to build as a UTC datetime.
    endyear : str
        year of the ending date of the emission windows.
    endmonth : str
        month of the ending date of the emission windows.
    endday : str
        day of the ending date of the emission windows.
    datapath : str
        path to find the RFS fire data and parametres.
    emissionmonth : str
        reduced month name from the start date.
    self.emissionyear : =emissiondate.strftime('%y')
    self.offset=str(offset)
    self.emission_name_year=self.emissionmonth+self.emissionyear
    self.emission_name_day=self.emissionmonth+self.startday
    binpath_CSIRO : str
        path to find the binary to build individual scar emissions.
    bin_CSIRO : str
        binary filename to build individual scar emissions.
    bininitline : str
        init line to setup env when running a binary
    userfile : str
        name of an user file = 'cogen_24h_emissions.csv'
    templatepath : str
        path to the template directory.
    templatefile : str
        name of the templatefile to build an emission file from one bushfire shape.
    buildemissionpath : str
        root of the main emission directory to build in. 
    buildemissionsubdir : str
        path from the main emission directory to build in.
    Firedir : str
        sub directory where to find the source fire data geometry.
    datafiredir :  str
        full path to find the source fire data scars.
    FireFileMatchingPattern : str
        matching pattern to discover the scar fire data in the Firedir directory.
    fire_list : list of str
        list of detected fire files
    currentsmegfire : list of str
        filtered list of fires belonging to the current smeg (smoke emission group).
    OneSmegPerFire : bool
        boolean var to set one smeg per fire detected (max 8 smeg?).
    smeg_dir : str
        sub directory name where to build the emissions from the fire in the smeg.
    current_smeg : int
        counter to iterate on the smeg.
    DELWPfuellayer : str
        filename of the DELWP fuel layer.
    RFSfuellayer : str
        filename of the RFS fuel layer.
    VASTfuellayer : str       
        filename of the VAST fuel layer.
    prefix_fire_file : str
        str built from the datafiredir and the os separator.
    InvividualFireEmissionFileMatchingPattern : str
        matching pattern to discover the individual emission built by the CSIRO script to merge them by psem.
    FireEmissionFilelist : list of str
        list of filename of the fire emission built.
    fileoutput_PSEM : str
        output filename of the merged fire emissions.
    templatefile_PSEM : str
        name of the templatefile to merge all the emission files from the bushfire(s).
    binpath_PSEM :str
        path to the binary for the multiple emission files merge.
    bin_PSEM : str
        name of the binary for the multiple emission files merge.
    numberofdays_PSEM : int
        number of days (files) to generate
    smoke_speciation_tables : str
        full path to find the smoke speciation tables.
    """
    def __init__(self, logger, justif,
                 templatepath, templatefile, 
                 buildemissionpath, buildemissionsubdir, 
                 binpath_CSIRO, bin_CSIRO, bininitline,
                 startdate, enddate, emissiondate, TimestampUTC, burn_start_hour,
                 datapath, offset, 
                 Run_HRB, Run_Bushfire,
                 binpath_PSEM, bin_PSEM, templatefile_PSEM,
                 **kwargs):
        """
        This class defines and configures a CSIRO bushfire emission object.
        It contains all the parameters and the methods to build CSIRO bushfire emissions.
        
        Parameters
        ---------- 
        logger : logging.logger
            instance of a logger to output message.
        justif : int
            max message width to justify logger output.    
        templatepath : str
            path to the template directory.
        templatefile : str
            name of the templatefile to build an emission file from one bushfire shape.
        buildemissionpath : str
            root of the main emission directory to build in. 
        buildemissionsubdir : str
            path from the main emission directory to build in.
        binpath_CSIRO : str
            path to find the binary to build individual scar emissions.
        bin_CSIRO : str
            binary filename to build individual scar emissions.
        bininitline : str
            init line to setup env when running a binary
        startdate : datetime
            start date of the emissions to build as a datetime with defined timezone.
        enddate : datetime
            end date date of the burns as a datetime with defined timezone.
        emissiondate : datetime
            emission date for when EDMS emissions are involved.
        TimestampUTC : datetime
            timestamp in UTC to mark the run. 
        datapath : str
            path to find the RFS fire data and parametres.
        offset : str
            time difference to offset the emissions.
        binpath_PSEM :str
            path to the binary for the multiple emission files merge.
        bin_PSEM : str
            name of the binary for the multiple emission files merge.
        templatefile_PSEM : str
            name of the templatefile to merge all the emission files from the bushfire(s).
        burn_function : numpy array of double
            emission burn function of the scar.
        burn_profile_24h : numpy array of double
            burn function in 24h to write into the template.
        burn_profile_24h_configured : bool
            flag to remember to first configure the burn profile before writing the template.
        burn_start_hour : int
            burn start hour in 24h
        """
        self.justif = justif
        self.logger = logger          
        self.startdate = startdate.astimezone(pytz.utc)
        aedt = pytz.timezone('Australia/Sydney')
        self.startdateAEDT = startdate.astimezone(aedt)
        self.TimestampUTC = TimestampUTC

        self.Run_HRB = Run_HRB 
        self.Run_Bushfire = Run_Bushfire
        
        
        #print self.startdate
        self.startyear = startdate.strftime('%Y')
        self.startmonth = startdate.strftime('%m')
        self.startday = startdate.strftime('%d')
        self.enddate = enddate.astimezone(pytz.utc)
        self.endyear = enddate.strftime('%Y')
        self.endmonth = enddate.strftime('%m')
        self.endday = enddate.strftime('%d')
        self.datapath = datapath
        self.emissionmonth = string.lower(startdate.strftime('%b'))
        self.emissionyear = emissiondate.strftime('%y')
        self.offset = str(offset)
        self.emission_name_year = self.emissionmonth+self.emissionyear
        self.emission_name_day = self.emissionmonth+self.startday
        self.binpath = binpath_CSIRO
        self.bin = bin_CSIRO
        self.bininitline = bininitline
        self.userfile = 'cogen_24h_emissions.csv'
        self.templatepath = templatepath
        self.templatefile = templatefile
        self.buildemissionpath = buildemissionpath
        self.buildemissionsubdir = buildemissionsubdir
        #self.datafiredir=os.path.join(self.datapath,startdate.strftime('%Y%m%dUTC'))

        self.Firedir = '{date}_{timestamp}'.format(date=self.startdateAEDT.strftime('%Y%m%dAEDT'),
                                                   timestamp = self.TimestampUTC.strftime('%Y%m%dT%H%M%S')
                                                   )
        self.HRB_dir = "HRBs"
        self.Wildfire_dir = "Wildfires"


        self.datafiredir_HRB = kwargs.get('datafiredir_HRB', None)
        self.datafiredir_Wildfire = kwargs.get('datafiredir_Wildfire', None)
        self.datafiredir_list = []
        self.datafiredir_base = os.path.join(self.datapath,self.Firedir)
        if not self.datafiredir_HRB:
            self.datafiredir_HRB = os.path.join(self.datafiredir_base, self.HRB_dir)
        if not self.datafiredir_Wildfire:
            self.datafiredir_Wildfire = os.path.join(self.datafiredir_base, self.Wildfire_dir)
                                                   
        #self.FireFileMatchingPattern='xfire_*.csv'
        self.FireFileMatchingPattern = '*.csv'
        self.fire_list = []
        self.fire_list_HRB = None
        self.fire_list_Wildfire = None
        self.currentsmegfire = None
        self.OneSmegPerFire = False
        #self.OneSmegPerFire = True
        self.smeg_dir = None
        self.current_smeg = None
        
        #self.DELWPfuellayer=os.path.join(self.datapath,'delwp_aug16')
        #self.RFSfuellayer=os.path.join(self.datapath,'rfs_geo_100m')
        #self.VASTfuellayer =os.path.join(self.datapath,'vast')       
        self.DELWPfuellayer = os.path.join('delwp_aug16')
        self.RFSfuellayer = os.path.join('rfs_geo_100m')
        self.VASTfuellayer = os.path.join('vast')       
        
        #Default Scar resolution if not specified in the individual scar file
        self.ResolutionInM = 1000.
        self.ResolutionInM = 100.
        
        self.Dilution = 1.
        #self.Dilution = 1E-1
        #self.Dilution = 0.15
        #self.Dilution = 2E-1
        #self.Dilution = 1E-2
        
        #self.prefix_fire_file = self.datafiredir+os.sep
        self.prefix_fire_file = None
        
        #self.OutputBinaryEmissions = True
        #self.OutputBinaryEmissions = False
        self.OutputBinaryEmissions = kwargs.get('binary', True)
        
        if self.OutputBinaryEmissions:
            self.InvividualFireEmissionFileMatchingPattern = 'fems_*.pse.bin'
            self.fileoutput_PSEM_template = 'fems_{date}_smeg_{smegnumber}.pse.bin'
            self.BuildASCIIIndividualFireEmission = 'F'
        else:
            self.InvividualFireEmissionFileMatchingPattern = 'fems_*.pse'
            self.fileoutput_PSEM_template = 'fems_{date}_smeg_{smegnumber}.pse'
            self.BuildASCIIIndividualFireEmission = 'T'
        
        self.fileoutput_list = []
        
        #self.InvividualFireEmissionFileMatchingPattern = 'fems_*_xfire_*.pse.bin'
        #self.InvividualFireEmissionFileMatchingPattern = 'fems_OneDay_*.pse.bin'
        #self.InvividualFireEmissionFileMatchingPattern = 'fems_OneDay_*.pse'
        #self.InvividualFireEmissionFileMatchingPattern = 'fems_*.pse.bin'
        #self.InvividualFireEmissionFileMatchingPattern = 'fems_*.pse'
        self.FireEmissionFilelist = []
        
        #self.InvividualFireEmissionPerDayFileMatchingPattern = 'fems_{date}_*.pse'
        #self.InvividualFireEmissionPerDayFileMatchingPattern = 'fems_{date}_*.pse.bin'
        self.FireEmissionPerDayFilelist = []
        
        #self.fileoutput_PSEM = 'fems_'+self.startyear+self.startmonth+self.startday
        
        self.fileoutput_oneday_PSEM = 'fems_OneDay_'
        self.templatefile_PSEM = templatefile_PSEM
        self.numberofdays_PSEM = 1
        ################################
        ##bug correction on PSEM merge?
        self.numberofdays_PSEM = (enddate - startdate).days
        ################################
        self.binpath_PSEM = binpath_PSEM
        self.bin_PSEM = bin_PSEM
        
        #self.fileoutput='emsn_gmr_'+self.startmonth+self.startday+'_rfs.pse.bin'
        #self.fullpathfileoutput=os.path.join(self.buildemissionpath,self.buildemissionsubdir,self.fileoutput)
     
        # number of CTM tracers (set by CTM chemistry mechanism
        # pointer to primary PM2.5 emissions in the build emissions code
        self.n_trac = 44
        self.PM25 = 36
        #prefix of smoke emission group directory names
        #smeg_name=smeg_ 
        #prefix of Alan's files        
        #smoke_name=${datapath}/xfire_    
        
        #self.fire_list_fname='fire_list.txt'
        #self.fire_list_file=os.path.join(self.datapath,self.fire_list_fname)
        #self.fire_list=None
        #SMoke Emission Group required
        self.nsmeg = None
        self.smeg_dir_names = []
        self.smeg_relative_dir_names = []
        self.smoke_speciation_tables = os.path.join(self.datapath,"smoke_speciation_tables.txt")
        # burning profile
        self.burn_function_hrb = np.array([0.057, 0.170, 0.237, 0.230, 0.170, 0.102, 0.034], dtype='d')
        #bushfire LST temporal distribution of emissions (from Meyer et al., 2008)
        self.burn_function_wildfire = np.array([0.027,0.027,0.027,0.027,0.026,0.026,
                                                0.025,0.036,0.046,0.055,0.062,0.066,  
                                                0.067,0.066,0.064,0.061,0.056,0.049, 
                                                0.040,0.033,0.030,0.028,0.028,0.027]) * self.Dilution
        self.burn_function = []
        self.burn_profile_24h = np.zeros([24], dtype = 'd')
        self.burn_profile_24h_configured = False
        self.burn_profile = []
        self.burn_profile_configured = False
        self.burn_start_hour = burn_start_hour
        return

############################################################################################################################
    def fullnameoutputfile(self):
        """
        Method to output the output full path filename
        """
        name = os.path.join(self.buildemissionpath,self.buildemissionsubdir, self.fileoutput)
        return name

############################################################################################################################
    def fullnameoutputfile_list(self):
        """
        Method to output the output full path filename
        """
        namelist = [os.path.join(self.buildemissionpath, 
                                 self.buildemissionsubdir,
                                 ff) for ff in self.fileoutput_list]
        return namelist

############################################################################################################################
    def relativenameoutputfile(self):
        """
        Method to output the output relative (to the build emission directory) path filename
        """
        name = os.path.join(self.buildemissionsubdir,self.fileoutput)
        return name

############################################################################################################################
    def DataFireDirExist(self):
        """
        Method to detect the existence of the bushfire data directory 
        """
        if not os.path.isdir(self.datafiredir):
            self.logger.error("ERROR")
            self.logger.error("NO CSIRO BUSHFIRE DATA DIR FOUND")
            self.logger.error("You need to find %s",self.datafiredir)
            sys.exit("Sorry :(")

############################################################################################################################
    def DataFireDirExist_HRB(self):
        """
        Method to detect the existence of the bushfire data directory 
        """
        if not os.path.isdir(self.datafiredir_HRB):
            self.logger.error("ERROR")
            self.logger.error("NO CSIRO HRB BUSHFIRE DATA DIR FOUND")
            self.logger.error("You need to find %s",self.datafiredir_HRB)
            sys.exit("Sorry :(")

############################################################################################################################
    def DataFireDirExist_Wildfire(self):
        """
        Method to detect the existence of the bushfire data directory 
        """
        if not os.path.isdir(self.datafiredir_Wildfire):
            self.logger.error("ERROR")
            self.logger.error("NO CSIRO WILDFIRE BUSHFIRE DATA DIR FOUND")
            self.logger.error("You need to find %s",self.datafiredir_Wildfire)
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def DeleteDirAndItsContents(self, directory):
        """
        Method to delete previous fire directory dataset
        """
        if os.path.isdir(directory):
            self.logger.warn("Previous dataset found")
            self.logger.warn("Removing %s and all its contents",directory)
            shutil.rmtree(directory)
        else:
            self.logger.info("No previous dataset found ... Continuing")
            
        return          

############################################################################################################################
    def DeleteFile(self, filename):
        """
        Method to delete previous output emission file
        """
        if os.path.isfile(filename):
            self.logger.warn("Previous dataset found")
            self.logger.warn("Removing %s ",filename)
            shutil.rmtree(filename)
        else:
            self.logger.info("No previous dataset found ... Continuing")
            
        return          

############################################################################################################################
    def FireInDataFireDirExist(self):
        """
        Method to check the existence of fire data in the source directory
        """
        listdir = os.listdir(self.datafiredir)
        filteredlist = fnmatch.filter(listdir,self.FireFileMatchingPattern)
        self.fire_list = filteredlist
        if not filteredlist:
            self.logger.error("ERROR")
            self.logger.error("NO CSIRO BUSHFIRE DATA FOUND")
            self.logger.error("You need to find fire filename matching pattern %s",self.FireFileMatchingPattern)
            self.logger.error("In the following dir %s", self.datafiredir  )
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def FireInDataFireDirExist_HRB(self):
        """
        Method to check the existence of fire data in the source directory
        """
        listdir = os.listdir(self.datafiredir_HRB)
        filteredlist = fnmatch.filter(listdir,self.FireFileMatchingPattern)
        self.fire_list_HRB = filteredlist
        if not filteredlist:
            self.logger.error("ERROR")
            self.logger.error("NO CSIRO HRB BUSHFIRE DATA FOUND")
            self.logger.error("You need to find fire filename matching pattern %s",self.FireFileMatchingPattern)
            self.logger.error("In the following dir %s", self.datafiredir_HRB  )
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def FireInDataFireDirExist_Wildfire(self):
        """
        Method to check the existence of fire data in the source directory
        """
        listdir = os.listdir(self.datafiredir_Wildfire)
        filteredlist = fnmatch.filter(listdir,self.FireFileMatchingPattern)
        self.fire_list_Wildfire = filteredlist
        if not filteredlist:
            self.logger.error("ERROR")
            self.logger.error("NO CSIRO WILDFIRE BUSHFIRE DATA FOUND")
            self.logger.error("You need to find fire filename matching pattern %s",self.FireFileMatchingPattern)
            self.logger.error("In the following dir %s", self.datafiredir_Wildfire  )
            sys.exit("Sorry :(")
        return          

############################################################################################################################
    def BuildFireEmissionFilelist(self):
        """
        Method to build the list of detected fires
        """
        listdir=os.listdir(self.smeg_dir)
        filteredlist=fnmatch.filter(listdir,self.InvividualFireEmissionFileMatchingPattern)
        self.logger.info('reading emission files %s matching a pattern= %s',filteredlist,self.InvividualFireEmissionFileMatchingPattern)
        self.logger.info('in the directory = %s', self.smeg_dir) 
        relative_path = os.path.relpath(self.smeg_dir, start = os.path.join(self.buildemissionpath, self.buildemissionsubdir))     
        self.FireEmissionFilelist=[os.path.join(relative_path, ffile) for ffile in filteredlist]
        return          

############################################################################################################################
    def BuildFireEmissionPerDayFilelist(self, date):
        """
        Method to build the list of detected fires
        """
        listdir=os.listdir(self.smeg_dir)
        filteredlist=fnmatch.filter(listdir,self.InvividualFireEmissionPerDayFileMatchingPattern.format(date=date.strftime('%Y%m%d')))
        self.logger.debug('reading emission files %s matching a pattern= %s',filteredlist,self.InvividualFireEmissionPerDayFileMatchingPattern)
        self.logger.debug('in the directory = %s', self.smeg_dir)      
        self.FireEmissionPerDayFilelist=[os.path.join('smeg_'+self.current_smeg,ffile) for ffile in filteredlist]
        return          

############################################################################################################################
    def BuildBurnProfile_tiled(self):
        """
        Method to build the burn profile of the scar, depends on the burn starting hour. 
        inset the burn function into the burn profile, then roll it to match the starting hour.
        """
        #inserting the burn function into the burn profile at 00
        self.burn_profile_24h[0:len(self.burn_function)] = self.burn_function
        # rolling the array to match the burn start hour
        self.burn_profile_24h = np.roll(self.burn_profile_24h, self.burn_start_hour)
        self.burn_profile = np.tile(self.burn_profile_24h, self.numberofdays_PSEM)
        self.burn_profile_24h_configured = True
        self.burn_profile_configured = True
        
        self.logger.warning("CSIRO Burn starting hour is {:d}h".format(self.burn_start_hour).ljust(self.justif,'.'))
        self.logger.info("CSIRO burn profile will burn {:5.1f} % of the scar".format(np.trapz(self.burn_profile_24h)*100.).ljust(self.justif,'.'))
        self.logger.info("Burn profile".ljust(self.justif-10,'.') + 'CONFIGURED')
        return          

############################################################################################################################
    def BuildBurnProfile_non_tiled(self, current_smeg):
        """
        Method to build the burn profile of the scar, depends on the burn starting hour. 
        inset the burn function into the burn profile, then roll it to match the starting hour.
        """
        self.burn_profile = np.zeros([24 * self.numberofdays_PSEM], dtype = 'd')
        #inserting the burn function into the burn profile at 00
        #print len(self.burn_function[current_smeg]), self.burn_profile, self.burn_function[current_smeg]
        self.burn_profile[0:len(self.burn_function[current_smeg])] = self.burn_function[current_smeg]
        # rolling the array to match the burn start hour
        self.burn_profile = np.roll(self.burn_profile, self.burn_start_hour)

        self.burn_profile_24h_configured = True
        self.burn_profile_configured = True
        
        self.logger.warning("CSIRO Burn starting hour is {:d}h".format(self.burn_start_hour).ljust(self.justif,'.'))
        self.logger.info("CSIRO burn profile will burn {:5.1f} % of the scar".format(np.trapz(self.burn_profile)*100.).ljust(self.justif,'.'))
        self.logger.info("Burn profile".ljust(self.justif-10,'.') + 'CONFIGURED')
        return          

############################################################################################################################
    def Build_template(self):
        """ 
        This method writes the config file from a template to build individual emission 
        """
        dirextension=self.buildemissionsubdir
        buildemissionpath=self.buildemissionpath
        Build_dirs(buildemissionpath, dirextension, self.logger, self.justif)
        smeg_dir = self.smeg_dir
        filename=os.path.join(smeg_dir,'build_smoke_emissions.run')
        File= open(filename,'w')

        template=JJU.LoadTemplate(self.templatepath,self.templatefile)

        emission=template.render({'BushfireCSIROConfig':self })
        File.write(emission)
        File.close()
        return 

############################################################################################################################
    def Build_bushfire(self):
        """ 
        This method writes the emission file for the rfs emission using the detected fire then merging them into one emission file
        """
        self.logger.info(''.ljust(self.justif,'-'))
        self.logger.info(' CSIRO Bushfire emissions '.center(self.justif,'|'))
        self.logger.info(''.ljust(self.justif,'-'))
        
        #self.logger.info("It has been detected %i fire(s) in the CSIRO bushfire module : %s",len(self.fire_list),self.fire_list) 
                 
        dirextension = self.buildemissionsubdir
        buildemissionpath = self.buildemissionpath
        Build_dirs(buildemissionpath, dirextension, self.logger, self.justif)
        
        ##Old Behaviour
        ##firedir
        #self.DataFireDirExist()
        ##fire in firedir
        #self.FireInDataFireDirExist()
        #self.logger.info("It has been detected %d fire(s) in the CSIRO bushfire module : %s",len(self.fire_list),self.fire_list) 

        #do we build a SMoke Emission Group per fire, or put them all in one? (max 8 smeg?)
        #if self.OneSmegPerFire:
        #    self.nsmeg=min(8,len(self.fire_list))
        #
        #else:
        #    self.nsmeg=1
        #self.logger.debug(" It has been detected %d fire(s) in the CSIRO bushfire module that are going to be divided into %d emission group(s)",len(self.fire_list),self.nsmeg) 

        #firedir
        #self.DataFireDirExist()
        
        self.nsmeg = 0
        IndexFire = []
        SmokeEmissionGroupFire = []
        #fire in firedir
        if self.Run_HRB:
            #firedir
            self.DataFireDirExist_HRB()
            #fire data
            self.FireInDataFireDirExist_HRB()
            self.nsmeg += 1 
            self.smeg_relative_dir_names.append(self.HRB_dir)
            IndexFire += range(len(self.fire_list_HRB))
            SmokeEmissionGroupFire +=  [self.nsmeg -1] * len(self.fire_list_HRB)
            self.fire_list += self.fire_list_HRB
            self.burn_function.append(self.burn_function_hrb)
            self.datafiredir_list.append(self.datafiredir_HRB)  

            self.logger.info("It has been detected %d HRB fire(s) in the CSIRO bushfire module : %s",len(self.fire_list_HRB),self.fire_list_HRB) 
        if self.Run_Bushfire:
            #firedir
            self.DataFireDirExist_Wildfire()
            #fire data
            self.FireInDataFireDirExist_Wildfire()
            self.nsmeg += 1 
            self.smeg_relative_dir_names.append(self.Wildfire_dir)
            IndexFire += range(len(self.fire_list_Wildfire))
            SmokeEmissionGroupFire +=  [self.nsmeg -1] * len(self.fire_list_Wildfire)
            self.fire_list += self.fire_list_Wildfire
            self.burn_function.append(self.burn_function_wildfire)
            self.datafiredir_list.append(self.datafiredir_Wildfire)  
            self.logger.info("It has been detected %d Wildfire fire(s) in the CSIRO bushfire module : %s",len(self.fire_list_Wildfire),self.fire_list_Wildfire) 

#        #SMoke Emission Group required
#        #self.nsmeg=np.floor(len(self.fire_list)-1)/(self.n_trac+1)
#        self.nsmeg=np.floor_divide(len(self.fire_list),self.n_trac)
#        self.logger.debug(" It has been detected %i fire(s) in the CSIRO bushfire module that are going to be divided into %i emission group(s)",len(self.fire_list),self.nsmeg) 
#        
        # now build the species mapping list
        self.t_map=-1*np.ones([self.n_trac+1],dtype=np.int32)
        self.t_map[-1]=self.PM25
        self.logger.debug("Species mapping list %s",self.t_map) 

#        IndexFire,SmokeEmissionGroupFire=np.divmod(range(len(self.fire_list)),self.nsmeg)
#        for current_smeg in range(self.nsmeg):
#            self.logger.info("processing fire from smeg n = %i ",current_smeg)
#            self.smeg_dir = os.path.join(buildemissionpath,dirextension,"smeg_"+str(current_smeg))
#            #deleting the directory to get rid of previous fires that may exist from previous runs 
#            self.DeleteDirAndItsContents(self.smeg_dir)
#            
#            self.smeg_dir_names.append(self.smeg_dir)
#            self.current_smeg = str(current_smeg)
#            Build_dirs(os.path.join(buildemissionpath,dirextension), "smeg_"+str(current_smeg), self.logger, self.justif)
#        
#            maskfire = SmokeEmissionGroupFire == current_smeg 
##            print maskfire,self.fire_list,self.fire_list[maskfire]
##            self.currentsmegfire =  self.fire_list[maskfire] 
#            self.currentsmegfire = [os.path.splitext(self.fire_list[i])[0] for i in xrange(len(self.fire_list)) if maskfire[i]]

        for current_smeg, current_smeg_name in enumerate(self.smeg_relative_dir_names):
            self.logger.info("processing fire from smeg {name}".format(name = current_smeg_name))
            self.smeg_dir = os.path.join(buildemissionpath, dirextension, current_smeg_name)
            #deleting the directory to get rid of previous fires that may exist from previous runs 
            self.DeleteDirAndItsContents(self.smeg_dir)
            
            self.smeg_dir_names.append(self.smeg_dir)
            self.current_smeg = str(current_smeg)
            Build_dirs(os.path.join(buildemissionpath,dirextension), current_smeg_name, self.logger, self.justif)
            
            #print SmokeEmissionGroupFire, current_smeg
            maskfire = np.array(SmokeEmissionGroupFire) == current_smeg 
            #print maskfire,self.fire_list,self.fire_list[maskfire]
#            self.currentsmegfire =  self.fire_list[maskfire] 
            self.currentsmegfire = [os.path.splitext(self.fire_list[i])[0] for i in xrange(len(self.fire_list)) if maskfire[i]]
            
            #self.BuildBurnProfile()
            self.BuildBurnProfile_non_tiled(current_smeg)
            if self.burn_profile_configured:
                self.datafiredir = self.datafiredir_list[current_smeg]
                self.prefix_fire_file = self.datafiredir+os.sep

                self.Build_template()
            else:    
                self.logger.error("ERROR")
                self.logger.error("NO CSIRO BURN PROFILE FOUND")
                sys.exit("Sorry :(")

#            logger.debug('-------------------------------')
#            logger.debug('Diff Rfs_CSIRO')
#            Built_file=os.path.join(self.smeg_dir,'build_smoke_emissions.run')
#            IL.PrintDiff(Built_file, testfile_rfs_CSIRO,logger)
            
            #RunDir=os.path.join(buildemissionpath,dirextension)
            RunDir=self.smeg_dir
            my_env = os.environ.copy()
            #commandline="source ~/.bashrc | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp | " + \
            #  os.path.join(self.binpath,self.bin) +" " + self.datapath + os.sep + " T"
            
            #commandline = "source /etc/profile.d/modules.sh | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp | " + \
            #           os.path.join(self.binpath, self.bin) + " " + self.datapath + os.sep + " T"
            
            run = "{bin} {data} T".format(bin = os.path.join(self.binpath, self.bin), 
                                          data = self.datapath + os.sep)
        
            Runline = "{init} {run}".format(init=self.bininitline, run = run)
            
            print Runline
            #sys.exit()
            PIPE = subprocess.PIPE
            
            ##print subprocess.Popen([Runline],shell=True, env=my_env, cwd=RunDir).wait()
            p1 = subprocess.Popen([Runline],shell=True, env=my_env, cwd=RunDir, stdout = PIPE, stdin = PIPE, stderr = PIPE)
    
            out, err = p1.communicate()
            output = out
            error = err
    
            self.logger.info(output)
            self.logger.error('Errors:')
            self.logger.error(error)

            

            ##################################3
            # Merging into one

            #psem common var
            templatepath_PSEM=self.templatepath
            templatefile_PSEM=self.templatefile_PSEM
            buildemissionpath_PSEM=os.path.join(buildemissionpath,dirextension)
            binpath_PSEM=self.binpath_PSEM
            bin_PSEM=self.bin_PSEM
            
            ## first merging all the fire(s) for one day
            #if  self.numberofdays_PSEM > 1:
            #    for daynumber in range(self.numberofdays_PSEM):
            #        OneDayDate =  self.startdate + dt.timedelta(days = daynumber)
            #        #Building individual fire emission per day file list
            #        self.BuildFireEmissionPerDayFilelist(OneDayDate)
            #        #merging into one
            #        self.logger.info("Merging all the emission fire file %s in the SMEG_%i for day %s ",
            #                         self.FireEmissionPerDayFilelist,current_smeg,OneDayDate.strftime('%Y%m%d UTC'))
            #        listfilename = self.FireEmissionPerDayFilelist
            #        fileoutput_oneday_PSEM = os.path.join("smeg_"+str(current_smeg), self.fileoutput_oneday_PSEM + 'smeg_' + str(current_smeg) + OneDayDate.strftime('_%Y%m%dUTC') + '.pse.bin')
            #        fileoutput_oneday_PSEM = os.path.join("smeg_"+str(current_smeg), self.fileoutput_oneday_PSEM + 'smeg_' + str(current_smeg) + OneDayDate.strftime('_%Y%m%dUTC') +'.pse')
            # 
            #        #deleting previous file that may exist
            #        self.DeleteFile(fileoutput_oneday_PSEM)
            #        
            #        #self.fileoutput=fileoutput_oneday_PSEM
            #        RunDir_PSEM = self.smeg_dir 
            #        numberofdays_PSEM = self.numberofdays_PSEM
            #        FileMatchingPattern = self.InvividualFireEmissionFileMatchingPattern
            #        PSEMergeMultiConfig = PSEMM.PSEmergeMultiConfigClass(templatepath_PSEM,templatefile_PSEM,buildemissionpath_PSEM,listfilename,
            #          fileoutput_oneday_PSEM,numberofdays_PSEM,binpath_PSEM,bin_PSEM,FileMatchingPattern)
            #        PSEMergeMultiConfig.Build_dirs(self.logger)
            #        PSEMergeMultiConfig.Build_template(self.logger)
            #        #PSEMergeMultiConfig.FileInListExist(logger)
            #        PSEMergeMultiConfig.Run_Merge(self.logger)

            ## then merging all the days 
            # now merging individual fires for ndays into one file for N days      
            #Building individual fire emission file list
            self.BuildFireEmissionFilelist()
            #merging into one
            self.logger.info("Merging all the emission fire file %s in the SMEG_%i ",self.FireEmissionFilelist,current_smeg)
            listfilename = self.FireEmissionFilelist

            #fileoutput_PSEM=self.fileoutput_PSEM+'_smeg_'+str(current_smeg)+'.pse.bin'
            #fileoutput_PSEM=self.fileoutput_PSEM+'_smeg_'+str(current_smeg)+'.pse'
            
            fileoutput_PSEM = self.fileoutput_PSEM_template.format(date = self.startdate.strftime('%Y%m%d'),
                                                                    smegnumber = current_smeg)

            #RunDir_PSEM = RunDir
            RunDir_PSEM = os.path.join(buildemissionpath,dirextension)

            #deleting previous file that may exist
            self.DeleteFile(fileoutput_PSEM)
            
            self.fileoutput = fileoutput_PSEM
            self.fileoutput_list.append(fileoutput_PSEM)
            numberofdays_PSEM = self.numberofdays_PSEM
            FileMatchingPattern = self.InvividualFireEmissionFileMatchingPattern
            PSEMergeMultiConfig = PSEMM.PSEmergeMultiConfigClass(self.logger, self.justif, templatepath_PSEM, templatefile_PSEM,
                                                               buildemissionpath_PSEM, listfilename, fileoutput_PSEM, numberofdays_PSEM,
                                                               binpath_PSEM, bin_PSEM, self.bininitline,
                                                               FileMatchingPattern)
            PSEMergeMultiConfig.Build_dirs()
            PSEMergeMultiConfig.Build_template()
            self.logger.info('PSEMergeMultiConfig template'.ljust(self.justif-2,'.')+"OK")
            #PSEMergeMultiConfig.FileInListExist()
            PSEMergeMultiConfig.Run_Merge()
            self.logger.info('PSE Multi Merge'.ljust(self.justif-8,'.')+"COMPLETE")
#        for index,fire in enumerate(self.fire_list):
#            current_smeg=SmokeEmissionGroupFire[index]
#            fire_number=IndexFire[index]
#            logger.info("processing fire from smeg n = %i index i = %i",current_smeg,fire_number)

        return 
    
############################################################################################################################
def Build_dirs(dir1, dir2, logger, justif):
    """
    Function to build a directory dir1/dir2
    
    Parameters
    ----------
    dir1 : str
        first directory name
    dir2 : str
        second directory name
    logger : logging.logger
        logger instance to output messages
    justif : int
        message width to justify the looger output.
        
    Returns
    -------
    None
        nothing! but creates the directory dir1/dir2        
    """
    path=os.path.join(dir1,dir2)
    try :
        os.makedirs(path)
        logger.info('Creation fire Dir= {msg}'.format(msg=path).ljust(justif-2,'.') + 'OK')
    except:
        logger.debug('Fire Dir = {msg}'.format(msg=path).ljust(justif-13,'.') + 'Already exist')
        pass
    return
###########################################################################################
############################################################################################################################
############################################################################################################################

if __name__ == '__main__':    
    import logging
    import datetime as dt
    import Sources.InitLogging as IL
    import Sources.DateMagics as DM
    loggername='build_RFS_CSIRO'
    logger=IL.Initialise_logging(loggername)
    justif = 102
    
    buildemissionpath='Emissions/'
    Basebinpath='/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran/'
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates/'
    #bininitline = "source /home/barthelemyx/modulefiles/init.bash | ulimit -s 300000 |"
    PartitionName = 'CAS'
    PartitionName = 'SLES15'
    bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | srun -p {partition} -N1 -x irscomp12 "
    #bininitline_template = "source ~/modulefiles/init.bash | ulimit -s 350000 | valgrind --leak-check=yes "
    bininitline = bininitline_template.format(partition = PartitionName)
    bininitline = "source ~/modulefiles/init.bash | ulimit -s 350000 | "

    templatefile_rfs_CSIRO = 'Build_bushfire_csiro.template'
    testfile_rfs_CSIRO     = './TestFiles/build_smoke_emissions.run'
    binpath_rfs_CSIRO      = os.path.join(Basebinpath, 'build_smoke_emissions')
    #bin_rfs_CSIRO          = 'build_smoke_emissions'
    bin_rfs_CSIRO          = 'build_smoke_emissions_opt_sles15_heap349_mcmedium'
    bin_rfs_CSIRO          = 'build_smoke_emissions_debug_sles15_heap349_mcmedium'
    bin_rfs_CSIRO          = 'build_smoke_emissions_opt_sles15_heap_mcmedium'
    buildemissionsubdir_rfs_CSIRO = os.path.join('pse','bushfireCSIRO')
    #buildemissionsubdir_rfs_CSIRO = os.path.join('/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-1-PSE-only','bushfireCSIRO_corrected_D1.0')
    
    #startdate=dt.datetime(2016,5,4)
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2018,1,24),"AEDT")
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2018,3,23),"AEDT")
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2019,11,19),"UTC")
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2020,2,4),"UTC")
    #startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2019,12,9),"UTC")
    startdate, dummy1, dummy2 = DM.DateMagics(dt.datetime(2020,4,27),"UTC")
    enddate = startdate + dt.timedelta(days=5,hours=00)
    
    emissiondate = dt.datetime(2008,1,1)
    emissions_rfs_DATA_CSIRO = '/mnt/climate/cas/project/EmissionSources-Forecast/bushfireCSIRO'
    burn_start_hour = 0
    
    Run_HRB = True
    #Run_HRB = False
    Run_Bushfire = True
    Run_Bushfire = False
    
    datafiredir_Wildfire = '/mnt/climate/cas/scratch/bushfire-emissions-20191119'
    datafiredir_HRB  = '/mnt/climate/cas/scratch/bushfire-emissions-20191119'
    datafiredir_Wildfire = '/mnt/climate/cas/scratch/bushfire-emissions-20191119-Only3'
    datafiredir_HRB  = '/mnt/climate/cas/scratch/bushfire-emissions-20191119-Only3'
    datafiredir_HRB  = '/mnt/climate/cas/scratch/airflow/emissions/stage/2020-04-29/CSEM_HRB_emission_model--2020-04-29T00-12-05'
    #datafiredir_HRB  = '/home/barthelemyx/Projects/AQ-Forecast/Firetest'
    
    datafiredir_Wildfire = '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191119-Only3'
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191208T22-02'
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-20191209T22-02'
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km/bushfire-emissions-1-PSE-only'
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/workflows/stage/emissions/2020-02-03/CSEM_bushfire_emission_model--2020-02-03T22-01-26' 
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/Busfire-Scar-tests/PSE_1km'
    datafiredir_Wildfire =  '/mnt/climate/cas/scratch/airflow/emissions/stage/2020-04-29/CSEM_HRB_emission_model--2020-04-29T00-12-05'
    binpath_PSEM = os.path.join(Basebinpath,'psemergem')
    bin_PSEM = 'psemergem'
    bin_PSEM = 'psemergem_opt_sles15_heap349_mcmedium'
    bin_PSEM = 'psemergem_opt_sles15_heap_mcmedium'
    templatefile_PSEM = 'PSEmergeMulti.template'
#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
    offset = 0
    
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2018,3,23,1,0,8),"UTC")
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2018,5,28,4,17,15),"UTC")
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,8,22,2,00),"UTC")
    TimestampUTC, TimestampAEST, TimestampAEDT = DM.DateMagics(dt.datetime(2019,12,8,22,2,00),"UTC")

    logger.debug('Building emission tests')
#--------------------------------------------------------------------------------------------------------------------------------------------------------
    BushfireCSIROConfig=BushfireCSIROConfigClass(logger, justif,
                                                 templatepath, templatefile_rfs_CSIRO, 
                                                 buildemissionpath, buildemissionsubdir_rfs_CSIRO, 
                                                 binpath_rfs_CSIRO, bin_rfs_CSIRO, bininitline,
                                                 startdate, enddate, emissiondate, TimestampUTC, burn_start_hour,
                                                 emissions_rfs_DATA_CSIRO, offset,
                                                 Run_HRB, Run_Bushfire,
                                                 binpath_PSEM, bin_PSEM, templatefile_PSEM,
                                                 datafiredir_Wildfire = datafiredir_Wildfire,
                                                 datafiredir_HRB = datafiredir_HRB,
                                                 binary = False)
    BushfireCSIROConfig.Build_bushfire()
    print BushfireCSIROConfig.fullnameoutputfile_list()
#    BushfireOEHConfig.Build_template(logger)
#    logger.debug('-------------------------------')
#    logger.debug('Diff Rfs_OEH')
#    Built_file=os.path.join(buildemissionpath,buildemissionsubdir_rfs_OEH,'run_rfs.run')
#    IL.PrintDiff(Built_file, testfile_rfs_OEH,logger)
#    
#    BushfireOEHConfig.Build_bushfire(logger)
#    logger.debug('BushFire full output name= %s',BushfireOEHConfig.fullnameoutputfile())

#--------------------------------------------------------------------------------------------------------------------------------------------------------

