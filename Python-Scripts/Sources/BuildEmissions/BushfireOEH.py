import os
import string
###########################################################################################
class BushfireOEHConfigClass(object):
    '''
    This class defines a emission object, that contains all the parameters to build OEH bushfire module 
    
    The class is self contained and can be initialised from original data, it doesn't depend on higher level classes.
    it contains all the methods to configure and generate emissions.
    
    Attributes
    -----------
        logger : logging.logger
            instance of a logger to output message
        justif : int
            max message width to justify logger output     
        startdate : datetime
            start date of the emissions to build as a UTC datetime.
        startyear : str
            year of the starting date of the emission windows.
        startmonth : str
            month of the starting date of the emission windows.
        startday : str
            day of the starting date of the emission windows.
        enddate : datetime
            end date of the emissions to build as a UTC datetime.
        endyear : str
            year of the ending date of the emission windows.
        endmonth : str
            month of the ending date of the emission windows.
        endday : str
            day of the ending date of the emission windows.
        datapath : str
            path to find the RFS fire data and parametres.
        emissiondate : datetime.datetime
            date of the emission 
        emissionmonth : str
            reduced month name from the emission date.
        emissionyear : str
            year of the emissions
        self.offset_veh : int
            hour offset of the emission to take in account timezones 
        binpath : str
            path to find the binary to build individual scar emissions.
        bin : str
            binary filename to build individual scar emissions.
        bininitline : str
            init line to setup env when running a binary
        userfile : str
            name of an user file = 'cogen_24h_emissions.csv'
        templatepath : str
            path to the template directory.
        templatefile : str
            name of the templatefile to build an emission file.
        buildemissionpath : str
            root of the main emission directory to build in. 
        buildemissionsubdir : str
            path from the main emission directory to build in.
    '''
    def __init__(self, logger, justif, 
                 templatepath, templatefile,
                 buildemissionpath, buildemissionsubdir,
                 binpath, bin, 
                 startdate, enddate, emissiondate,
                 datapath, offset):
        self.startdate=startdate
        self.startyear=startdate.strftime('%Y')
        self.startmonth=startdate.strftime('%m')
        self.startday=startdate.strftime('%d')
        self.endyear=enddate.strftime('%Y')
        self.endmonth=enddate.strftime('%m')
        self.endday=enddate.strftime('%d')
        self.datapath=datapath
        self.emissionmonth=string.lower(startdate.strftime('%b'))
        self.emissionyear=emissiondate.strftime('%y')
        self.offset=str(offset)
        self.emission_name_year=self.emissionmonth+self.emissionyear
        self.emission_name_day=self.emissionmonth+self.startday
        self.binpath=binpath
        self.bin=bin
        self.bininitline = bininitline
        self.userfile='cogen_24h_emissions.csv'
        self.templatepath=templatepath
        self.templatefile=templatefile
        self.buildemissionpath=buildemissionpath
        self.buildemissionsubdir=buildemissionsubdir
        self.BaseCasePemsFile='pems_'+self.startyear+self.startmonth+self.startday+'_bushfire.in'
        self.fileoutput='emsn_gmr_'+self.startmonth+self.startday+'_rfs.pse.bin'
        self.fullpathfileoutput=os.path.join(self.buildemissionpath,self.buildemissionsubdir,self.fileoutput)

###################################
    def fullnameoutputfile(self):
        import os
        name=os.path.join(self.buildemissionpath,self.buildemissionsubdir, self.fileoutput)
        return name

###################################
    def relativenameoutputfile(self):
        import os
        name=os.path.join(self.buildemissionsubdir,self.fileoutput)
        return name

###################################
    def DataFileExist(self,logger):
        import os
        import sys
        if not os.path.isfile(os.path.join(self.datapath,self.BaseCasePemsFile)):
            logger.error("ERROR")
            logger.error("NO OEH BUSHFIRE FILE(S) FOUND")
            logger.error("You need to find %s",self.BaseCasePemsFile )
            logger.error("In the following dir %s", self.datapath  )
            sys.exit("Sorry :(")
        return          
        
############################################################################################################################
    def Build_template(self,logger):
        """ This function write the config file to build emission for the power plants
        """
        import os
        import JinjaUtils as JJU
        dirextension=self.buildemissionsubdir
        buildemissionpath=self.buildemissionpath
        Build_dirs(buildemissionpath,dirextension,logger)

        filename=os.path.join(buildemissionpath,dirextension,'run_rfs.run')
        File= open(filename,'w')

        template=JJU.LoadTemplate(self.templatepath,self.templatefile)

        emission=template.render({'Generic':self })
        File.write(emission)
        File.close()
        return 

############################################################################################################################
    def Build_bushfire(self,logger):
        """ This function write the emission file for the rfs emission
        """
        import os
        import subprocess        
        dirextension=self.buildemissionsubdir
        buildemissionpath=self.buildemissionpath
        Build_dirs(buildemissionpath,dirextension,logger)
        self.Build_template(logger)

        RunDir=os.path.join(buildemissionpath,dirextension)
        my_env = os.environ.copy()

        #commandline="source ~/.bashrc | module use /mnt/appsource/local/CAS/USERMODULES | module load InitModule-temp | " + \
        # os.path.join(self.binpath,self.bin) +" run_rfs.run"

        run = "{bin} run_rfs.run".format(bin=os.path.join(self.binpath, self.bin))
        
        Runline = self.bininitline + run

        print subprocess.Popen([Runline],shell=True, env=my_env, cwd=RunDir).wait()
    
        return 
    
############################################################################################################################
def Build_dirs(dir1,dir2,logger):
    import os
    path=os.path.join(dir1,dir2)
    try :
        logger.info('Creating Working dir= %s',path)
        os.makedirs(path)
    except:
        logger.debug('Dir already exist')
        pass
    return
###########################################################################################
############################################################################################################################
############################################################################################################################

if __name__ == '__main__':    
    import os
    import logging
    import datetime as dt
    import InitLogging as IL
    loggername='build_RFS_OEH'
    logger=IL.Initialise_logging(loggername)
    justif = 100
    
    buildemissionpath='Emissions/'
    Basebinpath='/home/barthelemyx/Projects/AQ-Forecast/Emission-Building-Fortran/'
    templatepath='/home/barthelemyx/Projects/AQ-Forecast/Python-Scripts/Templates/'
    bininitline = "source /home/barthelemyx/modulefiles/init.bash | ulimit -s 300000 |"

    templatefile_rfs_OEH='Build_pems_generic.template'
    testfile_rfs_OEH='./TestFiles/run_rfs.run'
    binpath_rfs_OEH=os.path.join(Basebinpath,'build_elevated_emissions_bushfire')
    bin_rfs_OEH='build_elevated_emissions_bushfire_noglo'
    buildemissionsubdir_rfs_OEH=os.path.join('pse','bushfire')
    
    startdate=dt.datetime(2017,8,14)
    enddate=startdate+dt.timedelta(days=3,hours=00)
    emissiondate=dt.datetime(2008,1,1)
    emissiondate_veh=dt.datetime(2008,2,1)
    emissiondate_whe=dt.datetime(2008,7,1)
    emissions_rfs_DATA='/mnt/climate/cas/project/ccam-ctm/emission_modelling/bushfireDF'

#   offset is the difference betrwenn local time and UTC, (remember daylight savings)
    offset=-11
    offset_veh=-11
    offset_whe=-10

    logger.debug('Building emission tests')
#--------------------------------------------------------------------------------------------------------------------------------------------------------
    BushfireOEHConfig = BushfireOEHConfigClass(logger, justif,
                                               templatepath, templatefile_rfs_OEH,
                                               buildemissionpath, buildemissionsubdir_rfs_OEH,
                                               binpath_rfs_OEH, bin_rfs_OEH, bininitline,
                                               startdate, enddate, emissiondate_veh,
                                               emissions_rfs_DATA, offset)
    BushfireOEHConfig.Build_template(logger)
    logger.debug('-------------------------------')
    logger.debug('Diff Rfs_OEH')
    Built_file=os.path.join(buildemissionpath,buildemissionsubdir_rfs_OEH,'run_rfs.run')
    IL.PrintDiff(Built_file, testfile_rfs_OEH,logger)
    
    BushfireOEHConfig.Build_bushfire(logger)
    logger.debug('BushFire full output name= %s',BushfireOEHConfig.fullnameoutputfile())

#--------------------------------------------------------------------------------------------------------------------------------------------------------

