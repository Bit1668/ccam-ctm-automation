'''
This code generate some test files in netCDF
'''
import numpy as np
import netCDF4 as nc4
import datetime as dt
import matplotlib.pyplot as pl 

################################################################################
def GenerateDust(filename, longitudesDef, latitudesDef, timeDef, emissionDef, binDef):
    
    fp = nc4.Dataset(filename, "w", format='NETCDF4')
    #fp = nc4.Dataset(filename, "w", format='NETCDF3_64BIT_DATA')

    
    time = fp.createDimension("time", None)
    lat = fp.createDimension("lat", latitudesDef['n'])
    lon = fp.createDimension("lon", longitudesDef['n'])
    dust_bins = fp.createDimension("dust_bins", binDef['dust_bins'])
    bnds = fp.createDimension("bnds", binDef["bnds"])
    
    times = fp.createVariable("time","f8",("time",))
    latitudes = fp.createVariable("lat","f4",("lat",))
    longitudes = fp.createVariable("lon","f4",("lon",))
    dust_bins = fp.createVariable("dust_bins","f8",("dust_bins",))
    dust_bins_bnds = fp.createVariable("dust_bins_bnds","f8",('dust_bins', 'bnds'))
    # two dimensions, time unlimited
    Dust = fp.createVariable("vertical_dust_flux","f8",("time",'dust_bins', "lat","lon",),zlib=True, fill_value = 10.)
    
    fp.setncattr("emission_type", "area")
    fp.setncattr("generating_class", "core.outputs.area_source_emissions.AreaSourceEmissionsFile")
    
    latitudes.units = "degrees_north"
    latitudes.long_name = "latitude"
    latitudes.standard_name = "latitude"
    latitudes.axis = "Y"

    longitudes.units = "degrees_east"
    longitudes.long_name = "longitude"
    longitudes.standard_name = "longitude"
    longitudes.axis = "X"

    times.units = "minutes since {date}".format(date= dt.datetime.strftime(timeDef["stardate"], "%Y-%m-%d %H:%M:%S") )
    times.long_name = "time" 
    times.standard_name = "time"
    times.calendar = "standard" 
    times.axis = "T" 
    
    dust_bins.axis = "Z"
    dust_bins.bounds = "dust_bins_bnds"
    
    Dust.standard_name = "vertical_wind_blown_dust_flux"
    Dust.units = "g/m^2/s"
    
    latitudes[:] = np.linspace(latitudesDef['x0'], latitudesDef['xn'], latitudesDef['n'])
    longitudes[:] = np.linspace(longitudesDef['x0'], longitudesDef['xn'], longitudesDef['n'])
    
    dates = [timeDef["stardate"] + dt.timedelta(minutes = ii* timeDef["timestepinminutes"]) for ii in range(timeDef['nsteps'])]
    times[:] = nc4.date2num(dates, units=times.units, calendar=times.calendar) 
    
    #filling
    Dust[:, :, :, :] = 0.
    for key in emissionDef.keys():
        mask_lon = np.logical_and(longitudes[:] > emissionDef[key]['LL']['lon'], longitudes[:]  < emissionDef[key]['UR']['lon'])
        mask_lat = np.logical_and(latitudes[:]  > emissionDef[key]['LL']['lat'], latitudes[:]  < emissionDef[key]['UR']['lat'])
        #Dust[:, :, mask_lat, mask_lon] = emissionDef[key]['value']
        Dust[:, 0, mask_lat, mask_lon] = emissionDef[key]['value']
    for bin in range(binDef['dust_bins']):
        dust_bins_bnds[bin,:] = binDef['binning'][bin:bin+2]
        #print bin, dust_bins_bnds[:][bin,], binDef['binning'][bin:bin+2]
    dust_bins[:] = np.mean(dust_bins_bnds[:], axis =1)
    #print fp['vertical_dust_flux'][:]
    
    print 'final', dust_bins_bnds[:], dust_bins[:]
   
    pl.figure(0)
    pl.contourf(longitudes[:], latitudes[:], fp['vertical_dust_flux'][0,0,:,:])
    pl.figure(1)
    pl.contourf(longitudes[:], latitudes[:], fp['vertical_dust_flux'][2,0,:,:])
    pl.figure(2)
    pl.contourf(longitudes[:], latitudes[:], fp['vertical_dust_flux'][2,2,:,:])

    
   
    fp.sync()
    fp.close()
    return
################################################################################
################################################################################
def ModifyDust(filename, longitudesDef, latitudesDef, timeDef, emissionDef, binDef):
    
    fp = nc4.Dataset(filename, "a", format='NETCDF4')

    
    
    latitudes = fp['lat'][:]
    longitudes = fp['lon'][:]
    
    #zeroing file
    fp['vertical_dust_flux'][:][:, :, :, :] = 0.
    for key in emissionDef.keys():
        mask_lon = np.logical_and(longitudes[:] > emissionDef[key]['LL']['lon'], longitudes[:]  < emissionDef[key]['UR']['lon'])
        mask_lat = np.logical_and(latitudes[:]  > emissionDef[key]['LL']['lat'], latitudes[:]  < emissionDef[key]['UR']['lat'])
        fp['vertical_dust_flux'][:, :, mask_lat, mask_lon] = emissionDef[key]['value']
    pl.figure(0)
    pl.contourf(longitudes[:], latitudes[:], fp['vertical_dust_flux'][:][0,0,:,:])

    
   
    fp.sync()
    fp.close()
    return
################################################################################
if __name__ == '__main__':

    filename = 'TestDust2.nc4'
    #filename = 'TestDust.nc'
    longitudesDef = {"n" : 100, 'x0': 127.18, 'xn': 175.18}
    latitudesDef  = {"n" : 110, 'x0': -57.86, 'xn': -9.86}
    binDef = {'dust_bins' : 6, 'bnds' : 2, 'binning': [0.039, 2.5, 5., 10., 20., 40, 60.]}
    startdate = dt.datetime(2018,4,2)
    timeDef = {"stardate": startdate, 'nsteps': 120, "timestepinminutes": 60 }
 

    emissionDef = {
        'L1' : {'LL': {'lon':131. , 'lat':-25. },
                'UR': {'lon':135. , 'lat':-15. },
                'value': 1000.,              
                   } ,   
        'L2' : {'LL': {'lon':131. , 'lat':-30. },
                'UR': {'lon':140. , 'lat':-25. },
                'value': 1000.,              
                   } ,
        'I' : {'LL': {'lon':143. , 'lat':-30. },
               'UR': {'lon':147. , 'lat':-15. },
               'value': 1000.,              
                   } ,
        'S1' : {'LL': {'lon':150. , 'lat':-18. },
                'UR': {'lon':157. , 'lat':-15. },
                'value': 1000.,              
                   } ,
        'S2' : {'LL': {'lon':150. , 'lat':-21. },
                'UR': {'lon':152. , 'lat':-18. },
                'value': 1000.,              
                   } ,
        'S3' : {'LL': {'lon':150. , 'lat':-23. },
                'UR': {'lon':155. , 'lat':-21. },
                'value': 1000.,              
                   } ,
        'S4' : {'LL': {'lon':155. , 'lat':-27. },
                'UR': {'lon':157. , 'lat':-21. },
                'value': 1000.,              
                   } ,
        'S5' : {'LL': {'lon':150. , 'lat':-30. },
                'UR': {'lon':157. , 'lat':-27. },
                'value': 1000.,              
                   } ,
        'A1' : {'LL': {'lon':160. , 'lat':-30. },
                'UR': {'lon':163. , 'lat':-15. },
                'value': 1000.,              
                   } ,
        'A2' : {'LL': {'lon':160. , 'lat':-17. },
                'UR': {'lon':168. , 'lat':-15. },
                'value': 1000.,              
                   } ,
        'A3' : {'LL': {'lon':160. , 'lat':-23. },
                'UR': {'lon':168. , 'lat':-21. },
                'value': 1000.,              
                   } ,
        'A4' : {'LL': {'lon':165. , 'lat':-30. },
                'UR': {'lon':168. , 'lat':-15. },
                'value': 1000.,  
                }            
    }
    
    GenerateDust(filename, longitudesDef, latitudesDef, timeDef, emissionDef, binDef)
    
    filename = 'ACCESS-R-dust-forecast-ctm_bin_range_area_emissions-2018-11-20-23.nc'
    #ModifyDust(filename, longitudesDef, latitudesDef, timeDef, emissionDef, binDef)
    pl.show()   
    
