import sys
import os
import datetime as dt
sys.path.append('./Python-Scripts/Sources')
import PrepCTM as PCTM
import RunCTM as RCTM
import ResolutionProperties as RP
if __name__ == '__main__':
    
##############################################
# scenarios and options
##############################################
    MONTH='jan'
    CHEM='cb05_aer2'
    SCENARIO='forecast'
    SIOA='ISORROPIA_IIa'
    SOA='VBS2'
    TRANSPORT='WALCEK'
    DIFF='noNWPDIFFUSIVITY'
    BIO=MONTH
    YEAR=2016
    
    #PrepBin='/mnt/project/ccam-ctm/exec/ctm_prep_init_cb05_aer2'
    PrepBin='/mnt/project/CTM/software/August-16/BIN/ctm_prep_init_cb05_aer2'
    
    CcamOutputBaseDir='/mnt/project/ccam/ccam_output/forecast'
    CTMBin='ccam-ctm_cb05_aer2_filename'
    CTMBinPath='/mnt/project/CTM/software/August-16/BIN'
    
    RunTimeHours=8
    RuntimeCPU=40
    
    gen_meteo_config_filename='gen_meteo_config.txt'

##############################################
#start date; end date
# used by CTM
##############################################
    YEAR_S=2017
    MONTH_S=01
    DAY_S=20
    DAY_S_CTM=20
    Startdate=dt.datetime(YEAR_S,MONTH_S,DAY_S,00)

    YEAR_EMISSIONS=YEAR_S
    MONTH_EMISSIONS=MONTH

##############################################
# simulation hours
##############################################
    DAY_N=3
    HOUR_N = DAY_N * 24

##########################################################################################################################################

##############################################
#  end date
# used by CTM
##############################################
    EndDate=Startdate+dt.timedelta(days=DAY_N,hours=00)
    YEAR_E=EndDate.year
    MONTH_E=EndDate.month
    DAY_E=EndDate.day

    ResolutionNames=['nsw','gmr','gsyd']


#########################################################
#  Clean
#######################################################
    PCTM.UnlinkEmissionFiles(CHEM,MONTH)
    ResolutionCTM=RP.ResolutionProperties()
    GridResolutions=[ResolutionCTM[name][1] for name in ResolutionNames]
    print GridResolutions
    for res in GridResolutions:
        RCTM.CleaningSymLinkingOriginalCCAMFilesPreRun(YEAR,res,DAY_N)
    RCTM.UnlinkingWorkingDir(GridResolutions)

 
