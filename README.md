# CSIRO Chemical Transport Model Automation #

This CTM automation contains everyting necessary to setup and run the CSIRO CTM code.
The project is an open source project lead by the [NSW Office of Environment](http://www.environment.nsw.gov.au/).

There are different stage in the execution of the automation:
* Main configuration
The system can be run as full automated, of manually for specific cases, depending on the way to invoke the script. Rerunning forecast for specific date is an example of a automated manual script, where the script just specify and iterate over the dates.
the script is able to get prescribed manual emissions, or grabbing them from the AIRFLOW scheduler.
* Emissions
There is a whole module called BuildEmission, which task is to ... build the emissions in a compatible format with the CTM. It combines the EDMS emissions with offline modelled emission modules, like bushfire or dust.

* Preparing the run.
The script then prepare the sources files to run the Prep module. it sets up the Met , soil, Leaf Area Index files, grid configurations, and so on. Then it runs the prep script.

* Run
then the script position the runtime variable, and submit it to the IRS scheduler, SLURM  


### Where from here? ###

* Installation and deployment
* User manual
* Development
* Help and support
* API documentation

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
