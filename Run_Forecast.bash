#!/usr/bin/env bash
pp=/usr/bin/python2.7
dir=/home/barthelemyx/Projects/AQ-Forecast
source /home/barthelemyx/modulefiles/init.bash
module load Python2-User-Module
#module load Airflow-dev
cd $dir
echo "changed to "$dir
ulimit -s 350000
#$pp Python-Scripts/Ccam-CTM-forecast.py
$pp Ensemble.py
