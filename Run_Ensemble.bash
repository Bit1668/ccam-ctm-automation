!#/bin/bash
pp=/mnt/appsource/python/python-2.7.13/bin/python
dir=/mnt/climate/cas/scratch/AQ-Forecast/ccam-ctm-automation-Airflow
source /etc/profile.d/modules.sh
module use /mnt/appsource/local/CAS/USERMODULES
module load InitModule-temp
module load Airflow-dev
cd $dir
echo "changed to "$dir
$pp Ensemble.py
