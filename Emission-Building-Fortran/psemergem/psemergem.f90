!-----------------------------------------------------------------------------|
!> Merge multiple .pse files into a single .pse file
!> @author
!> Martin Cope, CSIRO
!> @brief
!> Program to merge and arbitrary number of CTM .pse files
!> Output emissions in CTM point source format
!-----------------------------------------------------------------------------|
! Program to merge two pse files
! Modifications
! Version     When     Who    What
!   1.0    17/05/2017  mec  First documented version
! mc csiro 31/1/2012
! mc csiro 18/7/2015 Changed .run to be consistent with program name
! mc csiro 10/8/2016 Added in arbitrary masking files
! mc csiro 08/1/2017 Arbitary number of input files
! XB  OEH  28/05/2019 correction of bugs due to more anal compiler. define qstemp as temp array to pass the argument
!-----------------------------------------------------------------------------|
PROGRAM psemergem
USE file_management
USE DummySource
USE masking
USE TAPMemissions
IMPLICIT NONE
INTEGER, PARAMETER :: filelen=2048
CHARACTER(LEN=filelen), ALLOCATABLE, DIMENSION(:) :: inFile
CHARACTER(LEN=filelen) :: outFile
CHARACTER(LEN=80) :: rec
INTEGER, PARAMETER :: inUnit=20
INTEGER, PARAMETER :: outUnit=10
LOGICAL, ALLOCATABLE, DIMENSION(:) :: binaryFormat
LOGICAL                            :: outFormat
!
INTEGER            :: f,n,s,t,h,i,ios,p,p1
INTEGER            :: n_files!number of input files
INTEGER, PARAMETER :: nt=24  !data are in 24h blocks
INTEGER            :: ndays  !number of days to process
INTEGER :: nsp1,nsp          !number of species
INTEGER, ALLOCATABLE, DIMENSION(:) :: ns1    !number of sources in each file
INTEGER, ALLOCATABLE, DIMENSION(:) :: ptr    !pointer to each pse data set
INTEGER ::  ns               !total number of sources
CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: SpeciesNames1
CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: SpeciesNames
REAL, ALLOCATABLE, DIMENSION(:) :: SpeciesMW1
REAL, ALLOCATABLE, DIMENSION(:) :: SpeciesMW
!
INTEGER, ALLOCATABLE, DIMENSION(:) :: ids !source id
REAL, ALLOCATABLE, DIMENSION(:) :: xs    !stack coordinates (m)
REAL, ALLOCATABLE, DIMENSION(:) :: ys    !stack coordinates (m)
REAL, ALLOCATABLE, DIMENSION(:) :: ds    !stack diameter (m)
REAL, ALLOCATABLE, DIMENSION(:) :: hs    !stack height (m)
REAL, ALLOCATABLE, DIMENSION(:,:) :: vs  !exit velocity (m/s)
REAL, ALLOCATABLE, DIMENSION(:,:) :: ts  !temperature (K)
REAL, ALLOCATABLE, DIMENSION(:,:,:) :: qs, qstemp !emission rates (g/s)
!
LOGICAL :: do_Masking
CHARACTER(LEN=filelen) :: m_f         !masking file name
INTEGER                :: m_p         !number of mask points
REAL                   :: m_r         !mask radius of influence
REAL, ALLOCATABLE, DIMENSION(:) :: m_x,m_y !mask centroids
INTEGER                            :: nsf   !number of non-filtered sources
INTEGER, ALLOCATABLE, DIMENSION(:) :: m_l   !non-masked points
1 FORMAT(a)

!-----------------------------------------------------------------------------|
! Open the two .pse files
! Open the combined .pse file
!-----------------------------------------------------------------------------|
OPEN(UNIT=7,FILE='psemergem.run',STATUS='OLD')
READ(7,1)rec
!
READ(7,1)rec
READ(7,1)outFile
outFormat=retrieveCITemsFormat(TRIM(outFile))
IF(outFormat)THEN
  OPEN(UNIT=outUnit,FILE=TRIM(outFile),STATUS='UNKNOWN',FORM='UNFORMATTED')
ELSE
  OPEN(UNIT=outUnit,FILE=TRIM(outFile),STATUS='UNKNOWN',FORM='FORMATTED')
END IF
READ(7,1)rec
READ(7,*)ndays

!-----------------------------------------------------------------------------|
! Count up number of input pse files
! Allocate primary arrays
!-----------------------------------------------------------------------------|
REWIND 7
IF(.NOT.foundKeyWord(7,'startfilelist'))STOP 'Fatal error'
n_files=countInputFileList(7,'endfilelist')
IF(n_files==0)STOP 'Fatal error'
!
ALLOCATE(infile(n_files))
ALLOCATE(binaryFormat(n_files))
!
REWIND 7
IF(.NOT.foundKeyWord(7,'startfilelist'))STOP 'Fatal error'
DO f=1,n_files
  READ(7,1,IOSTAT=ios)inFile(f)
  IF(ios /= 0)THEN
    PRINT *,'Error reading pse file name: ',TRIM(inFile(f))
    STOP 'Fatal error'
  END IF
  binaryFormat(f)=retrieveCITemsFormat(TRIM(inFile(f)))
  IF(binaryFormat(f))THEN
    OPEN(UNIT=inUnit+f,FILE=TRIM(inFile(f)),STATUS='OLD',FORM='UNFORMATTED')
  ELSE
    OPEN(UNIT=inUnit+f,FILE=TRIM(inFile(f)),STATUS='OLD',FORM='FORMATTED')
  END IF
END DO

!-----------------------------------------------------------------------------|
! Arbitrarily read in a masking filename
!-----------------------------------------------------------------------------|
m_p=0
do_Masking=.FALSE.
REWIND 7
IF(foundKeyWord(7,'maskingfile'))THEN
  IF(ios == 0)THEN
    READ(7,1,IOSTAT=ios)m_f
    IF(ios == 0)do_Masking=getMaskingMetaData(m_f,m_p,m_r)
  END IF
END IF
CLOSE(7)

!-----------------------------------------------------------------------------|
! Process the masking file
!-----------------------------------------------------------------------------|
IF(do_Masking)THEN
  ALLOCATE(m_x(m_p),m_y(m_p))
  do_Masking=getMaskingData(m_f,m_x,m_y)
  PRINT *,'Processing the following masking file'
  PRINT *,TRIM(m_f)
END IF

!-----------------------------------------------------------------------------|
! Retrieve number of species from each data file and cross check
!-----------------------------------------------------------------------------|
nsp=retrieveTAPMspeciesNumbers(inUnit+1,binaryFormat(1))
DO f=2,n_files
  nsp1=retrieveTAPMspeciesNumbers(inUnit+f,binaryFormat(f))
  IF(nsp /= nsp1)THEN
    PRINT *,'Miss-match in the number of species: ',nsp,nsp1
    PRINT *,'pse file: ',TRIM(inFile(f))
    STOP 'Fatal Error'
  END IF
END DO !pse source file loop

ALLOCATE(SpeciesNames(nsp))
ALLOCATE(SpeciesMW(nsp))
IF(.NOT.retrieveTAPMspeciesNames(inUnit+1,binaryFormat(1),      &
                                 nsp,SpeciesNames,SpeciesMW))THEN
  PRINT *,'Error reading species names from: ',TRIM(inFile(1))
  STOP 'Fatal Error'
END IF
!
ALLOCATE(SpeciesNames1(nsp))
ALLOCATE(SpeciesMW1(nsp))
DO f=2,n_files
  IF(.NOT.retrieveTAPMspeciesNames(inUnit+f,binaryFormat(f),      &
                                  nsp,SpeciesNames1,SpeciesMW1))THEN
    PRINT *,'Error reading species names from: ',TRIM(inFile(f))
    STOP 'Fatal Error'
  END IF
  DO s=1,nsp1
    IF(SpeciesNames1(s) /= SpeciesNames(s))THEN
      PRINT *,'Miss-match of species names: ',SpeciesNames(s),' ',SpeciesNames1(s)
      PRINT *,'pse file: ',TRIM(inFile(f))
      STOP 'Fatal Error'
    END IF
  END DO !species loop
END DO ! pse file loop

!-----------------------------------------------------------------------------|
! Retrieve number of sources from each data file. Work out total
! number of sources and allocate space
!-----------------------------------------------------------------------------|
ns=0
ALLOCATE(ns1(n_files))
ALLOCATE(ptr(n_files))
ptr(1)=1
DO f=1,n_files
  ns1(f)=retrieveTAPMpseSourceNumbers(inUnit+f,binaryFormat(f))
  ns=ns+ns1(f)
  IF(f < n_files)ptr(f+1)=ptr(f)+ns1(f)
END DO
!
ALLOCATE(ids(ns))
ALLOCATE(xs(ns))
ALLOCATE(ys(ns))
ALLOCATE(ds(ns))
ALLOCATE(hs(ns))

!-----------------------------------------------------------------------------|
! Retrieve time invariant data
!-----------------------------------------------------------------------------|
DO f=1,n_files
  p=ptr(f)
  IF(.NOT.retrieveTAPMpseTimeInvariant(inUnit+f,binaryFormat(f),   &
                                      ns1(f),ids(p),xs(p),ys(p),ds(p),hs(p)))THEN
    PRINT *,'Error reading species names from: ',TRIM(inFile(f))
    STOP 'Fatal Error'
  END IF
END DO ! pse file loop

!-----------------------------------------------------------------------------|
! Optionally set up logical masks for the first pse file
!-----------------------------------------------------------------------------|
ALLOCATE(m_l(ns1(1)))
IF(do_Masking)THEN
  n=ns1(1)
  CALL buildMask(xs(1:n),ys(1:n),m_x,m_y,m_r,nsf,m_l)
END IF

!-----------------------------------------------------------------------------|
! Write out the combined data set
!-----------------------------------------------------------------------------|
IF(.NOT.writeTAPM_header(outUnit,outFormat,nsp,SpeciesNames,SpeciesMW))THEN
  PRINT *,'Error writing header to: ',TRIM(outFile)
  STOP 'Fatal Error'
END IF
IF(.NOT.writeTAPMpseTimeInvariant(outUnit,outFormat,   &
                                        ns,ids,xs,ys,ds,hs))THEN
    PRINT *,'Error writing time invariant data to: ',TRIM(outFile)
    STOP 'Fatal Error'
END IF

!-----------------------------------------------------------------------------|
! Loop over time varying data and retrieve. 
! For file lengths < ndays, rewind and re-read
! Optionally mask out data from the first point source data set
! Write out the combined data set
!-----------------------------------------------------------------------------|
ALLOCATE(vs(ns,nt))
ALLOCATE(ts(ns,nt))
ALLOCATE(qs(ns,nsp,nt))
print *,"TOTO",ns,nsp,nt, shape(qs)
DO t=1,ndays
  PRINT *,'Processing day: ',t
  DO f=1,n_files
    p=ptr(f)
    p1=p+ns1(f)-1
    print *,'TOTO1',p,p1,ns1(f)
    PRINT *,'Reading data from: ',TRIM(inFile(f))
    print *, "shapeT1", shape(qs(p:p1,:,:))
    !allocate(qstemp(ns1(f),nsp,nt))
    !qstemp = qs(p:p1,:,:)
    IF(.NOT. retrieveTAPMpseTimeVarying(inUnit+f,binaryFormat(f),    &
                                      ns1(f),nsp,nt,SpeciesNames,    &
                                      vs(p:p1,1:nt),                 &
                                      ts(p:p1,1:nt),                 &
                                      qs(p:p1,:,:) )  )              &
                                      THEN
      PRINT *,'Error reading from the input file: ',TRIM(inFile(f))
      STOP 'Fatal Error'
    END IF
    !deallocate(qstemp)
  END DO !source file loop
!
  IF(do_Masking)CALL putMask(qs(1:ns1(1),1:nsp,1:nt),m_l(1:nsf))
!
  IF(.NOT.writeTAPMpseTimeVarying(outUnit,outFormat,   &
                                  ns,nsp,nt,SpeciesNames,vs,ts,qs))THEN
    PRINT *,'Error writing to the file: ',TRIM(outFile)
    STOP 'Fatal Error'
  END IF
END DO  !day loop

!-----------------------------------------------------------------------------|
! Close up the files and finish
!-----------------------------------------------------------------------------|
DO f=1,n_files
  CLOSE(inUnit+f)
END DO
CLOSE(outUnit)
DEALLOCATE(infile)
DEALLOCATE(binaryFormat)

DEALLOCATE(SpeciesNames)
DEALLOCATE(SpeciesMW)
DEALLOCATE(SpeciesNames1)
DEALLOCATE(SpeciesMW1)
DEALLOCATE(ns1)
DEALLOCATE(ptr)
DEALLOCATE(ids)
DEALLOCATE(xs)
DEALLOCATE(ys)
DEALLOCATE(ds)
DEALLOCATE(hs)
DEALLOCATE(vs)
DEALLOCATE(ts)
DEALLOCATE(qs)
IF(do_Masking)DEALLOCATE(m_x,m_y)

PRINT *,'Successful completion'
END PROGRAM psemergem
