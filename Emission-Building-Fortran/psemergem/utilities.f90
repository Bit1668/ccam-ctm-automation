!-------------------------------------------------------------------------------
!> i/o, date conversion and line compression routines
!> @author
!> Martin Cope CSIRO
!-------------------------------------------------------------------------------
MODULE file_management
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC foundKeyWord,countInputfileList,UpperCase
CONTAINS

!-------------------------------------------------------------------------------
!  UpperCase
!-------------------------------------------------------------------------------
!> Function to convert characters from lower to upper case
!> @author
!> Martin Cope CSIRO
!
!! \param string text string with embedded blanks
!! \result UpperCase text string without the blanks
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
FUNCTION UpperCase(string)
!-----------------------------------------------------------------------------|
! Function to force text to upper case
! MC CSIRO 6/03
!-----------------------------------------------------------------------------|
 IMPLICIT NONE
 CHARACTER(LEN=*), INTENT(IN) :: string
 CHARACTER(LEN=LEN_TRIM(string)) :: UpperCase
 INTEGER :: length,uppc,i
!
 length=LEN_TRIM(string)
 UpperCase=string
 uppc=ICHAR('a')-ICHAR('A')
 IF(length > 0)THEN
   DO i=1,length
     IF(UpperCase(i:i) >='a' .AND. UpperCase(i:i)<='z') &
        UpperCase(i:i)=CHAR(ICHAR(UpperCase(i:i))-uppc)
   END DO
 END IF
END FUNCTION UpperCase

!-------------------------------------------------------------------------------
!  foundKeyWord
!-------------------------------------------------------------------------------
!> Function to identify a control word in a .run file
!> @author
!> Martin Cope CSIRO
!
!! \param funit file unit
!! \param searchword is the keyword string
!! \result foundKeyWord returns true if found
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
FUNCTION foundKeyWord(funit,searchword)
!-----------------------------------------------------------------------------|
!  Function to search for a keyword in an input file
!-----------------------------------------------------------------------------|
LOGICAL :: foundKeyword
INTEGER, INTENT(IN)              :: funit      !file unit number
CHARACTER(LEN=*), INTENT(IN)     :: searchword !word to fine
CHARACTER(LEN=LEN_TRIM(searchword)) :: upc_searchword !upper case verions
CHARACTER(LEN=80)                :: keyword        !returned word
INTEGER                          :: ios            !i/o read flag       
!
foundKeyword=.FALSE.
upc_searchword=UpperCase(TRIM(ADJUSTL(searchword)))
DO
  READ(funit,1,IOSTAT=ios)keyword
  PRINT *,UpperCase(TRIM(ADJUSTL(keyword)))
  IF(ios /= 0)THEN
    PRINT *,'Cannot find the input file name list keyword: ',searchword 
    RETURN 
  END IF
!
  foundKeyword=(upc_searchword == UpperCase(TRIM(ADJUSTL(keyword))))
  IF(foundKeyword)RETURN
END DO
1 FORMAT(a)
END FUNCTION foundKeyWord

!-------------------------------------------------------------------------------
!  countInputFileList
!-------------------------------------------------------------------------------
!> Function to count number of files in a list
!> @author
!> Martin Cope CSIRO
!
!! \param funit file unit
!! \param searchword is the keyword string
!! \result n_files number of input files
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
!-------------------------------------------------------------------------------
FUNCTION countInputFileList(funit,searchword) RESULT(n_files)
INTEGER :: n_files
INTEGER, INTENT(IN) ::  funit                      !file unit number
CHARACTER(LEN=*), INTENT(IN)     :: searchword     !word to find
CHARACTER(LEN=LEN_TRIM(searchword)) :: upc_searchword !upper case version
CHARACTER(LEN=80)                :: keyword        !returned word
INTEGER                          :: ios   
!
n_files=0
upc_searchword=UpperCase(TRIM(ADJUSTL(searchword)))
!
DO
  READ(funit,1,IOSTAT=ios)keyword
  IF(ios /= 0)THEN
    PRINT *,'Error cannot find the input file name list keyword: ',searchword 
    RETURN
  END IF
!
  IF(upc_searchword == UpperCase(TRIM(ADJUSTL(keyword))))RETURN
  n_files=n_files+1
END DO
1 FORMAT(a)
END FUNCTION countInputFileList

END MODULE file_management

!-------------------------------------------------------------------------------
!> Routines to mask out put source emissions located in a user defined area
!> @author
!> Martin Cope CSIRO
!-------------------------------------------------------------------------------
MODULE masking
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC getMaskingMetaData,getMaskingData,buildMask,putMask
!
INTEGER, PARAMETER :: m_u=11
INTEGER :: ios,m,s,ns,m_p,i
CHARACTER(LEN=80) :: rec

CONTAINS

!-------------------------------------------------------------------------------
!  getMaskingMetaData
!-------------------------------------------------------------------------------
!> Function to retrieve masking metadata from a file
!> @author
!> Martin Cope CSIRO
!
!! \param m_name masking file name
!! \param m_p number of masking points
!! \param m_r radius of each masking point
!! \result getMaskingMetaData true if successful
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
!-------------------------------------------------------------------------------
  FUNCTION getMaskingMetaData(m_name,m_p,m_r)
  LOGICAL :: getMaskingMetaData
  CHARACTER(LEN=*), INTENT(IN) :: m_name
  INTEGER, INTENT(OUT) :: m_p
  REAL, INTENT(OUT) :: m_r
!
1 FORMAT(a)
  getMaskingMetaData=.FALSE.
  OPEN(m_u,FILE=TRIM(m_name),STATUS='OLD',FORM='FORMATTED',IOSTAT=ios)
  IF(ios /= 0)RETURN
  DO i=1,2
    READ(m_u,1,IOSTAT=ios)rec
    IF(ios /=0)RETURN
  END DO
  READ(m_u,*,IOSTAT=ios)m_p,m_r
  IF(ios /= 0)RETURN
  CLOSE(m_u)
  getMaskingMetaData=.TRUE.
  END FUNCTION getMaskingMetaData

!-------------------------------------------------------------------------------
!  getMaskingData
!-------------------------------------------------------------------------------
!> Function to retrieve masking data from a file
!> @author
!> Martin Cope CSIRO
!
!! \param m_name masking file name
!! \param m_x centre of masking points
!! \param m_y centre of each masking points
!! \result getMaskingData true if successful
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
!-------------------------------------------------------------------------------
  FUNCTION getMaskingData(m_name,m_x,m_y)
  LOGICAL :: getMaskingData
  CHARACTER(LEN=*), INTENT(IN) :: m_name
  INTEGER :: m_p
  REAL, DIMENSION(:), INTENT(OUT) :: m_x
  REAL, DIMENSION(:), INTENT(OUT) :: m_y
!
1 FORMAT(a)
  getMaskingData=.FALSE.
  OPEN(m_u,FILE=TRIM(m_name),STATUS='OLD',FORM='FORMATTED',IOSTAT=ios)
  IF(ios /= 0)RETURN
  DO i=1,4
    READ(m_u,1,IOSTAT=ios)rec
    IF(ios /=0)RETURN
  END DO
  m_p=SIZE(m_x)
  READ(m_u,*,IOSTAT=ios)(m_x(m),m_y(m),m=1,m_p)
  IF(ios /=0)RETURN
  CLOSE(m_u)
  getMaskingData=.TRUE.
  END FUNCTION getMaskingData

!-------------------------------------------------------------------------------
!  buildMask
!-------------------------------------------------------------------------------
!> Routine to generate indice of masked sources
!> @author
!> Martin Cope CSIRO
!
!! \param xs source locations
!! \param ys source locations
!! \param m_x centre of masking points
!! \param m_y centre of each masking points
!! \param nsf number of masked sources
!! \param m_l indices of masked sources
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
!-------------------------------------------------------------------------------! 
  SUBROUTINE buildMask(xs,ys,m_x,m_y,r,nsf,m_l)
  IMPLICIT NONE
  REAL, INTENT(IN)               :: r       !search radius
  REAL, INTENT(IN), DIMENSION(:) :: xs,ys   !source locations
  REAL, INTENT(IN), DIMENSION(:) :: m_x,m_y !mask centres
  INTEGER, INTENT(OUT)               :: nsf !number of masked sources
  INTEGER, INTENT(OUT), DIMENSION(:) :: m_l !indices of masked sources
  INTEGER :: m,s
!
  ns=SIZE(xs)
  m_p=SIZE(m_x)
  nsf=0
  m_l=0
!
  DO s=1,ns
    DO m=1,m_p
      IF( SQRT((xs(s)-m_x(m))*(xs(s)-m_x(m))+     &
               (ys(s)-m_y(m))*(ys(s)-m_y(m))) < r )THEN
        nsf=nsf+1
        m_l(nsf)=s
        EXIT
      END IF
    END DO !mask loop
  END DO  !source loop
  END SUBROUTINE buildMask

!-------------------------------------------------------------------------------
!  putMask
!-------------------------------------------------------------------------------
!> Routine to set masked source emissions to zero
!> @author
!> Martin Cope CSIRO
!
!! \param qs all sources
!! \param m_l indices of masked sources
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
!-------------------------------------------------------------------------------! 
  SUBROUTINE putMask(qs,m_l)
  IMPLICIT NONE
  REAL, INTENT(INOUT), DIMENSION(:,:,:) :: qs!source emission rates
  INTEGER, INTENT(IN), DIMENSION(:) :: m_l   !pointer to masked sources
  INTEGER :: nsf    !number of masked sources
  nsf=SIZE(m_l)
  DO s=1,nsf
    qs(m_l(s),:,:)=0.0
  END DO
  END SUBROUTINE putMask
END MODULE masking

!-----------------------------------------------------------------------------|
!> Parameters for a dummy .pse file.
!> This is output if the software fails due to errors or lack of input data
!> @author
!> Martin Cope CSIRO
!-----------------------------------------------------------------------------|
MODULE DummySource
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC dmy_hrs,dmy_ncmts,dmy_comment,     &
       dmy_ns,dmy_x,dmy_y,dmy_hs,dmy_vs,dmy_rs,dmy_ts, &
       writeZeroTimeInvariantFile,writeZeroSourceFile
!
! Parameters for a dummy .pse file
! This is output if the software fails
! due to errors or lack of input data
!
INTEGER, PARAMETER :: dmy_hrs=24
INTEGER, PARAMETER :: dmy_ncmts=3
CHARACTER(LEN=80), DIMENSION(dmy_ncmts) :: dmy_comment =(/    &
                            'Dummy pse file with zero emissions',   &
                            'Written by build_hourlyFire_emissions',&
                            '*'   /)
INTEGER, PARAMETER :: dmy_ns=1
REAL, PARAMETER :: dmy_x=0.0
REAL, PARAMETER :: dmy_y=0.0
REAL, PARAMETER :: dmy_hs=0.0
REAL, PARAMETER :: dmy_vs=1.0
REAL, PARAMETER :: dmy_rs=1.0
REAL, PARAMETER :: dmy_ts=298.0
!
CONTAINS

!-------------------------------------------------------------------------------
!  writeZeroTimeInvariantFile
!-------------------------------------------------------------------------------
!> Write out time invariant data for a dummy .pse file
!> @author
!> Martin Cope CSIRO
!
!! \param fun file unit
!! \param binaryFormat ascii or binary?
!! \result writeZeroTimeInvariantFile true if a successful write
!
! Modifications
!  When         Who      What
!-------------------------------------------------------------------------------
LOGICAL FUNCTION writeZeroTimeInvariantFile(fun,binaryFormat)
IMPLICIT NONE
INTEGER, INTENT(IN) :: fun
LOGICAL, INTENT(IN) :: binaryFormat
INTEGER :: s
!
writeZeroTimeInvariantFile=.FALSE.
IF(binaryFormat)THEN
!-------------------------------------------------------------------------------
! Write out source time invariant data
!-------------------------------------------------------------------------------
  WRITE(fun,ERR=94)dmy_ns,dmy_hrs
  DO s=1,dmy_ns
    WRITE(fun,ERR=94)s,dmy_x,dmy_y,dmy_hs,dmy_rs,1.,0.,0.
  END DO
ELSE
  WRITE(fun,3,ERR=94)dmy_ns,dmy_hrs
  DO s=1,dmy_ns
    WRITE(fun,4,ERR=94)s,dmy_x,dmy_y,dmy_hs,dmy_rs,0.,0.,0.
  END DO
END IF
writeZeroTimeInvariantFile=.TRUE.
RETURN
3 FORMAT(2(i3,1x))
4 FORMAT(i7,7(1x,f11.2))

!-------------------------------------------------------------------------------
! Errors
!-------------------------------------------------------------------------------
94 PRINT *,'Error writing PSE header to file'
END FUNCTION writeZeroTimeInvariantFile

!-------------------------------------------------------------------------------
!  writeZeroSourceFile
!-------------------------------------------------------------------------------
!> Write out time varying data for a dummy .pse file
!> @author
!> Martin Cope CSIRO
!
!! \param fun file unit
!! \param binaryFormat ascii or binary?
!! \param ts start hour
!! \param te end hour
!! \param ns number of sources
!! \param vs exit velocity (m/s)
!! \param tp temperature (C)
!! \param ne number of emission species
!! \result writeZeroTimeInvariantFile true if a successful write
!
! Modifications
!  When         Who      What
!-------------------------------------------------------------------------------
LOGICAL FUNCTION writeZeroSourceFile(fun,binaryFormat,ts,te,ns,vs,tp,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: fun
LOGICAL, INTENT(IN) :: binaryFormat
INTEGER, INTENT(IN) :: ts,te,ns,ne
REAL, INTENT(IN)    :: vs,tp
INTEGER             :: ih,f,s,ios
!
writeZeroSourceFile=.FALSE.
IF(binaryFormat)THEN
  DO ih=ts,te
    DO f=1,ns
      WRITE(fun,IOSTAT=ios)vs,tp,(0.0,s=1,ne)
      IF(ios /= 0)RETURN
    END DO
  END DO
ELSE
  DO ih=ts,te
    DO f=1,ns
      WRITE(fun,3,IOSTAT=ios)vs,tp,(0.0,s=1,ne)
3     FORMAT(F6.1,',',F4.0,100(',',G14.7))
      IF(ios /=0)RETURN
    END DO
  END DO
END IF
writeZeroSourceFile=.TRUE.
END FUNCTION writeZeroSourceFile
END MODULE DummySource