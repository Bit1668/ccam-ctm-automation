!-----------------------------------------------------------------------------|
! This file contains routines for reading extended TAPM emission files
!-----------------------------------------------------------------------------|
MODULE CTM_debug
IMPLICIT NONE
SAVE
LOGICAL, PARAMETER :: debugplot=.FALSE.
END MODULE CTM_debug

!-------------------------------------------------------------------------------
!> TAPM emissions i/o routines
!> @author
!> Martin Cope CSIRO
!-------------------------------------------------------------------------------
MODULE TAPMemissions
IMPLICIT NONE
SAVE
CONTAINS

!-------------------------------------------------------------------------------
!  retrieveTAPMgrid
!-------------------------------------------------------------------------------
!!> Function  to read in the geographic descriptors of a TAPM source file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \param nx grid dimension
!! \param ny grid dimension
!! \param dx cell size (m)
!! \param dy cell size (m)
!! \param x0 sw corner of domain (m)
!! \param y0 sw corner of domain (m)
!! \result retrieveTAPMgrid true if successful
!
! Modification                                  When                Who
!  Version 1.0                                 27/3/2007            mec
!-----------------------------------------------------------------------------|
FUNCTION retrieveTAPMgrid(inFile,binaryFormat,&
                          nx,ny,dx,dy,              &
                          x0,y0)
USE CTM_debug
IMPLICIT NONE
LOGICAL :: retrieveTAPMgrid
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
INTEGER, INTENT(OUT)           :: nx,ny         !grid dimension
REAL, INTENT(OUT)              :: dx,dy         !cell size
REAL, INTENT(OUT)              :: x0,y0         !south-west corner (of SW cell)
!
INTEGER                        :: ios           !read status
REAL                           :: cx,cy         !cell centre
retrieveTAPMgrid=.FALSE.

!-----------------------------------------------------------------------------|
! Retrieve the data
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)nx,ny,dx,dy,cx,cy
ELSE
  READ(inFile,*,IOSTAT=ios)nx,ny,dx,dy,cx,cy
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading grid definitions from a TAPM source file '
  RETURN
END IF
x0=cx-0.5*FLOAT(nx)*dx
y0=cy-0.5*FLOAT(ny)*dy
retrieveTAPMgrid=.TRUE.
END FUNCTION retrieveTAPMgrid

!-------------------------------------------------------------------------------
!  skipTAPMheader
!-------------------------------------------------------------------------------
!!> Function to skip over header information in a TAPM source file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \result skipTAPMheader true if successful
!
! Modification                                  When                Who
!  Version 1.0                                 27/3/2007            mec
!-----------------------------------------------------------------------------|
FUNCTION skipTAPMheader(inFile,binaryFormat)
USE CTM_debug
IMPLICIT NONE
LOGICAL :: skipTAPMheader
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
!
CHARACTER(LEN=80)              :: record
INTEGER                        :: i,s
INTEGER                        :: numberSpecies !number of TAPM species
CHARACTER(LEN=4)               :: speciesName   !name of chemical species
REAL                           :: MW            !species molecular weight
INTEGER                        :: nx,ny         !grid dimension
REAL                           :: dx,dy         !cell size
REAL                           :: cx,cy         !centre of grid
INTEGER                        :: ios           !i/o error flag
skipTAPMheader=.FALSE.

!-----------------------------------------------------------------------------|
! Read over the comment lines
!-----------------------------------------------------------------------------|
DO
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)record
  ELSE
    READ(infile,'(a)',IOSTAT=ios)record
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a TAPM source file header record'
    RETURN
  END IF
  IF(record(1:1) == '*')EXIT
END DO

!-----------------------------------------------------------------------------|
! Read over species definitions
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)numberSpecies
ELSE
  READ(infile,*,IOSTAT=ios)numberSpecies
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading a TAPM source file species number record'
  RETURN
END IF

DO s=1,numberSpecies
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)i,speciesName,MW
  ELSE
    READ(infile,*,IOSTAT=ios)i,speciesName,MW
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a TAPM source file species name record'
    RETURN
  END IF
END DO

!-----------------------------------------------------------------------------|
! Read over grid definition
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)nx,ny,dx,dy,cx,cy
ELSE
  READ(inFile,*,IOSTAT=ios)nx,ny,dx,dy,cx,cy
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading grid definitions from a TAPM source file'
  RETURN
END IF

skipTAPMheader=.TRUE.
END FUNCTION skipTAPMheader

!-------------------------------------------------------------------------------
!  skipTAPMpseHeader
!-------------------------------------------------------------------------------
!!> Function to skip over header information in a TAPM pse source file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \result skipTAPMpseHeader true if successful
!
! Modification                                  When                Who
!  Version 1.0                                 27/3/2007            mec
!-----------------------------------------------------------------------------|
FUNCTION skipTAPMpseHeader(inFile,binaryFormat)
USE CTM_debug
IMPLICIT NONE
LOGICAL :: skipTAPMpseHeader
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
!
CHARACTER(LEN=80)              :: record
INTEGER                        :: i,s,ids,ios
INTEGER                        :: numberSpecies !number of TAPM species
INTEGER                        :: numberSources !number of TAPM point sources
CHARACTER(LEN=4)               :: speciesName   !name of chemical species
REAL                           :: MW            !species molecular weight
INTEGER                        :: nx,ny         !grid dimension
REAL                           :: dx,dy         !cell size
REAL                           :: cx,cy         !centre of grid
REAL                           :: xs,ys        !source location (m)
REAL                           :: ds           !stack diameter (m)
REAL                           :: hs           !stack height (m)
skipTAPMpseHeader=.FALSE.

!-----------------------------------------------------------------------------|
! Read over the comment lines
!-----------------------------------------------------------------------------|
DO
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)record
  ELSE
    READ(infile,'(a)',IOSTAT=ios)record
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a pse TAPM source file header record'
    RETURN
  END IF
  IF(record(1:1) == '*')EXIT
END DO

!-----------------------------------------------------------------------------|
! Read over species definitions
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)numberSpecies
ELSE
  READ(infile,*,IOSTAT=ios)numberSpecies
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading a pse TAPM source species record'
  RETURN
END IF

DO s=1,numberSpecies
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)i,speciesName,MW
  ELSE
    READ(infile,*,IOSTAT=ios)i,speciesName,MW
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a ps TAPM source species name record: ',s
    RETURN
  END IF
END DO

!-----------------------------------------------------------------------------|
! Read over number of sources and number of hours
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)numberSources
ELSE
  READ(infile,*,IOSTAT=ios)numberSources
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading a pse TAPM source number record'
  RETURN
END IF

!-----------------------------------------------------------------------------|
! Read over time invariant source definitions
!-----------------------------------------------------------------------------|
DO s=1,numberSources
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)ids,xs,ys,hs,ds
  ELSE
    READ(infile,*,IOSTAT=ios)ids,xs,ys,hs,ds
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a TAPM pse time invariant record'
    WRITE(*,*)'Last source read was: ',s
    RETURN
  END IF
END DO


skipTAPMpseHeader=.TRUE.
END FUNCTION skipTAPMpseHeader

!-------------------------------------------------------------------------------
!  retrieveTAPMspeciesNumbers
!-------------------------------------------------------------------------------
!!> Function to retrieve number of species from a TAPM emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \result retrieveTAPMspeciesNumbers number of species
!
! Modification                                  When                Who
!  Version 1.0                                 27/3/2007            mec
!-----------------------------------------------------------------------------|
FUNCTION retrieveTAPMspeciesNumbers(inFile,binaryFormat)
IMPLICIT NONE
INTEGER :: retrieveTAPMspeciesNumbers
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
!
CHARACTER(LEN=80)              :: record
INTEGER                        :: i,s,ios
INTEGER                        :: numberSpecies !number of TAPM species
retrieveTAPMspeciesNumbers=-1

!-----------------------------------------------------------------------------|
! Read over the comment lines
!-----------------------------------------------------------------------------|
DO
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)record
  ELSE
    READ(infile,'(a)',IOSTAT=ios)record
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a TAPM source file header record'
    RETURN
  END IF
  IF(record(1:1) == '*')EXIT
END DO

!-----------------------------------------------------------------------------|
! Read number of species
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)numberSpecies
ELSE
  READ(infile,*,IOSTAT=ios)numberSpecies
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading a TAPM source file species number record'
  RETURN
END IF

retrieveTAPMspeciesNumbers=numberSpecies
END FUNCTION retrieveTAPMspeciesNumbers

!-------------------------------------------------------------------------------
!  retrieveTAPMpseSourceNumbers
!-------------------------------------------------------------------------------
!!> Function to retrieve number of species from a TAPM emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \result retrieveTAPMpseSourceNumbers number of pse sources
!
! Modification                                  When                Who
!  Version 1.0                                 27/3/2007            mec
!-----------------------------------------------------------------------------|
FUNCTION retrieveTAPMpseSourceNumbers(inFile,binaryFormat)
IMPLICIT NONE
INTEGER :: retrieveTAPMpseSourceNumbers
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
!
CHARACTER(LEN=80)              :: record
INTEGER                        :: i,s,ios
INTEGER                        :: numberSources !number of TAPM species
retrieveTAPMpseSourceNumbers=-1

!-----------------------------------------------------------------------------|
! Read number of sources
!-----------------------------------------------------------------------------|
IF(binaryFormat)THEN
  READ(inFile,IOSTAT=ios)numberSources
ELSE
  READ(infile,*,IOSTAT=ios)numberSources
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error reading a pse TAPM source file source number record'
  RETURN
END IF
retrieveTAPMpseSourceNumbers=numberSources
END FUNCTION retrieveTAPMpseSourceNumbers

!-------------------------------------------------------------------------------
!  retrieveTAPMpseSourceNumbers
!-------------------------------------------------------------------------------
!!> Function to retrieve time invariant point source data
!!> from a TAPM pse emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \param ns number of sources
!! \param ids source id numbers
!! \param xs source location (m)
!! \param ys source location (m)
!! \param ds source diameter (m)
!! \param hs source stack height (m)
!! \result retrieveTAPMpseTimeInvariant true if successful read
!
! Modification                                  When                Who
!  Added in ids as an argument                 01/02/2012           mec
!  bug- radius is input, convert to diameter   19/04/2007           mec
! Version 1.0                                  27/03/2007           mec
!-----------------------------------------------------------------------------|
FUNCTION retrieveTAPMpseTimeInvariant(inFile,binaryFormat,   &
                                      ns,ids,xs,ys,ds,hs)
IMPLICIT NONE
LOGICAL :: retrieveTAPMpseTimeInvariant
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
INTEGER, INTENT(IN)            :: ns            !number of point sources
INTEGER, DIMENSION(ns), INTENT(OUT) :: ids      !source id
REAL, DIMENSION(ns), INTENT(OUT):: xs,ys        !source location (m)
REAL, DIMENSION(ns), INTENT(OUT):: ds           !stack diameter (m)
REAL, DIMENSION(ns), INTENT(OUT):: hs           !stack height (m)
!
REAL    :: rs                                   !stack radius (m)
INTEGER :: s,ios
retrieveTAPMpseTimeInvariant=.FALSE.
DO s=1,ns
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)ids(s),xs(s),ys(s),hs(s),rs
  ELSE
    READ(infile,*,IOSTAT=ios)ids(s),xs(s),ys(s),hs(s),rs
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error reading a TAPM pse time invariant record'
    WRITE(*,*)'Last source read was: ',s
    RETURN
  END IF

!-----------------------------------------------------------------------------|
! Convert radius to diameter
!-----------------------------------------------------------------------------|
  ds(s)=2.0*rs
END DO

retrieveTAPMpseTimeInvariant=.TRUE.
END FUNCTION retrieveTAPMpseTimeInvariant

!-------------------------------------------------------------------------------
!  writeTAPMpseTimeInvariant
!-------------------------------------------------------------------------------
!!> Function to write time invariant point source data
!!> from a TAPM pse emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \param ns number of sources
!! \param ids source id numbers
!! \param xs source location (m)
!! \param ys source location (m)
!! \param ds source diameter (m)
!! \param hs source stack height (m)
!! \result writeTAPMpseTimeInvariant true if successful read
!
! Modification                                  When                Who
! Increased field size for stack heights       13/08/2015           mec
! Removed bug where rs=0 for ASCII output      12/08/2015           mec
! Added extra precision for lat/long           12/08/2015           mec
! Convert to write from read                   01/02/2012           mec
!  bug- radius is input, convert to diameter   19/04/2007           mec
! Version 1.0                                  27/03/2007           mec
!-----------------------------------------------------------------------------|
FUNCTION writeTAPMpseTimeInvariant(inFile,binaryFormat,   &
                                      ns,ids,xs,ys,ds,hs)
IMPLICIT NONE
LOGICAL :: writeTAPMpseTimeInvariant
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
INTEGER, INTENT(IN)            :: ns            !number of point sources
INTEGER, DIMENSION(ns), INTENT(IN) :: ids      !source id
REAL, DIMENSION(ns), INTENT(IN):: xs,ys        !source location (m)
REAL, DIMENSION(ns), INTENT(IN):: ds           !stack diameter (m)
REAL, DIMENSION(ns), INTENT(IN):: hs           !stack height (m)
!
REAL    :: rs                                   !stack radius (m)
INTEGER :: s,ios
writeTAPMpseTimeInvariant=.FALSE.
IF(binaryFormat)THEN
   WRITE(inFile,IOSTAT=ios)ns
ELSE
   WRITE(infile,*,IOSTAT=ios)ns
END IF
IF(ios /= 0)THEN
  WRITE(*,*)'Error writing a pse TAPM source number record'
  RETURN
END IF

DO s=1,ns
  rs=ds(s)*0.5
  IF(binaryFormat)THEN
    WRITE(inFile,IOSTAT=ios)ids(s),xs(s),ys(s),hs(s),rs
  ELSE
    WRITE(infile,1,IOSTAT=ios)ids(s),xs(s),ys(s),hs(s),rs
    1 FORMAT(i5,2(',',F13.4),',',F10.1,',',F4.1)
  END IF
  IF(ios /= 0)THEN
    WRITE(*,*)'Error writing a TAPM pse time invariant record'
    WRITE(*,*)'Last source read was: ',s
    RETURN
  END IF

END DO

writeTAPMpseTimeInvariant=.TRUE.
END FUNCTION writeTAPMpseTimeInvariant

!-------------------------------------------------------------------------------
!  retrieveTAPMpseTimeVarying
!-------------------------------------------------------------------------------
!!> Function to retrieve time varying point source data
!!> from a TAPM pse emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \param ns number of sources
!! \param ne number of emission species
!! \param nt number of hours
!! \param snames species names
!! \param vs plume velocity (m/s)
!! \param ts plume temperature (K)
!! \param qs emissions (g/s)
!! \result retrieveTAPMpseTimeVarying true if successful read
!
! Modification                                  When                Who
! Version 1.0                                  27/3/2007            mec
! Fix time skipping bug when hitting eof       11/2/2008            mec
!-----------------------------------------------------------------------------|
FUNCTION retrieveTAPMpseTimeVarying(inFile,binaryFormat,   &
                                      ns,ne,nt,sNames,vs,ts,qs)
USE CTM_debug
IMPLICIT NONE
LOGICAL :: retrieveTAPMpseTimeVarying
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
INTEGER, INTENT(IN)            :: ns            !number of point sources
INTEGER, INTENT(IN)            :: ne            !number of emission species
INTEGER, INTENT(IN)            :: nt            !number of hours
CHARACTER(LEN=4), DIMENSION(ne), INTENT(IN):: sNames        !species names
CHARACTER(LEN=80) :: errorMsg
REAL, DIMENSION(ns,nt), INTENT(OUT):: vs        !plume velocity (m/s)
REAL, DIMENSION(ns,nt), INTENT(OUT):: ts        !plume temperature (degK)
REAL, DIMENSION(ns,ne,nt)          :: qs        !stack emissions (g/s)
!
INTEGER :: s,t,q,ios
LOGICAL :: DoneRewind
retrieveTAPMpseTimeVarying=.FALSE.
DoneRewind=.FALSE.
t=1
DO    !time loop
  DO s=1,ns
    IF(binaryFormat)THEN
      READ(inFile,IOSTAT=ios,IOMSG=errorMsg)vs(s,t),ts(s,t),(qs(s,q,t),q=1,ne)
    ELSE
      READ(infile,*,IOSTAT=ios)vs(s,t),ts(s,t),(qs(s,q,t),q=1,ne)
    END IF
    IF(ios > 0)THEN
      WRITE(*,*)'Error reading a TAPM pse time varying record'
      WRITE(*,*)'Last source and time read was: ',s,t
      RETURN

!-----------------------------------------------------------------------------|
!  Hit end of file, rewind and move to the start of the time varying data
!  Note that we now require (11/2/2008) complete (ns) records of pse data
!   in each hour
!-----------------------------------------------------------------------------|
    ELSEIF(ios < 0)THEN
      IF(s /=1)THEN
        WRITE(*,*)'Error a TAPM pse file has incomplete emissions data'
        WRITE(*,*)'Last source read was: ',s,' out of: ',ns
        WRITE(*,*)'Reading hour record: ',t
        RETURN
      ELSE
        REWIND inFile
        DoneRewind=.true.
        IF(.NOT.skipTAPMpseHeader(inFile,binaryFormat))THEN
          WRITE(*,*)'Error reading header information on a TAPM pse file'
          RETURN
        END IF
        EXIT
      END IF!rewind
    END IF!eof found
  END DO!source loop

  IF(.NOT.DoneRewind)THEN
    t=t+1
    IF(t > nt)EXIT
  ELSE
    DoneRewind=.FALSE.
  END IF
END DO!time loop

!-----------------------------------------------------------------------------|
!  Dump out diurnal totals
!-----------------------------------------------------------------------------|
if(debugPlot)then
  OPEN(unit=80,FILE='pse_totals.csv')
  WRITE(80,*)'PSE_emissions_(kg/h)'
  WRITE(80,*)'t',(',',snames(q),q=1,ne)
  DO t=1,nt
    WRITE(80,*)t,(',',SUM(qs(:,q,t))*3.6,q=1,ne)
  END DO
  CLOSE(80)
end if

retrieveTAPMpseTimeVarying=.TRUE.
END FUNCTION retrieveTAPMpseTimeVarying

!-------------------------------------------------------------------------------
!  retrieveTAPMspeciesNames
!-------------------------------------------------------------------------------
!!> Function to retrieve names of species from a TAPM emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \param numberspecies number of species
!! \param speciesnames  names of species
!! \param speciesmw species molecular weight (g)
!! \result retrieveTAPMspeciesNames true if successful read
! Modification                                  When                Who
!  Version 1.0                                 27/3/2007            mec
!-----------------------------------------------------------------------------|
FUNCTION retrieveTAPMspeciesNames(inFile,binaryFormat,      &
                                  numberSpecies,SpeciesNames,SpeciesMW)
IMPLICIT NONE
LOGICAL :: retrieveTAPMspeciesNames
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
INTEGER, INTENT(IN)            :: numberSpecies !number of TAPM species
CHARACTER(LEN=4), DIMENSION(numberSpecies), INTENT(OUT) :: SpeciesNames
REAL, DIMENSION(numberSpecies), INTENT(OUT) :: SpeciesMW !molecular weights
!
CHARACTER(LEN=80)              :: record
INTEGER                        :: i,s,ios
retrieveTAPMspeciesNames=.FALSE.

!-----------------------------------------------------------------------------|
! Read names of species
!-----------------------------------------------------------------------------|
DO s=1,numberSpecies
  IF(binaryFormat)THEN
    READ(inFile,IOSTAT=ios)i,speciesNames(s),speciesMW(s)
  ELSE
    READ(infile,*,IOSTAT=ios)i,speciesNames(s),speciesMW(s)
  END IF
  IF(ios > 0)THEN
    WRITE(*,*)'Error reading a TAPM source file species names record'
    RETURN
  END IF
END DO
retrieveTAPMspeciesNames=.TRUE.
END FUNCTION retrieveTAPMspeciesNames

!-------------------------------------------------------------------------------
!  writeTAPMpseTimeVarying
!-------------------------------------------------------------------------------
!!> Function to retrieve time varying point source data
!!> from a TAPM pse emissions file
!> @author
!> Martin Cope CSIRO
!! \param inFile file unit number
!! \param binaryFormat ascii or binary?
!! \param ns number of sources
!! \param ne number of emission species
!! \param nt number of hours
!! \param snames species names
!! \param vs plume velocity (m/s)
!! \param ts plume temperature (K)
!! \param qs emissions (g/s)
!! \result writeTAPMpseTimeVarying true if successful read
!
! Modification                                  When                Who
! Version 1.0                                  27/3/2007            mec
! Fix time skipping bug when hitting eof       11/2/2008            mec
! Modified from retrieve to write              01/2/2012            mec
! Made ascii write field larger to avoid **    12/8/2015            mec
!-----------------------------------------------------------------------------|
FUNCTION writeTAPMpseTimeVarying(inFile,binaryFormat,   &
                                      ns,ne,nt,sNames,vs,ts,qs)
IMPLICIT NONE
LOGICAL :: writeTAPMpseTimeVarying
INTEGER, INTENT(IN)            :: inFile        !file unit
LOGICAL, INTENT(IN)            :: binaryFormat  !file is binary
INTEGER, INTENT(IN)            :: ns            !number of point sources
INTEGER, INTENT(IN)            :: ne            !number of emission species
INTEGER, INTENT(IN)            :: nt            !number of hours
CHARACTER(LEN=4), DIMENSION(ne), INTENT(IN):: sNames        !species names
REAL, DIMENSION(ns,nt), INTENT(IN)    :: vs        !plume velocity (m/s)
REAL, DIMENSION(ns,nt), INTENT(IN)    :: ts        !plume temperature (degK)
REAL, DIMENSION(ns,ne,nt), INTENT(IN) :: qs        !stack emissions (g/s)
!
INTEGER :: s,t,q,ios
LOGICAL :: DoneRewind
writeTAPMpseTimeVarying=.FALSE.

DO t=1,nt   !time loop
  DO s=1,ns
    IF(binaryFormat)THEN
      WRITE(inFile,IOSTAT=ios)vs(s,t),ts(s,t),(qs(s,q,t),q=1,ne)
    ELSE
      WRITE(infile,1,IOSTAT=ios)vs(s,t),ts(s,t),(qs(s,q,t),q=1,ne)
      !1 FORMAT(F6.1,',',F4.0,1000(',',G14.7))
      1 FORMAT(F8.2,',',F6.1,1000(',',G14.7))
    END IF
    IF(ios > 0)THEN
      WRITE(*,*)'Error writing a TAPM pse time varying record'
      WRITE(*,*)'Last source and time written was: ',s,t
      RETURN
    END IF

  END DO !source loop

END DO !time loop

writeTAPMpseTimeVarying=.TRUE.
END FUNCTION writeTAPMpseTimeVarying

!-------------------------------------------------------------------------------
!  retrieveCITemsFormat
!-------------------------------------------------------------------------------
!!> Function to determine if a CIT emissions file is Fortran unformatted
!> @author
!> Martin Cope CSIRO
!! \param fname file name
!
! Modified MC 20/06/07 to search the entire filename for '.bin'
!-----------------------------------------------------------------------------|
FUNCTION retrieveCITemsFormat(fname)
USE CTM_debug
USE file_management
IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN) :: fname !file name
LOGICAL :: retrieveCITemsFormat
INTEGER :: nameLength,j
!
retrieveCITemsFormat=.TRUE.
nameLength=LEN_TRIM(fname)
do j=1,nameLength-3
  if(UPPERcase(fname(j:j+3)) == '.BIN')return
end do
retrieveCITemsFormat=.FALSE.
END FUNCTION retrieveCITemsFormat

!-------------------------------------------------------------------------------
!  writeTAPM_header
!-------------------------------------------------------------------------------
!!> Function to determine if a CIT emissions file is Fortran unformatted
!> @author
!> Martin Cope CSIRO
!! \param funit file unit number
!! \param fform ascii or binary?
!! \param nlcc number of species
!! \param lcca species names
!! \param mw species molecular weight (g)
!! \result writeTAPM_header true is successful
!
! Modified MC 20/06/07 to search the entire filename for '.bin'
!-----------------------------------------------------------------------------|
FUNCTION writeTAPM_header(funit,fform,nlcc,lcca,mw)
IMPLICIT NONE
INTEGER, INTENT(IN) :: funit  !file unit
INTEGER, INTENT(IN) :: nlcc   !number of species
CHARACTER(LEN=4), DIMENSION(nlcc), INTENT(IN) :: lcca !species names
REAL, DIMENSION(nlcc), INTENT(IN) :: mw               !molecular weights
LOGICAL :: fForm
LOGICAL :: writeTAPM_header
CHARACTER(LEN=8) :: systemDate
CHARACTER(LEN=10) :: systemTime
CHARACTER(LEN=120) :: comment
INTEGER :: i
writeTAPM_header=.FALSE.

!-------------------------------------------------------------------------------
! Generate header for CIT aems file
!-------------------------------------------------------------------------------
CALL DATE_and_TIME(systemDate,systemTime)
IF(fForm)THEN
  comment='version_02'
  WRITE(funit,ERR=94)comment
  comment='Tapm emissions file. Generated by psmerge'
  WRITE(funit,ERR=94)comment
  comment='File was created (yyyymmdd): '//systemDate//' at time (hhmmss.sss):'//systemTime
  WRITE(funit,ERR=94)comment
  comment='GramPerSec :TAPM emission units of g/s'
  WRITE(funit,ERR=94)comment
  comment='*'
  WRITE(funit,ERR=94)comment
  WRITE(funit,ERR=94)nlcc
  DO i=1,nlcc
    WRITE(funit,ERR=94)i,lcca(i),mw(i)
  END DO
ELSE
  WRITE(funit,1,ERR=94)'Version_02'
  WRITE(funit,1,ERR=94)'Tapm emissions file. Generated by psmerge'
1 FORMAT(100a)
  WRITE(funit,1,ERR=94)'File was created (yyyymmdd): ',systemDate,' at time (hhmmss.sss):',systemTime
  WRITE(funit,1,ERR=94)'GramPerSec :TAPM emission units of g/s'
  WRITE(funit,1,ERR=94)'*'
  WRITE(funit,*,ERR=94)nlcc
  DO i=1,nlcc
    WRITE(funit,2,ERR=94)i,lcca(i),mw(i) 
2   FORMAT(i3,1x,a,1x,F5.1)
  END DO
END IF
writeTAPM_header=.TRUE.
RETURN
!-------------------------------------------------------------------------------
! Errors
!-------------------------------------------------------------------------------
94 PRINT *,'Error writing TAPM header to file'
RETURN
END FUNCTION writeTapm_header
END MODULE TAPMemissions
