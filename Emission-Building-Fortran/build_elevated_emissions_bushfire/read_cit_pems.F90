!     ******************************************************************
!     read_cit_pems.F90
!     Copyright(c) CSIRO 2000
!
!     Created: 23/05/2005 11:13:24 AM
!     Author : Sunhee Lee
!     Last change: MC 12/04/2007 5:02:28 PM
!     ******************************************************************

!-----------------------------------------------------------------------------|
!-----------------------------------------------------------------------------|
! Read pems point source file and retrieve data for all point sources
!-----------------------------------------------------------------------------|
subroutine read_cit_pems(ngrid,emis,hs,ds,ts,vfs,xpoint,ypoint)
!  SHL: 7/3/2005: Add more error messages
!  SHL: 9/10/2003: different hour has diff npoint source and
!      it is not necessary synchronised with all hours
!      ie location (nn) for ith hour /= location (nn) for jth hour

!use ctm_global_variable
use ctm_debug
implicit none
integer, intent(in) :: ngrid                       !current CTM grid
real, intent(out) :: emis(npmax(ngrid),e_ns,num_hrs) !point source emission data
real, intent(out) :: hs(npmax(ngrid))              !ps stack height (m)
real, intent(out) :: ds(npmax(ngrid))              !ps diameter (m)
real, intent(out) :: ts(npmax(ngrid))              !ps eflux temperature (K)
real, intent(out) :: vfs(npmax(ngrid))             !ps volume flow rate (m3/s)
real, intent(out) :: xpoint(npmax(ngrid))          !ps location
real, intent(out) :: ypoint(npmax(ngrid))          !ps location

!-----------------------------------------------------------------------------|
!     N-air*10-6/60=101325/8.31/298/60*10-6 from ppm-m/min to g/sec
!     c_fac*dx*dy*Mwght will be complete conversion factor
!-----------------------------------------------------------------------------|
real, parameter :: c_fac=6.81943e-7
real, parameter :: phi=3.1419117
!
integer :: idt
integer :: ids(npmax(ngrid))          !point source id number
integer :: Sindex                     !point source location in index table
!
real, dimension(e_ns) :: dum          !point source emissions
real :: hst,dst,tst,vfst,xt,yt        !point source characteristics
real :: mwght(e_ns)                   !species molecular weights
real :: dummy
real :: xpos,ypos
real :: xlong_end,ylat_end !extent of CTM grid
real :: emsn_xlong_offset  !centre of sw cell of emissions grid
real :: emsn_ylat_offset   !centre of sw cell of emissions grid
!
real, dimension(e_ns) :: mwDummy                !molecular weights (dummy variables)
character (len=4), dimension(e_ns) :: lccaDummy !species names (dummy variables)
character (len=1)                  :: srcType   !G/E source identification
integer :: nlccDummy                            !number of species (dummy variable)
!
logical :: done,rewindFlag
logical, external :: retrieveCITpems_header     !read emissions header

integer :: stat,record
integer :: i,j,ih,l,kk,itime,ii,jj
integer :: fUnit
logical :: fForm
logical :: gPerSecLdummy
character(len=260) :: fileName
!-----------------------------------------------------------------------------|
!  Request CIT point source emissions file name
!-----------------------------------------------------------------------------|
fUnit=pemsFunit(ngrid)
fForm=pemsForm(ngrid)
fileName=pems_fname(ngrid)
mwght=pems_mw(:,ngrid)

!-----------------------------------------------------------------------------|
!  check overlap between CTM and CIT grids
!  CIT format gives SW corner not center as with CTM- move it.
!-----------------------------------------------------------------------------|
emsn_xlong_offset=emsn_xlong(ngrid)+0.5*emsn_dx(ngrid)
emsn_ylat_offset=emsn_ylat(ngrid)+0.5*emsn_dy(ngrid)

xlong_end=xlong(ngrid)+(nx(ngrid)-1)*dx(ngrid)
ylat_end=ylat(ngrid)+(ny(ngrid)-1)*dy(ngrid)

!-----------------------------------------------------------------------------|
! Loop over CTM data time interval (usually hourly)
!-----------------------------------------------------------------------------|
emis=0.
itime=1
ih=0
i=0
j=0
rewindFlag=.FALSE.
do !hour loop
  record=0

!-----------------------------------------------------------------------------|
! Inner loop over CIT individual data records
!-----------------------------------------------------------------------------|
  do
    if(fForm)then
      read(fUnit,iostat=stat)srcType,ih,i,j,idt,hst,dst,tst,vfst,xt,yt,  &
                             (dum(l),l=1,e_ns)
    else
      read(fUnit,200,iostat=stat)ih,i,j,idt,hst,dst,tst,vfst,xt,yt,  &
                                (dum(l),l=1,e_ns)
      200 format(1x,i2,i3,i3,i5,6f8.1,/,(8e10.3))
    end if
    if(stat > 0)then !i/o error; fatal
      print *,'Error reading point source emissions'
      print *,'File name is: ',trim(filename)
      print *,'Last valid time and source location read are: ',ih,i,j
      print *,'Please check the file for errors'
      stop'Fatal error'
    else if(stat < 0)then
      exit !eof or eor
    end if!iostat

    if(i == 999)exit  !end of hour

!-----------------------------------------------------------------------------|
!  Check that source lies within CTM grid
!  Get stack index number
!-----------------------------------------------------------------------------|
    xpos=emsn_xlong_offset+(i-1)*emsn_dx(ngrid)
    ypos=emsn_ylat_offset+(j-1)*emsn_dy(ngrid)

    if(xpos >= xlong(ngrid) .and. xpos <= xlong_end .and.   &
       ypos >= ylat(ngrid) .and. ypos <= ylat_end) then
!
      call find_stack_index(idt,pems_MasterID(1,ngrid),npmax(ngrid),sIndex)

!-----------------------------------------------------------------------------|
!  load data into master arrays
!  convert temperature from degC to degK
!  convert volume flow rate to flow velocity
!-----------------------------------------------------------------------------|
      ids(sIndex)=idt
      hs(sIndex)=hst
      ds(sIndex)=dst
      ts(sIndex)=tst+273.16
      if(dst > 0. ) then
         vfs(sIndex)=vfst*4./(phi*dst**2.)
      else
         vfs(sIndex)=0.
      end if

!-----------------------------------------------------------------------------|
!  Note- location is to nearest grid point.
!  Not good if CIT grid resolution << CTM grid resolution
!-----------------------------------------------------------------------------|
      xpoint(sIndex)=emsn_xlong_offset+float(i-1)*emsn_dx(ngrid)
      ypoint(sIndex)=emsn_ylat_offset+float(j-1)*emsn_dy(ngrid)

!-----------------------------------------------------------------------------|
!  Optionally change emission rate units from ppm-m/min to g/s
!-----------------------------------------------------------------------------|
      if(pems_gramPerSecL(ngrid))then
        do l=1,pems_nlcc(ngrid)
          emis(sIndex,l,itime)=dum(l)
        end do
      else
        do l=1,pems_nlcc(ngrid)
          emis(sIndex,l,itime)=dum(l)*c_fac*mwght(l)*emsn_dx(ngrid)*emsn_dy(ngrid)
        end do
      end if

    end if! source lies within CTM grid
    record=record+1
  end do! record loop

!-----------------------------------------------------------------------------|
! Hit an end of hour indicator or eof or eor
!-----------------------------------------------------------------------------|
  if(stat < 0)then !eof or eor
    if(debug)write(outD,*)'Hit eof in pems file. Rewinding'
    rewind fUnit
    done=retrieveCITpems_header(e_ns,fUnit,fForm,nlccDummy,lccaDummy,mwDummy,gPerSecLdummy)
    if(.not.done)then
      print *,'Error reading the header of the pems emissions file: ',trim(filename)
      stop'Fatal error'
    end if!header read error

    if(itime==1 .AND. record==0 .AND. rewindFlag)exit !no data in file
    rewindFlag=.TRUE.
  else
    if(debug)write(outD,*)'read_cit_aems, hour= ',itime-1, ' has been completed'
    if(itime==num_hrs)exit !have read in full data period
    itime=itime+1
  end if!eof or eor
end do !hour loop
return

!-----------------------------------------------------------------------------|
!  File i/o errors
!-----------------------------------------------------------------------------|

end subroutine read_cit_pems



