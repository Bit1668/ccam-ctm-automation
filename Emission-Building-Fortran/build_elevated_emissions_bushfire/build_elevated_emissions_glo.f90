!******************************************************************
!     CIT2TAPMpems
! Program to set up Tapm emission files for multiple days
! ******************************************************************
!-----------------------------------------------------------------------------|
! Modifications
!  When         Who      What
! 19/10/2016    hd    correct the bug in calculating the stack cross section
! area
! 31/12/15      mec   all point source OC emissions are involatile
! 15/08/15      mec   and back again
! 13/08/15      mec   Changed from fdp to shipping CB05 emissions
! 12/08/15      mec   PART now input in g/s not ppm-m/min
! 03/08/15      mec   added three SOx tracers
! 28/07/15      mec   Fixed bug in the tag software
! 27/07/15      mec   Added capability to tag sources based on minimum source height
! 22/07/15      mec   set character read formats
! 22/07/14      mec   modified to track 8 power stations- currently hardwired
! 11/09/14      mec   ec/oc size distribution now matches Sydney obs
!                     height-based source filtering now disabled
! 15/08/14      mec   sulfate currently accum + coarse; no sources with H>150m; T>60C
! 16/06/14      mec   Corrected bug in sulfate mapping to output arrays
! 14/06/14      mec   Using coarser size distribution for su
! 11/04/13      mec   Speciated PM2.5 into ec, oc in addition to ot
! 02/04/13      mec   Added in GLOMAP particle size data
! 14/06/12      mec   Included CB05 speciation for user input sources
! 04/06/12      mec   Re-enabled command line argument- but backwards compatible
! 13/03/12      mec   Add ptol and pxyl
!-----------------------------------------------------------------------------|
  MODULE CTM_interfaces
   INTERFACE
     FUNCTION UpperCase(string,length)
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: length
      CHARACTER(LEN=length) :: UpperCase
      CHARACTER(LEN=length), INTENT(IN) :: string
     END FUNCTION
   ENDINTERFACE
 END MODULE CTM_interfaces
 MODULE ctm_debug
   IMPLICIT NONE
   SAVE
   LOGICAL, PARAMETER :: debug=.TRUE.
   INTEGER, PARAMETER :: outD= 47
    END MODULE ctm_debug
    
 MODULE cb05_module
   IMPLICIT NONE
   SAVE

!-----------------------------------------------------------------------------|
!  Assume PART=pm10
!-----------------------------------------------------------------------------|
  REAL :: pmRatio
   
!-----------------------------------------------------------------------------|
! Names of CB05 emissions + relevant GLOMAP ptcl components and modes
! Note that the second CB05 option includes SOx tracers for FDP
!-----------------------------------------------------------------------------|
!  INTEGER, PARAMETER :: ncb05=24
!  CHARACTER(LEN=4), DIMENSION(ncb05), PARAMETER :: cb05species  =(/  &
!             'ETHA','IOLE','ALDX','OC25',     &
!       'OC10','EC25','EC10','OT25','OT10',     &
!       'ASO4','AS10','APA1','APA2','APA3',     &
!       'APA4','APA5','APA6','APA7','APA8',     &
!       'APA9','PTOL','PXYL','PBNZ','LEVO'/)
    INTEGER, PARAMETER :: ncb05=27
    CHARACTER(LEN=4), DIMENSION(ncb05), PARAMETER :: cb05species  =(/  &
                'ETHA','IOLE','ALDX','OC25',     &
         'OC10','EC25','EC10','OT25','OT10',     &
         'ASO4','AS10','APA1','APA2','APA3',     &
         'APA4','APA5','APA6','APA7','APA8',     &
         'APA9','PTOL','PXYL','PBNZ','LEVO',     &
         'SOX1','SOX2','SOX3'/)
!
   INTEGER, PARAMETER :: nglo=24
   CHARACTER(LEN=4), DIMENSION(nglo), PARAMETER :: glospecies  =(/  &
        'SU1 ','SU2 ','SU3 ','SU4 ',            &
        'BC2 ','BC3 ','BC4 ','BC5 ',            &
        'OC1 ','OC2 ','OC3 ','OC4 ','OC5 ',     &
        'DU3 ','DU4 ','DU6 ','DU7 ',            &
!        
        'NUCS','AITS','ACCS','COAS','AITI','ACCI','COAI'          /)

!-----------------------------------------------------------------------------|
! Set up parameters and definitions for tagged tracers
!-----------------------------------------------------------------------------|
   LOGICAL :: ltag=.FALSE.
   INTEGER :: itag
   INTEGER, PARAMETER :: ntag=50         !total of 50 tracers (25 tags, 25 clocks)
   INTEGER, PARAMETER :: halfNtag=ntag/2 !number of unique sources
   CHARACTER(LEN=4), DIMENSION(ntag), PARAMETER :: tagspecies  =(/  &
        'SX1 ','SX2 ','SX3 ','SX4 ','SX5 ',     &
        'SX6 ','SX7 ','SX8 ','SX9 ','SX10',     &
        'SX11','SX12','SX13','SX14','SX15',     &
        'SX16','SX17','SX18','SX19','SX20',     &
        'SX21','SX22','SX23','SX24','SX25',     &
        'SR1 ','SR2 ','SR3 ','SR4 ','SR5 ',     &
        'SR6 ','SR7 ','SR8 ','SR9 ','SR10',     &
        'SR11','SR12','SR13','SR14','SR15',     &
        'SR16','SR17','SR18','SR19','SR20',     &
        'SR21','SR22','SR23','SR24','SR25'     &
        /)

!-----------------------------------------------------------------------------|
! MWs (g/mole) of CB05 emissions + relevant GLOMAP ptcl components and modes
! Note second CB05 list includes the SOX tracers
!-----------------------------------------------------------------------------|
!  REAL, DIMENSION(ncb05), PARAMETER :: cb05mw =(/ &
!      30.1,48.,44.,1.,        &
!       1.,1.,1.,1.,1.,        &
!       1.,1.,1.,1.,1.,        &
!       1.,1.,1.,1.,1.,        &
!       1.,92.0,106.0,78.1,1./)

    REAL, DIMENSION(ncb05), PARAMETER :: cb05mw =(/ &
             30.1,48.,44.,1.,   &
         1.,1.,1.,1.,1.,        &
         1.,1.,1.,1.,1.,        &
         1.,1.,1.,1.,1.,        &
         1.,92.0,106.0,78.1,1., &
         64.0,64.0,64.0/)
!        
   REAL, DIMENSION(nGLO), PARAMETER :: GLOmw =(/ & 
        98.0,98.0,98.0,98.0,   &
        12.0,12.0,12.0,12.0,   &
        16.8,16.8,16.8,16.8,16.8,   &
        100.,100.,100.,100.,        &
        1.0,1.0,1.0,1.0,1.0,1.0,1.0 & 
        /)

   REAL, DIMENSION(ntag), PARAMETER :: TAGmw =(/ &   !tracers currently have the properties of SO2
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.,   &
        64.,64.,64.,64.,64.   &
       /)
        
!-----------------------------------------------------------------------------|
! Indices pointing to modes for a 7-mode GLOMAP configuration 
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: modes=7
   INTEGER, PARAMETER :: NUCS=1  !nucln. soluble
   INTEGER, PARAMETER :: AITS=2  !Aitken soluble
   INTEGER, PARAMETER :: ACCS=3  !accum. soluble
   INTEGER, PARAMETER :: COAS=4  !coarse soluble
   INTEGER, PARAMETER :: AITI=5  !Aitken insoluble
   INTEGER, PARAMETER :: ACCI=6  !accum. insoluble
   INTEGER, PARAMETER :: COAI=7  !coarse insoluble
   INTEGER, PARAMETER :: nInvolatile = 3 !VBS categories treated as involatile
   REAL :: modevol      
   REAL :: lgsd
   LOGICAL, PARAMETER :: glomap=.FALSE.
   REAL, PARAMETER :: ppi=3.141592654

!-----------------------------------------------------------------------------|
!          h2so4  bc     oc    nacl   dust    so
! n.b. mm_bc=0.012, mm_oc=0.012*1.4=0.168 (1.4 C-H ratio)
! Mass density of components (kg m^-3)
!-----------------------------------------------------------------------------|
    INTEGER, PARAMETER :: ncp=6
    REAL, DIMENSION(ncp) ::  rhocomp=(/1769.0,1500.0,1500.0,1600.0,2650.0,1500.0/)
    INTEGER, PARAMETER :: su_p=1
    INTEGER, PARAMETER :: bc_p=2
    INTEGER, PARAMETER :: oc_p=3
    INTEGER, PARAMETER :: ss_p=4
    INTEGER, PARAMETER :: du_p=5
    INTEGER, PARAMETER :: so_p=6

!-----------------------------------------------------------------------------|
! Size distribution particle definitions for industrial sources
! -Sulfate
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: n_su=2                       !number of modes
   REAL, DIMENSION(n_su) :: d_su=(/150.,1500./)       !geometric mean diameter (nm)
   REAL, DIMENSION(n_su) :: gsig_su=(/1.59,2.00/)     !geometric standard deviation
   REAL, DIMENSION(n_su) :: f_su=(/0.5,0.5/)          !fraction in each mode
   INTEGER, DIMENSION(n_su) :: mode_su=(/ACCS,COAS/)  !modes emitted into        
   !INTEGER, PARAMETER :: n_su=2                        !number of modes
   !REAL, DIMENSION(n_su) :: d_su=(/10.,70./)           !geometric mean diameter (nm)
   !REAL, DIMENSION(n_su) :: gsig_su=(/1.60,2.00/)      !geometric standard deviation
   !REAL, DIMENSION(n_su) :: f_su=(/0.05,0.95/)         !fraction in each mode
   !INTEGER, DIMENSION(n_su) :: mode_su=(/NUCS,AITS/)   !modes emitted into        

!-----------------------------------------------------------------------------|
! Size distribution particle definitions for industrial sources
! -OC and EC
! Modified to match Sydney observations
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: n_ecoc=1
   !REAL, DIMENSION(n_ecoc) :: d_ecoc=(/60.0/)
   !REAL, DIMENSION(n_ecoc) :: gsig_ecoc=(/1.59/)
   REAL, DIMENSION(n_ecoc) :: d_ecoc=(/37.0/)
   REAL, DIMENSION(n_ecoc) :: gsig_ecoc=(/2.3/)

   REAL, DIMENSION(n_ecoc) :: f_ecoc=(/1.0/)
   INTEGER, DIMENSION(n_ecoc) :: mode_ecoc=(/AITI/) 

!-----------------------------------------------------------------------------|
! Size distribution particle definitions for industrial sources
! -other
!   Begin with OC/EC distribution and modify as other data becomes available
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: n_du=1
   REAL, DIMENSION(n_du) :: d_du=(/150./)
   REAL, DIMENSION(n_du) :: gsig_du=(/1.59/)
   REAL, DIMENSION(n_du) :: f_du=(/1.0/)
   INTEGER, DIMENSION(n_du) :: mode_du=(/ACCI/) 
   
!-----------------------------------------------------------------------------|
! Speciation of OC into VBS OM
!   -need to add reference here
! Treat as involatile (1.6 = OC -> OM)
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: nvbs = 9
!  REAL, DIMENSION(nvbs),parameter :: vbs= (/0.048,0.096,0.144,0.224,0.288,0.480,0.640, &
!                                         0.800 ,1.280 /)
   REAL, DIMENSION(nvbs),parameter :: vbs= (/1.600,0.000,0.000,0.000,0.000,0.000,0.000, &
                                          0.000 ,0.000 /)
      
!-----------------------------------------------------------------------------|
!  Factors to go from NOx to NH3 and PM2.5 to mass components
!-----------------------------------------------------------------------------|
   REAL :: nh3,etha,iole,aldx,oc25,oc10,ec25,ec10,aso4
   REAL :: as10,ot25,ot10,ftol,fxyl,fbnz,levo,sox1,sox2,sox3
   REAL :: su1,su2,su3,su4,bc2,bc3,bc4,bc5,oc1,oc2,oc3,oc4,oc5,du3,du4,du6,du7
   REAL :: nnucs,naits,naccs,naiti,nacci,ncoas,ncoai
   
   INTEGER :: lno,lno2,lpm25,lso2,lole,lald2,ltol,lxyl
   REAL, DIMENSION(nvbs) :: apa

!-----------------------------------------------------------------------------|
!  Source is specifed in run file: 1=coal power, 2=gas power only, 3=rest
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: num_Source=3
   REAL, PARAMETER, Dimension(num_source) :: noxTOnh3 = (/ 0.0244,0.0244,0.0244/)
   REAL, PARAMETER, Dimension(num_source) :: noxTOnh3_tall =  &
                                             (/0.0000432,0.0000432,0.0000432/)
! SHL: 24/09/2014:  The raio is modified based on CARB emission file
!  REAL, PARAMETER, Dimension(num_source) :: pm25TOoc25 = (/ 0.2270,0.610,0.0699 /)
!  REAL, PARAMETER, Dimension(num_source) :: pm25TOec25 = (/ 0.0210,0.130,0.011/)
!  REAL, PARAMETER, Dimension(num_source) :: pm25TOot25 = (/ 0.1710,0.1710,0.1710/)
!  REAL, PARAMETER, Dimension(num_source) :: pm25TOot10 = (/ 0.4526,0.4526,0.4526/)
   REAL, PARAMETER, Dimension(num_source) :: pm25TOoc25 = (/ 0.0650,0.0   ,0.2974/)
   REAL, PARAMETER, Dimension(num_source) :: pm25TOec25 = (/ 0.1400,0.5   ,0.02634/)
   REAL, PARAMETER, Dimension(num_source) :: pm25TOot25 = (/ 0.05  ,0.3   ,0.2587/)
!  the ratio is calculated using ratio of 10/25 in CIT2TAPMpems*90
   REAL, PARAMETER, Dimension(num_source) :: pm25TOoc10 = pm25TOoc25 *0.759
   REAL, PARAMETER, Dimension(num_source) :: pm25TOec10 = pm25TOec25 *0.435
   REAL, PARAMETER, Dimension(num_source) :: pm25TOot10 = pm25TOot25 *2.6468
!  values are  from  CIT2TAPMpems*90
   REAL, PARAMETER, Dimension(num_source) :: so2TOaso4 = (/ 0.053,0.053,0.053/)
   REAL, PARAMETER, Dimension(num_source) :: so2TOaso4_tall = (/ 0.03,0.03,0.03/)
   REAL, PARAMETER, Dimension(num_source) :: so2TOas10 = (/ 0.0,0.0,0.0/)
   REAL, PARAMETER, Dimension(num_source) :: xylTOpxyl  = (/ 1.0,1.0,1.0/)
   REAL, PARAMETER, Dimension(num_source) :: tolTOptol  = (/ 1.0,1.0,1.0/)
   REAL, PARAMETER, Dimension(num_source) :: tolTOpbnz  = (/ 0.014,0.014,0.014/)
   
   LOGICAL, PARAMETER :: cb05=.TRUE.
END MODULE cb05_module

program CIT2TAPMpems
   use cb05_module
   use CTM_interfaces
   use CTM_debug
   implicit none
   character(len=160) :: grid_fname,file1,user_fname
   character(len=120) :: comment
   character(len=1) ::char1
   character (len=120) :: INpems_fname,OUTpems_fname
   logical :: INpemsForm    ! input point source file format
   logical :: OUTpemsForm   ! outputpoint source file format
   integer :: INpemsFunit   !area source file unit
   integer :: OUTpemsFunit   !area source file unit
   integer, parameter:: maxPointSources=10000
   integer :: ns    ! no of species in used in chemical eqns
   integer :: npmax ! total indiviual stack number
   integer :: npmax1 !total stack number + user stack number
   integer, dimension(maxPointSources) :: pems_MasterID
   integer :: ne       ! no of species in emission files
   integer :: pems_nlcc     !number of species in  minventory
   integer :: max_hr    !hour that has maaax stack number
   integer :: max_hr_stack_number  !stack numnber at max_hr
   integer :: offset !hour offset
   real :: emsn_xlong     !origin
   real :: emsn_ylat      !origin
   real :: emsn_dx        !cell width
   real :: emsn_dy        !cell width
   real, allocatable, dimension(:,:) :: mw             !molecular weight (aems)
   real, allocatable, dimension(:) :: pems_mw          !molecular weight (mv)
   character(len=4), allocatable, dimension(:) :: pems_lcca !(species names; mv)
   logical :: done,scaled
   logical, external :: retrieveCITpems_header
   logical, external :: retrieveCITgrid
   integer, external :: retrieveCIT_speciesNumbers
   logical, external :: retrieveCITemsFormat

   real, allocatable,dimension(:,:,:) :: emis !point source emission data
   real, allocatable,dimension(:,:,:) :: emis_cb05 !point source emission data
   real, allocatable,dimension(:,:,:) :: emis_glo  !point source emission data
   real, allocatable,dimension(:,:,:) :: emis_tag  !point source emission data
   real, allocatable, dimension(:) :: hs              !ps stack height (m)
   real, allocatable, dimension(:) :: rs              !ps radius (m)
   real, allocatable, dimension(:,:) :: ts            !ps eflux temperature (K)
   real, allocatable, dimension(:,:) :: vfs           !ps volume flow rate (m3/s)
   real, allocatable, dimension(:) :: xpoint          !ps location
   real, allocatable, dimension(:) :: ypoint          !ps location

!---------------------------------------------------------------------!
! User emissions
!---------------------------------------------------------------------!
   integer :: numberSources
   integer :: startHour,stopHour !(0,23)
   integer, allocatable,dimension(:) :: stack_id   !id number
   real, allocatable,dimension(:) :: stack_x       !(m)
   real, allocatable,dimension(:) :: stack_y       !(m)
   real, allocatable,dimension(:) :: stack_radius  !(m)
   real, allocatable,dimension(:) :: stack_velocity !(m/s)
   real, allocatable,dimension(:) :: stack_height   !(m)
   real, allocatable,dimension(:) :: stack_temperature !(K)
   real, allocatable,dimension(:,:) :: stack_emissions !(g/s)
   
!-----------------------------------------------------------------------------|
!     N-air*10-6/60=101325/8.31/298/60*10-6 from ppm-m/min to g/sec
!     c_fac*dx*dy*Mwght will be complete conversion factor
!-----------------------------------------------------------------------------|
   real, parameter :: c_fac=6.81943e-7
   real, parameter :: phi=3.1419117
   real, parameter :: Re=6371007
   real            :: PIO180
   real            :: factor
!
   integer :: idt
   integer, allocatable,dimension(:) :: ids          !point source id number
   integer :: Sindex                     !point source location in index table
   integer,parameter :: num_hrs=24
!
   real, allocatable,dimension(:)  :: dum            !point source emissions
   real, allocatable,dimension(:)  :: mwght            !point source emissions
   real :: hst,dst,tst,vfst,xt,yt        !point source characteristics
   real :: dummy

!-------------------------------------------------------------------------------
! CIT and TAPM spatial grid definitions
!-------------------------------------------------------------------------------
   real :: xpos,ypos
   real :: xlong_end,ylat_end !extent of CTM grid
   integer ::emsn_nx,emsn_ny,v
   integer :: zone            !AMG zone
   real :: emsn_xlong_offset  !centre of sw cell of emissions grid
   real :: emsn_ylat_offset   !centre of sw cell of emissions grid
   real :: emsn_xlong_end  !centre of NE cell of emissions grid
   real :: emsn_ylat_end   !centre of NE cell of emissions grid
   real :: emsn_xlong_center  !centre of emissions grid
   real :: emsn_ylat_center   !centre of emissions grid
!
   real,allocatable,dimension(:) :: mwDummy   !molecular weights (dummy variables)
   character (len=4),allocatable, dimension(:) :: lccaDummy !species names (dummy variables)
   integer :: nlccDummy                          !number of species (dummy variable)
!
   logical :: rewindFlag
   logical :: doUserSources
   logical, external :: writeTAPM_header
   character(len=4) :: model !(CCAM; TAPM; UM)
   integer, parameter :: mode=0  ! source mode 0=egm 1=egm+lpm  set as 0
   real, parameter :: amode=0.  ! source mode 0=egm 1=egm+lpm  set as 0
!
   integer :: i,im1,j,ii,s,e,itime,ios,ih,record,l,stat,t
   integer :: error
   integer :: sourceGroup  ! 1=genCoal, 2=genGas and 3=the rest pems source
   logical :: AMGtoGEO

   PIO180=4.0*ATAN(1.0)/180.

!-----------------------------------------------------------------------------|
! Process headers from the point source (pems) files
!-----------------------------------------------------------------------------|
    if(debug)open(outD,file='debug.txt')
    call getarg(1,file1,error)
    if(error==-1)file1='cit2tapmpems_cb05.run'
    open(11,file=trim(file1))
    write(*,*) trim(file1)
2   format()
3   format(a)
    read(11,3) comment
    read(11,3) grid_fname
    write(*,*)'grid_fname:',trim(grid_fname)

!-----------------------------------------------------------------------------|
! Read in flag to convert AMG coordinates to lat/long
!-----------------------------------------------------------------------------|
    read(11,*) comment
    read(11,*) AMGtoGEO
!
    read(11,3) comment
    read(11,3) INpems_fname
    write(*,*)' input pems_fname:',trim(INpems_fname)
    read(11,3) comment
    read(11,*) pmRatio,sourceGroup
    write(*,*)' pmRatio=', pmRatio, sourceGroup
    read(11,3) comment
    read(11,3) OUTpems_fname
    write(*,*)'output pems_fname:',trim(OUTpems_fname)
    done=retrieveCITgrid(40,grid_fname,model,emsn_nx,emsn_ny,      &
          emsn_dx,emsn_dy,emsn_xlong,emsn_ylat,zone)
    if(.not.done)then
      print *,'Error accessing the cit_grid file'
      stop 'Fatal Error (check pre-processor list file)'
    end if
    
!-----------------------------------------------------------------------------|
!  check overlap between CTM and CIT grids
!  CIT format gives SW corner not center as with CTM- move it.
!-----------------------------------------------------------------------------|
    emsn_xlong_offset=emsn_xlong+0.5*emsn_dx
    emsn_ylat_offset=emsn_ylat+0.5*emsn_dy
    emsn_xlong_end=emsn_xlong_offset+(emsn_nx-1)*emsn_dx
    emsn_ylat_end=emsn_ylat_offset+(emsn_ny-1)*emsn_dy
    emsn_xlong_center=(emsn_xlong_offset+ emsn_xlong_end)*0.5
    emsn_ylat_center=(emsn_ylat_offset+ emsn_ylat_end)*0.5
    print *,ADJUSTL(TRIM(model))
    IF(ADJUSTL(TRIM(model))=='CCAM' .OR. ADJUSTL(TRIM(model))=='UM')THEN 
      factor=c_fac*Re*emsn_dy*PIO180*Re*COS(emsn_ylat_center*PIO180)*emsn_dx*PIO180
    ELSE
      factor=c_fac*emsn_dx*emsn_dy
    END IF
     
    if(len_trim(INpems_fname)==0)then
      print *,'Point source file name is blank'
      print *,'Grid number: ',i
      print *,'Please check that all emission files are defined in the GUI'
      stop 'Fatal Error'
    end if

    INpemsFunit=51
    INpemsForm=retrieveCITemsFormat(TRIM(INpems_fname))
    if(INpemsForm)then
       open(unit=INpemsFunit,file=TRIM(INpems_fname),status='old',IOSTAT=ios, &
             form='unformatted')
    else
       open(unit=INpemsFunit,file=TRIM(INpems_fname),status='old',IOSTAT=ios)
    end if

    OUTpemsFunit=53
    OUTpemsForm=retrieveCITemsFormat(TRIM(OUTpems_fname))

    if(OUTpemsForm)then
       open(unit=OUTpemsFunit,file=TRIM(OUTpems_fname),status='unknown',IOSTAT=ios, &
             form='unformatted')
    else
       open(unit=OUTpemsFunit,file=TRIM(OUTpems_fname),status='unknown',IOSTAT=ios)
       write(*,*)'open outpt ASCII file', TRIM(OUTpems_fname)
    end if
    
!-----------------------------------------------------------------------------|
! SHL:  usually check with other ems filse but here no other file so use the same pems
! to maintain consistency
!-----------------------------------------------------------------------------|
    ne=retrieveCIT_speciesNumbers(INpemsFunit,INpems_fname)
    write(*,*)'read_varaible:ne=',ne
    allocate (pems_mw(ne))
    allocate (pems_lcca(ne))
    rewind (INpemsFunit)
    call read_cit_pems_header(INpemsFunit,INpemsForm,INpems_fname,1,      &
          npmax,pems_MasterID,ne,pems_nlcc,pems_lcca,pems_mw,maxpointSources)
    write(*,*)'npmax=',npmax

!-------------------------------------------------------------------------------
! Start generating:Read  output AEMS file name
! First optionally map names for CB05 speciation
!-------------------------------------------------------------------------------
IF(cb05)THEN
  lno=-1
  lno2=-1
  lso2=-1
  lpm25=-1
  ltol=-1
  lxyl=-1
  DO s=1,ne
     IF(pems_lcca(s) == 'NO')lno=s
     IF(pems_lcca(s) == 'NO2')lno2=s
     IF(pems_lcca(s) == 'SO2')lso2=s
     IF(pems_lcca(s) == 'PART')lpm25=s
     IF(pems_lcca(s) == 'TOL')ltol=s
     IF(pems_lcca(s) == 'XYL')lxyl=s
  END DO
  IF(lno+lno2+lso2+lpm25+ltol+lxyl < 6)THEN
    PRINT *,'Cannot find a species'
    PRINT *,'no: ',lno
    PRINT *,'no2: ',lno2
    PRINT *,'so2: ',lso2
    PRINT *,'pm25: ',lpm25
    PRINT *,'ltol: ',ltol
    PRINT *,'lxyl: ',lxyl
    STOP 'fatal error'
  END IF
END IF

!-----------------------------------------------------------------------------|
! Now check to see if some user input sources are requested
! If so modify npmax accordingly
!-----------------------------------------------------------------------------|
    read(11,*)comment
    read(11,*)doUserSources
    npmax1=npmax
    read(11,*) comment
    read(11,*) user_fname
    if(doUserSources)then
      open(unit=8,file=user_fname,status='old')
      read(8,*) comment
      read(8,*)numberSources
      
!-----------------------------------------------------------------------------|
! Modify npmax accordingly
!-----------------------------------------------------------------------------|
      npmax1 = npmax + numberSources
      read(8,*) comment
      read(8,*)startHour,stopHour
      allocate(stack_id(numberSources))
      allocate(stack_x(numberSources))
      allocate(stack_y(numberSources))
      allocate(stack_radius(numberSources))
      allocate(stack_velocity(numberSources))
      allocate(stack_temperature(numberSources))
      allocate(stack_height(numberSources))
      allocate(stack_emissions(ne,numberSources))

      read(8,*) comment
      do s=1,numberSources
        read(8,*)stack_id(s),          &
           stack_x(s),           &
           stack_y(s),           &
           stack_radius(s),      &
           stack_velocity(s),    &
           stack_temperature(s), &
           stack_height(s),      &
          (stack_emissions(e,s),e=1,ne)
!       print *,'Completed reading user emissions from source: ',s
      end do
    end if

!-----------------------------------------------------------------------------|
! Read in time offset
!-----------------------------------------------------------------------------|
    read(11,*,iostat=ios)comment
    offset=0
    if(ios==0)read(11,*)offset
   
!-----------------------------------------------------------------------------|
! Allocate space for source data
!-----------------------------------------------------------------------------|
    allocate (emis(npmax1,ne,num_hrs))
    if(cb05)then
      allocate (emis_cb05(npmax1,ncb05,num_hrs))
      emis_cb05=0.0
    end if
    if(glomap)then
      allocate (emis_glo(npmax1,nGLO,num_hrs))
      emis_glo=0.0
    end if
    if(ltag)then
      allocate (emis_tag(npmax1,ntag,num_hrs))
      emis_tag=0.0
    end if

    allocate (hs(npmax1))
    allocate (rs(npmax1))
    allocate (ts(npmax1,num_hrs))
    allocate (vfs(npmax1,num_hrs))
    allocate (xpoint(npmax1))
    allocate (ypoint(npmax1))
    allocate (ids(npmax1))
    allocate (dum(ne))
    allocate (mwght(ne))
    allocate (mwdummy(ne))
    allocate (lccadummy(ne))
    mwght=pems_mw(:)

    done=writeTAPM_header(OUTpemsFunit,OUTpemsForm,pems_nlcc,pems_lcca,pems_mw)
    IF(OUTpemsForm)THEN
!      WRITE(OUTpemsFunit,ERR=1)emsn_nx,emsn_ny,emsn_dx,emsn_dy,emsn_xlong_center,emsn_ylat_center
      WRITE(OUTpemsFunit,ERR=1)npmax1,num_hrs
    ELSE
!      WRITE(OUTpemsFunit,100,ERR=1)emsn_nx,emsn_ny,emsn_dx,emsn_dy,emsn_xlong_center,emsn_ylat_center
      100 FORMAT(i4,1x,i4,2(F10.3,1x),2(F15.3,1x))
      WRITE(OUTpemsFunit,*,ERR=1)npmax1,num_hrs
    END IF

!-----------------------------------------------------------------------------|
! Loop over CTM data time interval (usually hourly)
!-----------------------------------------------------------------------------|
    emis=0.
    hs=0.
    rs=0.
    ts=0.
    vfs=0.
    xpoint=0.
    ypoint=0.
    ids=0
!   itime=1
    ih=0
    i=0
    j=0
    rewindFlag=.FALSE.
    do t=1,num_hrs !hour loop
      record=0

!-----------------------------------------------------------------------------|
! Inner loop over CIT individual data records
! hst=meter dst=diameter, tst=degC vfst=volumeext rate xt/yt=8n km
! Added hour offset for LST to EST/UTC conversion
!-----------------------------------------------------------------------------|
     do
        if(INpemsForm)then
          read(INpemsFunit,iostat=stat)ih,i,j,idt,hst,dst,tst,vfst,xt,yt,  &
                             (dum(l),l=1,ne)
        else
!         read(INpemsFunit,200,iostat=stat)ih,i,j,idt,hst,dst,tst,vfst,xt,yt,  &
!                               (dum(l),l=1,ne)
!         200 format(1x,i2,i3,i3,i5,6f8.1,/,(8e10.3))
! patch
          read(INpemsFunit,200,iostat=stat)ih,i,j,idt,hst,dst,tst,vfst,xt,yt
          200 format(1x,i2,i3,i3,i5,6f8.1)
          if(i /= 999 .AND. j /= 999)then
            read(INpemsFunit,*,iostat=stat)(dum(l),l=1,ne)
          else
            read(INpemsFunit,'()',iostat=stat)
            read(INpemsFunit,'()',iostat=stat)
          endif
! end of patch
        end if

!-----------------------------------------------------------------------------|
! This is a cludge to remove large power station emissions from the GMR
!-----------------------------------------------------------------------------|
        !IF(hst > 150. .AND. tst > 60.)dum=0.0

        if(stat > 0)then !i/o error; fatal
          print *,'Error reading point source emissions'
          print *,'File name is: ',trim(INpems_fname)
          print *,'Last valid time and source location read are: ',ih,i,j
          print *,'Please check the file for errors'
          stop 'Fatal error'
        else if(stat < 0)then
          exit !eof or eor
        end if !iostat

        if(i == 999)exit  !end of hour

        call find_stack_index(idt,pems_MasterID,npmax,sIndex)
        itime=MOD(ih+offset,24)
        do
          if(itime >= 0)exit
          itime=itime+24
        end do
        itime=itime+1

!-----------------------------------------------------------------------------|
!  load data into master arrays
!  convert diameter to radius
!  convert temperature from degC to degK
!  convert volume flow rate (m3/s) to flow velocity (m/s)
!  convert postion in km to metre
! Correct the bug in calculating the area of the stack circle section (pi*r**2)
!-----------------------------------------------------------------------------|
        if (ih == 23) write(*,*) 'sindex',sindex,ids(sindex)
        ids(sIndex)=idt
        hs(sIndex)=hst
        rs(sIndex)=dst/2.
        ts(sIndex,itime)=tst+273.16
        if(dst > 0.) then
           vfs(sIndex,itime)=vfst/(phi*(rs(sIndex))**2.)
        else
           vfs(sIndex,itime)=0.
        end if

!-----------------------------------------------------------------------------|
!  GEO assumes that AMG coordinates are converted to lat/long
!  This needs to be independent of model which is used for emission scaling
!-----------------------------------------------------------------------------|
!       IF(ADJUSTL(TRIM(model))=='CCAM' .OR. ADJUSTL(TRIM(model))=='UM')THEN 
        IF(AMGtoGEO)THEN
          call TOGEO (xt*1000.,yt*1000.,zone,ypoint(sIndex),xpoint(sIndex))
          ypoint(sIndex)=-ypoint(sIndex) !southern hemisphere
!         ypoint(sIndex)=SIGN(ypoint(sIndex),emsn_ylat_center)
!         IF(ABS(ypoint(sIndex)) > 180. .OR. ABS(xpoint(sIndex)) > 180.)THEN
!           ypoint(sIndex) = 0.0
!           xpoint(sIndex) = 0.0
!         END IF 
!         print *,'xt, yt, x, y: ',xt*1000.,yt*1000.,xpoint(sIndex),ypoint(sIndex)
        else
          xpoint(sIndex)=xt*1000.
          ypoint(sIndex)=yt*1000.
        end if

!-----------------------------------------------------------------------------|
!  CIT emission rate units in ppm/min convert to g/s
!  Modified to filter out PART which is already in g/s
!-----------------------------------------------------------------------------|
        do l=1,pems_nlcc
          if(l == lpm25)then
            emis(sIndex,l,itime)=dum(l)
          else
            emis(sIndex,l,itime)=dum(l)*pems_mw(l)*factor
          end if
        end do
        record=record+1

      end do ! record loop

!-----------------------------------------------------------------------------|
! Hit an end of hour indicator or eof or eor
!-----------------------------------------------------------------------------|
      if(stat < 0)then !eof or eor
        if(debug)write(outD,*)'Hit eof in pems file. Rewinding'
        rewind INpemsFunit
        done=retrieveCITpems_header(ne,INpemsFunit,INpemsForm,nlccDummy,lccaDummy,mwDummy)
        if(.not.done)then
          print *,'Error reading the header of the pems emissions file: ',trim(INpems_fname)
          stop 'Fatal error'
        end if !header read error

!     if(itime==1 .AND. record==0 .AND. rewindFlag)exit !no data in file
        if(ih==0 .AND. record==0 .AND. rewindFlag)exit !no data in file
        rewindFlag=.TRUE.
      else
        if(debug)write(outD,*)'read_cit_aems, hour= ',itime-1, ' has been completed'
    end if  !eof or eor
  end do !hour loop
  
!-----------------------------------------------------------------------------|
!  Now optionally load in user input sources
!-----------------------------------------------------------------------------|
  if(doUserSources)then
    do s=1,numberSources
      l=npmax+s
      ids(l)=stack_id(s)
      xpoint(l)=stack_x(s)
      ypoint(l)=stack_y(s)
      hs(l)=stack_height(s)
      rs(l)=stack_radius(s)
      do i=1,num_hrs
        im1=i-1
        ts(l,i)=stack_temperature(s)
        vfs(l,i)=stack_velocity(s)
        if(im1 >=startHour .and. im1 <= stopHour)then
          do e=1,ne
            emis(l,e,i)=stack_emissions(e,s)
          end do !species
        end if
      end do !hours of data
    end do
  end if !user sources
  
!-----------------------------------------------------------------------------|
!  Add in additional cb05 species
!  First set PM2.5
!----------------- ------------------------------------------------------------|
  do sIndex=1,npmax1
    emis(sIndex,lpm25,:)=emis(sIndex,lpm25,:)*PmRatio
  end do

  if(cb05)then
   if(ltag)emis_tag=0.0
   do itime=1,num_hrs
    itag=1
    do sIndex=1,npmax1
      etha=0.
      iole=0.
      aldx=0.
      levo=0.
      sox1=0.
      sox2=0.
      sox3=0.
      if(hs(sIndex) < 50.)then
        aso4=emis(sIndex,lso2,itime)*so2TOaso4 (sourceGroup)
        nh3=(emis(sIndex,lno,itime)*46./30.+emis(sIndex,lno2,itime))*noxTOnh3(sourceGroup)
      else
        sox1=emis(sIndex,lso2,itime)
        aso4=emis(sIndex,lso2,itime)*so2TOaso4_tall(sourceGroup)
        nh3=(emis(sIndex,lno,itime)*46./30.+emis(sIndex,lno2,itime))*noxTOnh3_tall(sourceGroup)
      end if 
      as10=emis(sIndex,lso2,itime)*so2TOas10(sourceGroup)
      emis(sIndex,lso2,itime)=emis(sIndex,lso2,itime)*    &
                               (1.-(so2TOaso4(sourceGroup) +so2TOas10(sourceGroup)))
      oc25=emis(sIndex,lpm25,itime)*pm25TOoc25(sourceGroup)
      oc10=emis(sIndex,lpm25,itime)*pm25TOoc10(sourceGroup)
      ec25=emis(sIndex,lpm25,itime)*pm25TOec25(sourceGroup)
      ec10=emis(sIndex,lpm25,itime)*pm25TOec10(sourceGroup)
      ot25=emis(sIndex,lpm25,itime)*pm25TOot25(sourceGroup)
      ot10=emis(sIndex,lpm25,itime)*pm25TOot10(sourceGroup)
!
      ftol=emis(sIndex,ltol,itime)*tolTOptol(sourceGroup)
      fxyl=emis(sIndex,lxyl,itime)*xylTOpxyl(sourceGroup)
      fbnz=emis(sIndex,ltol,itime)*tolTOpbnz(sourceGroup)
!
      do v=1,nvbs
        apa(v)=(oc25+oc10)*vbs(v)
      end do
!
!
!  NH3 removed as this is now in the OEH inventory
!  mec csiro 18/7/2015
!     emis_cb05(sIndex,1,itime)=nh3
!     emis_cb05(sIndex,2,itime)=etha
!     emis_cb05(sIndex,3,itime)=iole
!     emis_cb05(sIndex,4,itime)=aldx
!     emis_cb05(sIndex,5,itime)=oc25
!     emis_cb05(sIndex,6,itime)=oc10
!     emis_cb05(sIndex,7,itime)=ec25
!     emis_cb05(sIndex,8,itime)=ec10
!     emis_cb05(sIndex,9,itime)=ot25
!     emis_cb05(sIndex,10,itime)=ot10
!     emis_cb05(sIndex,11,itime)=aso4
!     emis_cb05(sIndex,12,itime)=as10
!     emis_cb05(sIndex,13:12+nvbs,itime)=apa(:)
!     emis_cb05(sIndex,13+nvbs,itime)=ftol
!     emis_cb05(sIndex,14+nvbs,itime)=fxyl
!     emis_cb05(sIndex,15+nvbs,itime)=levo

      emis_cb05(sIndex,1,itime)=etha
      emis_cb05(sIndex,2,itime)=iole
      emis_cb05(sIndex,3,itime)=aldx
      emis_cb05(sIndex,4,itime)=oc25
      emis_cb05(sIndex,5,itime)=oc10
      emis_cb05(sIndex,6,itime)=ec25
      emis_cb05(sIndex,7,itime)=ec10
      emis_cb05(sIndex,8,itime)=ot25
      emis_cb05(sIndex,9,itime)=ot10
      emis_cb05(sIndex,10,itime)=aso4
      emis_cb05(sIndex,11,itime)=as10
      emis_cb05(sIndex,12:11+nvbs,itime)=apa(:)
      emis_cb05(sIndex,12+nvbs,itime)=ftol
      emis_cb05(sIndex,13+nvbs,itime)=fxyl
      emis_cb05(sIndex,14+nvbs,itime)=fbnz
      emis_cb05(sIndex,15+nvbs,itime)=levo
!
       emis_cb05(sIndex,16+nvbs,itime)=sox1
       emis_cb05(sIndex,17+nvbs,itime)=sox2
       emis_cb05(sIndex,18+nvbs,itime)=sox3

!-----------------------------------------------------------------------------|
! Add in tagged sources. Take first ntag sources with hs >= 140m  
!-----------------------------------------------------------------------------|
     if(ltag)then
      if(hs(sIndex) >= 140. .AND. itag <= halfntag)then
        emis_tag(sIndex,itag,itime)=emis(sIndex,lso2,itime)
        emis_tag(sIndex,itag+halfntag,itime)=emis(sIndex,lso2,itime)
        itag=itag+1
      end if
     end if
 
!-------------------------------------------------------------------------------
!  GLOMAP
! .. Calculate total particle volume (nm3 per m2 per s)
! .. Calculate total particle number (per m2 per s)
!  Sulfate - load cmp mass into su2 and su3, number into aits, accs
!-------------------------------------------------------------------------------
      IF(glomap)THEN
        su1=0.0
        su2=0.0
        su3=0.0
        su4=0.0
        bc2=0.0
        bc3=0.0
        bc4=0.0
        bc5=0.0
        oc1=0.0
        oc2=0.0
        oc3=0.0
        oc4=0.0
        oc5=0.0
        du3=0.0
        du4=0.0
        du6=0.0
        du7=0.0
        nnucs=0.0
        naits=0.0
        naccs=0.0
        ncoas=0.0
        naiti=0.0
        nacci=0.0
        ncoai=0.0

!-------------------------------------------------------------------------------
!   Soluble nucleation mode
!-------------------------------------------------------------------------------
        !su1=f_su(1)*aso4
        !emis_glo(sIndex,1,itime)=su1
        !
        !modevol=1.0E27*(su1)/rhocomp(su_p)*1.0E-03        
        !lgsd=LOG(gsig_su(1))
        !nnucs=modevol/((ppi/6.0)*(d_su(1)**3.0)*EXP(4.5*lgsd*lgsd))     
        !emis_glo(sIndex,18,itime)=nnucs

!-------------------------------------------------------------------------------
!   Soluble aitken mode
!-------------------------------------------------------------------------------
        !su2=f_su(2)*aso4
        !emis_glo(sIndex,2,itime)=su2
        !
        !modevol=1.0E27*(su2)/rhocomp(su_p)*1.0E-03        
        !lgsd=LOG(gsig_su(2))
        !naits=modevol/((ppi/6.0)*(d_su(2)**3.0)*EXP(4.5*lgsd*lgsd))     
        !emis_glo(sIndex,19,itime)=naits
        
!-------------------------------------------------------------------------------
!   Soluble Accumulation mode
!-------------------------------------------------------------------------------
        su3=f_su(1)*(aso4+as10)
        emis_glo(sIndex,3,itime)=su3
        
        modevol=1.0E27*(su3)/rhocomp(su_p)*1.0E-03        
        lgsd=LOG(gsig_su(1))
        naccs=modevol/((ppi/6.0)*(d_su(1)**3.0)*EXP(4.5*lgsd*lgsd))     
        emis_glo(sIndex,20,itime)=naccs

!-------------------------------------------------------------------------------
!   Soluble coarse mode
!-------------------------------------------------------------------------------
        su4=f_su(2)*(aso4+as10)
        emis_glo(sIndex,4,itime)=su4
        
        modevol=1.0E27*(su4)/rhocomp(su_p)*1.0E-03        
        lgsd=LOG(gsig_su(2))
        ncoas=modevol/((ppi/6.0)*(d_su(2)**3.0)*EXP(4.5*lgsd*lgsd))     
        emis_glo(sIndex,21,itime)=ncoas
  
!-------------------------------------------------------------------------------
!   BC and EC
!   InSoluble Aitken mode
!-------------------------------------------------------------------------------
        bc5=f_ecoc(1)*(ec25+ec10)
        !emis_glo(sIndex,7,itime)=bc5
        emis_glo(sIndex,8,itime)=bc5

        oc5=0.0
        DO s=1,nInvolatile
          oc5=oc5+apa(s)
        END DO
!       emis_glo(sIndex,11,itime)=oc5
        emis_glo(sIndex,13,itime)=oc5
           
        modevol=1.0E27*(bc5/rhocomp(bc_p)+oc5/rhocomp(oc_p))*1.0E-03       
        lgsd=LOG(gsig_ecoc(1))
        naiti=modevol/((ppi/6.0)*(d_ecoc(1)**3.0)*EXP(4.5*lgsd*lgsd))
        !emis_glo(sIndex,19,itime)=naiti
        emis_glo(sIndex,22,itime)=naiti

!-------------------------------------------------------------------------------
!   Other
!   Load into accumulation mode insoluble
!-------------------------------------------------------------------------------
        du6=f_du(1)*ot25
        !emis_glo(sIndex,14,itime)=du6
        emis_glo(sIndex,16,itime)=du6

        modevol=1.0E27*(du6)/rhocomp(du_p)*1.0E-03        
        lgsd=LOG(gsig_du(1))
        nacci=modevol/((ppi/6.0)*(d_du(1)**3.0)*EXP(4.5*lgsd*lgsd))
        !emis_glo(sIndex,20,itime)=nacci
        emis_glo(sIndex,23,itime)=nacci
       
      end if !glomap
      
     end do
   end do
  end if

!-----------------------------------------------------------------------------|
!  Now write output in TAPM format
!  A few options are available
! 1/ formatted/unformatted
! 2/ CBO5 or CIT
! 3/ If CBO5, add tagged species?
! 4/ if CB05, add GLOMAP species?
!-----------------------------------------------------------------------------|
  if(OUTpemsForm)then
    do i=1,npmax1
!     write(OUTpemsFunit,err=1) mode,xpoint(i),ypoint(i),hs(i),ds(i),amode+1,amode,amode
      write(OUTpemsFunit,err=1) ids(i),xpoint(i),ypoint(i),hs(i),rs(i),amode+1,amode,amode
    enddo
    if(cb05)then
      if(glomap)then
        do i=1,num_hrs
          do ii=1,npmax1
            write(OUTpemsFunit,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne), &
                                                        (emis_cb05(ii,l,i),l=1,ncb05), &
                                                        (emis_glo(ii,l,i),l=1,nglo)
          end do !point sources
        end do !hrs
      else !no glomap
        if(ltag)then
          do i=1,num_hrs
            do ii=1,npmax1
              write(OUTpemsFunit,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne), &
                                                          (emis_cb05(ii,l,i),l=1,ncb05), &
                                                          (emis_tag(ii,l,i),l=1,Ntag)
            enddo
          enddo
        else !cb05 only
          do i=1,num_hrs
            do ii=1,npmax1
              write(OUTpemsFunit,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne), &
                                                          (emis_cb05(ii,l,i),l=1,ncb05)
            enddo
          enddo
        endif !tagged species?
      end if!glomap?
    else !no cb05, write out cit species only
      do i=1,num_hrs
        do ii=1,npmax1
          write(OUTpemsFunit,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne)
        enddo
      enddo
    end if
  else !formatted output
    do i=1,npmax1
!     write(OUTpemsFunit,110,err=1) mode,xpoint(i),ypoint(i),hs(i),ds(i),amode+1,amode,amode
      write(OUTpemsFunit,110,err=1) ids(i),xpoint(i),ypoint(i),hs(i),rs(i),amode+1,amode,amode
      if(debug) write(outD, *)i,ids(i)
    enddo
    if(cb05)then
      if(glomap)then
        do i=1,num_hrs
          do ii=1,npmax1
            write(OUTpemsFunit,120,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne), &
                                                            (emis_cb05(ii,l,i),l=1,ncb05), &
                                                            (emis_glo(ii,l,i),l=1,nglo)
          end do !point sources
        end do !hrs
      else !no glomap
        if(ltag)then
          do i=1,num_hrs
            do ii=1,npmax1
              write(OUTpemsFunit,120,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne), &
                                                              (emis_cb05(ii,l,i),l=1,ncb05), &
                                                              (emis_tag(ii,l,i),l=1,Ntag)
            enddo
          enddo
        else !cb05 only
          do i=1,num_hrs
            do ii=1,npmax1
              write(OUTpemsFunit,120,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne), &
                                                              (emis_cb05(ii,l,i),l=1,ncb05)
            enddo
          enddo
        end if
      end if !glomap?
    else !cit emissions only
      do i=1,num_hrs
        do ii=1,npmax1
          write(OUTpemsFunit,120,err=1)vfs(ii,i),ts(ii,i),(emis(ii,l,i), l=1,ne)
        enddo
      enddo
    end if !cb05?
  end if !formatted output?
110 format(i7,7(1x,f11.2))
120 format(f6.2,1x,f6.1,1x,100(E10.3))
   close (OUTpemsFunit)
   close (INpemsFunit)
   stop
1 if(debug) then
    write(outD, *)'Error writing Tapm PEMS to  file ', trim(OUTpems_Fname)
  endif

end program CIT2TAPMpems
      SUBROUTINE TOGEO (EAST,NORTH,ZONE,LAT,LONG)
!                   Convert AMG to geographicals
!
!         CONVERT AMG COORDINATES TO GEOGRAPHICALS
!
!         RIGOROUS FORMULAE
!
!         INPUT -     EAST         -  AMG EAST COORDINATE
!                     NORTH        -  AMG NORTH COORDINATE
!                     ZONE         -  AMG ZONE
!
!         OUTPUT -    LAT          -  LATITUDE - S is positive
!                     LONG         -  LONGITUDE - E is positive
!                                     both are in decimals
!                                          eg 37.5 = 37 deg 30 min
!
      IMPLICIT NONE
      REAL    EAST, NORTH, LAT, LONG, PHIDSH, DPHI, OMEGA,     &
                TRUEN, CMEAST, EQNRTH, EDASH, EDASH2,          &
                TANPHI, TAN2, TAN4, COSPHI, PSI, PSI2, PSI3,   &
                NU, RHO, CM, CIRC, EDSHSQ, SF,                 &
                A0, C, D1, D2, D3, D4
      INTEGER ZONE

      DATA CIRC   / 57.295779513D0 /
      DATA CMEAST / 5D5 /
      DATA EQNRTH / 1D7 /
      DATA SF     / .9996D0 /

      DATA C      / 6399617.225D0 /
      DATA EDSHSQ / 6.739660796D-3 /

      DATA A0     / 111133.348785D0 /
      DATA D1     / 2.518887693D-3 /
      DATA D2     / 3.701129D-6 /
      DATA D3     / 7.448D-9 /
      DATA D4     / 1.7D-11 /
!
!         CALCULATE FOOT LATITUDE
!
      TRUEN = (NORTH-EQNRTH)/SF
      PHIDSH = TRUEN/(A0*CIRC)
      DPHI = D1*SIN(2*PHIDSH) + D2*SIN(4*PHIDSH)   &
           + D3*SIN(6*PHIDSH) + D4*SIN(8*PHIDSH)
      PHIDSH = PHIDSH + DPHI
!
!         CALCULATE VALUES OF FUNCTIONS FOR FOOT LATITUDE
!
      TANPHI = ATAN(PHIDSH)
      TAN2 = TANPHI*TANPHI
      TAN4 = TAN2*TAN2
      COSPHI = COS(PHIDSH)
      PSI = 1. + EDSHSQ * COSPHI*COSPHI
      PSI2 = PSI*PSI
      PSI3 = PSI2*PSI
      NU = C/SQRT(PSI)
      RHO = NU/PSI
      EDASH = (EAST-CMEAST)/(SF*NU)
      EDASH2 = EDASH*EDASH
!
!         CALCULATE CORRECTION TO FOOT LATITUDE
!
      DPHI  = EDASH2/2.                                                   &
            - EDASH2**2/24.  * (-4.*PSI2 + 9.*PSI*(1.-TAN2) + 12.*TAN2)   &
            + EDASH2**3/720. * (8.*PSI2*PSI2*(11.-24.*TAN2)               &
                             - 12.*PSI3*(21.-71.*TAN2)                    &
                             + 15.*PSI2*(15.-98.*TAN2+15.*TAN4)           &
                             + 180.*PSI*(5.*TAN2-3.*TAN4) + 360.*TAN4)

      DPHI  = DPHI * PSI*TANPHI
      LAT   = -CIRC*(PHIDSH - DPHI)
!
!         CALCULATE LONGITUDE DIFFERENCE FROM CENTRAL MERIDIAN
!
      CM = 6.*(ZONE-49.)+111.

      OMEGA = 1.                                         &
            - EDASH2/6.      * (PSI + 2.*TAN2)           &
            + EDASH2**2/120. * (-4.*PSI3*(1.-6.*TAN2)    &
                             + PSI2*(9.-68.*TAN2)        &
                             + 72.*PSI*TAN2 + 24.*TAN4)

      OMEGA = OMEGA * EDASH/COSPHI
      LONG  = CM + OMEGA*CIRC

      RETURN
      END
