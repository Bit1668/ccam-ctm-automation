!-------------------------------------------------------------------------------
!> i/o, date conversion and line compression routines
!> @author
!> Martin Cope CSIRO
!-------------------------------------------------------------------------------
MODULE utilities
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC surfer_dump,JulianDayNumber,GregorianDate,Compress,UpperCase
CONTAINS

!-------------------------------------------------------------------------------
!  surfer_dump
!-------------------------------------------------------------------------------
!> Routine dumps out data in Surfer ASCII format
!> @author
!> Martin Cope CSIRO
!
!! \param iunit Fortran output unig
!! \param x0 centre of sw cell (user coordinates)
!! \param y0 centre of sw cell (user coordinates)
!! \param dx grid spacing (user coordinates)
!! \param dy grid spacing (user coordinates)
!! \param grid1 raster data set
!! \param filename name of output file
!
! Modifications
!  When         Who      What
!-------------------------------------------------------------------------------
SUBROUTINE surfer_dump(iunit,x0,y0,dx,dy,grid1,filename)
IMPLICIT NONE
INTEGER, INTENT(IN) :: iunit                !Fortran I/O unit
REAL, INTENT(IN) :: x0,y0,dx,dy             !grid location/size
REAL, INTENT(IN), DIMENSION(:,:) :: grid1  !data
REAL, ALLOCATABLE, DIMENSION(:,:)  :: grid
CHARACTER(LEN=*), INTENT(IN) :: filename    !output filename
INTEGER :: nx,ny                !grid dimensions
INTEGER :: j
!
nx=SIZE(grid1,DIM=1)
ny=SIZE(grid1,DIM=2)
ALLOCATE(grid(nx,ny))
grid=grid1

!-------------------------------------------------------------------------------
! Open the file unit write out header information
!-------------------------------------------------------------------------------
OPEN(UNIT=iunit,FILE=TRIM(filename),STATUS='unknown')
!
WRITE(iunit,'(a)')'DSAA'
WRITE(iunit,'(i6,1x,i6)')nx,ny
WRITE(iunit,*)x0,x0+FLOAT(nx-1)*dx
WRITE(iunit,*)y0,y0+FLOAT(ny-1)*dy
IF(ABS(MINVAL(grid)-MAXVAL(grid))< 1.0E-15)grid(1,1)=0.001
WRITE(iunit,*)MINVAL(grid),MAXVAL(grid)
!
DO j=1,ny
  WRITE(iunit,101)grid(:,j)
! 101 FORMAT(10(e10.3,1x))
  101 FORMAT(100(e14.7,1x))
! WRITE(iunit,'()')
END DO
CLOSE(iunit)
DEALLOCATE(grid)
END SUBROUTINE surfer_dump

FUNCTION JulianDayNumber(y,m,d)
!------------------------------------------------------------------------------------|
! Function to calculate Julian Day Number
!------------------------------------------------------------------------------------|
IMPLICIT NONE
INTEGER :: JulianDayNumber
INTEGER, INTENT(IN) :: y !year
INTEGER, INTENT(IN) :: d !day
INTEGER, INTENT(IN) :: m !month

JulianDayNumber = ( 1461 * ( y + 4800 + ( m - 14 ) / 12 ) ) / 4 +   &
                 ( 367 * ( m - 2 - 12 * ( ( m - 14 ) / 12 ) ) ) / 12 - &
                 ( 3 * ( ( y + 4900 + ( m - 14 ) / 12 ) / 100 ) ) / 4 + &
                  d - 32075
END FUNCTION JulianDayNumber

!-------------------------------------------------------------------------------
!  GregorianDate
!-------------------------------------------------------------------------------
!> Returns year, month, day given Julian day
!> @author
!> Martin Cope CSIRO
!! \param jd Julian Day
!! \result GregorianDate (1) year, (2) month, (3) day
!
! Modifications
!  When         Who      What
!-------------------------------------------------------------------------------
FUNCTION GregorianDate(jd)
IMPLICIT NONE
INTEGER, DIMENSION(3) :: GregorianDate
INTEGER, INTENT(IN) :: jd !julian day number
INTEGER :: y,m,d          !year,month,day
INTEGER :: l,n,i,j        !intermediates

l = jd + 68569
n = ( 4 * l ) / 146097
l = l - ( 146097 * n + 3 ) / 4
i = ( 4000 * ( l + 1 ) ) / 1461001
l = l - ( 1461 * i ) / 4 + 31
j = ( 80 * l ) / 2447
d = l - ( 2447 * j ) / 80
l = j / 11
m = j + 2 - ( 12 * l )
y = 100 * ( n - 49 ) + i + l

GregorianDate(1)=y
GregorianDate(2)=m
GregorianDate(3)=d
END FUNCTION GregorianDate

!-------------------------------------------------------------------------------
!  Compress
!-------------------------------------------------------------------------------
!> Function to remove in-text blanks and right-pad with blanks
!> @author
!> Martin Cope CSIRO
!
!! \param text text string with embedded blanks
!! \result Compress text string without the blanks
!
! Modifications
!  When         Who      What
!-------------------------------------------------------------------------------
FUNCTION Compress(text)
 IMPLICIT NONE
 CHARACTER(LEN=*), INTENT(IN) :: text
 CHARACTER(LEN=:), ALLOCATABLE :: compress
 INTEGER :: length,i,ip1
!
 compress=TRIM(text)
 length=LEN_TRIM(text)
 IF(length > 0)THEN
   i=1
   DO
     IF(i >= length)EXIT
     ip1=i+1
     IF(compress(i:i) == ' ')THEN
       compress(i:length-1)=compress(ip1:length)
       compress(length:length)=' '
       length=length-1
     END IF
     i=ip1
   END DO
 END IF
END FUNCTION Compress

!-------------------------------------------------------------------------------
!  UpperCase
!-------------------------------------------------------------------------------
!> Function to convert characters from lower to upper case
!> @author
!> Martin Cope CSIRO
!
!! \param string text string with embedded blanks
!! \result UpperCase text string without the blanks
!
! Modifications
!  When         Who      What
! 03/05/17      mec      Cleaned up version of CTM routine
!-------------------------------------------------------------------------------
FUNCTION UpperCase(string)
IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN) :: string
CHARACTER(LEN=:), ALLOCATABLE :: UpperCase
INTEGER :: length,i,uppc
!
UpperCase=Compress(string)
uppc=ICHAR('a')-ICHAR('A')
IF(uppc==0)RETURN
length=LEN_TRIM(UpperCase)
IF(length > 0)THEN
  DO i=1,length
    IF(UpperCase(i:i) >='a' .AND. UpperCase(i:i)<='z') &
       UpperCase(i:i)=CHAR(ICHAR(UpperCase(i:i))-uppc)
  END DO
END IF
END FUNCTION UpperCase
END MODULE utilities
