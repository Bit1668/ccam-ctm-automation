!-------------------------------------------------------------------------------
!  fueldata
!-------------------------------------------------------------------------------
!> \brief Fuel density files
!> VAST, DELWP, RFS
!> Store fine fuels; coarse woody debris
!> @author
!> Martin Cope CSIRO
!
! Modifications
!  When         Who      What
! 15/05/17      mec      Added parameters for plume rise type and heat flux
! 02/05/17      mec      Fuel layers are now defined in the configuration file
!-----------------------------------------------------------------------------|
MODULE fueldata
IMPLICIT NONE
SAVE

PRIVATE
PUBLIC n_fl,nv_fl,ps_fl,pe_fl,               &
       nx_fl,ny_fl,fn_fl,scal_fl,            &
       x0_fl,y0_fl,dx_fl,fuellayer,          &
       burn_desc,plume_desc,                 &
       fine,cwd,flmg,smldr,                  &
       patchiness,hflx,                      &
       k_rdr,rdr,decay,max_smoulder_time,    &
       kgCtokgDry,do_smoulder
!
INTEGER :: n_fl   !number of fuel layer file sets
INTEGER :: nv_fl !length of fuel layer vector
INTEGER, ALLOCATABLE, DIMENSION(:) :: ps_fl  !points to start of a fuel layer
INTEGER, ALLOCATABLE, DIMENSION(:) :: pe_fl  !points to end of a fuel layer
!
INTEGER, ALLOCATABLE, DIMENSION(:) :: nx_fl !fuel layer dimensions
INTEGER, ALLOCATABLE, DIMENSION(:) :: ny_fl !fuel layer dimensions
REAL, ALLOCATABLE, DIMENSION(:) :: x0_fl    !fuel layer origin (deg;m)
REAL, ALLOCATABLE, DIMENSION(:) :: y0_fl    !fuel layer origin (deg;m)
REAL, ALLOCATABLE, DIMENSION(:) :: dx_fl    !fuel layer grid spacing (deg;m)
REAL, ALLOCATABLE, DIMENSION(:,:) :: scal_fl!scale factors for each fuel layer
REAL, ALLOCATABLE, DIMENSION(:,:) :: fuellayer  !fuel density (tonnes-dry/ha)
CHARACTER(LEN=1024), ALLOCATABLE, DIMENSION(:) :: fn_fl !fuel layer names
!
REAL, PARAMETER :: kgCtokgDry=0.50       !ratio of carbon mass burned to dry fuel mass burned
REAL, PARAMETER :: max_smoulder_time=96  !track smouldering from up to x-hr old fires
REAL :: patchiness                       !burn patchiness (all fires)
!
REAL            :: k_rdr                 !smouldering residence time coefficient
REAL            :: rdr                   !residence time (hr)           
REAL :: decay                            !smouldering decay rate (1/hr)
!
INTEGER, PARAMETER :: fine=1           !fine fuels
INTEGER, PARAMETER :: cwd =2           !coarse woody debris
INTEGER, PARAMETER :: flmg=1           !flaming fire
INTEGER, PARAMETER :: smldr=2          !smouldering fire
CHARACTER(LEN=10), DIMENSION(2) :: burn_desc=(/'flaming','smouldering'/)
CHARACTER(LEN=10), DIMENSION(2) :: plume_desc=(/'MIXED','LOFTED'/)
REAL, DIMENSION(2)              :: hflx !smoke heat flux (W/m2) or moisture (%)
!
LOGICAL, PARAMETER :: do_smoulder=.true.
END MODULE fueldata
!
!-----------------------------------------------------------------------------|
!> Store emission speciation for various fire behaviour and veg biomes
!> @author
!> Martin Cope CSIRO
!
!> @todo Make emission temporal profiles selectable
!
! Modifications
!  When         Who      What
! 02/05/17      mec  Relative hourly emissions profile now read from the config file
!-----------------------------------------------------------------------------|
MODULE emissions
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC nspec,n_sspg,name_nspec,mw,FireFactors,nems,name_ems,map_ems,   &
       mpped_mw,mcfr,hprof,fn_sspg,name_sspg,mcfr_readfromfile
!
INTEGER :: nspec   !number of species in each smoke speciation group
INTEGER :: n_sspg   !number of smoke speciation groups
REAL, ALLOCATABLE, DIMENSION(:) :: mw !molecular weight (g)
CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: name_nspec
CHARACTER(LEN=512), ALLOCATABLE, DIMENSION(:) :: name_sspg
CHARACTER(LEN=512) :: fn_sspg  !name of smoke speciation file
REAL, ALLOCATABLE, DIMENSION(:,:) :: FireFactors
INTEGER :: landuse             !speciation landuse category
INTEGER :: nems
CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: name_ems
INTEGER, ALLOCATABLE, DIMENSION(:) :: map_ems
REAL, ALLOCATABLE, DIMENSION(:) :: mpped_mw
!
!REAL, DIMENSION(0:23) :: mcfr  !relative 24-h emissions profile 
!REAL, DIMENSION(0:23) :: hprof !normalised hourly emission profile
!REAL, DIMENSION(0:1023) :: mcfr  !relative 24-h emissions profile 
!REAL, DIMENSION(0:1023) :: hprof !normalised hourly emission profile
REAL, ALLOCATABLE, DIMENSION(:) :: mcfr, mcfr_readfromfile !relative 24-h emissions profile 
REAL, ALLOCATABLE, DIMENSION(:) :: hprof !normalised hourly emission profile
END MODULE emissions

!-----------------------------------------------------------------------------|
!> Parameters for a dummy .pse file.
!> This is output if the software fails due to errors or lack of input data
!> @author
!> Martin Cope CSIRO
!-----------------------------------------------------------------------------|
MODULE DummySource
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC dmy_hrs,dmy_ncmts,dmy_comment,     &
       dmy_ns,dmy_x,dmy_y,dmy_hs,dmy_vs,dmy_rs,dmy_ts
!
INTEGER, PARAMETER :: dmy_hrs=24
INTEGER, PARAMETER :: dmy_ncmts=3
CHARACTER(LEN=80), DIMENSION(dmy_ncmts) :: dmy_comment =(/    &
                            'Dummy pse file with zero emissions',   &
                            'Written by build_hourlyFire_emissions',&
                            '*'   /)
INTEGER, PARAMETER :: dmy_ns=1
REAL, PARAMETER :: dmy_x=0.0
REAL, PARAMETER :: dmy_y=0.0
REAL, PARAMETER :: dmy_hs=0.0
REAL, PARAMETER :: dmy_vs=1.0
REAL, PARAMETER :: dmy_rs=1.0
REAL, PARAMETER :: dmy_ts=298.0
END MODULE DummySource

!-----------------------------------------------------------------------------|
!> Parameters used for outputting diagnostic data
!> @author
!> Martin Cope CSIRO
!-----------------------------------------------------------------------------|
MODULE diagnostics
IMPLICIT NONE
SAVE
CHARACTER(LEN=1), PARAMETER :: cma=','
END MODULE diagnostics

!-------------------------------------------------------------------------------
!> Routines for calculating and outputing hourly smoke emissions
!> @author
!> Martin Cope CSIRO
!-------------------------------------------------------------------------------
MODULE smoke_utilities
USE DummySource
USE utilities
IMPLICIT NONE
SAVE
PRIVATE
PUBLIC writePSE_header,readScarFile_meta,readScarFile_data,     &
       writeZeroEmissions,writeFlamingAndSmoulderingEmissions,  &
       writeZeroFlamingAndSmoulderingEmissions,                 &
       writeSmoulderingEmissions,                               &
       read_fl_header,read_fl_grid
CONTAINS

!-------------------------------------------------------------------------------
!  read_fl_header
!-------------------------------------------------------------------------------
!> Read the meta data from a fuel layer files in ESRI .asc format
!> @author
!> Martin Cope CSIRO
!! \param funit Fortan i/o unit
!! \param fn Name of the fuel layer file
!! \param nx,ny Number of data points
!! \param x0,y0 Origin of the fuel layer grid (deg;m)
!! \param dx Grid spacing (deg;m)
!
! Modifications
!  When         Who      What
! 03/05/17      mec     v1.0
!-----------------------------------------------------------------------------|
FUNCTION read_fl_header(funit,fn,nx,ny,x0,y0,dx) RESULT(done)
IMPLICIT NONE
LOGICAL :: done
CHARACTER(LEN=*), INTENT(IN) :: fn
INTEGER, INTENT(IN) :: funit
INTEGER, INTENT(OUT) :: nx,ny
REAL, INTENT(OUT) :: x0,y0,dx
!
INTEGER :: ios
CHARACTER(LEN=512) :: line
!
done=.FALSE.
OPEN(UNIT=funit,FILE=TRIM(fn),STATUS='OLD',IOSTAT=ios)
IF(ios /= 0)THEN
  PRINT *,'Error opening: ',TRIM(fn)
  RETURN
END IF

!-------------------------------------------------------------------------------
! Read in the meta data
!-------------------------------------------------------------------------------
READ(funit,1)line
READ(line(14:),*)nx
READ(funit,1)line
READ(line(14:),*)ny
READ(funit,1)line
READ(line(14:),*)x0
READ(funit,1)line
READ(line(14:),*)y0
READ(funit,1)line
READ(line(14:),*)dx
CLOSE(funit)
done=.TRUE.
1 FORMAT(a)
END FUNCTION read_fl_header

!-------------------------------------------------------------------------------
!  read_fl_grid
!-------------------------------------------------------------------------------
!> Read the meta data from a fuel layer files in ESRI .asc format
!> @author
!> Martin Cope CSIRO
!! \param funit Fortan i/o unit
!! \param nx Number of e/w points
!! \param ny Number of n/s points
!! \param fn Name of the fuel layer file
!! \param scal Global scal factor (e.g. kg-C to kg-dry)
!! \param grid Returned data (kg-dry/ha)
!
! Modifications
!  When         Who      What
! 03/05/17      mec     v1.0
!-----------------------------------------------------------------------------|
FUNCTION read_fl_grid(funit,nx,ny,fn,scal,grid) RESULT(done)
IMPLICIT NONE
LOGICAL :: done
CHARACTER(LEN=*), INTENT(IN) :: fn
INTEGER, INTENT(IN) :: funit
INTEGER, INTENT(IN) :: nx,ny
REAL, INTENT(IN) :: scal
REAL, DIMENSION(:), INTENT(OUT) :: grid
!
INTEGER :: i,ios,l,x,y
CHARACTER(LEN=512) :: line
!
line=''
!
done=.FALSE.
OPEN(UNIT=funit,FILE=TRIM(fn),STATUS='OLD',IOSTAT=ios)
IF(ios /= 0)THEN
  PRINT *,'Error opening: ',TRIM(fn)
  RETURN
END IF

!-------------------------------------------------------------------------------
! Read over the meta data
!-------------------------------------------------------------------------------
DO i=1,6
  READ(funit,1)line
END DO
!
DO y=ny,1,-1
  l=nx*(y-1)
  READ(funit,*,IOSTAT=ios)(grid(l+x),x=1,nx)
  IF(ios /=0)EXIT
END DO
!
IF(ios /= 0)THEN
  PRINT *,'Error reading: ',TRIM(fn)
  RETURN
END IF
!
grid=grid*scal
CLOSE(funit)
done=.TRUE.
1 FORMAT(a)
END FUNCTION read_fl_grid

!-------------------------------------------------------------------------------
!  writePSE_header
!-------------------------------------------------------------------------------
!> Write out a .pse file header
!> @author
!> Martin Cope CSIRO
!! \param funit  Fortan i/o unit
!! \param fform  Formatted/Binary output
!! \param nspc   Number of species
!! \param species Model species names
!! \param mw Molecular weights (g)
!
! Modifications
!  When         Who      What
! 08/07/16      mec     2016 Tasmanian fires format
! 13/12/15      mec     v1.0
!-----------------------------------------------------------------------------|
FUNCTION writePSE_header(funit,fform,nspc,species,mw)
IMPLICIT NONE
LOGICAL :: writePSE_header !> read a pse header file
INTEGER, INTENT(IN) :: funit  !>file unit
LOGICAL, INTENT(IN) :: fform  !>file format (.true. = binary)
INTEGER, INTENT(IN) :: nspc !>number of inventory species
REAL, DIMENSION(nspc), INTENT(IN) :: mw               !>molecular weights (g)
CHARACTER(LEN=4), DIMENSION(nspc), INTENT(IN) :: species !species names
CHARACTER(LEN=8) :: systemDate
CHARACTER(LEN=10) :: systemTime
CHARACTER(LEN=80) :: comment
INTEGER :: i,c,s
writePSE_header=.FALSE.

!-------------------------------------------------------------------------------
! Generate header for Tapm aems file
! Start with binary output option
!-------------------------------------------------------------------------------
CALL DATE_and_TIME(systemDate,systemTime)
IF(Fform)THEN
  comment='Version_01'
  WRITE(funit,ERR=94)comment
  comment='CTM pse emissions file. Generated by build_hourlyfire_emissions'
  WRITE(funit,ERR=94)comment
  comment='File was created (yyyymmdd): '//systemDate//' at time (hhmmss.sss): '//systemTime
  DO c=1,dmy_ncmts
     WRITE(funit,ERR=94)dmy_comment(c) !note final comment is a '*'
  END DO

!-------------------------------------------------------------------------------
! Write out species list
!-------------------------------------------------------------------------------
  WRITE(funit,ERR=94)nspc
  DO s=1,nspc
    WRITE(funit,ERR=94)s,species(s),mw(s)
  END DO

!-------------------------------------------------------------------------------
! Write out source time invariant data
!-------------------------------------------------------------------------------
  WRITE(funit,ERR=94)dmy_ns,dmy_hrs
  DO s=1,dmy_ns
    WRITE(funit,ERR=94)s,dmy_x,dmy_y,dmy_hs,dmy_rs,1.,0.,0.
  END DO

!-------------------------------------------------------------------------------
! ASCII
!-------------------------------------------------------------------------------
ELSE
  WRITE(funit,1,ERR=94)'Version_01'
  WRITE(funit,1,ERR=94)'CTM pse emissions file. Generated by build_hourlyfire_emissions'
1 FORMAT(100a)
  WRITE(funit,1,ERR=94)'File was created (yyyymmdd): ',systemDate,' at time (hhmmss.sss): ',systemTime
  DO c=1,dmy_ncmts
     WRITE(funit,1,ERR=94)dmy_comment(c)
  END DO

!-------------------------------------------------------------------------------
! Write out species list
!-------------------------------------------------------------------------------
  WRITE(funit,3,ERR=94)nspc
  DO s=1,nspc
    WRITE(funit,2,ERR=94)s,species(s),mw(s)
  END DO

!-------------------------------------------------------------------------------
! Write out source time invariant data
!-------------------------------------------------------------------------------
  WRITE(funit,3,ERR=94)dmy_ns,dmy_hrs
  DO s=1,dmy_ns
    WRITE(funit,4,ERR=94)s,dmy_x,dmy_y,dmy_hs,dmy_rs,0.,0.,0.
  END DO
END IF !binary/ascii
writePSE_header=.TRUE.
RETURN
2 FORMAT(i3,1x,a,1x,F5.1)
3 FORMAT(2(i3,1x))
4 FORMAT(i7,7(1x,f11.2))

!-------------------------------------------------------------------------------
! Errors
!-------------------------------------------------------------------------------
94 PRINT *,'Error writing PSE header to file'
END FUNCTION writePSE_header

!-------------------------------------------------------------------------------
!  readScarFile_meta
!-------------------------------------------------------------------------------
!> Read meta data from a fire scar file
!> @author
!> Martin Cope CSIRO
!! \param jdFs fire scar start Julian day
!! \param hrFs fire scar start hour (LST)
!! \param jdFe fire scar end Julian day
!! \param hrFe fire scar end hour (LST)
!! \param nF number of points in scar raster
!! \param tzone time zone of LST
!! \param scarFileName name of fire scar
!! \param location location of scar data file
!! \result readscar true if a successful read
!
! Modifications
! when     who    what
!29/05/20  XB    adding the optional argument of the resolution in m of the fire scar. returns unchanged if not found.
!03/01/17  mec   header now contains fire start/end date-hours
!21/12/16  mec   check flag in fire raster to see if a cell is burning
!-------------------------------------------------------------------------------
LOGICAL FUNCTION readScarFile_meta(jdFs,hrFs,jdFe,hrFe,nF,tzone, ResolutionInM,   &
                                   scarFileName,location) RESULT(readScar)
USE diagnostics
IMPLICIT NONE
INTEGER, INTENT(IN)  :: tzone !model data time difference
INTEGER, INTENT(OUT) :: jdFs,jdFe  !date of scar
INTEGER, INTENT(OUT) :: hrFs,hrFe  !hour of scar
INTEGER, INTENT(OUT) :: nF   !number of points in the scar
real, INTENT(INOUT) :: ResolutionInM  !optional resolution in m of the fire scar
CHARACTER(LEN=*), INTENT(IN) :: scarFileName
CHARACTER(LEN=*), INTENT(IN) :: location
!
INTEGER :: yrF,mthF,dyF,minF,ios
REAL :: lat,lng, ResolutionInM_old 
INTEGER :: burn
CHARACTER(LEN=12) :: scarFileDate,scarFileDate1
CHARACTER(LEN=1024) :: text
CHARACTER(LEN=20) :: text2
!
readScar=.FALSE.
PRINT *,'Reading data set for the scarfile: ',TRIM(location)//TRIM(scarFileName)//'.csv'

!-------------------------------------------------------------------------------
! Open up the .csv file containing the coordinates of the fire cells
!-------------------------------------------------------------------------------
OPEN(UNIT=8,FILE=TRIM(location)//TRIM(scarFileName)//'.csv',IOSTAT=ios,STATUS='OLD')
IF(ios /= 0)THEN
  PRINT *,'Error opening: ',TRIM(location)//TRIM(scarFileName)//'.csv' 
  RETURN
END IF
!-------------------------------------------------------------------------------
! Read fire start/end data and times
!-------------------------------------------------------------------------------
! XB here is the trick
!if ResolutionInM is set before reading it, the value doesn't change if the read doesn't find it
!print *, ResolutionInM
ResolutionInM_old = ResolutionInM
READ(8,'(A)',IOSTAT=IOS)text
READ(text,*,IOSTAT=IOS)scarFileDate,scarFileDate1,text2,ResolutionInM
!print  *,ios
IF(ios /= 0)THEN
    !backspace(8)
    READ(text,*,IOSTAT=IOS)scarFileDate,scarFileDate1
    ResolutionInM = ResolutionInM_old
    !print  *,ios
    endif
!print *, text2, ResolutionInM
READ(scarFileDate,'(i4.4,5(i2.2))')yrF,mthF,dyF,hrFs,minF
jdFs=JulianDayNumber(yrF,mthF,dyF)
hrFs=hrFs+NINT(FLOAT(minF)/60.)-tzone
IF(hrFs < 0)THEN
  hrFs=24+hrFs
  jdFs=jdFs-1
ELSE IF(hrFs > 23)THEN
  hrFs=hrFs-24
  jdFs=jdFs+1
END IF
!
READ(scarFileDate1,'(i4.4,5(i2.2))')yrF,mthF,dyF,hrFe,minF
jdFe=JulianDayNumber(yrF,mthF,dyF)
hrFe=hrFe+NINT(FLOAT(minF)/60.)-tzone
IF(hrFe < 0)THEN
  hrFe=24+hrFe
  jdFe=jdFe-1
ELSE IF(hrFe > 23)THEN
  hrFe=hrFe-24
  jdFe=jdFe+1
END IF

!-------------------------------------------------------------------------------
! Count up number of burning cells
!-------------------------------------------------------------------------------
PRINT *,'Counting points in the data set: ',TRIM(scarFileDate)
nF=0
!READ(8,*)
DO
  READ(8,*,IOSTAT=ios)lng,lat,burn
  IF(ios /=0)EXIT
  IF(burn==1)nF=nF+1
END DO
CLOSE(8)

!-------------------------------------------------------------------------------
! If there are no fire cells and this is the first file
!  then write out a dummy .pse file and exit
!-------------------------------------------------------------------------------
IF(nF==0)THEN
  PRINT *,'Error no fire points in the file: ',TRIM(scarFileDate)
  RETURN
ELSE
  PRINT *,'Counted: ',Nf,' fire points in the file: ',TRIM(scarFileDate)
END IF !no fires available for this data file
!
readScar=.TRUE.
1 FORMAT (a)
END FUNCTION readScarFile_meta

!-------------------------------------------------------------------------------
!  readScarFile_data
!-------------------------------------------------------------------------------
!> Read locations of burning raster points
!> @author
!> Martin Cope CSIRO
!! \param scarFileDate date of scar file
!! \param location location of scar data file
!! \param lat array of latitudes
!! \param lon array of longitudes
!! \param nf number of points in scar raster
!! \result readscar true if a successful read
!
! Modifications
!  When         Who      What
! 21/12/16      mec     Only load in burning points in the raster
!-----------------------------------------------------------------------------
LOGICAL FUNCTION readScarFile_data(scarFileDate,location,lat,lon,nf) RESULT(readScar)
USE diagnostics
IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN)   :: scarFileDate  !date of scar file
CHARACTER(LEN=*), INTENT(IN)   :: location
INTEGER, INTENT(IN)            :: nf            !number of data points
REAL, DIMENSION(:), INTENT(INOUT) :: lat,lon    !lat/long of data points
INTEGER :: f,ios
REAL :: lng,ltd
INTEGER :: burn
!
readScar=.FALSE.
!-------------------------------------------------------------------------------
! Open up the .csv file containing the coordinates of the fire cells
!-------------------------------------------------------------------------------
OPEN(UNIT=8,FILE=TRIM(location)//TRIM(scarFileDate)//'.csv',IOSTAT=ios,STATUS='OLD')
IF(ios /= 0)THEN
  PRINT *,'Error opening: ',TRIM(location)//TRIM(scarFileDate)//'.csv' 
  RETURN
END IF

!-------------------------------------------------------------------------------
! Read in lat/lon coordinates
! Now read in header for RFS files
!-------------------------------------------------------------------------------
READ(8,*)
f=0
DO
  READ(8,*,IOSTAT=ios)lng,ltd,burn
  IF(ios /= 0)THEN
    PRINT *,'Error reading fire points in the file: ',TRIM(location)//TRIM(scarFileDate)//'.csv'
    CLOSE(8)
    RETURN
  END IF !error when re-reading the data from this file
!
  IF(burn == 1)THEN
    f=f+1
    lon(f)=lng
    lat(f)=ltd
    IF(f == nf)EXIT
  END IF !burning point
END DO
!
readScar=.TRUE.
CLOSE(8)
END FUNCTION readScarFile_data

!-------------------------------------------------------------------------------
!  writeZeroEmissions
!-------------------------------------------------------------------------------
!> Write out a file of zero emissions
!> @author
!> Martin Cope CSIRO
!
!! \param ASCIIhuh ASCII or binary?
!! \param ts first hour (model time)
!! \param te last hour (model time)
!! \param ns number of source points
!! \param vs smoke rise speed (m/s)
!! \param tp smoke temperature (K)
!! \param ne number of species
!! \result done true if a successful write
!
! Modifications
!  When         Who      What
!-------------------------------------------------------------------------------
LOGICAL FUNCTION writeZeroEmissions(ASCIIhuh,ts,te,ns,vs,tp,ne) RESULT(done)
USE diagnostics
IMPLICIT NONE
LOGICAL, INTENT(IN) :: ASCIIhuh
INTEGER, INTENT(IN) :: ts,te,ns,ne
REAL, INTENT(IN)    :: vs,tp
INTEGER             :: ih,f,s,ios
!
done=.FALSE.
IF(ASCIIhuh)THEN
  DO ih=ts,te
    DO f=1,ns
      WRITE(14,3,IOSTAT=ios)vs,tp,(0.0,s=1,ne)
3     FORMAT(F6.1,',',F4.0,100(',',G14.7))
      IF(ios /= 0)RETURN
    END DO
  END DO
ELSE
  DO ih=ts,te
    DO f=1,ns
      WRITE(14,IOSTAT=ios)vs,tp,(0.0,s=1,ne)
      IF(ios /=0)RETURN
    END DO
  END DO
END IF
done=.TRUE.
END FUNCTION writeZeroEmissions

!-------------------------------------------------------------------------------
!  writeFlamingAndSmoulderingEmissions
!-------------------------------------------------------------------------------
!> Calculate and write out hourly emissions from the flaming and smouldering components of a fire
!> @author
!> Martin Cope CSIRO
!! \param ASCIIhuh ASCII or binary?
!! \param fdiHuh scale emissions by hourly normalised FDI
!! \param ts first hour (model time)
!! \param te last hour (model time)
!! \param tzone conversion from LST to model time
!! \param fl scar number
!! \param n_fp number of active points in each scar
!! \param fp_p point to the next fire scar
!! \param vs smoke rise speed (m/s)
!! \param tp smoke temperature (K)
!! \param burntype biome/mce data set
!! \param fuelBurnt cumulative amount of fuel burnt (kg-dry/cell)
!! \param fuelIgnited_cwd cwd ignited this hour (kg-dry/cell)
!! \result done true if a successful write
!
! Modifications
!  When         Who      What
!  3/12/2019    XB       remove the mod function to avoid repetition of the first day in writing emmissions
!  5/12/2019    XB       change writing format of the hour to accomodate 4 digits   
!  5/05/2020    XB       add tfire var, as the absolute start hour of the fire. needed to compute the local hour of the fire to synchronise the emission profile function   
!-------------------------------------------------------------------------------
LOGICAL FUNCTION writeFlamingAndSmoulderingEmissions(ASCIIhuh,fdiHuh,ts,te,tzone,tfire,&
                                  fl,n_fp,fp_p,                   &
                                  vs,tp,burntype,fuelBurnt,fuelIgnited_cwd,OneFileOnly) RESULT(done)
USE diagnostics
USE emissions
USE fueldata, ONLY: fine,cwd,flmg,smldr,decay,do_smoulder
IMPLICIT NONE
LOGICAL, INTENT(IN) :: ASCIIhuh
LOGICAL, INTENT(IN) :: fdiHuh
INTEGER, INTENT(IN) :: ts,te,tzone,tfire         !time (hrs)
INTEGER, INTENT(IN) :: fl                        !scar number
INTEGER, DIMENSION(:) :: n_fp                    !number of fire points in each scar
INTEGER, DIMENSION(:) :: fp_p                    !pointer to next scar
REAL, DIMENSION(:),   INTENT(IN) :: vs           !plume spread flag (flmg,smldr)
REAL, INTENT(IN) :: tp                           !eflux temperature (K)
INTEGER, DIMENSION(:), INTENT(IN)   :: burntype  !fire classification
REAL, DIMENSION(:,:), INTENT(INOUT) :: fuelBurnt !(kg-dry/cell)
REAL, DIMENSION(:), INTENT(IN)      :: fuelIgnited_cwd !(kg-dry/cell)
!
INTEGER :: ih,f,s,ios,lst,fs,ptr
REAL(KIND=8), DIMENSION(2)  :: fuel_burnt_in_scar!total fuel burnt this hour (kg)
REAL                        :: cwd_still_smldrng !old cwd still burning (kg-dry/cell)
REAL                        :: ff_igntd_brnt     !ff ignited and burnt this hour (kg)
REAL,DIMENSION(n_fp(fl))    :: cwd_igntd_brnt    !cwd ignited and burnt this hour (kg)
!XB
Logical           :: OneFileOnly
INTEGER           :: tfire_local_hour              !time (hrs)
done=.FALSE.
!

DO ih=ts,te
  fuel_burnt_in_scar=0.0D00
  !XB
  if (OneFileOnly) then
      lst = ih + tzone
  else
      lst = MOD(ih+tzone,24)
  endif
  IF(lst < 0)lst=lst+24
  
  !Here we compute the fire local time, to know where to synchronise the emission profile hprof
  tfire_local_hour=max(ih - tfire,0) 
!-------------------------------------------------------------------------------
! Calculate and write out emissions from flaming and smouldering fine fuels
! Also cwd ignited this hour and lofted with the fine fuels
! 1/ calculate mass of fuel burnt this hour (kg) and -> (g)
! 2/ speciate into combustion products 
!-------------------------------------------------------------------------------
  !print * , ih, hprof(lst),ts,te, fuelBurnt(1,flmg)*hprof(lst), fuelIgnited_cwd(1)*hprof(lst)*(1.0-decay)
  DO f=1,n_fp(fl)
    ptr=fp_p(fl) + f - 1   
    ff_igntd_brnt=fuelBurnt(ptr,flmg)*hprof(tfire_local_hour)                  !kg ff ignited and burnt this hour
    cwd_igntd_brnt(f)=fuelIgnited_cwd(ptr)*hprof(tfire_local_hour)*(1.0-decay) !kg cwd ignited and burnt this hour 
   
    IF(ASCIIhuh)THEN
      WRITE(14,3,IOSTAT=ios)vs(flmg),298.,                        &
             (MAX(ff_igntd_brnt*FireFactors(map_ems(s),burntype(flmg))   +                &
                  cwd_igntd_brnt(f)*FireFactors(map_ems(s),burntype(smldr)),0.)/3600.,s=1,nems)
3     FORMAT(F6.1,',',F4.0,100(',',G14.7))
    ELSE
      WRITE(14,IOSTAT=ios)vs(flmg),298.,                        &
             (MAX(ff_igntd_brnt*FireFactors(map_ems(s),burntype(flmg))   +                &
                  cwd_igntd_brnt(f)*FireFactors(map_ems(s),burntype(smldr)),0.)/3600.,s=1,nems)
    END IF
    IF(ios /= 0)RETURN
    fuel_burnt_in_scar(flmg)=fuel_burnt_in_scar(flmg)+DBLE(ff_igntd_brnt)       !cumulative kg ff burnt
    fuel_burnt_in_scar(smldr)=fuel_burnt_in_scar(smldr)+DBLE(cwd_igntd_brnt(f)) !diagnostics
  END DO !loop over flaming fire points in current scar

!-------------------------------------------------------------------------------
! Calculate and write out emissions from the smouldering cwd
!    ignited in earlier hours and still smouldering
!-------------------------------------------------------------------------------
  DO f=1,n_fp(fl)
    ptr=fp_p(fl) + f - 1
    cwd_still_smldrng=MAX(0.,fuelBurnt(ptr,cwd)*decay)       !carry over from last hour. (note 1-decay factor above  
                                                             !implicitly gives the frac burning this hour) (kg/ha)
    IF(ASCIIhuh)THEN
      WRITE(14,3,IOSTAT=ios)vs(flmg),298.,  &
         (cwd_still_smldrng*FireFactors(map_ems(s),burntype(smldr))/3600.,s=1,nems)
    ELSE
      WRITE(14,IOSTAT=ios)vs(flmg),298.,      &
         (cwd_still_smldrng*FireFactors(map_ems(s),burntype(smldr))/3600.,s=1,nems)
    END IF

!-------------------------------------------------------------------------------
!  Update the array carrying the smouldering cwd to include newly ignited + old
!-------------------------------------------------------------------------------
    fuelBurnt(ptr,cwd)=cwd_still_smldrng+cwd_igntd_brnt(f)     !updated for next hour (kg/ha)
    fuel_burnt_in_scar(smldr)=fuel_burnt_in_scar(smldr)+DBLE(cwd_still_smldrng) !diagnostics
  END DO !loop over smouldering fire points in current scar
!
  WRITE(21,'(i4,2(a,G14.7))')ih,cma,fuel_burnt_in_scar(flmg)/1000.,cma,   &
                                    fuel_burnt_in_scar(smldr)/1000.
END DO !hour loop
done=.TRUE.
END FUNCTION writeFlamingAndSmoulderingEmissions

!-------------------------------------------------------------------------------
!  writeSmoulderingEmissions
!-------------------------------------------------------------------------------
!> Calculate and write out hourly emissions from the smouldering component of a fire
!> @author
!> Martin Cope CSIRO
!
!! \param ASCIIhuh ASCII or binary?
!! \param ts first hour (model time)
!! \param te last hour (model time)
!! \param tzone conversion from LST to model time
!! \param fl scar number
!! \param n_fp number of active points in each scar
!! \param fp_p point to the next fire scar
!! \param vs smoke rise speed (m/s)
!! \param tp smoke temperature (K)
!! \param burntype biome/mce data set
!! \param fuelBurnt cumulative amount of fuel burnt (kg-dry/cell)
!! \result done true if a successful write
!
! Modifications
!  When         Who      What
!  5/12/2019    XB       change writing format of the hour to accomodate 4 digits   
!-------------------------------------------------------------------------------
LOGICAL FUNCTION writeSmoulderingEmissions(ASCIIhuh,ts,te,tzone,      &
                                  fl,n_fp,fp_p,                       &
                                  vs,tp,burntype,fuelBurnt) RESULT(done)
USE diagnostics
USE emissions
USE fueldata, ONLY: fine,cwd,flmg,smldr,decay,do_smoulder
IMPLICIT NONE
LOGICAL, INTENT(IN) :: ASCIIhuh
INTEGER, INTENT(IN) :: ts,te,tzone               !time (hrs)
INTEGER, INTENT(IN) :: fl                        !scar number
INTEGER, DIMENSION(:) :: n_fp                    !number of fire points in each scar
INTEGER, DIMENSION(:) :: fp_p                    !pointer to next scar
REAL, DIMENSION(:),   INTENT(IN) :: vs           !plume spread flag (flmg,smldr)
REAL, INTENT(IN) :: tp                           !eflux temperature (K)
INTEGER, DIMENSION(:), INTENT(IN) :: burntype    !fire classification
REAL, DIMENSION(:,:), INTENT(INOUT) :: fuelBurnt !(kg-dry/cell/hr)
!
INTEGER :: ih,f,s,ios,lst,ptr,fs
REAL :: cwd_brnt_this_hr  !cwd burnt during this time step (kg-day/cell)
REAL(KIND=8), DIMENSION(2) :: fuel_burnt_in_scar  !fuel burnt this hour (kg)
!
done=.FALSE.
DO ih=ts,te
  fuel_burnt_in_scar=0.0D00

!-------------------------------------------------------------------------------
!  Write out zero emissions from flaming and smouldering fine fuels  
!-------------------------------------------------------------------------------
  DO f=1,n_fp(fl)
    ptr=fp_p(fl) + f - 1
    IF(ASCIIhuh)THEN
      WRITE(14,3,IOSTAT=ios)vs(flmg),tp,(0.0, s=1,nems)
3     FORMAT(F6.1,',',F4.0,100(',',G14.7))
    ELSE
      WRITE(14,IOSTAT=ios)vs(flmg),tp,(0.0, s=1,nems)
    END IF
    IF(ios /= 0)RETURN
  END DO !loop over flaming fire points in current scar

!-------------------------------------------------------------------------------
! Calculate and write out emissions from the smouldering cwd. This is given by-
!  fraction of CWD in this scar ignited in earlier hours and still smouldering
!-------------------------------------------------------------------------------
  DO f=1,n_fp(fl)
    ptr=fp_p(fl) + f - 1
     cwd_brnt_this_hr=MAX(0.,fuelBurnt(ptr,cwd)*decay)      !remaining
     fuelBurnt(ptr,cwd)=cwd_brnt_this_hr                    !for the next hour

    IF(ASCIIhuh)THEN
      WRITE(14,3,IOSTAT=ios)vs(smldr),tp, &
         (cwd_brnt_this_hr*FireFactors(map_ems(s),burntype(smldr))/3600.,s=1,nems)
    ELSE
      WRITE(14,IOSTAT=ios)vs(smldr),tp,      &
         (cwd_brnt_this_hr*FireFactors(map_ems(s),burntype(smldr))/3600.,s=1,nems)
    END IF
    fuel_burnt_in_scar(smldr)=fuel_burnt_in_scar(smldr)+DBLE(cwd_brnt_this_hr) !diagnostics
  END DO !loop over smouldering fire points in current scar

  WRITE(21,'(i4,2(a,G14.7))')ih,cma,fuel_burnt_in_scar(flmg)/1000.,cma,   &
                                    fuel_burnt_in_scar(smldr)/1000.
END DO !hour loop
done=.TRUE.
END FUNCTION writeSmoulderingEmissions

!-------------------------------------------------------------------------------
!  writeZeroFlamingAndSmoulderingEmissions
!-------------------------------------------------------------------------------
!> Fill in the hours of a .pse file when a fire is neither flaming or smouldering
!> @author
!> Martin Cope CSIRO
!
!! \param ASCIIhuh ASCII or binary?
!! \param ts first hour (model time)
!! \param te last hour (model time)
!! \param fl scar number
!! \param n_fp number of active points in each scar
!! \param fp_p point to the next fire scar
!! \param vs smoke rise speed (m/s)
!! \param tp smoke temperature (K)
!! \result done true if a successful write
!
! Modifications
!  When         Who      What
!  5/12/2019    XB       change writing format of the hour to accomodate 4 digits   
!-------------------------------------------------------------------------------
LOGICAL FUNCTION writeZeroFlamingAndSmoulderingEmissions(ASCIIhuh,ts,te,      &
                                  fl,n_fp,fp_p,vs,tp) RESULT(done)
USE  diagnostics
USE fueldata, ONLY: flmg,smldr,do_smoulder
USE emissions
IMPLICIT NONE
LOGICAL, INTENT(IN) :: ASCIIhuh
INTEGER, INTENT(IN) :: ts,te                    !time (hrs)
INTEGER, INTENT(IN) :: fl                        !scar number
INTEGER, DIMENSION(:) :: n_fp                    !number of fire points in each scar
INTEGER, DIMENSION(:) :: fp_p                    !pointer to next scar
REAL, DIMENSION(:),   INTENT(IN) :: vs           !plume spread flag (flmg,smldr)
REAL, INTENT(IN) :: tp                           !eflux temperature (K)
!
INTEGER :: ih,f,s,ios
REAL(KIND=8), DIMENSION(2) :: fuel_burnt_in_scar  !fuel burnt this hour (kg)
!
done=.FALSE.
fuel_burnt_in_scar=0.0D00
DO ih=ts,te

!-------------------------------------------------------------------------------
!  Write out zero emissions from flaming and smouldering fine fuels  
!-------------------------------------------------------------------------------
  DO f=1,n_fp(fl)
    IF(ASCIIhuh)THEN
      WRITE(14,3,IOSTAT=ios)vs(flmg),tp,(0.0, s=1,nems)
3     FORMAT(F6.1,',',F4.0,100(',',G14.7))
    ELSE
      WRITE(14,IOSTAT=ios)vs(flmg),tp,(0.0, s=1,nems)
    END IF
    IF(ios /= 0)RETURN
  END DO !loop over flaming fire points in current scar

!-------------------------------------------------------------------------------
! Calculate and write out emissions from the smouldering cwd. This is given by-
!  fraction of CWD in this scar ignited in earlier hours and still smouldering
!-------------------------------------------------------------------------------
  DO f=1,n_fp(fl)
    IF(ASCIIhuh)THEN
      WRITE(14,3,IOSTAT=ios)vs(smldr),tp,(0.0, s=1,nems)
    ELSE
      WRITE(14,IOSTAT=ios)vs(smldr),tp,(0.0, s=1,nems)
    END IF
  END DO !loop over smouldering fire points in current scar
  
  WRITE(21,'(i4,2(a,G14.7))')ih,cma,fuel_burnt_in_scar(flmg),cma,fuel_burnt_in_scar(smldr)
END DO !hour loop
done=.TRUE.
END FUNCTION writeZeroFlamingAndSmoulderingEmissions
END MODULE smoke_utilities
