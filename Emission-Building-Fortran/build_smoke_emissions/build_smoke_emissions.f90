!> @author
!> Martin Cope, CSIRO
!> @brief
!> Calculate fire emissions using user input fire burn areas in raster format
!> Output emissions in CTM point source format
!-----------------------------------------------------------------------------|
! Modifications
!  Version      When     Who    What
!    v1.31   15/05/17    mec   Small changes to input orders to match documentation
!    v1.3    02/05/17    mec   Fuel layers are now defined in the configuration file
!                              Diurnal variation of emissions now defined in config file
!    v1.2    21/04/17    mec   More documentation; mfrc normalised for bushfires
!    v1.1    10/04/17    mec   Added documentation for DOXYGEN
!
!  When         Who      What
!  6/05/20      XB      add TotalNumberHoursFireStart to the flaming and smoudelring emissions subroutine to sync emission profile in time
!  5/02/20      XB      conditional exit when detecting fuel load inside scars
!  5/12/19      XB      non normalised emission function plus removal of first 24h roll back emissions 
! 31/05/18      XB      add 2nd custom input  to add the possibility of output 
!                        one file with XX hours instead of 1 file/day with 24hours (T/F)
! 10/10/17      XB      add custom input filename with preprocessor instructions 
! 26/02/17      mec     write out a file of processed dates
! 23/02/17      mec     renamed code for smoke forecasting system
! 17/02/17      mec     RFS fuel scaled by 0.64 to match range for DELWP fuel layers
!                       cleaned up code and clarified variable names
!                       planned burn start time moved to 12 LST
! 14/02/17      mec     MCE 95 scaled to 3.3- correct error in using prior factor of 6
!                       CWD during flaming is lofted with fine fuel smoke
! 09/02/17      mec     PM2.5 emission factors scaled by 6 based on DELWP project
! 08/02/17      mec     Revised the temporal profile to planned burn- based on DELWP
! 06/01/17      mec     Added in fuel load maps for RFS
! 05/01/17      mec     Check that fuel data exists at a given location in the DELWP file
! 03/01/17      mec     Loop over scar then date. Simplify tracking of smouldering
!                       
! 21/12/16      mec     Read over header in fire file (for RFS modelling)
! 18/12/16      mec     Optionally scale hourly flaming by FDI
! 01/11/16      mec     Added smoke-VBS to RI MCE=0.95,0.98 (Cat 6,7)
! 30/10/16      mec     Version 1.0 of new code
! 26/10/16      mec     Added smouldering emissions + code clean up
! 20/10/16      mec     Added in VBS smoke speciation for RI 0.95 (Category 6)
! 20/10/16      mec     Added in Hg0 emission rate for SOX1
! 13/12/15      mec     Error trapping for missing date in .run file
!                       this is used for runnning in forecast mode
!-----------------------------------------------------------------------------|
PROGRAM build_smoke_emissions
USE diagnostics
USE fueldata
USE emissions
USE DummySource
USE smoke_utilities
USE utilities
IMPLICIT NONE
INTEGER :: timediff        !diff between model time and fire start time (h)
INTEGER :: timediff_end    !diff between model time and fire end time (h)
INTEGER :: numberFireCells !number of grid cells covering the fire
REAL    :: cellLength_from_config_file      !side length of a fire cell (m)
REAL    :: cellarea        !area of a fire cell (ha)
!
INTEGER :: n_scr  !number of fire scars
INTEGER :: n_fp_tot !total number of scar fire points
INTEGER :: fs,fl
INTEGER, ALLOCATABLE, DIMENSION(:) :: jdFs,hrFs     !date and time of fire scar snapshot
INTEGER, ALLOCATABLE, DIMENSION(:) :: jdFe,hrFe     !date and time of fire scar snapshot
INTEGER, ALLOCATABLE, DIMENSION(:) :: n_fp,fp_p !fire spots, pointer to fire scars
REAL, ALLOCATABLE, DIMENSION(:) :: cellLat    !latitude of each fire grid (deg;radian)
REAL, ALLOCATABLE, DIMENSION(:) :: cellLon    !longitude of each fire grid (deg; radian)
REAL, ALLOCATABLE, DIMENSION(:,:) :: fuelLoad !carbon loading (kg-C/ha)
REAL, ALLOCATABLE, DIMENSION(:,:) :: fuelBurnt !kg dry matter/cell/hour
REAL, ALLOCATABLE, DIMENSION(:) :: fuelIgnited_cwd !kg dry matter/cell/hour
INTEGER, ALLOCATABLE, DIMENSION(:) :: fireDurationInHours !hours covered by the fire scar
real, ALLOCATABLE, DIMENSION(:)     :: cellLength        !side length of a fire cell (m) for the fire scar
!
CHARACTER(LEN=10), DIMENSION(2) :: plumerise !MIXED or LOFTED
REAL, DIMENSION(2) :: plumeheight    !emission height of the fire plume (m)
REAL, DIMENSION(2) :: plumespread    !vertical spread of the fire plume (m/s)
REAL, DIMENSION(2) :: bef            !burn efficiency factors (flmg;smldr)
INTEGER, DIMENSION(2) :: speciation  !speciation factors
CHARACTER(LEN=80), DIMENSION(6) :: pse_comment
!
CHARACTER(LEN=80) :: header
LOGICAL           :: degHuh  !coordinates in degree
REAL              :: lat0    !grid origin
REAL              :: lon0    !grid origin
LOGICAL           :: asciiHuh !ASCII output?
LOGICAL           :: fdiHuh  !scale hourly emissions by FDI?
LOGICAL           :: done,found
!
INTEGER           :: jdS,jdE,jdN,jd0
INTEGER           :: yrS,mthS,dyS,yrE,mthE,dyE 
INTEGER           :: tzone   !time zone of fires
INTEGER, DIMENSION(3) :: date
!
CHARACTER(LEN=1024) :: filename,fname,line,location
INTEGER, PARAMETER :: max_scr = 512     !maximum number of scar files to process
INTEGER :: MaxAllocationDim ! to keep the max of the total number of hours vs scarfirehours 
CHARACTER(LEN=1024), DIMENSION(max_scr)   :: scarFileName
!
INTEGER :: f,fm1,s,lat,long,ii,day,ihour,im,mm,z,ptr,p
INTEGER :: l,l1,k,ih,iz,i,j,h,t,ios,currentHour,lst
REAL :: t_hprof
REAL :: version     !version of the .run file

INTEGER :: MaxfireDurationInHours !hours covered by the fire scar

!XB
!  the first command line argument is path to find the RFS fire data and parametres
!  the second is the possibility of writing 1 file of XX hours instead of a file a day with 24 hours (True/False)
INTEGER           :: TotalNumberHours
INTEGER           :: TotalNumberHoursFireStart
INTEGER           :: TotalNumberHoursFireStop
INTEGER           :: LocalHourRelativeToTheFireScarStart
INTEGER           :: LocalHourRelativeToTheFireScarStop

Logical           :: OneFileOnly
CHARACTER(LEN=1024) :: OneFileOnlyString

Logical           :: Condition_RFS_layer
Logical           :: Condition_DELWP_and_VAST_layer
#ifdef CUSTOMFILENAME
      CHARACTER(len=100) :: filepath
      CALL get_command_argument(1, filepath)
      CALL get_command_argument(2, OneFileOnlyString)
      if (TRIM(OneFileOnlyString) == 'T') then 
          OneFileOnly = .true.
      else
          OneFileOnly = .false.
      endif       
#else
      OneFileOnly = .false.
#endif
lst = 0
!-------------------------------------------------------------------------------
! Input simulation start and end days
! Input time offset to go from model zone (i.e. UTC) to fire zone (i.e LST)
! Input fire cells and emissions (kg-C/ha)
!-------------------------------------------------------------------------------
OPEN(UNIT=7,FILE='build_smoke_emissions.run',STATUS='old')
1 FORMAT(100a)
READ(7,*)header,version
READ(7,1)header
READ(7,*)yrS,mthS,dyS,yrE,mthE,dyE,tzone
jd0=JulianDayNumber(yrS,1,1)
jdS=JulianDayNumber(yrS,mthS,dyS)
jdE=JulianDayNumber(yrE,mthE,dyE)
OPEN(UNIT=19,FILE='date_list.txt',STATUS='UNKNOWN')
DO jdN=jdS,jdE
  date=GregorianDate(jdN)
  WRITE(19,'(i4.4,2(i2.2))')date(1),date(2),date(3)
END DO
CLOSE(UNIT=19)

!XB One file only?
!print*,"OneFileOnly?",OneFileOnly
if (OneFileOnly) then
    TotalNumberHours = 24 * (jdE - jdS)
    !print*,"Before modif",jdS, jdE,  TotalNumberHours
    jdE = jdS 
    !print*,"After modif",jdS, jdE,  TotalNumberHours
else    
    TotalNumberHours = 24
endif  
allocate(mcfr_readfromfile(0:TotalNumberHours -1))
!-------------------------------------------------------------------------------
! Input smouldering residence time and burn patchiness
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,*)k_rdr,patchiness,cellLength_from_config_file

!-------------------------------------------------------------------------------
! Input burning efficiency and speciation table number  
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,*)bef(flmg),speciation(flmg),bef(smldr),speciation(smldr)

!-------------------------------------------------------------------------------
! Input emission height (m), vertical spread flag, hflux/fuel moisture 
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,*)plumeheight(flmg),plumerise(flmg),hflx(flmg),         &
         plumeheight(smldr),plumerise(smldr),hflx(smldr)

DO i=1,2
  IF(UpperCase(Compress(plumerise(i)) ) == TRIM(plume_desc(1)))THEN
    plumespread(i)=-999.
  ELSEIF(UpperCase(Compress(plumerise(i)) ) == TRIM(plume_desc(2)))THEN
    plumespread(i)=-99.
  ELSE
    PRINT *,'Plume rise option is not recognised: ',TRIM(plumerise(i))
    PRINT *,'Burn type is: ',burn_desc(i)
    STOP 'Fatal error'
  END IF
END DO 

!-------------------------------------------------------------------------------
! Convert cell area from m^2 to ha
! Smouldering residence time (hr); decay rate (1/hr)
!-------------------------------------------------------------------------------
 cellarea=cellLength_from_config_file*cellLength_from_config_file/10000.
rdr=k_rdr/(1.0-EXP(-1.0))
decay=1.0/EXP(1.0/rdr)

!-------------------------------------------------------------------------------
! Input ascii or binary output; scale hourly burn rate by FDI?
!-------------------------------------------------------------------------------
READ(7,1)header
DO im=1,5
 READ(7,'(a)')pse_comment(im)
ENDDO
pse_Comment(6)='*---End-of-comment ---*'
READ(7,1)header
READ(7,*)asciiHuh,fdiHuh

!-------------------------------------------------------------------------------
! Read in fuel layer files
! Naming convention is fine_dataset.asc; cwd_dataset.asc
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,1)header
READ(7,*)n_fl
ALLOCATE(ps_fl(n_fl),pe_fl(n_fl),fn_fl(n_fl),scal_fl(2,n_fl))
ALLOCATE(nx_fl(n_fl),ny_fl(n_fl))
ALLOCATE(x0_fl(n_fl),y0_fl(n_fl),dx_fl(n_fl))
!
nv_fl=0
READ(7,1)header
DO f=1,n_fl
  READ(7,1)header
  READ(7,*)fn_fl(f),scal_fl(:,f)
#ifdef CUSTOMFILENAME
      IF(.NOT.read_fl_header(19,TRIM(filepath)//'fine_'//TRIM(fn_fl(f))//'.asc',     &
                         nx_fl(f),ny_fl(f),x0_fl(f),y0_fl(f),dx_fl(f)))STOP'Fatal error'
  
#else
      IF(.NOT.read_fl_header(19,'fine_'//TRIM(fn_fl(f))//'.asc',     &
                         nx_fl(f),ny_fl(f),x0_fl(f),y0_fl(f),dx_fl(f)))STOP'Fatal error'
#endif
  nv_fl=nv_fl+nx_fl(f)*ny_fl(f)
END DO !fuel layer loop
!
ALLOCATE(fuellayer(nv_fl,2))
ps_fl(1)=1
pe_fl(1)=nx_fl(1)*ny_fl(1)
DO f=2,n_fl
  fm1=f-1
  ps_fl(f)=pe_fl(fm1)+1
  pe_fl(f)=pe_fl(fm1)+nx_fl(f)*ny_fl(f)
END DO
DO f=1,n_fl
#ifdef CUSTOMFILENAME
      IF(.NOT.read_fl_grid(19,nx_fl(f),ny_fl(f),TRIM(filepath)//'fine_'//TRIM(fn_fl(f))//'.asc',scal_fl(fine,f),   &
         fuellayer(ps_fl(f):pe_fl(f),fine)))STOP'Fatal error'
      IF(.NOT.read_fl_grid(19,nx_fl(f),ny_fl(f),TRIM(filepath)//'cwd_'//TRIM(fn_fl(f))//'.asc',scal_fl(cwd,f),     &
         fuellayer(ps_fl(f):pe_fl(f),cwd)))STOP'Fatal error'
#else
      IF(.NOT.read_fl_grid(19,nx_fl(f),ny_fl(f),'fine_'//TRIM(fn_fl(f))//'.asc',scal_fl(fine,f),             &
         fuellayer(ps_fl(f):pe_fl(f),fine)))STOP'Fatal error'
      IF(.NOT.read_fl_grid(19,nx_fl(f),ny_fl(f),'cwd_'//TRIM(fn_fl(f))//'.asc',scal_fl(cwd,f),               &
         fuellayer(ps_fl(f):pe_fl(f),cwd)))STOP'Fatal error'
#endif
!
  CALL surfer_dump(19,x0_fl(f),y0_fl(f),dx_fl(f),dx_fl(f),                 &
   RESHAPE(fuellayer(ps_fl(f):pe_fl(f),fine),(/nx_fl(f),ny_fl(f)/)),TRIM(fn_fl(f))//'_fine_t-DryPerHa.grd')
  CALL surfer_dump(19,x0_fl(f),y0_fl(f),dx_fl(f),dx_fl(f),                 &
   RESHAPE(fuellayer(ps_fl(f):pe_fl(f),cwd),(/nx_fl(f),ny_fl(f)/)),TRIM(fn_fl(f))//'_cwd_t-DryPerHa.grd')
END DO !fuel layer loop

!-------------------------------------------------------------------------------
! Read in speciation tables
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,1)header
READ(7,1)fn_sspg
OPEN(UNIT=19,FILE=TRIM(fn_sspg),STATUS='OLD',IOSTAT=ios)
IF(ios /=0)THEN
  PRINT *,'Error opening the smoke emission factor file: ',TRIM(fn_sspg)
  STOP'Fatal error'
END IF
!
READ(19,1)header
READ(19,1)header
READ(19,1)header
READ(19,*)n_sspg,nspec
ALLOCATE(name_nspec(nspec),mw(nspec),name_sspg(n_sspg))
READ(19,1)header
READ(19,*)name_nspec
READ(19,1)header
READ(19,*)mw
!
ALLOCATE(FireFactors(nspec,n_sspg))
DO s=1,n_sspg
  header=''
  DO WHILE(UpperCase(compress(header))/= 'STARTGROUP') 
    READ(19,1,IOSTAT=ios)header
    IF(ios /= 0)THEN
      PRINT *,'Error reading from smoke speciation file: ',TRIM(fn_sspg)
      PRINT *,'Last record read: ',TRIM(header)
      STOP'Fatal error'
    END IF !file i/o error
  END DO
  READ(19,1)name_sspg(s) 
  READ(19,*)FireFactors(:,s)
END DO  !read in smoke speciation files
CLOSE(19)
!
OPEN(UNIT=19,FILE='smoke_speciation_factors_gPerkg.txt')
DO s=1,n_sspg
  WRITE(19,1)TRIM(name_sspg(s)),' (g/kg-dry fuel)'
  DO l=1,nspec,10
    WRITE(19,3)(name_nspec(l1),l1=l,MIN(nspec,l+9))
    WRITE(19,4)(FireFactors(l1,s),l1=l,MIN(nspec,l+9))
  END DO
  WRITE(19,1)' '
END DO
CLOSE(19)
3 FORMAT(10(3x,a,4x))
4 FORMAT(10(e10.3,1x))

!-------------------------------------------------------------------------------
! Read in indices to map from internal speciation tables to 
!  user defined emission species
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,1)header
READ(7,*)nems
ALLOCATE(name_ems(nems))
ALLOCATE(map_ems(nems))
ALLOCATE(mpped_mw(nems))
READ(7,1)header
READ(7,1)header
READ(7,*)(name_ems(s),map_ems(s),s=1,nems)
!READ(7,*)map_ems
!
WHERE(map_ems(:) == -1)
  map_ems(:)=nspec
END WHERE 
mpped_mw=mw(map_ems(:))

!-------------------------------------------------------------------------------
! Input the fraction of 24-h emissions emitted each hour (SUM_fi=1.0)
! fi is input in LST and corrected to model time using time zone
!-------------------------------------------------------------------------------
READ(7,1)header
READ(7,1)header
READ(7,*)mcfr_readfromfile

!-------------------------------------------------------------------------------
! Input generic name of the fire scar raster file list
! Loop on processing data
!  -number of scar data sets
!  -date/time of scar
!  -number of points in each scar
! Convert to model time zone
!
! If a file does not exist then write out dummy file
!-------------------------------------------------------------------------------
ALLOCATE(jdFs(max_scr))
ALLOCATE(hrFs(max_scr))
ALLOCATE(jdFe(max_scr))
ALLOCATE(hrFe(max_scr))
ALLOCATE(n_fp(max_scr))
ALLOCATE(fireDurationInHours(max_scr))
ALLOCATE(cellLength(max_scr))
!
READ(7,1)header
READ(7,1)location
READ(7,1)header
!
n_scr=0
fireDurationInHours = 0
 cellLength(:) = cellLength_from_config_file

DO s=1,max_scr
  f=n_scr+1
  READ(7,1,IOSTAT=IOS)scarFileName(f)
  IF(ios /= 0)EXIT
  IF(.NOT.readScarFile_meta(jdFs(f),hrFs(f),jdFe(f),hrFe(f),n_fp(f),tzone, cellLength(f),          &
                            scarFileName(f),location))THEN
    PRINT *,'Error retrieving data count from: ',TRIM(scarFileName(f))//'.csv' 
    CYCLE
  END IF
  fireDurationInHours(f)=24*(jdFe(f)-jdFs(f))+hrFe(f)-hrFs(f)+1
  print*, 'fireduration',s,f,fireDurationInHours(f)
  n_scr=n_scr+1
END DO
CLOSE(7)
!
IF(n_scr==0)THEN
  PRINT *,'Error reading fire data file name'
  PRINT *,'No fire data found for this day. Writing dummy .pse file'
  DO jdN=jdS,jdE
    date=GregorianDate(jdN)
    WRITE(filename,'(i4.4,2(i2.2),a,a)')date(1),date(2),date(3),   &
                                      '_'//TRIM(scarFileName(f))
    IF(ASCIIhuh)THEN
      fname='fems_'//TRIM(filename)//'.pse'
      OPEN(14,file=trim(fname), form='formatted')
    ELSE
      fname='fems_'//TRIM(filename)//'.pse.bin'
      OPEN(14,file=trim(fname), form='unformatted')
    END IF !ascii or binary
    done=writePSE_header(14,.NOT.ASCIIhuh,nems,name_ems,mpped_mw)
    done=writeZeroEmissions(ASCIIhuh,0,dmy_hrs,dmy_ns,dmy_vs,dmy_ts,nems)
    CLOSE(14)
  END DO !loop over sim days
  STOP 'No data available for this day'
END IF !write out a dummy file

!-------------------------------------------------------------------------------
! Set up data arrays and pointers to each scar data set
!-------------------------------------------------------------------------------
n_fp_tot=SUM(n_fp(1:n_scr))
ALLOCATE(cellLat(n_fp_tot))
ALLOCATE(cellLon(n_fp_tot))
ALLOCATE(fuelLoad(n_fp_tot,2))
ALLOCATE(fuelBurnt(n_fp_tot,2))
ALLOCATE(fuelIgnited_cwd(n_fp_tot))
ALLOCATE(fp_p(n_scr))
fp_p(1)=1
DO f=2,n_scr
  fp_p(f)=fp_p(f-1)+n_fp(f-1)
END DO 
fuelLoad=0.0
fuelBurnt=0.0
fuelIgnited_cwd=0.0

!-------------------------------------------------------------------------------
! Read in data
!-------------------------------------------------------------------------------
DO f=1,n_scr
  IF(.NOT.readScarFile_data(scarFileName(f),location,cellLat(fp_p(f):),    &
                            cellLon(fp_p(f):),n_fp(f))) THEN
    PRINT *,'Error retrieving fire raster data from: ',TRIM(scarFileName(f))//'.csv' 
    STOP 'Fatal error'
  END IF

!-------------------------------------------------------------------------------
! Now map the fuel load
! First check to see if the fire falls in the DELWP domain; then RFS;
!   if not use VAST
!-------------------------------------------------------------------------------
  OPEN(UNIT=19,FILE=TRIM(scarFileName(f))//'_fuel-Load_t-dPerha.csv')
  WRITE(19,*)'Fuel load (t-dry/ha) for each fire cell within the burn scar polygon'
  WRITE(19,*)'x,y,ff(t-dry/ha),cwd(t-dry/ha)'
  DO p=1,n_fp(f)
    ptr=fp_p(f) + p - 1
    found=.FALSE.
    fuelLoad(ptr,:)=0.0
    DO l=1,n_fl
      i=NINT((cellLon(ptr)-x0_fl(l))/dx_fl(l)+1.0)
      j=NINT((cellLat(ptr)-y0_fl(l))/dx_fl(l)+1.0)
      IF(i >= 1 .AND. i <= nx_fl(l) .AND. j >=1 .AND. j <= ny_fl(l))THEN
        found=.TRUE.
        k=nx_fl(l)*(j-1)+i+ps_fl(l)-1
        fuelLoad(ptr,fine)=fuellayer(k,fine)
        fuelLoad(ptr,cwd)=fuellayer(k,cwd)
        
!       conditional exit of a loop: if too big or negative, it means it's undefined in the current fuel layer
!        write(*,'(A14,1x,2(g14.7,1x), I1, 1x, 5(I5,1x), 2(g10.3,1x))') 'scar fuel load',cellLat(ptr),cellLon(ptr),l,i,j, ptr,k,ps_fl(l), fuelLoad(ptr,fine), fuelLoad(ptr,cwd)
        Condition_RFS_layer = (abs(fuelLoad(ptr,fine)) .le. 10000.)
        Condition_DELWP_and_VAST_layer = (fuelLoad(ptr,fine) .ge. 0.)
        if (Condition_RFS_layer .and. Condition_DELWP_and_VAST_layer) then
            exit
            endif
        Condition_RFS_layer = (abs(fuelLoad(ptr,cwd)) .le. 10000.)
        Condition_DELWP_and_VAST_layer = (fuelLoad(ptr,cwd) .ge. 0.)
        if (Condition_RFS_layer .and. Condition_DELWP_and_VAST_layer) then
            exit
            endif
        
      END IF !fire lies in current fuel layer
    END DO !fuel layer loop
!
    IF(.NOT.found)THEN
      PRINT *,'Warning, point: ',cellLat(ptr),cellLon(ptr)
      PRINT *,'Lies outside all the available fuel layer domains'
    END IF
!
    WRITE(19,'(2(g14.7,a),2(g10.3,a))')cellLon(ptr),cma,cellLat(ptr),cma,fuelLoad(ptr,fine),cma,fuelLoad(ptr,cwd)
  END DO !cells in each fire scar file
  CLOSE(19)
 
!-------------------------------------------------------------------------------
! Calculate the fuel consumption from fine fuels
!-------------------------------------------------------------------------------
  cellarea = cellLength(f) * cellLength(f)/10000.
  OPEN(UNIT=19,FILE=TRIM(scarFileName(f))//'_fuel_consumed_t-dry.csv')
  WRITE(19,'(a)')'Fuel consumed (t-dry) for each fire cell within the burn scar polygon'
  WRITE(19,*)'fl (t/ha) x patchiness x cell area (ha) x bef'
  WRITE(19,*)'patchiness,cell area (ha),fine-bef,cwd-bef,fireduration(hrs)'
  WRITE(19,*)patchiness,cma,cellarea,cma,bef(fine),cma,bef(cwd),cma,fireDurationInHours(f)
  WRITE(19,*)'x,y,ff(t-dry),cwd(t-dry)'
!
  DO p=1,n_fp(f)
    ptr=fp_p(f) + p - 1
!
    fuelBurnt(ptr,flmg)=fuelLoad(ptr,fine)*bef(fine)*      &
                           patchiness*1000.*cellarea          !kg-dry ff matter per source cell
!    print*,'1', p,ptr, fuelBurnt(ptr,flmg),fuelLoad(ptr,fine), bef(fine), patchiness*1000., cellarea 
!-------------------------------------------------------------------------------
! First calculate the CWD fuel consumption
!-------------------------------------------------------------------------------
!    print*,'2', fuelLoad(ptr,cwd), bef(cwd), patchiness*1000., cellarea
    fuelIgnited_cwd(ptr)=fuelLoad(ptr,cwd)*bef(cwd)*        &
                          patchiness*1000.*cellarea            !kg-dry cwd matter per source cell
    WRITE(19,'(2(g14.7,a),2(g10.3,a))')cellLon(ptr),cma,cellLat(ptr),cma,   &
                                       fuelBurnt(ptr,flmg)/1000.,cma,fuelIgnited_cwd(ptr)/1000.

  END DO !cells in each fire scar file
  CLOSE(19)

END DO !fire scar file loop

!-------------------------------------------------------------------------------
! Free up some space. Fuel layer grids are no longer needed
!-------------------------------------------------------------------------------
DEALLOCATE(ps_fl,pe_fl,fn_fl,scal_fl)
DEALLOCATE(nx_fl,ny_fl)
DEALLOCATE(x0_fl,y0_fl,dx_fl)
DEALLOCATE(fuellayer)
  
!-------------------------------------------------------------------------------
!   Loop over each fire scar
!   Optionally calculate the hourly emission weighting
!   For each scar loop over the simulation days
!-------------------------------------------------------------------------------


!! allocate the final tables
MaxfireDurationInHours = maxval(fireDurationInHours)
MaxAllocationDim = max(MaxfireDurationInHours, TotalNumberHours)
allocate(mcfr(0:MaxAllocationDim-1 ))
allocate(hprof(0:MaxAllocationDim-1 ))
!print* ,"max=",MaxfireDurationInHours, TotalNumberHours
!init
mcfr = 0.d0
mcfr(0:TotalNumberHours -1) = mcfr_readfromfile(0:TotalNumberHours -1)
DO fl=1,n_scr
  hprof = 0.d0
  OPEN(UNIT=21,FILE=TRIM(scarFileName(fl))//'_fuel_burnt_in_scar_tPerhr.csv')
  WRITE(21,*)'hour,ff(t/hr),cwd(t/hr)'
!  
  hprof=1.0/FLOAT(fireDurationInHours(fl)) !uniform emission rate
  t_hprof=0.0
  !print*, 'properties',fireDurationInHours(fl), mcfr
  IF(fdiHuh)THEN                           !hourly varying emission rate
    DO ih=1,fireDurationInHours(fl)
      !!! XB modif, t_hprof is now of total fire len, not 24h to repeat
      if (OneFileOnly) then
          lst=hrFs(fl)+ih-1+tzone
          !print*,"LST", ih, lst
      else 
          lst=MOD(hrFs(fl)+ih-1+tzone,24)      !map from model time to LST
      endif
      IF(lst < 0)lst=lst+24
      t_hprof=t_hprof+mcfr(lst)            !accumulate hourly weighting
      print*, 'inside:',ih, lst, t_hprof, mcfr(lst)
    END DO
!    IF(t_hprof > 1.0E-06)THEN
!      hprof=mcfr/t_hprof                   !normalised hourly emission weighting
!    END IF
    hprof=mcfr                   !Not normalised hourly emission weighting


  END IF  

!
  DO jdN=jdS,jdE
   timeDiff=24*(jdFs(fl)-jdN)  
   timeDiff_end=24*(jdFe(fl)-jdN)     
   !IF(timeDiff >= 0 .AND. timeDiff_end <= max_smoulder_time)THEN   !+ve time diff means the fire is/has burnt  
   !TotalNumberHours
   TotalNumberHoursFireStart = 24 * (jdFs(fl) - jdS) + hrFs(fl) 
   TotalNumberHoursFireStop = 24 * (jdFe(fl) - jdS) + hrFe(fl) 
   !LocalHourRelativeToTheFireScarStart = 0
   !LocalHourRelativeToTheFireScarStop = TotalNumberHoursFireStop - TotalNumberHoursFireStart
        !print *, "Fire days and hours", jdFs(fl), hrFs(fl),  jdFe(fl), hrFe(fl)
        
!-------------------------------------------------------------------------------
! In the burn window of the fire, 
! 1/ set up scar specific variables
! 2/ set up a .pse file for this day
!-------------------------------------------------------------------------------
    numberFireCells=2*n_fp(fl)  !fines flaming/smouldering and CWD smouldering      
    date=GregorianDate(jdN)

    WRITE(filename,'(i4.4,2(i2.2),a,a)')date(1),date(2),date(3),'_',TRIM(scarFileName(fl)) 
    IF(ASCIIhuh)THEN
      fname='fems_'//TRIM(filename)//'.pse'
      OPEN(14,file=trim(fname), form='formatted')
    ELSE
      fname='fems_'//TRIM(filename)//'.pse.bin'
      OPEN(14,file=trim(fname), form='unformatted')
    END IF !ascii or binary

!-------------------------------------------------------------------------------
! Write out the header
!-------------------------------------------------------------------------------
    IF(asciihuh)THEN   
      DO ii=1,6
        WRITE(14,'(a)')pse_Comment(ii)
      ENDDO
      WRITE(14,'(i4)')nems
      DO i=1,nems
        WRITE(14,'(i3,1x,a,1x,F6.2)')i,name_ems(i),mw(map_ems(i))
      END DO
! XB
!      WRITE(14,'(i5,1x,i2)')numberFireCells,24
      WRITE(14,'(i7,1x,i5)')numberFireCells, TotalNumberHours 
    ELSE
      DO ii=1,6
        WRITE(14)pse_Comment(ii)
      ENDDO    
      WRITE(14) nems
      DO i=1,nems
        WRITE(14)i,name_ems(i),mw(map_ems(i))
      END DO
! XB
!      WRITE(14)numberFireCells,24
      WRITE(14)numberFireCells, TotalNumberHours 
    END IF !ascii or binary

!-------------------------------------------------------------------------------
! Write out point source information
! Flamming and smouldering phases from the current scar;
!-------------------------------------------------------------------------------
    DO p=1,n_fp(fl)
      ptr=fp_p(fl) + p -1
      IF(ASCIIhuh)THEN
        WRITE(14,2)ptr,cellLon(ptr),cellLat(ptr),plumeheight(flmg),1.0,1.0,0.0,0.0
      ELSE
        WRITE(14)ptr,cellLon(ptr),cellLat(ptr),plumeheight(flmg),1.0,1.0,0.0,0.0
      END IF !ascii or binary?

    END DO !flaming points in the current fire scar

    DO p=1,n_fp(fl)
      ptr=fp_p(fl) + p -1
      IF(ASCIIhuh)THEN
        WRITE(14,2)ptr,cellLon(ptr),cellLat(ptr),plumeheight(smldr),1.0,1.0,0.0,0.0
  2     FORMAT(i5,2(',',F12.4),',',F10.1,4(',',F3.0))
      ELSE
        WRITE(14)ptr,cellLon(ptr),cellLat(ptr),plumeheight(smldr),1.0,1.0,0.0,0.0
      END IF !ascii or binary? 

    END DO !cwd smouldering points in the current fire scar

!-------------------------------------------------------------------------------
! Loop over hours for each day of the simulation
!         - zero emissions until the start of the fire
!         - ff emissions until the flaming phase ends
!         - cwd emissions until the smouldering phase ends
!         - zero emissions until the end of the simulation 
!-------------------------------------------------------------------------------
! the onefileonly option
! if yes, have to count in hours, not days 
    if (.NOT. OneFileOnly) then
        PRINT *,'Processing smoke for the day: ',date
        IF(jdN < jdFs(fl) )THEN  !outside of the fire time window
            IF(.NOT.writeZeroFlamingAndSmoulderingEmissions(       &
                         ASCIIhuh,0,23,                &
                         fl,n_fp,fp_p,plumespread,298.))THEN
                PRINT *,'Error writing zero pre-fire emissions for the day: ',date
                STOP 'Fatal error'
                END IF
        ELSEIF(jdN > jdFe(fl))THEN             !smouldering only
            IF(.NOT.writeSmoulderingEmissions(                        &
                            ASCIIhuh,0,23,tzone,        &
                            fl,n_fp,fp_p,                       &
                            plumespread,298.,speciation,fuelBurnt))THEN
                PRINT *,'Error writing smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF

        ELSEIF(jdN == jdFs(fl))THEN             !fire starts on this day
            IF(.NOT.writeZeroFlamingAndSmoulderingEmissions(       &
                         ASCIIhuh,0,hrFs(fl)-1,               &
                         fl,n_fp,fp_p,plumespread,298.))THEN
                PRINT *,'Error writing zero pre-fire emissions for the day: ',date
                STOP 'Fatal error'
                END IF
            IF(jdN == jdFe(fl))THEN              !fire starts and flaming ends on this day
                IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                             ASCIIhuh,fdiHuh,hrFs(fl),hrFe(fl),tzone, 0,    &
                             fl,n_fp,fp_p,                                &
                             plumespread,298.,speciation,                 &
                             fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                    PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                    STOP 'Fatal error'
                    END IF
                IF(.NOT.writeSmoulderingEmissions(                      &
                            ASCIIhuh,hrFe(fl)+1,23,tzone,        &
                            fl,n_fp,fp_p,                       &
                            plumespread,298.,speciation,fuelBurnt))THEN
                      PRINT *,'Error writing smouldering emissions for the day: ',date
                      STOP 'Fatal error'
                      END IF
            ELSE                           !fire starts but does not end on this day
                IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                             ASCIIhuh,fdiHuh,hrFs(fl),23,tzone, 0,          &
                             fl,n_fp,fp_p,                                &
                             plumespread,298.,speciation,                 &
                             fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                    PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                    STOP 'Fatal error'
                    END IF
                END IF
        ELSEIF(jdN == jdFe(fl))THEN    !fire already burning, but flaming ends on this day
            IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                           ASCIIhuh,fdiHuh,0,hrFe(fl),tzone, 0,           &
                           fl,n_fp,fp_p,                                &
                           plumespread,298.,speciation,                 &
                           fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF
            IF(.NOT.writeSmoulderingEmissions(                      &
                          ASCIIhuh,hrFe(fl)+1,23,tzone,          &
                          fl,n_fp,fp_p,                       &
                          plumespread,298.,speciation,fuelBurnt))THEN
                PRINT *,'Error writing smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF
        ELSE  !fire is flaming and smouldering for the entirety of this day                       
            IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                           ASCIIhuh,fdiHuh,0,23,tzone, 0,                 &
                           fl,n_fp,fp_p,                                &
                           plumespread,298.,speciation,                 &
                           fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF

            END IF
        CLOSE(14)

! the onefileonly option
    else
    
        print *, "Hours", TotalNumberHours, TotalNumberHoursFireStart, TotalNumberHoursFireStop
        PRINT *,'Processing smoke for the day: ',date
!        IF(jdN < jdFs(fl) )THEN  !outside of the fire time window
        IF(TotalNumberHours < TotalNumberHoursFireStart )THEN  !outside of the fire time window
            print*, 'outside windows'
            IF(.NOT.writeZeroFlamingAndSmoulderingEmissions(       &
                         ASCIIhuh,0,TotalNumberHours-1,                &
                         fl,n_fp,fp_p,plumespread,298.))THEN
                PRINT *,'Error writing zero pre-fire emissions for the day: ',date
                STOP 'Fatal error'
                END IF
!        ELSEIF(jdN > jdFe(fl))THEN             !smouldering only
        ELSEIF( TotalNumberHoursFireStop <= 0 )THEN             !smouldering only
            print*, 'smouldering only'
            IF(.NOT.writeSmoulderingEmissions(                        &
                            ASCIIhuh,0,TotalNumberHours-1,tzone,        &
                            fl,n_fp,fp_p,                       &
                            plumespread,298.,speciation,fuelBurnt))THEN
                PRINT *,'Error writing smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF

!        ELSEIF(jdN == jdFs(fl))THEN             !fire starts on this day
        ELSEIF ((TotalNumberHoursFireStart >= 0 ) .and. (TotalNumberHoursFireStart <= TotalNumberHours )) THEN             !fire starts on this day
            print*, 'fire starts on this day'
            IF(.NOT.writeZeroFlamingAndSmoulderingEmissions(       &
                         ASCIIhuh,0,TotalNumberHoursFireStart-1,               &
                         fl,n_fp,fp_p,plumespread,298.))THEN
                PRINT *,'Error writing zero pre-fire emissions for the day: ',date
                STOP 'Fatal error'
                END IF
!          IF(jdN == jdFe(fl))THEN              !fire starts and flaming ends on this day
            IF(TotalNumberHoursFireStop <= TotalNumberHours) THEN              !fire starts and flaming ends on this day
                print*, 'fire starts and flaming ends on this day'
                IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                             ASCIIhuh,fdiHuh,TotalNumberHoursFireStart,TotalNumberHoursFireStop,tzone, TotalNumberHoursFireStart,    &
                             fl,n_fp,fp_p,                                &
                             plumespread,298.,speciation,                 &
                             fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                    PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                    STOP 'Fatal error'
                    END IF
                IF(.NOT.writeSmoulderingEmissions(                      &
                            ASCIIhuh,TotalNumberHoursFireStop+1,TotalNumberHours,tzone,        &
                            fl,n_fp,fp_p,                       &
                            plumespread,298.,speciation,fuelBurnt)) THEN
                    PRINT *,'Error writing smouldering emissions for the day: ',date
                    STOP 'Fatal error'
                    END IF
            ELSE                           !fire starts but does not end on this day
                print*, 'fire starts but does not end on this day'
                IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                             ASCIIhuh,fdiHuh,TotalNumberHoursFireStart,TotalNumberHours-1,tzone,TotalNumberHoursFireStart,           &
                             fl,n_fp,fp_p,                                &
                             plumespread,298.,speciation,                 &
                             fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                    PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                    STOP 'Fatal error'
                    END IF
                END IF
!        ELSEIF(jdN == jdFe(fl))THEN    !fire already burning, but flaming ends on this day
        ELSEIF ((TotalNumberHoursFireStart < 0) .and.                       &  
               (TotalNumberHoursFireStop > 0)  .and.                        &
               (TotalNumberHoursFireStop < TotalNumberHours) ) THEN    !fire already burning, but flaming ends on this day
            print*, 'fire already burning, but flaming ends on this day'
            IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                           ASCIIhuh,fdiHuh,0,TotalNumberHoursFireStop,tzone,TotalNumberHoursFireStart,            &
                           fl,n_fp,fp_p,                                &
                           plumespread,298.,speciation,                 &
                           fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF
            IF(.NOT.writeSmoulderingEmissions(                      &
                          ASCIIhuh,TotalNumberHoursFireStop+1,TotalNumberHours - 1,tzone,          &
                          fl,n_fp,fp_p,                       &
                          plumespread,298.,speciation,fuelBurnt))THEN
                PRINT *,'Error writing smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF
        ELSE  !fire is flaming and smouldering for the entirety of this day                       
            print*, 'fire is flaming and smouldering for the entirety of this day '
            IF(.NOT.writeFlamingAndSmoulderingEmissions(                      &
                           ASCIIhuh,fdiHuh,0,TotalNumberHours - 1,tzone, TotalNumberHoursFireStart,                 &
                           fl,n_fp,fp_p,                                &
                           plumespread,298.,speciation,                 &
                           fuelBurnt,fuelIgnited_cwd,OneFileOnly))THEN
                PRINT *,'Error writing flaming and smouldering emissions for the day: ',date
                STOP 'Fatal error'
                END IF

            END IF
            CLOSE(14)
    
    !  the onefileonly option
        endif

!
   !END IF !current day lies within the time duration of the fire (flaming + smouldering)
  END DO !simulation day loop
  CLOSE(21) !diagnotics
END DO !fire scar loop
!
DEALLOCATE(name_nspec,mw,name_sspg)
DEALLOCATE(FireFactors)
DEALLOCATE(name_ems)
DEALLOCATE(map_ems)
DEALLOCATE(mpped_mw)
DEALLOCATE(jdFs)
DEALLOCATE(hrFs)
DEALLOCATE(jdFe)
DEALLOCATE(hrFe)
DEALLOCATE(n_fp)
DEALLOCATE(cellLat)
DEALLOCATE(cellLon)
DEALLOCATE(fuelLoad)
DEALLOCATE(fuelBurnt)
DEALLOCATE(fuelIgnited_cwd)
DEALLOCATE(fp_p)
!
PRINT *,'Smoke emissions completed'
END PROGRAM build_smoke_emissions
