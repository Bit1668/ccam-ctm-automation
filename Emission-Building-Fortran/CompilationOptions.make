F90=mpifort
F90=ifort
FF=ifort
#####fast
#OPT= -O3 -xHost -ip -no-prec-div -static-intel -fPIC -fp-model precise -fomit-frame-pointer -xHost -lpthread -Bstatic
OPT= -O3 -xHost -ipo -static-intel -fPIC -fp-model precise #-v
OPT= -O3 -xHost -ipo -shared-intel -fPIC -fp-model precise -heap-arrays 349 -mcmodel=medium#-static#-v
NOOPT= -xHost -static-intel -fPIC -fp-model precise -mcmodel=medium#-v
NOOPT= -xHost -static-intel -fPIC -fp-model precise -heap-arrays 349 -mcmodel=medium#-static#-v

OPT= -O3 -xHost -ipo -shared-intel -fPIC -fp-model precise -heap-arrays -mcmodel=medium#-static#-v
NOOPT= -xHost -static-intel -fPIC -fp-model precise -heap-arrays -mcmodel=medium#-static#-v


##### debug
#OPT = -O0 -static-intel -fPIC -fp-stack-check -g -traceback -check all
#NOOPT=    -static-intel -fPIC -fp-stack-check -g -traceback -check all   #-v

PREPROCFLAGS = -fpp -DCUSTOMFILENAME 

PARFLAGS = -qopenmp 
PARFLAGS =  

DEBUG = -O0 -shared-intel -fPIC -fp-stack-check -g -debug all -traceback -check all -fpe0 -warn -check bounds -check uninit -ftrapuv  -heap-arrays 349 -mcmodel=medium
DEBUG = -O0 -shared-intel -fPIC -fp-stack-check -g -debug all -traceback -check all -fpe0 -warn -check bounds -check uninit -ftrapuv  -heap-arrays -mcmodel=medium



#ISORROPIA_CTM = $(DEBUG)

FFLAGS = $(PREPROCFLAGS) $(OPT) 
#FFLAGS = $(PREPROCFLAGS) $(DEBUG)

