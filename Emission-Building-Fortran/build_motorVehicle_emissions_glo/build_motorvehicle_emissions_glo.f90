!-----------------------------------------------------------------------------|
! CITtoCTMmvems_glo
! Program to convert CIT emission file format to CTM format 
! Copyright CSIRO 2015
!-----------------------------------------------------------------------------|
! Modifications
!  When         Who      What
! 01/08/2018    XB    output filename is just the name in the config file(l 694)
! 02/12/15      mec   vbs now user input; optional SO3oPM 
! 15/08/15      mec   Logical flags now set for fdp or shipping
! 13/08/15      mec   Changed from FDP to shipping CB05 species
! 12/08/15      mec   PART now in g/s
! 17/07/15      mec   Added interface for ctm_writeHeader
!                     Skip over GLOMAP definitions if glo=.FALSE.
! 10/06/15      mec   Now optionally deallocate GLOMAP arrays
!                     Logical flag to output Surfer .grd files
! 29/05/15      mec:  Fixed d_ecoc mapping bug in writeTapmEmissions
! 20/05/15      mec:  Hardwired emission data now read in from a data file
!                     NH3 now read in from the CIT emission file
! 04/05/15      mec:  Modified vpx PM2.5 to include non-exhaust PM (assumed dust)
! 01/12/14      mec:  Integrated Sunhee's changes into the GLO version
! 22/09/14      SHL:  apply source specific speciation profile based on SPS report
!                     and Chow et al (2011)
! 27/08/14      SHL:   EPA OEH data set processing with PM25/Pm10 ratio
! 15/09/14      mec   Changed gsd back to 1.59 from 2.3
!                     Now using total PM10
! 11/09/14      mec   Changed size distribution for ec/oc
! 01/04/13      mec   Added in benzene
! 17/01/13      mec   Added in GLOMAP particle size data
! 16/11/12      mec   Added in levoglucosan
! 13/06/12      mec   Removed a bug which progressively scaled back some cb05 species
! 13/03/12      mec   Add ptol and pxyl
!-----------------------------------------------------------------------------|
 MODULE CTM_generic_interfaces
   INTERFACE
     FUNCTION UpperCase(string,length)
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: length
      CHARACTER(LEN=length) :: UpperCase
      CHARACTER(LEN=length), INTENT(IN) :: string
     END FUNCTION
   ENDINTERFACE
 END MODULE CTM_generic_interfaces

  MODULE CTM_interfaces
   INTERFACE
     FUNCTION UpperCase(string,length)
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: length
      CHARACTER(LEN=length) :: UpperCase
      CHARACTER(LEN=length), INTENT(IN) :: string
     END FUNCTION
   ENDINTERFACE
 END MODULE CTM_interfaces

 MODULE ctm_debug
   IMPLICIT NONE
   SAVE
   LOGICAL, PARAMETER :: debug=.TRUE.
   LOGICAL :: dumpGridFiles
   INTEGER, PARAMETER :: outD= 23
 END MODULE ctm_debug

 MODULE cb05_module
   IMPLICIT NONE
   SAVE

!-----------------------------------------------------------------------------|
! Names of CB05 emissions + relevant GLOMAP ptcl components and modes
! Note the second CB05 list is for FDP
! Note that sox_i must be added to the output list
!-----------------------------------------------------------------------------|
!  LOGICAL, PARAMETER ::  doSOX=.FALSE.
!  INTEGER, PARAMETER :: ncb05=24
!  CHARACTER(LEN=4), DIMENSION(ncb05), PARAMETER :: cb05species  =(/  &
!             'ETHA','IOLE','ALDX','OC25',     &
!       'OC10','EC25','EC10','OT25','OT10',     &
!       'ASO4','AS10','APA1','APA2','APA3',     &
!       'APA4','APA5','APA6','APA7','APA8',     &
!       'APA9','PTOL','PXYL','PBNZ','LEVO'/)
!
    LOGICAL, PARAMETER ::  doSOX=.TRUE.
    INTEGER, PARAMETER :: ncb05=27
    CHARACTER(LEN=4), DIMENSION(ncb05), PARAMETER :: cb05species  =(/  &
                'ETHA','IOLE','ALDX','OC25',     &
         'OC10','EC25','EC10','OT25','OT10',     &
         'ASO4','AS10','APA1','APA2','APA3',     &
         'APA4','APA5','APA6','APA7','APA8',     &
         'APA9','PTOL','PXYL','PBNZ','LEVO',     &
         'SOX1','SOX2','SOX3'/)
!
   INTEGER, PARAMETER :: nglo=24
   CHARACTER(LEN=4), DIMENSION(nglo), PARAMETER :: glospecies  =(/  &
        'SU1 ','SU2 ','SU3 ','SU4 ',            &
        'BC2 ','BC3 ','BC4 ','BC5 ',            &
        'OC1 ','OC2 ','OC3 ','OC4 ','OC5 ',     &
        'DU3 ','DU4 ','DU6 ','DU7 ',            &
!        
        'NUCS','AITS','ACCS','COAS','AITI','ACCI','COAI'          /)

!-----------------------------------------------------------------------------|
! MWs (g/mole) of CB05 emissions + relevant GLOMAP ptcl components and modes
! Note the second CB05 list of MWs are for FDP
!-----------------------------------------------------------------------------|
!  REAL, DIMENSION(ncb05), PARAMETER :: cb05mw =(/ &
!      30.1,48.,44.,1.,        &
!       1.,1.,1.,1.,1.,        &
!       1.,1.,1.,1.,1.,        &
!       1.,1.,1.,1.,1.,        &
!       1.,92.0,106.0,78.1,1./)
!
    REAL, DIMENSION(ncb05), PARAMETER :: cb05mw =(/ &
             30.1,48.,44.,1.,   &
         1.,1.,1.,1.,1.,        &
         1.,1.,1.,1.,1.,        &
         1.,1.,1.,1.,1.,        &
         1.,92.0,106.0,78.1,1., &
         64.0,64.0,64.0/)
!        
   REAL, DIMENSION(nGLO), PARAMETER :: GLOmw =(/ & 
        98.0,98.0,98.0,98.0,   &
        12.0,12.0,12.0,12.0,   &
        16.8,16.8,16.8,16.8,16.8,   &
        100.,100.,100.,100.,        &
        1.0,1.0,1.0,1.0,1.0,1.0,1.0 & 
        /)
        
!-----------------------------------------------------------------------------|
! Indices pointing to modes for a 7-mode GLOMAP configuration 
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: modes=7
   INTEGER, PARAMETER :: NUCS=1  !nucln. soluble
   INTEGER, PARAMETER :: AITS=2  !Aitken soluble
   INTEGER, PARAMETER :: ACCS=3  !accum. soluble
   INTEGER, PARAMETER :: COAS=4  !coarse soluble
   INTEGER, PARAMETER :: AITI=5  !Aitken insoluble
   INTEGER, PARAMETER :: ACCI=6  !accum. insoluble
   INTEGER, PARAMETER :: COAI=7  !coarse insoluble
!
   REAL, DIMENSION(modes) :: su !sulfate emissions (g/s)
   REAL, DIMENSION(modes) :: bc !bc emissions (g/s)
   REAL, DIMENSION(modes) :: oc !oc emissions (g/s)
   REAL, DIMENSION(modes) :: du !dust emissions (g/s)
   REAL, DIMENSION(modes) :: nd !number density (ptl/m2/s)

   INTEGER, PARAMETER :: nInvolatile = 3 !VBS categories treated as involatile
   REAL :: modevol      
   REAL :: lgsd
   REAL, PARAMETER :: ppi=3.141592654

!-----------------------------------------------------------------------------|
!          h2so4  bc     oc    nacl   dust    so
! n.b. mm_bc=0.012, mm_oc=0.012*1.4=0.168 (1.4 C-H ratio)
! Mass density of components (kg m^-3)
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: ncp=6
   REAL, DIMENSION(ncp) ::  rhocomp=(/1769.0,1500.0,1500.0,1600.0,2650.0,1500.0/)
   INTEGER, PARAMETER :: su_p=1
   INTEGER, PARAMETER :: bc_p=2
   INTEGER, PARAMETER :: oc_p=3
   INTEGER, PARAMETER :: ss_p=4
   INTEGER, PARAMETER :: du_p=5
   INTEGER, PARAMETER :: so_p=6

!-----------------------------------------------------------------------------|
! Size distribution particle definitions for motor vehicles
! -Sulfate
!-----------------------------------------------------------------------------|
   INTEGER :: n_su                                !number of modes
   INTEGER, ALLOCATABLE, DIMENSION(:) :: m_su     !mode number
   REAL, ALLOCATABLE, DIMENSION(:) :: d_su        !geometric mean diameter (nm)
   REAL, ALLOCATABLE, DIMENSION(:) :: gsig_su     !geometric standard deviation
   REAL, ALLOCATABLE, DIMENSION(:) :: f_su        !fraction in each mode

!-----------------------------------------------------------------------------|
! Size distribution particle definitions
! -OC and EC
!-----------------------------------------------------------------------------|
   INTEGER :: n_ecoc                            !number of modes
   INTEGER, ALLOCATABLE, DIMENSION(:) :: m_ecoc !mode number
   REAL, ALLOCATABLE, DIMENSION(:) :: d_ecoc    !geometric mean diameter (nm)
   REAL, ALLOCATABLE, DIMENSION(:) :: gsig_ecoc !geometric standard deviation
   REAL, ALLOCATABLE, DIMENSION(:) :: f_ecoc    !fraction in each mode

!-----------------------------------------------------------------------------|
! Size distribution particle definitions
! -other (dust; metals)
!-----------------------------------------------------------------------------|
   INTEGER :: n_du                              !number of modes
   INTEGER, ALLOCATABLE, DIMENSION(:) :: m_du   !mode number
   REAL, ALLOCATABLE, DIMENSION(:) :: d_du      !geometric mean diameter (nm)
   REAL, ALLOCATABLE, DIMENSION(:) :: gsig_du   !geometric standard deviation
   REAL, ALLOCATABLE, DIMENSION(:) :: f_du      !fraction in each mode

!-----------------------------------------------------------------------------|
! Speciation of OC into VBS OM
!-----------------------------------------------------------------------------|
   INTEGER, PARAMETER :: nvbs = 9
   !REAL, DIMENSION(nvbs),parameter :: vbs= (/0.048,0.096,0.144,0.224,0.288,0.480,0.640, &
   !                                          0.800 ,1.280 /)
   REAL, DIMENSION(nvbs) :: vbs  !volatility speciation

!-----------------------------------------------------------------------------|
!  Speciation factors
!-----------------------------------------------------------------------------|
   REAL :: PM2p5oPM10 !ratio of PM25/PM10
   REAL :: PM2p5to10   !coarse fraction ratio
   REAL :: ECoPM      !elemental carbon mass fraction of PM
   REAL :: OCoPM      !organic carbon mass fraction of PM
   REAL :: DUoPM      !dust/metals/misc mass fraction of PM
   REAL :: LEVOoPM    !levoglucosan mass fraction of PM

   REAL :: SO3oSOx    !SO3 mass fraction of SOx
   REAL :: SO3oPM     !SO3 mass fraction of PM
   REAL :: IOLEoOLE    !IOLE mass fraction of CB04 OLE 
   REAL :: ALDXoALD2   !ALDX mass fraction of CB04 ALD2 
   REAL :: ETHAoPAR    !ETHA mass fraction of CB04 PAR 
   REAL :: PXYLoXYL    !primary XYL mass fraction of CB04 XYL 
   REAL :: PTOLoTOL    !primary XYL mass fraction of CB04 XYL 
   REAL :: PBNZoTOL    !primary BNZ mass ratio to CB04 TOL 
      
!-----------------------------------------------------------------------------|
!  Species which extend CBIV emissions to CB05 + aerosols
!-----------------------------------------------------------------------------|
   REAL :: nh3,etha,iole,aldx,oc25,oc10,ec25,ec10,aso4
   REAL :: as10,ot25,ot10,ftol,fxyl,fbnz,levo
   REAL :: su1,su2,su3,su4,bc2,bc3,bc4,bc5,oc1,oc2,oc3,oc4,oc5,du3,du4,du6,du7
   REAL :: nnucs,naits,naccs,naiti,nacci,ncoas,ncoai
   REAL :: sox1,sox2,sox3
!
   INTEGER :: lno,lno2,lpm,lso2,lole,lald2,lpar,ltol,lxyl
   REAL, DIMENSION(nvbs) :: apa
   LOGICAL, PARAMETER :: cb05=.TRUE.   !include CB05_AER2 species
   LOGICAL :: glomap                   !include GLOMAP emissions

END MODULE cb05_module

PROGRAM CIT2CTMmvems_GLO
!-------------------------------------------------------------------------------
! Set up parameters
!-------------------------------------------------------------------------------
USE cb05_module
USE CTM_generic_interfaces
USE CTM_debug
IMPLICIT NONE
INTEGER, PARAMETER:: hoursPerDay=24
INTEGER, PARAMETER:: sat=5
INTEGER, PARAMETER:: sun=6
CHARACTER(LEN=1), PARAMETER:: comma=','
INTEGER, PARAMETER :: OUTaemsFunit=55
INTEGER, PARAMETER :: INaemsFunit=45
INTEGER, PARAMETER :: INaemsFunit2=46

!-------------------------------------------------------------------------------
! Dates over which the concatenated Tapm file is to be generated
! INaems_fname/funit is for weekDAY file and
! INaems_fname2/funit2 is for weekEND file
!-------------------------------------------------------------------------------
INTEGER :: yS,mS,dS                !start and end dates
INTEGER :: yE,mE,dE
INTEGER :: jdnS,jdnE,jdn          !Julian day numbers
INTEGER :: day                    !day of the week
INTEGER :: iyear,imonth
INTEGER :: offset           !hour offset ( h_new = h_old + offset)
INTEGER :: sourceIndex      !vpx=1, vpv=2, vdx=3, vlx=4
!
LOGICAL :: INaemsFform,INaemsFform2,OUTaemsFform
CHARACTER(LEN=250) :: INaems_fname,INaems_fname2,OUTaems_fname,grid_fname
CHARACTER(LEN=250) :: file1,OUTaems_genericName,line
CHARACTER(LEN=4) :: Ayear
CHARACTER (LEN=3), DIMENSION(12) :: mm_name=(/'jan','feb','mar', 'apr','may','jun', &
                                              'jul','aug','sep','oct','nov','dec'/)
LOGICAL retrieveCITgrid
LOGICAL retrieveCIT_emissions
LOGICAL retrieveCITemsFormat
LOGICAL retrieveCITaems_header
INTEGER retrieveCIT_speciesNumbers
!LOGICAL writeCTM_header
LOGICAL writeCTM_grid

!-------------------------------------------------------------------------------
! CIT and CTM spatial grid definitions
!-------------------------------------------------------------------------------
CHARACTER(LEN=8) :: date !yyyymmdd
CHARACTER(LEN=4) :: model !model type- TAPM;CCAM;UM
INTEGER ::  emsn_nx,emsn_ny,nx,ny
REAL    ::  emsn_dx,emsn_dy, emsn_x,emsn_y
REAL    ::  emsn_x_offset,emsn_y_offset
REAL    ::  emsn_x_end,emsn_y_end
REAL    ::  emsn_x_center,emsn_y_center
INTEGER ::  zone !MGA mapping zone
LOGICAL ::  nonUniform !if true- write out coordinates of each cell polygon

!-------------------------------------------------------------------------------
! Emission species definitions
!-------------------------------------------------------------------------------
INTEGER :: ne,ne2
INTEGER :: aems_nlcc
CHARACTER (LEN=4), allocatable, dimension(:) ::aems_lcca
REAL, allocatable, dimension(:) ::aems_mw
REAL, allocatable, dimension(:,:,:,:) ::Wdayemsn,Wendemsn
REAL, ALLOCATABLE, DIMENSION(:,:) :: dailyEmissions
REAL, ALLOCATABLE, DIMENSION(:,:) :: hourlyEmissions

!-------------------------------------------------------------------------------
! External i/o functions
!-------------------------------------------------------------------------------
INTEGER, EXTERNAL :: JulianDayNumber
LOGICAL :: done

!-------------------------------------------------------------------------------
! Miscellaneous
!-------------------------------------------------------------------------------
INTEGER :: jdnTs,jdnTe,h,s,i,j,l,ii,ios,stat,iyearOld,imonthOld,files,t
INTEGER, DIMENSION(hoursPerDay) :: yT,mT,dT,hT !time signatures for temp data
INTEGER, DIMENSION(3) :: gDate
INTERFACE
 FUNCTION GregorianDate(jd)
 INTEGER, DIMENSION(3) :: GregorianDate
 INTEGER, INTENT(IN) :: jd !julian day number
END FUNCTION GregorianDate
FUNCTION writeCTMemissions(fileUnit,fileform,numberSpecies,nx,ny,nh,grid)
USE cb05_module
IMPLICIT NONE
LOGICAL :: writeCTMemissions!true is successful write
INTEGER, INTENT(IN) :: fileUnit !I/O unit number
LOGICAL, INTENT(IN) :: fileform
INTEGER, INTENT(IN) :: nh       !number of hours to output
INTEGER, INTENT(IN) :: numberSpecies !number of inventory species
INTEGER, INTENT(IN) :: nx !number east/west points
INTEGER, INTENT(IN) :: ny !number of north/south points
REAL, DIMENSION(numberSpecies,nx,ny,nh), INTENT(IN) :: grid
END FUNCTION writeCTMemissions
FUNCTION writeCTM_header(funit,fform,nlcc,CITspecies,mw,nonUniform)
USE cb05_module
IMPLICIT NONE
LOGICAL :: writeCTM_header
INTEGER, INTENT(IN) :: funit  !file unit
LOGICAL, INTENT(IN) :: fform  !file unit
INTEGER, INTENT(IN) :: nlcc !number of inventory species
REAL, DIMENSION(nlcc), INTENT(IN) :: mw               !molecular weights
CHARACTER(LEN=4), DIMENSION(nlcc), INTENT(IN) :: CITspecies !species names
CHARACTER(LEN=10), OPTIONAL, INTENT(IN) :: nonUniform !non-uniform grid
END FUNCTION writeCTM_header
END INTERFACE

if(debug)open(outD,file='debug.txt')

!-------------------------------------------------------------------------------
! Read in control data
!-------------------------------------------------------------------------------
CALL getarg(1,file1)
OPEN(UNIT=7,FILE=trim(file1), status='old')
READ(7,1)
1 FORMAT()
READ(7,*)yS,mS,dS
READ(7,1)
READ(7,*)yE,mE,dE
!
jdnS=JulianDayNumber(yS,mS,dS)
jdnE=JulianDayNumber(yE,mE,dE)
iyearOld=yS
imonthOld=mS
PRINT *,'Start and end Julian Day Number: ',jdnS,jdnE
IF(jdnE < jdnS)THEN
  PRINT *,'The start and end dates are out of sequence'
  PRINT *,'Start date (year, month, day): ',yS,mS,dS
  PRINT *,'End date (year, month, day): ',yE,mE,dE
  STOP 'Fatal error'
END IF

!-------------------------------------------------------------------------------
! Read in geographical data- origin=centre of SW grid cell
!-------------------------------------------------------------------------------
READ(7,1)
READ(7,2)grid_fname
PRINT *,'grid_fname: ',TRIM(grid_fname)
done=retrieveCITgrid(40,grid_fname,model,emsn_nx,emsn_ny,      &
                    emsn_dx,emsn_dy,emsn_x,emsn_y,zone)
nx=emsn_nx
ny=emsn_ny

IF(.NOT.done)THEN
  PRINT *,'Error accessing the cit_grid file'
  STOP 'Fatal Error (check pre-processor list file)'
END IF
READ(7,1)
READ(7,*)nonUniform,dumpgridfiles

!-----------------------------------------------------------------------------|
!  Check overlap between CTM and CIT grids
!  CIT format gives SW corner not center as with CTM- move it.
!-----------------------------------------------------------------------------|
emsn_x_offset=emsn_x+0.5*emsn_dx
emsn_y_offset=emsn_y+0.5*emsn_dy
emsn_x_end=emsn_x_offset+(emsn_nx-1)*emsn_dx
emsn_y_end=emsn_y_offset+(emsn_ny-1)*emsn_dy
emsn_x_center=(emsn_x_offset+ emsn_x_end)*0.5
emsn_y_center=(emsn_y_offset+ emsn_y_end)*0.5

!-------------------------------------------------------------------------------
! Read weekday emissions file
!-------------------------------------------------------------------------------
READ(7,1)
READ(7,2) INaems_fname
INaemsFform=retrieveCITemsFormat(TRIM(INaems_fname))

IF(INaemsFform)THEN
   OPEN(unit=INaemsFunit,file=TRIM(INaems_fname),status='old',IOSTAT=ios, &
   form='unformatted')
ELSE
   OPEN(unit=INaemsFunit,file=TRIM(INaems_fname),status='old',IOSTAT=ios)
END IF

ne=retrieveCIT_speciesNumbers(INaemsFunit,INaems_fname)
IF(ne <=1)STOP 'Error reading CIT species numbers'
ALLOCATE (aems_mw(ne))
ALLOCATE (aems_lcca(ne))
REWIND (INaemsFunit)
done= retrieveCITaems_header(ne,INaemsFunit,INaemsFform,aems_nlcc,aems_lcca,aems_mw)

!-------------------------------------------------------------------------------
! Read  weekend emissions file
!-------------------------------------------------------------------------------
READ(7,1)
READ(7,2)INaems_fname2
INaemsFform2=retrieveCITemsFormat(TRIM(INaems_fname2))
IF(INaemsFform2)THEN
   OPEN(unit=INaemsFunit2,file=TRIM(INaems_fname2),status='old',IOSTAT=ios, &
   form='unformatted')
ELSE
   OPEN(unit=INaemsFunit2,file=TRIM(INaems_fname2),status='old',IOSTAT=ios)
END IF
ne2=retrieveCIT_speciesNumbers(INaemsFunit2,INaems_fname2)
IF(ne.ne.ne2) THEN
  WRITE(*,*)'number of species in weekDAY and WeekEND file differs: ',ne,ne2
  STOP
ELSE  ! set file position after heading 
  WRITE(*,*)'number of species in weekDAY and WeekEND file are same'
  REWIND (INaemsFunit2)
  done= retrieveCITaems_header(ne,INaemsFunit2,INaemsFform2,aems_nlcc,aems_lcca,aems_mw)
END IF

!-------------------------------------------------------------------------------
! READ output file name
! Read hour offset
!-------------------------------------------------------------------------------
READ(7,1)
READ(7,2)OUTaems_genericName
offset=0
READ(7,1,IOSTAT=ios)
IF(ios == 0)READ(7,*)offset

!-------------------------------------------------------------------------------
! Read in speciation data (mass fractions)
!-------------------------------------------------------------------------------
READ(7,1)
READ(7,*)PM2p5oPM10,ECoPM,OCoPM,DUoPM,LEVOoPM,SO3oPM,SO3oSOx
READ(7,1)
READ(7,*)(vbs(s),s=1,nvbs)
READ(7,1)
READ(7,*)IOLEoOLE,ALDXoALD2,ETHAoPAR
READ(7,1)
READ(7,*)PTOLoTOL,PXYLoXYL,PBNZoTOL
IF(PM2p5oPM10 > 1.0 .OR. PM2p5oPM10 < 0.)THEN
  PRINT *,'Error, PM2.5:PM10 fraction is out of range: ',PM2p5oPM10
  STOP 'Fatal error'
END IF
PM2p5to10=1.0-PM2p5oPM10

!-------------------------------------------------------------------------------
! GLOMAP definitions
!-------------------------------------------------------------------------------
READ(7,1)
READ(7,*)glomap
IF(glomap)THEN

!-------------------------------------------------------------------------------
! Sulfate; 
!-------------------------------------------------------------------------------
  READ(7,1)
  READ(7,2)line 
  READ(line,*)n_su              !number of modes
  ALLOCATE(m_su(MAX(n_su,1)))   !mode numbers
  ALLOCATE(d_su(MAX(n_su,1)))   !mode diameter (nm)
  ALLOCATE(gsig_su(MAX(n_su,1)))!geometric standard deviation
  ALLOCATE(f_su(MAX(n_su,1)))   !mass fraction
  IF(n_su > 0)THEN
    READ(line,*)n_su,(m_su(i),d_su(i),gsig_su(i),f_su(i),i=1,n_su)
  ELSE
    PRINT *,'Error, must define at least 1 sulfate emission mode'
    STOP 'Fatal error'
  END IF !sulphate

!-------------------------------------------------------------------------------
! ec/oc; 
!-------------------------------------------------------------------------------
  READ(7,1)
  READ(7,2)line       
  READ(line,*)n_ecoc                !number of modes
  ALLOCATE(m_ecoc(MAX(n_ecoc,1)))   !mode numbers
  ALLOCATE(d_ecoc(MAX(n_ecoc,1)))   !mode diameter (nm)
  ALLOCATE(gsig_ecoc(MAX(n_ecoc,1)))!geometric standard deviation
  ALLOCATE(f_ecoc(MAX(n_ecoc,1)))   !mass fraction
  IF(n_ecoc > 0)THEN
    READ(line,*)n_ecoc,(m_ecoc(i),d_ecoc(i),gsig_ecoc(i),f_ecoc(i),i=1,n_ecoc)
  ELSE
    PRINT *,'Error, must define at least 1 ec/oc emission mode'
    STOP 'Fatal error'
  END IF !carbon

!-------------------------------------------------------------------------------
! du; 
!-------------------------------------------------------------------------------
  READ(7,1)
  READ(7,2)line       
  READ(line,*)n_du              !number of modes
  ALLOCATE(m_du(MAX(n_du,1)))   !mode numbers
  ALLOCATE(d_du(MAX(n_du,1)))   !mode diameter (nm)
  ALLOCATE(gsig_du(MAX(n_du,1)))!geometric standard deviation
  ALLOCATE(f_du(MAX(n_du,1)))   !mass fraction
  IF(n_du > 0)THEN
    READ(line,*)n_du,(m_du(i),d_du(i),gsig_du(i),f_du(i),i=1,n_du)
  ELSE
    PRINT *,'Error, must define at least 1 dust emission mode'
    STOP 'Fatal error'
  END IF !dust
ELSE !skip over glomap definitions
  DO l=1,6
    READ(7,1)
  END DO
END IF !glomap

!-------------------------------------------------------------------------------
! Search for cbiv master species
! Note this is now moved above the emission inputs- in order to 
!  trap PART which has different input units (mec 12/08/2015)
!-------------------------------------------------------------------------------
lno=-1
lno2=-1
lso2=-1
lpm=-1
lald2=-1
lole=-1
lpar=-1
ltol=-1
lxyl=-1
DO s=1,ne
  IF(aems_lcca(s) == 'NO')THEN
    lno=s
  ELSEIF(aems_lcca(s) == 'NO2')THEN
    lno2=s
  ELSEIF(aems_lcca(s) == 'SO2')THEN
    lso2=s
  ELSEIF(aems_lcca(s) == 'PART')THEN
    lpm=s
  ELSEIF(aems_lcca(s) == 'ALD2')THEN
    lald2=s
  ELSEIF(aems_lcca(s) == 'OLE')THEN
    lole=s
  ELSEIF(aems_lcca(s) == 'PAR')THEN
    lpar=s
  ELSEIF(aems_lcca(s) == 'TOL')THEN
    ltol=s
  ELSEIF(aems_lcca(s) == 'XYL')THEN
    lxyl=s
  END IF
END DO
IF(lno+lno2+lso2+lpm+lald2+lole+lpar+ltol+lxyl < 9)THEN
  PRINT *,'Cannot find a species'
  PRINT *,'no: ',lno
  PRINT *,'no2: ',lno2
  PRINT *,'so2: ',lso2
  PRINT *,'pm: ',lpm
  PRINT *,'ald2: ',lald2
  PRINT *,'lole: ',lole
  PRINT *,'ltol: ',ltol
  PRINT *,'lxyl: ',lxyl
  STOP 'fatal error'
END IF

!-------------------------------------------------------------------------------
! READ input CIT emissions for Wkday and wkEnd
! Added code to dump raw CIT emissions to a Surfer file 10/1/2011
! Added code to dump out hourly emissions 19/10/2011
!-------------------------------------------------------------------------------
ALLOCATE(WdayEmsn(ne,emsn_nx,emsn_ny,hoursPerDay))
ALLOCATE(WendEmsn(ne,emsn_nx,emsn_ny,hoursPerDay))
ALLOCATE(dailyEmissions(emsn_nx,emsn_ny))
ALLOCATE(hourlyEmissions(ne,hoursPerDay))
WdayEmsn=0.0
WendEmsn=0.0
done=retrieveCIT_emissions(INaemsFunit,INaemsFform,ne,emsn_nx,emsn_ny,hoursPerDay, &
                   emsn_dx,emsn_dy,aems_mw,WdayEmsn,offset,   &
                   model,emsn_x_center,emsn_y_center,lpm)
IF(.NOT.done)STOP 'Error retrieving weekday emissions'

!-------------------------------------------------------------------------------
! Write out diagnostics
!-------------------------------------------------------------------------------
IF(debug)THEN
 hourlyEmissions=0.0
 DO s=1,ne
   IF(dumpGridFiles)THEN
     dailyEmissions=0.0
     DO j=1,emsn_ny
       DO i=1,emsn_nx
         dailyEmissions(i,j)=dailyEmissions(i,j)+SUM(WdayEmsn(s,i,j,:))
       END DO
    END DO
    dailyEmissions=dailyEmissions*3.6
    CALL surfer_dump(99,emsn_nx,emsn_ny,emsn_x,emsn_y,  &
         emsn_dx,emsn_dy,dailyEmissions,'CITemissions_wkday_'//TRIM(aems_lcca(s))//'_kgPerDay.grd')
  END IF !write out grid files
  DO t=1,hoursPerDay
   hourlyEmissions(s,t)=hourlyEmissions(s,t)+SUM(WdayEmsn(s,:,:,t))
  END DO
 END DO
!
 hourlyEmissions=hourlyEmissions*3.6
 OPEN(UNIT=99,FILE='wkday_emissions_kgPerHour.csv',STATUS='UNKNOWN')
 WRITE(99,2)'hh',(',',TRIM(aems_lcca(s))//' (kg/h)',s=1,ne)
2 FORMAT(500a)
 DO t=1,hoursPerDay
  WRITE(99,3)t-1,(',',hourlyEmissions(s,t),s=1,ne)
3 FORMAT(i4,100(a,E14.7))
 END DO
 CLOSE(99)
END IF !output diagnostics?

done=retrieveCIT_emissions(INaemsFunit2,INaemsFform2,ne,emsn_nx,emsn_ny,hoursPerDay, &
                   emsn_dx,emsn_dy,aems_mw,WendEmsn,offset,   &
                   model,emsn_x_center,emsn_y_center,lpm)
IF(.NOT.done)STOP 'Error retrieving weekday emissions'

!-------------------------------------------------------------------------------
! Write out diagnostics
!-------------------------------------------------------------------------------
IF(debug)THEN
 hourlyEmissions=0.0
 DO s=1,ne
   IF(dumpGridFiles)THEN
     dailyEmissions=0.0
     DO j=1,emsn_ny
       DO i=1,emsn_nx
         dailyEmissions(i,j)=dailyEmissions(i,j)+SUM(WendEmsn(s,i,j,:))
       END DO
    END DO
    dailyEmissions=dailyEmissions*3.6
    CALL surfer_dump(99,emsn_nx,emsn_ny,emsn_x,emsn_y,  &
         emsn_dx,emsn_dy,dailyEmissions,'CITemissions_wkend_'//TRIM(aems_lcca(s))//'_kgPerDay.grd')
  END IF !write out grid files
  DO t=1,hoursPerDay
   hourlyEmissions(s,t)=hourlyEmissions(s,t)+SUM(WendEmsn(s,:,:,t))
  END DO
 END DO
!
 hourlyEmissions=hourlyEmissions*3.6
 OPEN(UNIT=99,FILE='wkend_emissions_kgPerHour.csv',STATUS='UNKNOWN')
 WRITE(99,2)'hh',(',',TRIM(aems_lcca(s))//' (kg/h)',s=1,ne)
 DO t=1,hoursPerDay
  WRITE(99,3)t-1,(',',hourlyEmissions(s,t),s=1,ne)
 END DO
 CLOSE(99)
END IF !write out diagnostics
!
CLOSE (7)
CLOSE (INaemsFunit)
CLOSE (INaemsFunit2)

!-------------------------------------------------------------------------------
! Loop over selected days
!-------------------------------------------------------------------------------
iyearOld=0
imonthOld=0
files=0
DO jdn=jdns,jdne
  day=MOD(jdn,7)
  PRINT *,'Julian day number and day: ',jdn,day
  gDate=GregorianDate(jdn)
  iyear=gDate(1)
  imonth=gDate(2)

!-------------------------------------------------------------------------------
!  If a new month or year, then open a new file and write out the headers
!------------------------------------------------------------------------------- 
  IF(iyear /=iyearOld .OR. imonth /=imonthOld)THEN
    iyearOld=iyear
    imonthOld=imonth
    files=files+1
    IF(files > 1)CLOSE(OUTaemsFunit)
    WRITE(ayear,'(i4)')iyear
    OUTaemsFform=retrieveCITemsFormat(TRIM(OUTaems_genericName))
!    OUTaems_fname= TRIM(ayear)//'_'//TRIM(mm_name(imonth))//'_'//TRIM(OUTaems_genericName)
    OUTaems_fname= TRIM(OUTaems_genericName)
    IF(OUTaemsFform)THEN
      OPEN(UNIT=OUTaemsFunit,FILE=TRIM(OUTaems_fname),STATUS='UNKNOWN',FORM='UNFORMATTED')
    ELSE
      OPEN(UNIT=OUTaemsFunit,FILE=TRIM(OUTaems_fname),STATUS='UNKNOWN',FORM='FORMATTED')
    END IF

!-------------------------------------------------------------------------------
! nonUniform is set if the host NWP data are in lat/long
!-------------------------------------------------------------------------------
    IF(nonUniform)THEN
      done=writeCTM_header(OUTaemsFunit,OUTaemsFform,aems_nlcc,aems_lcca,aems_mw,'nonUniform')
      done=writeCTM_grid(OUTaemsFunit,OUTaemsFform,emsn_nx,emsn_ny,emsn_x_offset,emsn_y_offset,emsn_dx,emsn_dy,zone)
      IF(.NOT.done)STOP'Fatal error- writing emissions header'
    ELSE
      done=writeCTM_header(OUTaemsFunit,OUTaemsFform,aems_nlcc,aems_lcca,aems_mw)
      IF(OUTaemsFform)THEN
        WRITE(OUTaemsFunit)emsn_nx,emsn_ny,emsn_dx,emsn_dy,emsn_x_center,emsn_y_center
      ELSE
        WRITE(OUTaemsFunit,100)emsn_nx,emsn_ny,emsn_dx,emsn_dy,emsn_x_center,emsn_y_center
100     FORMAT(i4,1x,i4,2(F12.5,1x),2(F15.3,1x))
      END IF
    END IF !non-uniform domain?
  END IF  !new month? 

!-------------------------------------------------------------------------------
!  This section selects weekday or weekend emissions
!-------------------------------------------------------------------------------
  SELECT CASE (day)
    CASE (Sat)
      PRINT *,'Saturday'
      done=writeCTMemissions(OUTaemsFunit,OUTaemsFform,ne,nx,ny,hoursPerDay,WendEmsn)
      IF(.NOT.done)STOP'Fatal error writing out emissions'
    CASE (Sun)
      done=writeCTMemissions(OUTaemsFunit,OUTaemsFform,ne,nx,ny,hoursPerDay,WendEmsn)
      PRINT *,'Sunday'
      IF(.NOT.done)STOP'Fatal error writing out emissions'
    CASE DEFAULT
      PRINT *,'Weekday'
      done=writeCTMemissions(OUTaemsFunit,OUTaemsFform,ne,nx,ny,hoursPerDay,WdayEmsn)
      IF(.NOT.done)STOP'Fatal error writing out emissions'
  END SELECT
       
!     DO h=1,hoursPerDay
!       WRITE(outAll_hourly,*)yT(1),comma,mT(1),comma,dT(1),comma,h-1,(comma,    &
!                     SUM(DayEmsn(s,:,:,h))*gsTokghr,s=1,ne)
!     END DO
               
END DO !Julian day loop
CLOSE (OUTaemsFunit)

!-------------------------------------------------------------------------------
! Finished. Close up files and deallocate space
!-------------------------------------------------------------------------------
PRINT *,'Finished writing CTM emissions file'
IF(glomap)THEN
  DEALLOCATE(m_ecoc)
  DEALLOCATE(d_ecoc)
  DEALLOCATE(gsig_ecoc)
  DEALLOCATE(f_ecoc)
!
  DEALLOCATE(m_su)
  DEALLOCATE(d_su)
  DEALLOCATE(gsig_su)
  DEALLOCATE(f_su)
!
  DEALLOCATE(m_du)
  DEALLOCATE(d_du)
  DEALLOCATE(gsig_du)
  DEALLOCATE(f_du)
END IF
!
DEALLOCATE (aems_mw)
DEALLOCATE (aems_lcca)
DEALLOCATE(WdayEmsn)
DEALLOCATE(WendEmsn)
DEALLOCATE(dailyEmissions)
DEALLOCATE(hourlyEmissions)
STOP 'Success!'
!-------------------------------------------------------------------------------
! EOF or read error with temperature file
!-------------------------------------------------------------------------------
99 PRINT *,'Error or eof in the temperature file'
STOP'fatal error'
END PROGRAM CIT2CTMmvems_GLO


FUNCTION writeCTM_header(funit,fform,nlcc,CITspecies,mw,nonUniform)
!-------------------------------------------------------------------------------
! Generate header for CTM aems file
!-------------------------------------------------------------------------------
USE cb05_module
IMPLICIT NONE
LOGICAL :: writeCTM_header
INTEGER, INTENT(IN) :: funit  !file unit
LOGICAL, INTENT(IN) :: fform  !file unit
INTEGER, INTENT(IN) :: nlcc !number of inventory species
REAL, DIMENSION(nlcc), INTENT(IN) :: mw               !molecular weights
CHARACTER(LEN=4), DIMENSION(nlcc), INTENT(IN) :: CITspecies !species names
CHARACTER(LEN=10), OPTIONAL, INTENT(IN) :: nonUniform !non-uniform grid
CHARACTER(LEN=80) :: comment
CHARACTER(LEN=8) :: systemDate
CHARACTER(LEN=10) :: systemTime
INTEGER :: i
writeCTM_header=.FALSE.

CALL DATE_and_TIME(systemDate,systemTime)
IF(Fform)THEN
  comment='version_02'
  WRITE(funit,ERR=94)comment
  comment='CTM surface emissions file. Generated by CIT2CTMmvems_GLO'
  WRITE(funit,ERR=94)comment
  comment='File was created (yyyymmdd): '//systemDate//' at time (hhmmss.sss): '//systemTime
  WRITE(funit,ERR=94)comment
  comment='GramPerSec :TAPM emission units of g/s'
  WRITE(funit,ERR=94)comment
  IF(PRESENT(nonUniform))THEN
    comment=nonUniform//' flag to indicate a non-uniform grid'
    WRITE(funit,ERR=94)comment
    print *,'non-uniform grid'
  ELSE
    comment='mvems are in a uniform grid'
    WRITE(funit,ERR=94)comment
  END IF  
  comment='*'
  WRITE(funit,ERR=94)comment
!
  IF(cb05 .AND. .NOT.glomap)THEN
    WRITE(funit,ERR=94)nlcc+ncb05
  ELSEIF(cb05 .AND. glomap)THEN
    WRITE(funit,ERR=94)nlcc+ncb05+nglo
  ELSE
    WRITE(funit,ERR=94)nlcc
  END IF
  DO i=1,nlcc
    WRITE(funit,ERR=94)i,CITspecies(i),mw(i)
  END DO
  IF(cb05)THEN
    DO i=1,ncb05
      WRITE(funit,ERR=94)i+nlcc,cb05species(i),cb05mw(i)
    END DO
  END IF
  IF(glomap)THEN
    DO i=1,nglo
      WRITE(funit,ERR=94)i+nlcc+ncb05,glospecies(i),glomw(i)
    END DO
  END IF
ELSE
  WRITE(funit,1,ERR=94)'Version_02'
  WRITE(funit,1,ERR=94)'CTM surface emissions file. Generated by CIT2CTMmvems_GLO'
1 FORMAT(100a)
  WRITE(funit,1,ERR=94)'File was created (yyyymmdd): ',systemDate,' at time (hhmmss.sss): ',systemTime
  WRITE(funit,1,ERR=94)'GramPerSec :TAPM emission units of g/s'
  IF(PRESENT(nonUniform))THEN
    comment=nonUniform//' flag to indicate a non-uniform grid'
    WRITE(funit,1,ERR=94)comment
  ELSE
    comment='mvems are in a uniform grid'
    WRITE(funit,1,ERR=94)comment
  END IF  
  WRITE(funit,1,ERR=94)'*'
  IF(cb05 .AND. .NOT.glomap)THEN
    WRITE(funit,*,ERR=94)nlcc+ncb05
  ELSEIF(cb05 .AND.glomap)THEN
    WRITE(funit,*,ERR=94)nlcc+ncb05+nglo
  ELSE
    WRITE(funit,*,ERR=94)nlcc
  END IF
  DO i=1,nlcc
    WRITE(funit,2,ERR=94)i,CITspecies(i),mw(i)
2   FORMAT(i3,1x,a,1x,F5.1)
  END DO
  IF(cb05)THEN
    DO i=1,ncb05
      WRITE(funit,2,ERR=94)i+nlcc,cb05species(i),cb05mw(i)
    END DO
  END IF
  IF(glomap)THEN
    DO i=1,nglo
      WRITE(funit,2,ERR=94)i+nlcc+ncb05,glospecies(i),glomw(i)
    END DO
  END IF
END IF
writeCTM_header=.TRUE.
RETURN
!-------------------------------------------------------------------------------
! Errors
!-------------------------------------------------------------------------------
94 PRINT *,'Error writing CTM header to binary file'
RETURN
END FUNCTION writeCTM_header

FUNCTION writeCTMemissions(fileUnit,fileform,numberSpecies,nx,ny,nh,grid)
!-------------------------------------------------------------------------------
!Function to write out an hour of CTM format gridded emissions
!
! Modifications
!  When         Who      What
! 15/08/15      mec   Flags for fdp or shipping speciation
! 20/05/15      mec   Modifed to use generic parameters
! 02/12/14      mec   Source index for vehicle source groups added
! 18/01/13      mec   Added in GLOMAP particle component and number density
! 13/06/12      mec   Removed bug which progressively scaled back some cb05 species
! 14/03/12      mec   Add ptol and pxyl
!-------------------------------------------------------------------------------
USE cb05_module
IMPLICIT NONE
LOGICAL :: writeCTMemissions!true is successful write
INTEGER, INTENT(IN) :: fileUnit !I/O unit number
LOGICAL, INTENT(IN) :: fileform
INTEGER, INTENT(IN) :: nh            !number of hours to output
INTEGER, INTENT(IN) :: numberSpecies !number of inventory species
INTEGER, INTENT(IN) :: nx            !number east/west points
INTEGER, INTENT(IN) :: ny            !number of north/south points
REAL, DIMENSION(numberSpecies,nx,ny,nh), INTENT(IN) :: grid
REAL, DIMENSION(numberSpecies) :: gridpoint
!
INTEGER :: i,j,k,s,h,v,l
LOGICAL :: found
REAL, PARAMETER:: small=1.0E-25
writeCTMemissions=.FALSE.
!
DO h=1,nh
  PRINT *,'Writing emissions for hour: ',h-1
  IF(cb05)THEN
      DO j=1,ny
        DO i=1,nx

!-------------------------------------------------------------------------------
! Here we calculate cb05_aer2 species emissions
!-------------------------------------------------------------------------------
          gridpoint=grid(:,i,j,h)
          etha=gridpoint(lpar)*ETHAoPAR
          gridpoint(lpar)=gridpoint(lpar)*(1.-ETHAoPAR)
          iole=gridpoint(lole)*IOLEoOLE
          gridpoint(lole)=gridpoint(lole)*(1.-IOLEoOLE)
          aldx=gridpoint(lald2)*ALDXoALD2
          gridpoint(lald2)=gridpoint(lald2)*(1.-ALDXoALD2)
!
          oc25=gridpoint(lpm)*PM2p5oPM10*OCoPM
          oc10=gridpoint(lpm)*PM2p5to10*OCoPM
          ec25=gridpoint(lpm)*PM2p5oPM10*ECoPM
          ec10=gridpoint(lpm)*PM2p5to10*ECoPM
          ot25=gridpoint(lpm)*PM2p5oPM10*DUoPM
          ot10=gridpoint(lpm)*PM2p5to10*DUoPM
          levo=gridpoint(lpm)*LEVOoPM
!
          IF(SO3oPM < 0)THEN
            aso4=gridpoint(lso2)*PM2p5oPM10*SO3oSOx
            as10=gridpoint(lso2)*PM2p5to10*SO3oSOx
            gridpoint(lso2)=gridpoint(lso2)-aso4-as10
          ELSE
            aso4=gridpoint(lpm)*PM2p5oPM10*SO3oPM
            as10=gridpoint(lpm)*PM2p5to10*SO3oPM
          END IF
!
          ftol=gridpoint(ltol)*PTOLoTOL
          fxyl=gridpoint(lxyl)*PXYLoXYL
          fbnz=gridpoint(ltol)*PBNZoTOL
!
          DO v=1,nvbs
            apa(v)=(oc25+oc10)*vbs(v)
          END DO
!
          sox1=0.0
          sox2=0.0
          sox3=0.0
          
!-------------------------------------------------------------------------------
!  GLOMAP
! .. Calculate total particle volume (nm3 per m2 per s)
! .. Calculate total particle number (per m2 per s)
!  Sulfate - load cmp mass into su2 and su3, number into aits, accs
!-------------------------------------------------------------------------------
          IF(glomap)THEN
            su=0.0
            bc=0.0
            oc=0.0
            du=0.0
            nd=0.0

!-------------------------------------------------------------------------------
!   Sulfate
!    1.0E-03 is g/s to kg/s
!-------------------------------------------------------------------------------
            DO l=1,n_su
              su(m_su(l))=f_su(l)*(aso4+as10)
              modevol=1.0E27*(su(m_su(l)))/rhocomp(su_p)*1.0E-03        
              lgsd=LOG(gsig_su(l))
              nd(m_su(l))=modevol/((ppi/6.0)*(d_su(l)**3.0)*EXP(4.5*lgsd*lgsd))+nd(m_su(l))
            END DO !sulfate
       
!-------------------------------------------------------------------------------
!   BC and EC
!   InSoluble Aitken mode
!-------------------------------------------------------------------------------
            DO l=1,n_ecoc
              bc(m_ecoc(l))=f_ecoc(l)*(ec25+ec10)
              oc(m_ecoc(l))=0.0
              DO s=1,nInvolatile
                oc(m_ecoc(l))=oc(m_ecoc(l))+apa(s)
              END DO
              modevol=1.0E27*(bc(m_ecoc(l))/rhocomp(bc_p)+oc(m_ecoc(l))/rhocomp(oc_p))*1.0E-03       
              lgsd=LOG(gsig_ecoc(l))
              nd(m_ecoc(l))=modevol/((ppi/6.0)*(d_ecoc(l)**3.0)*EXP(4.5*lgsd*lgsd))+nd(m_ecoc(l))
              !nd(m_ecoc(l))=modevol/((ppi/6.0)*(d_ecoc(1)**3.0)*EXP(4.5*lgsd*lgsd))+nd(m_ecoc(l))
            END DO !ecoc

!-------------------------------------------------------------------------------
!   Dust
!-------------------------------------------------------------------------------
            DO l=1,n_du
              du(m_du(l))=f_du(l)*(ot25+ot10)
              modevol=1.0E27*(du(m_du(l)))/rhocomp(du_p)*1.0E-03        
              lgsd=LOG(gsig_du(l))
              nd(m_du(l))=modevol/((ppi/6.0)*(d_du(l)**3.0)*EXP(4.5*lgsd*lgsd))+nd(m_du(l))
            END DO  !dust

!-------------------------------------------------------------------------------
!   Output emissions in CTM format
!-------------------------------------------------------------------------------    
            IF(fileform)THEN
             IF(doSOx)then
              WRITE(fileUnit,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo,sox1,sox2,sox3,          &
                       su(NUCS),su(AITS),su(ACCS),su(COAS),                          &
                       bc(AITS),bc(ACCS),bc(COAS),bc(AITI),                          &
                       oc(NUCS),oc(AITS),oc(ACCS),oc(COAS),oc(AITI),                 &
                       du(ACCS),du(COAS),du(ACCI),du(COAI),                          &
                       nd(NUCS),nd(AITS),nd(ACCS),nd(COAS),nd(AITI),nd(ACCI),nd(COAI)                  
             ELSE
              WRITE(fileUnit,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo,                         &
                       su(NUCS),su(AITS),su(ACCS),su(COAS),                          &
                       bc(AITS),bc(ACCS),bc(COAS),bc(AITI),                          &
                       oc(NUCS),oc(AITS),oc(ACCS),oc(COAS),oc(AITI),                 &
                       du(ACCS),du(COAS),du(ACCI),du(COAI),                          &
                       nd(NUCS),nd(AITS),nd(ACCS),nd(COAS),nd(AITI),nd(ACCI),nd(COAI)                  
             END IF
            ELSE !formatted output
             IF(doSOx)then
              WRITE(fileUnit,1,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo,sox1,sox2,sox3,          &
                       su(NUCS),su(AITS),su(ACCS),su(COAS),                          &
                       bc(AITS),bc(ACCS),bc(COAS),bc(AITI),                          &
                       oc(NUCS),oc(AITS),oc(ACCS),oc(COAS),oc(AITI),                 &
                       du(ACCS),du(COAS),du(ACCI),du(COAI),                          &
                       nd(NUCS),nd(AITS),nd(ACCS),nd(COAS),nd(AITI),nd(ACCI),nd(COAI)                  
             ELSE
              WRITE(fileUnit,1,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo,sox1,sox2,sox3,          &
                       su(NUCS),su(AITS),su(ACCS),su(COAS),                          &
                       bc(AITS),bc(ACCS),bc(COAS),bc(AITI),                          &
                       oc(NUCS),oc(AITS),oc(ACCS),oc(COAS),oc(AITI),                 &
                       du(ACCS),du(COAS),du(ACCI),du(COAI),                          &
                       nd(NUCS),nd(AITS),nd(ACCS),nd(COAS),nd(AITI),nd(ACCI),nd(COAI)                  
            END IF                    
  1         FORMAT(8e10.3)
           END IF !formatted output
          ELSE
            IF(fileform)THEN
             IF(doSOx)then
              WRITE(fileUnit,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo,sox1,sox2,sox3
             ELSE
              WRITE(fileUnit,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo        
             END IF
            ELSE !formatted output
             IF(doSOx)then
              WRITE(fileUnit,1,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo,sox1,sox2,sox3
             ELSE
              WRITE(fileUnit,1,ERR=90)(gridpoint(k), k=1,numberSpecies), &
                       etha,iole,aldx,oc25,oc10,ec25,ec10,ot25,ot10,aso4,as10,   &
                      (apa(v),v=1,nvbs),ftol,fxyl,fbnz,levo
             END IF
            END IF                    
          
          END IF  !GLOMAP?
        END DO !i
      END DO  !j 
    ELSE !dont do cb05 speciation
      DO j=1,ny
        DO i=1,nx
          WRITE(fileUnit,ERR=90)(grid(k,i,j,h), k=1,numberSpecies) 
        END DO !i
      END DO  !j
    END IF
END DO  !h
writeCTMemissions=.TRUE.
RETURN
!-------------------------------------------------------------------------------
! i/o error
!-------------------------------------------------------------------------------
90 PRINT *,'Error writing to Tapm emission file'
PRINT *,'Hour and i-grid; j-grid: ',h-1,i,j
PRINT *,'Fatal error'
RETURN
END FUNCTION writeCTMEmissions

FUNCTION retrieveCITemsFormat(fname)
!-----------------------------------------------------------------------------|
! Function to determine if a CIT emissions file is Fortran unformatted
!-----------------------------------------------------------------------------|
USE CTM_debug
USE CTM_generic_interfaces
IMPLICIT NONE
CHARACTER(LEN=*), INTENT(IN) :: fname !file name
LOGICAL :: retrieveCITemsFormat
INTEGER :: nameLength
!
retrieveCITemsFormat=.FALSE.
nameLength=LEN_TRIM(fname)
if(UPPERcase(fname(nameLength-3:nameLength),4) /= '.BIN')return
retrieveCITemsFormat=.TRUE.
END FUNCTION retrieveCITemsFormat

FUNCTION retrieveCITgrid(fileUnit,fileName,model,nxCIT,nyCIT,   &
            dxCIT,dyCIT,x0CIT,y0CIT,zone)
!-------------------------------------------------------------------------------
! Routine reads a geographic file description in CIT_grid.dat format
!-------------------------------------------------------------------------------
USE CTM_debug
IMPLICIT NONE
LOGICAL :: retrieveCITgrid
INTEGER, INTENT(IN) :: fileUnit !file i/o unit
CHARACTER(LEN=*) :: fileName  !file name
CHARACTER(LEN=4), INTENT(OUT) :: model
INTEGER, INTENT(OUT) :: nxCIT !number east/west points
INTEGER, INTENT(OUT) :: nyCIT !number north/south points
REAL, INTENT(OUT) :: dxCIT !east/west grid spacing
REAL, INTENT(OUT) :: dyCIT !north/south grid spacing
REAL, INTENT(OUT) :: x0CIT !sw coordinate
REAL, INTENT(OUT) :: y0CIT !sw coordinate
INTEGER, INTENT(OUT) :: zone !MGA zone

CHARACTER(LEN=8) :: date !yyyymmdd
INTEGER :: ios
REAL :: top      !top of CIT model domain
REAL :: lat,long
!
retrieveCITgrid=.FALSE.
!
! Read in CIT grid
!
OPEN(fileUnit,file=TRIM(fileName),status='OLD',iostat=ios)
IF(ios > 0)THEN
  WRITE(*,*)'Error opening the grid definition file: ',TRIM(fileName)
  RETURN
END IF
READ(fileUnit,2,ERR=90,END=90)model
READ(fileUnit,1,ERR=90,END=90)
READ(fileUnit,*,ERR=90,END=90)date(1:4),date(5:6),date(7:8)
READ(fileUnit,1,ERR=90,END=90)
READ(fileUnit,*,ERR=90,END=90)nxCIT,nyCIT,x0CIT,y0CIT,    &
                              dxCIT,dyCIT,top,            &
                              lat,long,zone
CLOSE(fileUnit)
retrieveCITgrid=.TRUE.
RETURN
90 IF(debug)WRITE(outD,*)'Error reading from CIT_grid file'
CLOSE(fileUnit)
RETURN
1 FORMAT()
2 FORMAT(a)
END FUNCTION retrieveCITgrid

FUNCTION retrieveCIT_speciesNumbers(fUnit,fName)
!-----------------------------------------------------------------------------|
! Function to retrieve header from CIT emissions file
!-----------------------------------------------------------------------------|
USE CTM_debug
IMPLICIT NONE
INTEGER, INTENT(IN) :: fUnit
CHARACTER(LEN=*), INTENT(IN) :: fName
INTEGER :: retrieveCIT_speciesNumbers
!
INTEGER :: nlcc,ios
LOGICAL :: emsForm
LOGICAL, EXTERNAL :: retrieveCITemsFormat
CHARACTER(LEN=80) :: comment
retrieveCIT_speciesNumbers=-1

!-----------------------------------------------------------------------------|
! Read pems header
!-----------------------------------------------------------------------------|
if(debug)then
  write(outD,'()')
  write(outD,*)'Reading CIT emissions file header'
end if
emsForm = retrieveCITemsFormat(TRIM(fName))
do
  if(emsForm)then
    read(fUnit,end=2,err=3)comment
  else
    read(fUnit,1,end=2,err=2)comment
  1 format(a)
  end if
  if(comment(1:1) == '*') exit
  if(debug)write(outD,*)trim(comment)
end do

!-----------------------------------------------------------------------------|
!   read in number of species names
!-----------------------------------------------------------------------------|
if(emsForm)then
  read(fUnit,end=2,err=2)nlcc
else
  read(fUnit,*,end=2,err=2)nlcc
end if
if(debug)write(outD,*)'CIT emissions file: species names for ',nlcc,' species'
retrieveCIT_speciesNumbers=nlcc
return

!-----------------------------------------------------------------------------|
! Error
!-----------------------------------------------------------------------------|
2 if(debug)write(outD,*)'File error reading CIT emissions file header'
  return
3 print *,'File error'
  return
END FUNCTION retrieveCIT_speciesNumbers

FUNCTION retrieveCITaems_header(ne,funit,fForm,nlcc,lcca,mw)
!-----------------------------------------------------------------------------|
! Function to retrieve header from CIT emissions file
! Modified MC 11/2006 to search for the keywords
!  gramPerSec and nonUniform
!-----------------------------------------------------------------------------|
USE CTM_debug
USE ctm_generic_interfaces
IMPLICIT NONE
INTEGER, INTENT(IN) :: ne    !number of species from pems file
INTEGER, INTENT(IN) :: funit !file unit
LOGICAL, INTENT(IN) :: fForm !file format (.TRUE.=unformatted)
INTEGER, INTENT(OUT):: nlcc !number of species
CHARACTER(LEN=4), DIMENSION(*), INTENT(OUT) :: lcca !species names
REAL, DIMENSION(*), INTENT(OUT) :: mw               !molecular weights
LOGICAL :: retrieveCITaems_header
!
INTEGER :: l,i
CHARACTER(LEN=80) :: comment
retrieveCITaems_header=.FALSE.
!-----------------------------------------------------------------------------|
! Read header
!-----------------------------------------------------------------------------|
   if(debug)then
     write(outD,'()')
     write(outD,*)'Reading CIT emissions file header'
   end if
   do
     if(fForm)then
       read(funit,end=2,err=2)comment
     else
       read(funit,1,end=2,err=2)comment
       1 format(a)
     end if
     if(comment(1:1) == '*') exit
     comment=TRIM(ADJUSTL(comment))
     if(debug)write(outD,*)trim(comment)
   end do

!-----------------------------------------------------------------------------|
!   read in number of species names
!-----------------------------------------------------------------------------|
   if(fForm)then
     read(funit)nlcc
   else
     read(funit,*)nlcc
   end if
   if(debug)write(outD,*)'CIT emissions file: species names for ',nlcc,' species'

!-----------------------------------------------------------------------------|
!  Check that the expected and actual number of species match
!-----------------------------------------------------------------------------|
   if(nlcc /= ne)then
     print *,'Number of model species in the aems file is: ',nlcc
     print *,'Expected number of model species in the pems file is: ',ne
     print *,'Species numbers and names must match- please check the emission files'
     stop'Fatal error'
   end if

!-----------------------------------------------------------------------------|
!   read in species names
!-----------------------------------------------------------------------------|
   do i=1,nlcc
     if(fForm)then
       read(funit,end=2,err=2)l,lcca(i),mw(i)
     else
       read(funit,*,end=2,err=2)l,lcca(i),mw(i)
     end if
     if(debug)write(outD,*)l,lcca(i),mw(i)
   end do
   retrieveCITaems_header=.TRUE.
   return

!-----------------------------------------------------------------------------|
! Error
!-----------------------------------------------------------------------------|
2 if(debug)write(outD,*)'File error reading CIT emissions header'
  return
END FUNCTION retrieveCITaems_header
FUNCTION retrieveCIT_emissions(fileUnit,fileForm,numberSpecies,nx,ny,nh,dx,dy,mw,grid,offset, &
                               model,longC,latC,iPART)
!-------------------------------------------------------------------------------
!Function to read in CIT format gridded emissions
!
!             Modifications
! When     Who         What
! 12/08/15 mec    Now filter for PART species when converting from ppm/min to g/s
! 08/09/11 mec    Added hour offset to go from LST to EST or UTC
! 09/06/11 mec    Time counter is now set internally rather than taken from
!                  the CIT file (due to issue with DECCW files)
!-------------------------------------------------------------------------------
IMPLICIT NONE
LOGICAL :: retrieveCIT_emissions !true is successful write
INTEGER, INTENT(IN) :: fileUnit !I/O unit number
LOGICAL, INTENT(IN) :: fileForm !true if binary
INTEGER, INTENT(IN) :: nh       !number of hours
INTEGER, INTENT(IN) :: numberSpecies !number of species
INTEGER, INTENT(IN) :: nx !number east/west points
INTEGER, INTENT(IN) :: ny !number of north/south points
INTEGER, INTENT(IN) :: offset !hour offset
INTEGER, INTENT(IN) :: iPART  !indice of PART species
REAL, INTENT(IN) :: dx,dy !grid spacing
REAL, INTENT(IN) :: longC,latC  !grid centre longitude, latitude
CHARACTER(LEN=4), INTENT(IN) :: model !model (TAPM; CCAM; UM)
REAL, DIMENSION(numberSpecies) :: mw !molecular weight
REAL, DIMENSION(numberSpecies,nx,ny,nh), INTENT(OUT) :: grid
!
REAL, DIMENSION(numberSpecies) :: cellEmissions
INTEGER :: i,j,k,s,h,ih
LOGICAL :: found
REAL, PARAMETER :: small=1.0E-25
REAL, PARAMETER :: Re=6371007
REAL            :: PIO180
!-----------------------------------------------------------------------------|
!     N-air*10-6/60=101325/8.31/298/60*10-6 from ppm-m/min to g/sec
!     c_fac*dx*dy*Mwght will be complete conversion factor
!-----------------------------------------------------------------------------|
REAL            :: factor
REAL, PARAMETER :: c_fac=6.81943e-7
PIO180=4.0*ATAN(1.0)/180.

retrieveCIT_emissions=.FALSE.
print *,ADJUSTL(TRIM(model))
IF(ADJUSTL(TRIM(model))=='CCAM' .OR. ADJUSTL(TRIM(model))=='UM')THEN 
  factor=c_fac*Re*dy*PIO180*Re*COS(latC*PIO180)*dx*PIO180
ELSE
  factor=c_fac*dx*dy
END IF
!
ih=0
!ih=1
grid=0.0
print *,'nx, ny: ',nx,ny
DO !hour loop
  IF(fileFORM)then
    READ(fileUnit,END=1,ERR=2)h,i,j,(cellEmissions(s),s=1,numberSpecies)
  ELSE
  !  READ(fileUnit,200,END=1,ERR=2)h,i,j,(cellEmissions(s),s=1,numberSpecies)
  !200 FORMAT(1x,i2,i3,i3/(8e10.3))

!-----------------------------------------------------------------------------|
! Patch to read revised format for OEH CIT emissions
!-----------------------------------------------------------------------------|
    READ(fileUnit,200,END=1,ERR=2)h,i,j
  200 FORMAT(1x,i2,i3,i3)
    IF(i /= 999 .AND. j /= 999)THEN
      READ(fileUnit,*,END=1,ERR=2)(cellEmissions(s),s=1,numberSpecies)
    ELSE
      READ(fileUnit,'()',END=1,ERR=2)
      READ(fileUnit,'()',END=1,ERR=2)
    END IF 
!-----------------------------------------------------------------------------|
  END IF
  IF(i > 0 .AND. i <= nx .AND. j >0 .AND. j <=ny .AND. h >= 0 .AND. h <nh)THEN
!   ih=MOD(h+offset,24)+1
    ih=MOD(h+offset,24)
    DO
!     PRINT *,'ih: ',ih
      IF(ih >=0)EXIT
      ih=ih+24
    END DO
    ih=ih+1

!-----------------------------------------------------------------------------|
! PART emissions assumed to already be in g/s (mec 12/08/15)
! Bug where the conversion factor was repeatedly applied to the output grid
!  11/01/2011 mec
!-----------------------------------------------------------------------------|
!   grid(:,i,j,ih)=cellEmissions+grid(:,i,j,ih)*mw(:)*factor
!   grid(:,i,j,ih)=grid(:,i,j,ih) + cellEmissions*mw(:)*factor
    DO s=1,numberSpecies
      IF(s==iPART)THEN
        grid(s,i,j,ih)=grid(s,i,j,ih) + cellEmissions(s)
      ELSE
        grid(s,i,j,ih)=grid(s,i,j,ih) + cellEmissions(s)*mw(s)*factor
      END IF
    END DO !species loop
  ELSE
    PRINT *,'Finished processing hour: ',ih-1,h
!    ih=ih+1
!    IF(ih > nh)EXIT
  END IF
END DO
1 retrieveCIT_emissions=.TRUE.
! PRINT *,'Finished processing hour: ',ih-1
RETURN
!-------------------------------------------------------------------------------
! i/o error
!-------------------------------------------------------------------------------
2 PRINT *,'Error reading CIT emission file'
PRINT *,'Hour and i-grid; j-grid: ',h,i,j
PRINT *,'Fatal error'
RETURN
END FUNCTION retrieveCIT_emissions
FUNCTION writeCTM_grid(funit,binary,nx,ny,x0,y0,dx,dy,zone)
IMPLICIT NONE
INTEGER, INTENT(IN) :: funit!file unit
LOGICAL, INTENT(IN) :: binary!output format
INTEGER, INTENT(IN) :: nx !number east/west points
INTEGER, INTENT(IN) :: ny !number north/south points
REAL, INTENT(IN) :: x0   !east/west grid origin (SW cell centre) (m)
REAL, INTENT(IN) :: y0   !north/south grid origin (SW cell centre) (m)
REAL, INTENT(IN) :: dx   !east/west grid spacing (m)
REAL, INTENT(IN) :: dy   !north/south grid spacing (m)
INTEGER, INTENT(IN) :: zone !AMG zone number
!
LOGICAL :: writeCTM_grid
REAL, DIMENSION(nx+1,ny+1) :: x,y !grid points
REAL :: x1,y1
INTEGER :: i,j,nx1,ny1
writeCTM_grid=.FALSE.
nx1=nx+1
ny1=ny+1
!
! generate grid points, converting to AMG
! origin=centre of SW grid cell
!
DO j=1,ny1
  y1=(FLOAT(j)-1.0)*dy+y0
  DO i=1,nx1
    x1=(Float(i)-1.0)*dx+x0
    CALL toGEO(x1,y1,zone,y(i,j),x(i,j))
    y(i,j)=-y(i,j) !negative in southern hemisphere
  END DO
END DO
!
IF(binary)THEN
  WRITE(funit,ERR=1)nx,ny,x(2,1)-x(1,1),y(2,1)-y(1,1),0.5*(x(nx,ny)+x(1,1)),0.5*(y(nx,ny)+y(1,1))
  DO j=1,ny1
    WRITE(funit,ERR=1)(x(i,j),y(i,j),i=1,nx1)
  END DO
ELSE
  WRITE(funit,100,ERR=1)nx,ny,x(2,1)-x(1,1),y(1,2)-y(1,1),0.5*(x(nx,ny)+x(1,1)),0.5*(y(nx,ny)+y(1,1))
  100 FORMAT(i4,1x,i4,4(1x,F12.3))
  DO j=1,ny1
    WRITE(funit,200,ERR=1)(x(i,j),y(i,j),i=1,nx1)
    200 FORMAT(8E13.6)
  END DO
END IF
writeCTM_grid=.TRUE.
RETURN
!
1 PRINT *,'Error writing grid coordinates to CTM file'
PRINT *,'Last i,j: ',i,j
END FUNCTION writeCTM_grid
      SUBROUTINE TOGEO (EAST,NORTH,ZONE,LAT,LONG)
!                   Convert AMG to geographicals
!
!         CONVERT AMG COORDINATES TO GEOGRAPHICALS
!
!         RIGOROUS FORMULAE
!
!         INPUT -     EAST         -  AMG EAST COORDINATE
!                     NORTH        -  AMG NORTH COORDINATE
!                     ZONE         -  AMG ZONE
!
!         OUTPUT -    LAT          -  LATITUDE - S is positive
!                     LONG         -  LONGITUDE - E is positive
!                                     both are in decimals
!                                          eg 37.5 = 37 deg 30 min
!
      IMPLICIT NONE
      REAL    EAST, NORTH, LAT, LONG, PHIDSH, DPHI, OMEGA,     &
                TRUEN, CMEAST, EQNRTH, EDASH, EDASH2,          &
                TANPHI, TAN2, TAN4, COSPHI, PSI, PSI2, PSI3,   &
                NU, RHO, CM, CIRC, EDSHSQ, SF,                 &
                A0, C, D1, D2, D3, D4
      INTEGER ZONE

      DATA CIRC   / 57.295779513D0 /
      DATA CMEAST / 5D5 /
      DATA EQNRTH / 1D7 /
      DATA SF     / .9996D0 /

      DATA C      / 6399617.225D0 /
      DATA EDSHSQ / 6.739660796D-3 /

      DATA A0     / 111133.348785D0 /
      DATA D1     / 2.518887693D-3 /
      DATA D2     / 3.701129D-6 /
      DATA D3     / 7.448D-9 /
      DATA D4     / 1.7D-11 /
!
!         CALCULATE FOOT LATITUDE
!
      TRUEN = (NORTH-EQNRTH)/SF
      PHIDSH = TRUEN/(A0*CIRC)
      DPHI = D1*SIN(2*PHIDSH) + D2*SIN(4*PHIDSH)   &
           + D3*SIN(6*PHIDSH) + D4*SIN(8*PHIDSH)
      PHIDSH = PHIDSH + DPHI
!
!         CALCULATE VALUES OF FUNCTIONS FOR FOOT LATITUDE
!
      TANPHI = ATAN(PHIDSH)
      TAN2 = TANPHI*TANPHI
      TAN4 = TAN2*TAN2
      COSPHI = COS(PHIDSH)
      PSI = 1. + EDSHSQ * COSPHI*COSPHI
      PSI2 = PSI*PSI
      PSI3 = PSI2*PSI
      NU = C/SQRT(PSI)
      RHO = NU/PSI
      EDASH = (EAST-CMEAST)/(SF*NU)
      EDASH2 = EDASH*EDASH
!
!         CALCULATE CORRECTION TO FOOT LATITUDE
!
      DPHI  = EDASH2/2.                                                   &
            - EDASH2**2/24.  * (-4.*PSI2 + 9.*PSI*(1.-TAN2) + 12.*TAN2)   &
            + EDASH2**3/720. * (8.*PSI2*PSI2*(11.-24.*TAN2)               &
                             - 12.*PSI3*(21.-71.*TAN2)                    &
                             + 15.*PSI2*(15.-98.*TAN2+15.*TAN4)           &
                             + 180.*PSI*(5.*TAN2-3.*TAN4) + 360.*TAN4)

      DPHI  = DPHI * PSI*TANPHI
      LAT   = -CIRC*(PHIDSH - DPHI)
!
!         CALCULATE LONGITUDE DIFFERENCE FROM CENTRAL MERIDIAN
!
      CM = 6.*(ZONE-49.)+111.

      OMEGA = 1.                                         &
            - EDASH2/6.      * (PSI + 2.*TAN2)           &
            + EDASH2**2/120. * (-4.*PSI3*(1.-6.*TAN2)    &
                             + PSI2*(9.-68.*TAN2)        &
                             + 72.*PSI*TAN2 + 24.*TAN4)

      OMEGA = OMEGA * EDASH/COSPHI
      LONG  = CM + OMEGA*CIRC

      RETURN
      END
FUNCTION JulianDayNumber(y,m,d)
IMPLICIT NONE
INTEGER :: JulianDayNumber
INTEGER, INTENT(IN) :: y !year
INTEGER, INTENT(IN) :: d !day
INTEGER, INTENT(IN) :: m !month

JulianDayNumber = ( 1461 * ( y + 4800 + ( m - 14 ) / 12 ) ) / 4 +   &
                 ( 367 * ( m - 2 - 12 * ( ( m - 14 ) / 12 ) ) ) / 12 - &
                 ( 3 * ( ( y + 4900 + ( m - 14 ) / 12 ) / 100 ) ) / 4 + &
                  d - 32075
END FUNCTION JulianDayNumber

FUNCTION GregorianDate(jd)
IMPLICIT NONE
INTEGER, DIMENSION(3) :: GregorianDate
INTEGER, INTENT(IN) :: jd !julian day number
INTEGER :: y,m,d          !year,month,day
INTEGER :: l,n,i,j        !intermediates

l = jd + 68569
n = ( 4 * l ) / 146097
l = l - ( 146097 * n + 3 ) / 4
i = ( 4000 * ( l + 1 ) ) / 1461001
l = l - ( 1461 * i ) / 4 + 31
j = ( 80 * l ) / 2447
d = l - ( 2447 * j ) / 80
l = j / 11
m = j + 2 - ( 12 * l )
y = 100 * ( n - 49 ) + i + l

GregorianDate(1)=y
GregorianDate(2)=m
GregorianDate(3)=d
END FUNCTION GregorianDate
 
FUNCTION UpperCase(string,length)
! Function to force text to upper case
! MC CSIRO 6/03
!
 IMPLICIT NONE
!CHARACTER(LEN=LEN_TRIM(string)) :: UpperCase
 INTEGER, INTENT(IN) :: length
 CHARACTER(LEN=length) :: UpperCase
 CHARACTER(LEN=length), INTENT(IN) :: string
 INTEGER :: i,uppc
!
 UpperCase=string
 uppc=ICHAR('a')-ICHAR('A')
 IF(uppc==0)RETURN
!length=LEN_TRIM(string)
 IF(length > 0)THEN
   i=1
   DO i=1,length
     IF(UpperCase(i:i) >='a' .AND. UpperCase(i:i)<='z') &
        UpperCase(i:i)=CHAR(ICHAR(UpperCase(i:i))-uppc)
   END DO
 END IF
END FUNCTION UpperCase
SUBROUTINE surfer_dump(iunit,nx,ny,x0,y0,dx,dy,grid,filename)
!
! Routine dumps out data in Surfer ASCII format
!
IMPLICIT NONE
INTEGER, INTENT(IN) :: iunit                !Fortran I/O unit
INTEGER, INTENT(IN) :: nx,ny                !grid dimensions
REAL, INTENT(IN) :: x0,y0,dx,dy             !grid location/size
REAL, INTENT(IN), DIMENSION(nx,ny) :: grid  !data
CHARACTER(LEN=*), INTENT(IN) :: filename    !output filename
INTEGER :: j
!
! Open the file unit write out header information
!
OPEN(UNIT=iunit,FILE=TRIM(filename),STATUS='unknown')
!
WRITE(iunit,1)'DSAA'
1 FORMAT(a)
WRITE(iunit,'(i3,1x,i3)')nx,ny
WRITE(iunit,*)x0,(x0+FLOAT(nx-1)*dx)
WRITE(iunit,*)y0,(y0+FLOAT(ny-1)*dy)
WRITE(iunit,*)MINVAL(grid),MAXVAL(grid)
!
DO j=1,ny
  WRITE(iunit,101)grid(:,j)
  101 FORMAT(10(e10.3,1x))
  WRITE(iunit,'()')
END DO
CLOSE(iunit)
END SUBROUTINE surfer_dump
